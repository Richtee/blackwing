-- -------------------------------------------------------- 
-- 2015_03_15_00_auth.sql 
-- -------------------------------------------------------- 
DELETE FROM `rbac_permissions` WHERE `id`=776;
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES (776, 'Command: debug phase');

DELETE FROM `rbac_linked_permissions` WHERE `id`=192 AND linkedId=776;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES (192, 776);

DELETE FROM `rbac_permissions` WHERE `id`=776;
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES (776, 'Command: debug phase');

DELETE FROM `rbac_linked_permissions` WHERE `id`=192 AND linkedId=776;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES (192, 776);

 
 
-- -------------------------------------------------------- 
-- 2015_03_26_00_auth.sql 
-- -------------------------------------------------------- 
UPDATE `realmlist` SET `gamebuild`=19793 WHERE `id`=1;

UPDATE `battlenet_components` SET `Build`=19793 WHERE `Program`='WoW' AND `Platform`='base' AND `Build`=19634;
UPDATE `battlenet_components` SET `Build`=19802 WHERE `Program`='WoW' AND `Platform` IN ('Win','Wn64','Mc64') AND `Build`=19702;
 
 
-- -------------------------------------------------------- 
-- 2015_04_04_00_auth.sql 
-- -------------------------------------------------------- 
UPDATE `battlenet_components` SET `Build`=19831 WHERE `Program`='WoW' AND `Platform` IN ('Win','Wn64','Mc64') AND `Build`=19802;
 
 
-- -------------------------------------------------------- 
-- 2015_04_06_00_auth.sql 
-- -------------------------------------------------------- 
UPDATE `battlenet_components` SET `Build`=19865 WHERE `Program`='WoW' AND `Platform` IN ('Win','Wn64','Mc64') AND `Build`=19831;
 
 
-- -------------------------------------------------------- 
-- 2015_04_08_00_auth.sql 
-- -------------------------------------------------------- 
DELETE FROM `rbac_permissions` WHERE `id`=692;
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES (692, 'Command: debug send playscene');

DELETE FROM `rbac_linked_permissions` WHERE `id`=192 AND linkedId=692;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES (192, 692);
 
 
-- -------------------------------------------------------- 
-- 2015_04_10_00_auth.sql 
-- -------------------------------------------------------- 
UPDATE `rbac_permissions` SET `name`='Command: reload quest_locale' WHERE `id`=667;
 
 
-- -------------------------------------------------------- 
-- 2015_04_11_00_auth.sql 
-- -------------------------------------------------------- 
UPDATE `rbac_permissions` SET `name`='Command: reload quest_greeting' WHERE `id`=664;
 
 
-- -------------------------------------------------------- 
-- 2015_04_11_01_auth.sql 
-- -------------------------------------------------------- 
ALTER TABLE `battlenet_account_bans` DROP COLUMN `active`;
 
 
-- -------------------------------------------------------- 
-- 2015_04_21_00_auth.sql 
-- -------------------------------------------------------- 
DELETE FROM `rbac_permissions` WHERE `id` IN (10, 662);
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES
(10, 'Use character templates'),
(662, 'Command: reload character_template');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId` IN (10, 662);
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES
(196, 10),
(196, 662);
 
 
-- -------------------------------------------------------- 
-- 2015_05_02_00_auth.sql 
-- -------------------------------------------------------- 
DELETE FROM `rbac_permissions` WHERE `id`=12;
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES
(12, 'Command: go quest');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=12;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES
(198, 12);
 
 
-- -------------------------------------------------------- 
-- 2015_05_02_01_auth.sql 
-- -------------------------------------------------------- 
DELETE FROM `rbac_permissions` WHERE `id`=12;
DELETE FROM `rbac_permissions` WHERE `id`=834;
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES
(834, 'Command: go quest');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=12;
DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=834;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES
(198, 834);
 
 
-- -------------------------------------------------------- 
-- _MERGED.sql 
-- -------------------------------------------------------- 
 
 

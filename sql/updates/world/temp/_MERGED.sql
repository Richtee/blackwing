-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_03_28_00.sql 
-- -------------------------------------------------------- 
--
DELETE FROM `creature_addon` WHERE `guid` IN (28786, 28800, 28798);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_03_28_01.sql 
-- -------------------------------------------------------- 
--
DELETE FROM `creature_text` WHERE `entry`=18744 AND `id`=2;
UPDATE `smart_scripts` SET  `action_type`= 87, `action_param2`= 1874301 WHERE `entryorguid`=18743 AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE (`entryorguid`=1874301 AND `source_type`=9);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(1874301,9,0,0,0,0,100,0,0,0,0,0,5,274,0,0,0,0,0,11,18744,20,0,0,0,0,0,'Elrodan - Script - Aurosalia Play emote'),
(1874301,9,1,0,0,0,100,0,3000,3000,3000,3000,11,32826,0,0,0,0,0,1,0,0,0,0,0,0,0,'Elrodan - Script - cast Polymorph Cast Visual');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_03_30_00.sql 
-- -------------------------------------------------------- 
SET @Illidan:=23467;
SET @Morghor:=23139;
SET @Sinestra1:=23283;  
SET @Sinestra2:=23284;
SET @Yarzill:= 23141;
SET @Dragonmaw:=23146;

DELETE FROM `creature_template_addon` WHERE `entry` IN (@Illidan);
INSERT INTO `creature_template_addon` (`entry`,`bytes1`,`bytes2`, `emote`, `auras`) VALUES
(@Illidan,0,4097,0,"37816");

UPDATE `creature_template` SET `AIName`='SmartAI', `ScriptName`='' WHERE `entry` IN (@Illidan, @Morghor, @Yarzill, @Dragonmaw, 23468);
UPDATE `creature_template` SET `VehicleId`=321 WHERE `entry`=23468;

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (@Illidan, @Morghor, @Yarzill, @Dragonmaw, 23468, @Illidan*100);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (@Morghor*100, @Morghor*100+1, @Yarzill*100);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Morghor,0,0,0,19,0,100,0,11108,0,0,0,80,@Morghor*100,2,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - On quest accept (Lord Illidan Stormrage) - ActionList'),
(@Morghor*100,9,0,0,0,0,100,0,0,0,0,0,83,2,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Remove npcfla'),
(@Morghor*100,9,1,0,0,0,100,0,0,0,0,0,1,0,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Talk'),
(@Morghor*100,9,2,0,0,0,100,0,2000,2000,0,0,59,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Set Run OFF'),
(@Morghor*100,9,3,0,0,0,100,0,0,0,0,0,69,0,0,0,0,0,0,8,0,0,0,-5104.410156, 595.296997, 85.680908, 2.368710,'Overlord Mor''ghor - Action List - Go to pos'),
(@Morghor*100,9,4,0,0,0,100,0,9000,9000,0,0,1,1,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Talk'),
(@Morghor*100,9,5,0,0,0,100,0,9000,9000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Talk'),
(@Morghor*100,9,6,0,0,0,100,0,5000,5000,0,0,12,@Illidan,8,0,0,0,0,8,0,0,0,-5107.830078, 602.583984, 85.239304, 4.925980,'Overlord Mor''ghor - Action List - Summon'),
(@Morghor*100,9,7,0,0,0,100,0,3000,3000,0,0,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Talk'),
(@Morghor*100,9,8,0,0,0,100,0,1000,1000,0,0,11,68442,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Cast emote kneel'),
(@Morghor*100,9,9,0,0,0,100,0,2000,2000,0,0,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Talk'),
(@Morghor*100,9,10,0,0,0,100,0,6000,6000,0,0,1,0,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Talk'),
(@Morghor*100,9,11,0,0,0,100,0,0,0,0,0,5,1,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Play emote talk'),
(@Morghor*100,9,12,0,0,0,100,0,5000,5000,0,0,1,5,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Talk'),
(@Morghor*100,9,13,0,0,0,100,0,5000,5000,0,0,1,1,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Talk'),
(@Morghor*100,9,14,0,0,0,100,0,0,0,0,0,5,1,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Play emote talk'),
(@Morghor*100,9,15,0,0,0,100,0,3000,3000,0,0,1,2,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Talk'),
(@Morghor*100,9,16,0,0,0,100,0,0,0,0,0,5,1,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Play emote talk'),
(@Morghor*100,9,17,0,0,0,100,0,5000,5000,0,0,1,3,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Talk'),
(@Morghor*100,9,18,0,0,0,100,0,0,0,0,0,5,1,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Play emote talk'),
(@Morghor*100,9,19,0,0,0,100,0,5000,5000,0,0,1,4,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Talk'),
(@Morghor*100,9,20,0,0,0,100,0,0,0,0,0,5,1,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Play emote talk'),
(@Morghor*100,9,21,0,0,0,100,0,5000,5000,0,0,1,5,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Talk'),
(@Morghor*100,9,22,0,0,0,100,0,0,0,0,0,5,1,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Play emote talk'),
(@Morghor*100,9,23,0,0,0,100,0,2000,2000,0,0,85,41528,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - InvokerCast'),
(@Morghor*100,9,24,0,0,0,100,0,0,0,0,0,28,42016,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Remove aura'),
(@Morghor*100,9,25,0,0,0,100,0,5000,5000,0,0,1,6,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Talk'),
(@Morghor*100,9,26,0,0,0,100,0,0,0,0,0,5,53,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Illidan Play emote rowar'),
(@Morghor*100,9,27,0,0,0,100,0,5000,5000,0,0,1,6,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Talk'),
(@Morghor*100,9,28,0,0,0,100,0,0,0,0,0,5,254,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Emote land'), 
(@Morghor*100,9,29,0,0,0,100,0,1000,1000,0,0,41,0,0,0,0,0,0,19,@Illidan,15,0,0,0,0,0,'Overlord Mor''ghor - Action List - Despawn'), 
(@Morghor*100,9,30,0,0,0,100,0,1000,1000,0,0,28,68442,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - remove emote aura'),
(@Morghor*100,9,31,0,0,0,100,0,1000,1000,0,0,66,0,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Set orientation'),
(@Morghor*100,9,32,0,0,0,100,0,1000,1000,0,0,1,7,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Talk'),
(@Morghor*100,9,33,0,0,0,100,0,0,0,0,0,5,25,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Play emote'),
(@Morghor*100,9,34,0,0,0,100,0,0,0,0,0,64,1,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Store target'),
(@Morghor*100,9,35,0,0,0,100,0,0,0,0,0,100,1,0,0,0,0,0,19,@Yarzill,30,0,0,0,0,0,'Overlord Mor''ghor - Action List - Send target'),
(@Morghor*100,9,36,0,0,0,100,0,5000,5000,0,0,45,0,1,0,0,0,0,19,@Yarzill,30,0,0,0,0,0,'Overlord Mor''ghor - Action List - set Data'),
(@Morghor*100,9,37,0,0,0,100,0,2000,2000,0,0,12,23468,8,0,0,0,0,8,0,0,0,-5126.729004, 604.291626, 84.271423, 2.468847,'Overlord Mor''ghor - Action List - Summon'),
(@Morghor*100,9,38,0,0,0,100,0,1000,1000,0,0,85,46598,0,0,0,0,0,19,23468,40,0,0,0,0,0,'Overlord Mor''ghor - Action List - Cast to ride'),
(@Morghor*100,9,39,0,0,0,100,0,3000,3000,0,0,85,41540,0,0,0,0,0,7,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Cast taxi'),
(@Morghor*100,9,40,0,0,0,100,0,10000,10000,0,0,69,0,0,0,0,0,0,8,0,0,0,-5085.000000, 578.656982, 86.648300, 2.368710,'Overlord Mor''ghor - Action List - Go to pos'),
(@Morghor*100,9,41,0,0,0,100,0,9000,9000,0,0,66,0,0,0,0,0,0,8,0,0,0,-5085.000000, 578.656982, 86.648300, 2.368710,'Overlord Mor''ghor - Action List - Set orientation'),
(@Morghor*100,9,42,0,0,0,100,0,0,0,0,0,82,2,0,0,0,0,0,1,0,0,0,0,0,0,0,'Overlord Mor''ghor - Action List - Add npcfla'),
(@Yarzill,0,0,0,38,0,100,0,0,1,0,0,80,@Yarzill*100,2,0,0,0,0,1,0,0,0,0,0,0,0,'Yarzill - On Data set (Lord Illidan Stormrage) - ActionList'),
(@Yarzill*100,9,0,0,0,0,100,0,0,0,0,0,1,0,0,0,0,0,0,12,1,0,0,0,0,0,0,'Yarzill - Action List - Talk'), 
(@Yarzill*100,9,1,0,0,0,100,0,0,0,0,0,28,41528,0,0,0,0,0,12,1,0,0,0,0,0,0,'Yarzill - Action List - remove aura'),
(@Yarzill*100,9,2,0,0,0,100,0,0,0,0,0,28,41519,0,0,0,0,0,12,1,0,0,0,0,0,0,'Yarzill - Action List - remove aura'),
(@Yarzill*100,9,4,0,0,0,100,0,0,0,0,0,15,11108,0,0,0,0,0,12,1,0,0,0,0,0,0,'Yarzill - Action List - AREAEXPLOREDOREVENTHAPPENS'),
(@Illidan,0,0,0,54,0,100,0,0,0,0,0,80,@Illidan*100,0,0,0,0,0,1,0,0,0,0,0,0,0,'Illidan - Just summoned - Action List'),
(@Illidan*100,9,0,0,0,0,100,0,300,300,0,0,11,39990,0,0,0,0,0,1,0,0,0,0,0,0,0,'Illidan - Action List - cast'),
(@Illidan*100,9,1,0,0,0,100,0,0,0,0,0,50,185520,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Illidan - Action List - Summon Fel Fire (GO)'),
(23468,0,0,0,27,0,100,0,0,0,0,0,11,50630,0,0,0,0,0,1,0,0,0,0,0,0,0,'Yarzill Dragon - passenger boarded - Eject all passengers'),
(23468,0,1,0,28,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Yarzill Dragon- passenger removed - despawn');

DELETE FROM `creature_text` WHERE `entry`IN (@Illidan, @Morghor, @Sinestra2, @Yarzill, @Dragonmaw);
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`, `BroadcastTextID`) VALUES
(@Yarzill,0,0,'You will not harm the $g boy:girl;, Mor''ghor! Quickly, $n, climb on my back!',12,0,100,0,0,0, 'Yarzill', 21825),
(@Dragonmaw,0,0,'Who... Who is you?? What you want?',12,0,100,0,0,0, 'Dragonmaw Enforcer', 21300),
(@Dragonmaw,1,0,'Yes... Yes we move now... Please no hurt us...',12,0,100,0,0,0, 'Dragonmaw Enforcer', 21302),
(@Morghor,0,0,'Come, $n. Lord Stormrage awaits.',12,0,100,0,0,0, 'Overlord Mor''ghor', 21798),
(@Morghor,1,0,'Warriors of Dragonmaw, gather ''round! One among you has attained the rank of highlord! Bow your heads in reverence! Show your respect and allegiance to Highlord $n!',14,0,100,0,0,0, 'Overlord Mor''ghor', 21799),
(@Morghor,2,0,'Lord Illidan will be here shortly.',12,0,100,0,0,0, 'Overlord Mor''ghor', 21800),
(@Morghor,3,0,'All hail Lord Illidan!',14,0,100,0,0,0, 'Overlord Mor''ghor', 21802),
(@Morghor,4,0,'Lord Illidan, this is the Dragonmaw that I, and others, have told you about. He will lead us to victory!',12,0,100,0,0,0, 'Overlord Mor''ghor', 21803),
(@Morghor,5,0,'But... My lord, I do not understand. $n... He is the orc that has...',12,0,100,0,0,0, 'Overlord Mor''ghor', 21805),
(@Morghor,6,0,'It will be done, my lord.',12,0,100,0,0,0, 'Overlord Mor''ghor', 21823),
(@Morghor,7,0,'So you thought to make a fool of Mor''ghor, eh? Before you are delivered to Lord Illidan, you will feel pain that you could not know to exist. I will take pleasure in exacting my own vengeance.',12,0,100,0,0,0, 'Overlord Mor''ghor', 21824),

(@Morghor,8,0,'I will not drag this out any further than it needs, Lady Sinestra. You have bent my ear, now tell me what it is that you want from Dragonmaw.',12,0,100,0,0,0, 'Overlord Mor''ghor', 21307),
(@Morghor,9,0,'The... The master? He lives?',12,0,100,0,0,0, 'Overlord Mor''ghor', 21310),
(@Morghor,10,0,'%s stammers.',16,0,100,0,0,0, 'Overlord Mor''ghor', 21313),
(@Morghor,11,0,'Ye... Yes, yes... Of course. We need only the crystals and ore from this place. There is... We will need mounts.',12,0,100,0,0,0, 'Overlord Mor''ghor', 21314),

(@Illidan,0,0,'What is the meaning of this, Mor''ghor?',12,0,100,0,0,0, 'Illidan', 21804),
(@Illidan,1,0,'SILENCE!',12,0,100,0,0,0, 'Illidan', 21807),
(@Illidan,2,0,'Blathering idiot. You incomprehensibly incompetent buffoon...',12,0,100,0,0,0, 'Illidan', 21808),
(@Illidan,3,0,'THIS is your hero?',12,0,100,0,0,0, 'Illidan', 21809),
(@Illidan,4,0,'You have been deceived, imbecile.',12,0,100,0,0,0, 'Illidan', 21810),
(@Illidan,5,0,'This... whole... operation... HAS BEEN COMPROMISED!',12,0,100,0,0,0, 'Illidan', 21811),
(@Illidan,6,0,'I expect to see this insect''s carcass in pieces in my lair within the hour. Fail and you will suffer a fate so much worse than death.',12,0,100,0,0,0, 'Illidan', 21812),

(@Sinestra2,0,0,'Step aside lest I add you as another adornment to my armor. Your leader and I have matters to attend...',12,0,100,0,0,0, 'Sinestra', 21301),
(@Sinestra2,1,0,'$s smiles.',16,0,100,0,0,0, 'Sinestra', 21303),
(@Sinestra2,2,0,'I thought you would see it my way...',12,0,100,0,0,0, 'Sinestra', 21304),
(@Sinestra2,3,0,'Overlord Mor''ghor, I presume... A pleasure to finally make your acquaintance.',12,0,100,0,0,0, 'Sinestra', 21305),
(@Sinestra2,4,0,'I am Lady Sinestra.',12,0,100,0,0,0, 'Sinestra', 21306),
(@Sinestra2,5,0,'You have no doubt heard about Nefarian''s failures on Azeroth... While he has fallen, the experiment continues. My master... He continues the work that his progeny began.',12,0,100,0,0,0, 'Sinestra', 21308),
(@Sinestra2,6,0,'$s nods.',16,0,100,0,0,0, 'Sinestra', 21309),
(@Sinestra2,7,0,'You were once a chief lieutenant, Mor''ghor. Your work in Grim Batol is not easily forgotten.',12,0,100,0,0,0, 'Sinestra', 21311),
(@Sinestra2,8,0,'Now... We need the eggs that you recovered. The Netherwing eggs. They are, after all, a product of the master. We will pay whatever price that you ask.',12,0,100,0,0,0, 'Sinestra', 21312),
(@Sinestra2,9,0,'The master will be most pleased with this news. The Black Dragonflight will provide you all that you ask. You will be allowed to ride upon the backs of our drakes as needed. ',12,0,100,0,0,0, 'Sinestra', 21315),
(@Sinestra2,10,0,'I thank you for your graciousness, Mor''ghor. I must now take my leave.',12,0,100,0,0,0, 'Sinestra', 21316);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_03_30_01.sql 
-- -------------------------------------------------------- 
SET @Sinestra1:=23283;  
SET @Sinestra2:=23284;

SET @GUID:=28796; -- 1 free creature guid set by TC
SET @PATH := @Sinestra1*10;
DELETE FROM `creature` WHERE `id` IN (@Sinestra1);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `PhaseId`, `PhaseGroup`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`,`curhealth`) VALUES
(@GUID+0, @Sinestra1, 530, 1, 1, 1, -5240.304199, 682.020081, 157.604004, 5.723090, 900, 151760);

UPDATE `creature` SET `spawndist`=0,`MovementType`=2 WHERE `guid`=@GUID;
UPDATE `creature_template` SET  `InhabitType`=4, `AIName`='SmartAI', `flags_extra`=0 WHERE  `entry`=@Sinestra1;
UPDATE `creature_template` SET  `AIName`='SmartAI', `flags_extra`=0 WHERE  `entry`=@Sinestra2;
DELETE FROM `creature_addon` WHERE `guid`=@GUID;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(@GUID,@PATH,0,33554432,1,0,'');

DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5120.744629, 783.187988, 208.115173,0,0,0,0,100,0),
(@PATH,2,-5331.205078, 712.656494, 180.570953,0,0,0,0,100,0),
(@PATH,3,-5371.597656, 573.186890, 159.434875,0,0,0,0,100,0),
(@PATH,4,-5299.805176, 490.375488, 150.086655,0,0,0,0,100,0),
(@PATH,5,-5207.269043, 478.922729, 150.908600,0,0,0,0,100,0),
(@PATH,6,-5170.645996, 533.437805, 168.776611,0,0,0,0,100,0),
(@PATH,7,-5132.274902, 632.125549, 177.018936,0,0,0,0,100,0);

DELETE FROM `waypoints` WHERE `entry` IN(@Sinestra1*100);
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES
(@Sinestra1*100, 1, -5163.216309, 645.610779, 77.3899920, 'Lady Sinestra');

DELETE FROM `waypoints` WHERE `entry` IN(@Sinestra2*100);
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES
(@Sinestra2*100, 1, -5155.744629, 636.083862, 80.537260, 'Lady Sinestra'),
(@Sinestra2*100, 2, -5130.897949, 609.521179, 83.996864, 'Lady Sinestra'),
(@Sinestra2*100, 3, -5113.644531, 604.067078, 84.993507, 'Lady Sinestra'),
(@Sinestra2*100, 4, -5093.567383, 586.982605, 86.467323, 'Lady Sinestra'),
(@Sinestra2*100, 5, -5113.704590, 604.713928, 85.021675, 'Lady Sinestra'),
(@Sinestra2*100, 6, -5134.045410, 612.111572, 83.530479, 'Lady Sinestra'),
(@Sinestra2*100, 7, -5163.214844, 645.577454, 77.393990, 'Lady Sinestra');

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (@Sinestra1, @Sinestra1*100, @Sinestra2, @Sinestra2*100, @Sinestra2*100+1);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Sinestra1,0,0,0,1,0,100,0,7200000,7200000,7200000,7200000,53,1,@Sinestra1*100,0,0,0,1,1,0,0,0,0,0,0,0,"Time Watcher - OOC  - Start wp"),
(@Sinestra1,0,1,0,58,0,100,0,1,@Sinestra1*100,0,0,80,@Sinestra1*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - On way point ended - action list"),
(@Sinestra1*100,9,0,0,0,0,100,0,0,0,0,0,47,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Set visible off"),
(@Sinestra1*100,9,1,0,0,0,100,0,0,0,0,0,12,@Sinestra2,8,0,0,0,0,8,0,0,0,-5163.216309, 645.610779, 77.6899920, 5.328592,"Lady Sinestra - Action list - Spawn"),
(@Sinestra1*100,9,2,0,0,0,100,0,160000,160000,0,0,47,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - St visible on"),
(@Sinestra2,0,0,0,54,0,100,0,0,0,0,0,53,0,@Sinestra2*100,0,0,0,1,1,0,0,0,0,0,0,0,"Lady Sinestra - Just summoned - Start wp"),
(@Sinestra2,0,1,0,40,0,100,0,1,@Sinestra2*100,0,0,80,@Sinestra2*100,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - On wp1 - Action list"),
(@Sinestra2*100,9,0,0,0,0,100,0,0,0,0,0,54,20000,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Pause waypoint"),
(@Sinestra2*100,9,1,0,0,0,100,0,0,0,0,0,1,0,0,0,0,0,0,19,23146,20,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100,9,2,0,0,0,100,0,5000,5000,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100,9,3,0,0,0,100,0,9000,9000,0,0,1,1,0,0,0,0,0,19,23146,20,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100,9,4,0,0,0,100,0,4000,4000,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100,9,5,0,0,0,100,0,2000,2000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2,0,2,0,40,0,100,0,4,@Sinestra2*100,0,0,80,@Sinestra2*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - On wp4 - Action list"),
(@Sinestra2*100+1,9,0,0,0,0,100,0,0,0,0,0,54,65000,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Pause waypoint"),
(@Sinestra2*100+1,9,1,0,0,0,100,0,0,0,0,0,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,2,0,0,0,100,0,5000,5000,0,0,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,3,0,0,0,100,0,4000,4000,0,0,1,8,0,0,0,0,0,19,23139,20,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,4,0,0,0,100,0,9000,9000,0,0,1,5,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,5,0,0,0,100,0,9000,9000,0,0,1,9,0,0,0,0,0,19,23139,20,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,6,0,0,0,100,0,3000,3000,0,0,1,6,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,7,0,0,0,100,0,3000,3000,0,0,1,7,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,8,0,0,0,100,0,7000,7000,0,0,1,8,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,9,0,0,0,100,0,4000,4000,0,0,1,10,0,0,0,0,0,19,23139,20,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,10,0,0,0,100,0,3000,3000,0,0,1,11,0,0,0,0,0,19,23139,20,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,11,0,0,0,100,0,8000,8000,0,0,1,9,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2*100+1,9,12,0,0,0,100,0,8000,8000,0,0,1,10,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - Action list - Talk"),
(@Sinestra2,0,3,0,58,0,100,0,7,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Lady Sinestra - On wp Ended - Despawn");
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_03_30_02.sql 
-- -------------------------------------------------------- 
SET @Triggger:=34146;
SET @Triggger1:=34150;
SET @Triggger2:=34151;
SET @Jormungar:=34137; 

DELETE FROM `vehicle_template_accessory` WHERE `entry` IN (34146, 34151, 34150);
INSERT INTO `vehicle_template_accessory` (`entry`,`accessory_entry`,`seat_id`,`minion`,`description`,`summontype`,`summontimer`)VALUES
(34146,34137,0,0,'Snow Mound',8,0),
(34146,34137,1,0,'Snow Mound',8,0),
(34146,34137,2,0,'Snow Mound',8,0),
(34146,34137,3,0,'Snow Mound',8,0),
(34151,34137,0,0,'Snow Mound',8,0),
(34151,34137,1,0,'Snow Mound',8,0),
(34151,34137,2,0,'Snow Mound',8,0),
(34151,34137,3,0,'Snow Mound',8,0),
(34151,34137,4,0,'Snow Mound',8,0),
(34151,34137,5,0,'Snow Mound',8,0),
(34151,34137,6,0,'Snow Mound',8,0),
(34151,34137,7,0,'Snow Mound',8,0),
(34150,34137,0,0,'Snow Mound',8,0),
(34150,34137,1,0,'Snow Mound',8,0),
(34150,34137,2,0,'Snow Mound',8,0),
(34150,34137,3,0,'Snow Mound',8,0),
(34150,34137,4,0,'Snow Mound',8,0),
(34150,34137,5,0,'Snow Mound',8,0);

DELETE FROM `npc_spellclick_spells` WHERE `npc_entry` IN (34146, 34151, 34150);
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES
(34146, 43671, 1, 0),
(34151, 43671, 1, 0),
(34150, 43671, 1, 0);

UPDATE `creature_template` SET `flags_extra`= 2, `AIName`='SmartAI' WHERE `entry` IN (34146, 34150, 34151, 34137);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (34146,34150,34151,34137) AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (34146*100,34150*100,34151*100) AND `source_type`=9;
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (34146*100+1,34150*100+1,34151*100+1, @Jormungar*100) AND `source_type`=9;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(34146,0,0,0,10,0,100,1,0,15,1000,1000,80,34146*100,2,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - LOS - Action list'),
(34150,0,0,0,10,0,100,1,0,15,1000,1000,80,34150*100,2,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - LOS - Action list'),
(34151,0,0,0,10,0,100,1,0,15,1000,1000,80,34151*100,2,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - LOS - Action list'),
(34146*100,9,0,0,0,0,100,0,1000,1000,0,0,11,64629,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34146*100,9,1,0,0,0,100,0,4000,4000,0,0,11,64630,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34146*100,9,2,0,0,0,100,0,4000,4000,0,0,11,64631,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34146*100,9,3,0,0,0,100,0,4000,4000,0,0,11,64632,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34146*100,9,4,0,0,0,100,0,2000,2000,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Despawn'),
(34150*100,9,0,0,0,0,100,0,1000,1000,0,0,11,64629,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34150*100,9,1,0,0,0,100,0,4000,4000,0,0,11,64630,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34150*100,9,2,0,0,0,100,0,4000,4000,0,0,11,64631,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34150*100,9,3,0,0,0,100,0,4000,4000,0,0,11,64632,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34150*100,9,4,0,0,0,100,0,4000,4000,0,0,11,64633,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34150*100,9,5,0,0,0,100,0,4000,4000,0,0,11,64634,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34150*100,9,6,0,0,0,100,0,2000,2000,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Despawn'),
(34151*100,9,0,0,0,0,100,0,1000,1000,0,0,11,64629,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34151*100,9,1,0,0,0,100,0,4000,4000,0,0,11,64630,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34151*100,9,2,0,0,0,100,0,4000,4000,0,0,11,64631,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34151*100,9,3,0,0,0,100,0,4000,4000,0,0,11,64632,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34151*100,9,4,0,0,0,100,0,4000,4000,0,0,11,64633,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34151*100,9,5,0,0,0,100,0,4000,4000,0,0,11,64634,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34151*100,9,6,0,0,0,100,0,4000,4000,0,0,11,64635,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34151*100,9,7,0,0,0,100,0,4000,4000,0,0,11,64636,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Eject Passenger'),
(34151*100,9,8,0,0,0,100,0,2000,2000,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Snow Mound - Action list - Despawn'),
(@Jormungar,0,0,0,0,0,100,0,2000,5000,7000,9000,11,64638,0,0,0,0,0,2,0,0,0,0,0,0,0,'Winter Jormungar - IC - Acidic Bite'),
(@Jormungar,0,1,0,23,0,100,1,43671,0,0,0,80,@Jormungar*100,2,0,0,0,0,1,0,0,0,0,0,0,0,'Winter Jormungar - On aura remove - Action list'),
(@Jormungar*100,9,1,0,0,0,100,0,1000,1000,0,0,49,0,0,0,0,0,0,21,70,0,0,0,0,0,0,'Winter Jormungar - Action list - Start attack');

DELETE FROM `creature_template_addon` WHERE `entry` IN (34146, 34150, 34151);
INSERT INTO `creature_template_addon` (`entry`,`bytes1`,`bytes2`, `emote`, `auras`) VALUES
(@Triggger,0,4097,0,"64615"),
(@Triggger1,0,4097,0,"64615"),
(@Triggger2,0,4097,0,"64615");
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_03_30_03.sql 
-- -------------------------------------------------------- 
SET @JAINA         := 38606;
SET @MURADIN       := 38607;
SET @UTHER         := 38608;
SET @SYLVANAS      := 38609;
SET @DARION        := 37120;
SET @ALEXANDROS    := 38610;
SET @JAINAQUEST    := 24916;
SET @MURADINQUEST  := 24917;
SET @UTHERQUEST    := 24919;
SET @SYLVANASQUEST := 24918;
SET @DARIONQUEST   := 24915;

UPDATE `creature_template` SET `gossip_menu_id`=11065, `npcflag`=3, `speed_run`=1, `unit_class`=8, `unit_flags`=33088, `unit_flags2`=2099200, `AIName`='SmartAI' WHERE `entry`=@JAINA;
UPDATE `creature_template` SET `npcflag`=2, `speed_run`=1.714286, `unit_flags`=33600, `AIName`='SmartAI' WHERE `entry`=@MURADIN;
UPDATE `creature_template` SET `npcflag`=2, `unit_class`=2, `unit_flags`=32832, `unit_flags2`=2099200, `AIName`='SmartAI' WHERE `entry`=@UTHER;
UPDATE `creature_template` SET `npcflag`=2, `speed_walk`=0.888888, `speed_run`=0.9920629, `unit_class`=2, `unit_flags`=33088, `unit_flags2`=2099200, `AIName`='SmartAI' WHERE `entry`=@SYLVANAS;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ALEXANDROS;

DELETE FROM `quest_offer_reward` WHERE `ID` IN (@DARIONQUEST, @JAINAQUEST, @MURADINQUEST, @SYLVANASQUEST, @UTHERQUEST);
INSERT INTO `quest_offer_reward` (`ID`, `RewardText`) VALUES
(@DARIONQUEST, 'Impossible...$B$BFather, is it you?!$B$BYou have done me a great service, hero. I offer you the reins of my faithful steed. Do with it as you please, but do not forget those that assisted you in this monumental feat.'),
(@JAINAQUEST, 'What... how did you manage to come by this!?$B$BYou have done me a great service, hero. My heart could not bear to keep this locket, but I will place an enchantment upon it that you may find useful. Do with it as you please, but do not forget those that assisted you in this monumental feat.'),
(@MURADINQUEST, 'Aye. I know this blade... and I will treasure it always - a moment of time that will be lost forever.$B$BYou have done me a great service, hero. I offer you a gift from the Frostborn dwarves. Do with it as you please, but do not forget those that assisted you in this monumental feat.'),
(@SYLVANASQUEST, '<Sylvanas silently reflects for a moment before returning her gaze to you.>$B$BYou have done me a great service, hero. I offer you the boon of the Banshee Queen. Do with it as you please, but do not forget those that assisted you in this monumental feat.'),
(@UTHERQUEST, '<Uther stares at the medal, speechless for a moment.>$B$BArthas'' medal.$B$BI remember well the day I presented it to him...$B$BYou have done me a great service, hero. My soul may now rest in peace. I offer you a memory lost in time. Do with it as you please, but do not forget those that assisted you in this monumental feat.');

DELETE FROM `quest_request_items` WHERE `ID` IN (@DARIONQUEST, @JAINAQUEST, @MURADINQUEST, @SYLVANASQUEST, @UTHERQUEST);
INSERT INTO `quest_request_items` (`ID`, `CompletionText`) VALUES
(@DARIONQUEST, 'What is that you hold, $c?$B$BIt calls out to me, stirring feelings I thought to be long dead.'),
(@JAINAQUEST, 'Yes, $c? How can I assist you?'),
(@MURADINQUEST, 'Whatdya got there, $r?'),
(@SYLVANASQUEST, 'What is it, $r?'),
(@UTHERQUEST, 'How may I be of service, $r?');

UPDATE `creature_queststarter` SET `id`=@JAINA WHERE `id`=37120 AND `quest`=24916;
UPDATE `creature_queststarter` SET `id`=@MURADIN WHERE `id`=37120 AND `quest`=24917;
UPDATE `creature_queststarter` SET `id`=@UTHER WHERE `id`=37120 AND `quest`=24919;
UPDATE `creature_queststarter` SET `id`=@SYLVANAS WHERE `id`=37120 AND `quest`=24918;

UPDATE `creature_questender` SET `id`=@JAINA WHERE `id`=37120 AND `quest`=24916;
UPDATE `creature_questender` SET `id`=@MURADIN WHERE `id`=37120 AND `quest`=24917;
UPDATE `creature_questender` SET `id`=@UTHER WHERE `id`=37120 AND `quest`=24919;
UPDATE `creature_questender` SET `id`=@SYLVANAS WHERE `id`=37120 AND `quest`=24918;

DELETE FROM `spell_target_position` WHERE `ID`=72468;
INSERT INTO `spell_target_position` (`ID`, `EffectIndex`, `MapID`, `PositionX`, `PositionY`, `PositionZ`, `VerifiedBuild`) VALUES 
(72468, 0, 631, -66.8684, 2158.28, 30.73743, 17658);

DELETE FROM `gossip_menu` WHERE `entry`=11065 AND `text_id`=15382;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES 
(11065, 15382);

DELETE FROM `npc_text` WHERE `ID`=15382;
INSERT INTO `npc_text` (`ID`, `BroadcastTextID0`, `Probability0`, `BroadcastTextID1`, `Probability1`, `VerifiedBuild`) VALUES 
(15382, 38510, 1, 38511, 1, 19342);

DELETE FROM `creature_text` WHERE `entry` IN (@JAINA,@MURADIN,@UTHER,@SYLVANAS,@ALEXANDROS);
DELETE FROM `creature_text` WHERE `entry`=@DARION AND `groupid` IN (4,5,6);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextID`, `comment`) VALUES
(@JAINA, 0, 0, 'What''s this!?', 12, 0, 100, 5, 0, 17383, 38201, 'Lady Jaina Proudmoore to Player'),
(@JAINA, 1, 0, 'He... he kept it? All this time, he kept it!?', 12, 0, 100, 5, 0, 17384, 38202, 'Lady Jaina Proudmoore'),
(@JAINA, 2, 0, 'I knew! ', 12, 0, 100, 5, 0, 17385, 38203, 'Lady Jaina Proudmoore'),
(@JAINA, 3, 0, 'I sensed a part of him still alive! Trapped... struggling... Oh, Arthas!', 12, 0, 100, 396, 0, 17386, 38204, 'Lady Jaina Proudmoore'),
(@JAINA, 4, 0, 'Perhaps - perhaps he might someday remember what he once was.', 12, 0, 100, 396, 0, 17387, 38205, 'Lady Jaina Proudmoore'),
(@JAINA, 5, 0, 'By the Light, may he at last find rest, free from the icy grip of that terrible blade.', 12, 0, 100, 396, 0, 17388, 38206, 'Lady Jaina Proudmoore'),
(@MURADIN, 0, 0, 'Oh, lad...', 12, 0, 100, 396, 0, 17421, 38171, 'Muradin Bronzebeard'),
(@MURADIN, 1, 0, 'How I miss those endless days in Lordaeron...', 12, 0, 100, 396, 0, 17422, 38172, 'Muradin Bronzebeard'),
(@MURADIN, 2, 0, '...sharpenin'' yer skill with this dull blade...', 12, 0, 100, 396, 0, 17423, 38173, 'Muradin Bronzebeard'),
(@MURADIN, 3, 0, '...forgin'' ya into a weapon meant to withstand the demands of a great destiny.', 12, 0, 100, 396, 0, 17424, 38174, 'Muradin Bronzebeard'),
(@MURADIN, 4, 0, 'Heh... Yeh sure put them skills ta use, didn''t yeh, lad?', 12, 0, 100, 6, 0, 17425, 38175, 'Muradin Bronzebeard'),
(@MURADIN, 5, 0, 'If only I''da been able ta stop yeh that day, how different things mighta been.', 12, 0, 100, 396, 0, 17426, 38176, 'Muradin Bronzebeard'),
(@MURADIN, 6, 0, 'If only I''da never discovered that accursed blade...', 12, 0, 100, 396, 0, 17427, 38177, 'Muradin Bronzebeard'),
(@MURADIN, 7, 0, 'Farewell, Arthas... my brother.', 12, 0, 100, 396, 0, 17428, 38178, 'Muradin Bronzebeard'),
(@UTHER, 0, 0, 'Arthas...', 12, 0, 100, 396, 0, 17402, 38160, 'Uther the Lightbringer'),
(@UTHER, 1, 0, 'Alas, hero of Azeroth, you give me a greater gift than you know.', 12, 0, 100, 396, 0, 17403, 38161, 'Uther the Lightbringer'),
(@UTHER, 2, 0, 'Long have I struggled to forgive the prince for his terrible transgressions.', 12, 0, 100, 396, 0, 17404, 38162, 'Uther the Lightbringer'),
(@UTHER, 3, 0, 'My soul has been wracked with unbearable anxiety, dark thoughts... distancing me from the Light.', 12, 0, 100, 396, 0, 17405, 38163, 'Uther the Lightbringer'),
(@UTHER, 4, 0, 'I recall clearly the gleam of pride in his eye as he stood before me, eager to defeat the enemies of the Light...', 12, 0, 100, 396, 0, 17406, 38165, 'Uther the Lightbringer'),
(@UTHER, 5, 0, 'Eager to defend his people, no matter the cost.', 12, 0, 100, 396, 0, 17407, 38166, 'Uther the Lightbringer'),
(@UTHER, 6, 0, 'It is this memory of Arthas that I choose to keep in my heart.', 12, 0, 100, 396, 0, 17408, 38167, 'Uther the Lightbringer'),
(@UTHER, 7, 0, 'I shall always be in your debt, friend.', 12, 0, 100, 396, 0, 17409, 38168, 'Uther the Lightbringer'),
(@UTHER, 8, 0, 'Thank you.', 12, 0, 100, 0, 0, 17410, 38169, 'Uther the Lightbringer'),
(@SYLVANAS, 0, 0, 'So, it is done.', 12, 0, 100, 396, 0, 17376, 38179, 'Lady Sylvanas Windrunner'),
(@SYLVANAS, 1, 0, 'I had not dared to trust my senses - too many times has the Lich King made me to be a fool.', 12, 0, 100, 396, 0, 17377, 38180, 'Lady Sylvanas Windrunner'),
(@SYLVANAS, 2, 0, 'Finally, he has been made to pay for the atrocities he imposed upon my people.', 12, 0, 100, 396, 0, 17378, 38181, 'Lady Sylvanas Windrunner'),
(@SYLVANAS, 3, 0, 'May Azeroth never fail to remember the terrible price we paid for our weakness... for our pride.', 12, 0, 100, 396, 0, 17379, 38183, 'Lady Sylvanas Windrunner'),
(@SYLVANAS, 4, 0, 'But what now, hero? What of those freed from his grasp, but still shackled to their mortal coils?', 12, 0, 100, 6, 0, 17380, 38184, 'Lady Sylvanas Windrunner'),
(@SYLVANAS, 5, 0, 'Leave me. ', 12, 0, 100, 396, 0, 17381, 38185, 'Lady Sylvanas Windrunner'),
(@SYLVANAS, 6, 0, 'I have much to ponder.', 12, 0, 100, 396, 0, 17382, 38186, 'Lady Sylvanas Windrunner'),
(@ALEXANDROS, 0, 0, 'Darion, my son.', 12, 0, 100, 396, 0, 17414, 38152, 'Highlord Alexandros Mograine'),
(@ALEXANDROS, 1, 0, 'At last, I am able to lay my eyes upon you again.', 12, 0, 100, 396, 0, 17415, 38153, 'Highlord Alexandros Mograine'),
(@ALEXANDROS, 2, 0, 'The Lich King tormented me without end, Darion.', 12, 0, 100, 396, 0, 17416, 38154, 'Highlord Alexandros Mograine'),
(@ALEXANDROS, 3, 0, 'Endlessly, he sought to break my will, to force me to serve him, to bind me to his blade...', 12, 0, 100, 396, 0, 17417, 38155, 'Highlord Alexandros Mograine'),
(@ALEXANDROS, 4, 0, 'Finally, when events demanded his full attention, he left me.', 12, 0, 100, 396, 0, 17418, 38156, 'Highlord Alexandros Mograine'),
(@ALEXANDROS, 5, 0, 'The one memory, I clung to, Darion... The one thought that kept me from giving in...', 12, 0, 100, 396, 0, 17419, 38157, 'Highlord Alexandros Mograine'),
(@ALEXANDROS, 6, 0, 'It was your sacrifice, my son, that again saved me from eternal peril.', 12, 0, 100, 396, 0, 17420, 38158, 'Highlord Alexandros Mograine'),
(@DARION, 4, 0, 'Father...', 12, 0, 100, 396, 0, 17411, 38143, 'Highlord Darion Mograine'),
(@DARION, 5, 0, 'Father. I feared for your... your sanity.', 12, 0, 100, 396, 0, 17412, 38150, 'Highlord Darion Mograine'),
(@DARION, 6, 0, 'Father... For you, I would give my life a thousand times.', 12, 0, 100, 396, 0, 17413, 38151, 'Highlord Darion Mograine');

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (@JAINA, @JAINA*100, @MURADIN, @MURADIN*100, @UTHER, @UTHER*100, @SYLVANAS, @SYLVANAS*100, @DARION*100+1, @ALEXANDROS) AND `source_type` IN (0,9);
DELETE FROM `smart_scripts` WHERE `entryorguid`=@DARION AND `id`=7;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(@JAINA, 0, 0, 0, 20, 0, 100, 0, @JAINAQUEST, 0, 0, 0, 80, @JAINA*100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Quest ''Jaina''s Locket'' Finished - Run Script'),
(@JAINA*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Remove NPC Flags'),
(@JAINA*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @MURADIN, 0, 10, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Remove NPC Flags'),
(@JAINA*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @UTHER, 0, 10, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Remove NPC Flags'),
(@JAINA*100, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @SYLVANAS, 0, 10, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Remove NPC Flags'),
(@JAINA*100, 9, 4, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @DARION, 0, 10, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Remove NPC Flags'),
(@JAINA*100, 9, 5, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Say Line 0'),
(@JAINA*100, 9, 6, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Say Line 1'),
(@JAINA*100, 9, 7, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Say Line 2'),
(@JAINA*100, 9, 8, 0, 0, 0, 100, 0, 4000, 4000, 0, 0, 1, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Say Line 3'),
(@JAINA*100, 9, 9, 0, 0, 0, 100, 0, 10000, 10000, 0, 0, 1, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Say Line 4'),
(@JAINA*100, 9, 10, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Say Line 5'),
(@JAINA*100, 9, 11, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 81, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Restore NPC Flags'),
(@JAINA*100, 9, 12, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @MURADIN, 0, 10, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Restore NPC Flags'),
(@JAINA*100, 9, 13, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @UTHER, 0, 10, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Restore NPC Flags'),
(@JAINA*100, 9, 14, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @SYLVANAS, 0, 10, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Restore NPC Flags'),
(@JAINA*100, 9, 15, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 9, @DARION, 0, 10, 0, 0, 0, 0, 'Lady Jaina Proudmoore - On Script - Restore NPC Flags'),
(@MURADIN, 0, 0, 0, 20, 0, 100, 0, @MURADINQUEST, 0, 0, 0, 80, @MURADIN*100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Quest ''Muradin''s Lament'' Finished - Run Script'),
(@MURADIN*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Remove NPC Flags'),
(@MURADIN*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @JAINA, 0, 10, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Remove NPC Flags'),
(@MURADIN*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @UTHER, 0, 10, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Remove NPC Flags'),
(@MURADIN*100, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @SYLVANAS, 0, 10, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Remove NPC Flags'),
(@MURADIN*100, 9, 4, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @DARION, 0, 10, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Remove NPC Flags'),
(@MURADIN*100, 9, 5, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Say Line 0'),
(@MURADIN*100, 9, 6, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Say Line 1'),
(@MURADIN*100, 9, 7, 0, 0, 0, 100, 0, 4000, 4000, 0, 0, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Say Line 2'),
(@MURADIN*100, 9, 8, 0, 0, 0, 100, 0, 5000, 5000, 0, 0, 1, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Say Line 3'),
(@MURADIN*100, 9, 9, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Say Line 4'),
(@MURADIN*100, 9, 10, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Say Line 5'),
(@MURADIN*100, 9, 11, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Say Line 6'),
(@MURADIN*100, 9, 12, 0, 0, 0, 100, 0, 5000, 5000, 0, 0, 1, 7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Say Line 7'),
(@MURADIN*100, 9, 13, 0, 0, 0, 100, 0, 5000, 5000, 0, 0, 81, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Restore NPC Flags'),
(@MURADIN*100, 9, 14, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 9, @JAINA, 0, 10, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Restore NPC Flags'),
(@MURADIN*100, 9, 15, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @UTHER, 0, 10, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Restore NPC Flags'),
(@MURADIN*100, 9, 16, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @SYLVANAS, 0, 10, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Restore NPC Flags'),
(@MURADIN*100, 9, 17, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 9, @DARION, 0, 10, 0, 0, 0, 0, 'Muradin Bronzebeard - On Script - Restore NPC Flags'),
(@UTHER, 0, 0, 0, 20, 0, 100, 0, @UTHERQUEST, 0, 0, 0, 80, @UTHER*100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Quest ''The Lightbringer''s Redemption'' Finished - Run Script'),
(@UTHER*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Remove NPC Flags'),
(@UTHER*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @JAINA, 0, 10, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Remove NPC Flags'),
(@UTHER*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @MURADIN, 0, 10, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Remove NPC Flags'),
(@UTHER*100, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @SYLVANAS, 0, 10, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Remove NPC Flags'),
(@UTHER*100, 9, 4, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @DARION, 0, 10, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Remove NPC Flags'),
(@UTHER*100, 9, 5, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 0'),
(@UTHER*100, 9, 6, 0, 0, 0, 100, 0, 4000, 4000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 1'),
(@UTHER*100, 9, 7, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 2'),
(@UTHER*100, 9, 8, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 3'),
(@UTHER*100, 9, 9, 0, 0, 0, 100, 0, 9000, 9000, 0, 0, 1, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 4'),
(@UTHER*100, 9, 10, 0, 0, 0, 100, 0, 11000, 11000, 0, 0, 1, 5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 5'),
(@UTHER*100, 9, 11, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 6'),
(@UTHER*100, 9, 12, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 7'),
(@UTHER*100, 9, 13, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 8, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Say Line 8'),
(@UTHER*100, 9, 14, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 81, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Restore NPC Flags'),
(@UTHER*100, 9, 15, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 9, @JAINA, 0, 10, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Restore NPC Flags'),
(@UTHER*100, 9, 16, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @MURADIN, 0, 10, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Restore NPC Flags'),
(@UTHER*100, 9, 17, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @SYLVANAS, 0, 10, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Restore NPC Flags'),
(@UTHER*100, 9, 18, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 9, @DARION, 0, 10, 0, 0, 0, 0, 'Uther the Lightbringer - On Script - Restore NPC Flags'),
(@SYLVANAS, 0, 0, 0, 20, 0, 100, 0, @SYLVANASQUEST, 0, 0, 0, 80, @SYLVANAS*100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Quest ''Sylvanas'' Vengeance'' Finished - Run Script'),
(@SYLVANAS*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Remove NPC Flags'),
(@SYLVANAS*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @JAINA, 0, 10, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Remove NPC Flags'),
(@SYLVANAS*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @MURADIN, 0, 10, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Remove NPC Flags'),
(@SYLVANAS*100, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @UTHER, 0, 10, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Remove NPC Flags'),
(@SYLVANAS*100, 9, 4, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @DARION, 0, 10, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Remove NPC Flags'),
(@SYLVANAS*100, 9, 5, 0, 0, 0, 100, 0, 4000, 4000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Say Line 0'),
(@SYLVANAS*100, 9, 6, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Say Line 1'),
(@SYLVANAS*100, 9, 7, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Say Line 2'),
(@SYLVANAS*100, 9, 8, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Say Line 3'),
(@SYLVANAS*100, 9, 9, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Say Line 4'),
(@SYLVANAS*100, 9, 10, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Say Line 5'),
(@SYLVANAS*100, 9, 11, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 1, 6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Say Line 6'),
(@SYLVANAS*100, 9, 12, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 81, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Restore NPC Flags'),
(@SYLVANAS*100, 9, 13, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 9, @JAINA, 0, 10, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Restore NPC Flags'),
(@SYLVANAS*100, 9, 14, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @MURADIN, 0, 10, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Restore NPC Flags'),
(@SYLVANAS*100, 9, 15, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @UTHER, 0, 10, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Restore NPC Flags'),
(@SYLVANAS*100, 9, 16, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 9, @DARION, 0, 10, 0, 0, 0, 0, 'Lady Sylvanas Windrunner - On Script - Restore NPC Flags'),
(@DARION, 0, 7, 0, 20, 0, 100, 0, @DARIONQUEST, 0, 0, 0, 80, @DARION*100+1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Quest ''Mograine''s Reunion'' Finished - Run Script'),
(@DARION*100+1, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Remove NPC Flags'),
(@DARION*100+1, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @JAINA, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Remove NPC Flags'),
(@DARION*100+1, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @MURADIN, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Remove NPC Flags'),
(@DARION*100+1, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @UTHER, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Remove NPC Flags'),
(@DARION*100+1, 9, 4, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 9, @SYLVANAS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Remove NPC Flags'),
(@DARION*100+1, 9, 5, 0, 0, 0, 100, 0, 2000, 2000, 0, 0, 5, 432, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Play Emote 432'),
(@DARION*100+1, 9, 6, 0, 0, 0, 100, 0, 2000, 2000, 0, 0, 11, 72468, 3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Cast ''Summon Alexandros'''),
(@DARION*100+1, 9, 7, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 66, 0, 0, 0, 0, 0, 0, 19, @ALEXANDROS, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Set Orientation Closest Creature ''Highlord Alexandros Mograine'''),
(@DARION*100+1, 9, 8, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 4'),
(@DARION*100+1, 9, 9, 0, 0, 0, 100, 0, 4000, 4000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 9, @ALEXANDROS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 0'),
(@DARION*100+1, 9, 10, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 9, @ALEXANDROS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 1'),
(@DARION*100+1, 9, 11, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 5'),
(@DARION*100+1, 9, 12, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 2, 0, 0, 0, 0, 0, 9, @ALEXANDROS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 2'),
(@DARION*100+1, 9, 13, 0, 0, 0, 100, 0, 11000, 11000, 0, 0, 1, 3, 0, 0, 0, 0, 0, 9, @ALEXANDROS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 3'),
(@DARION*100+1, 9, 14, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 4, 0, 0, 0, 0, 0, 9, @ALEXANDROS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 4'),
(@DARION*100+1, 9, 15, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 5, 0, 0, 0, 0, 0, 9, @ALEXANDROS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 5'),
(@DARION*100+1, 9, 16, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 6, 0, 0, 0, 0, 0, 9, @ALEXANDROS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 6'),
(@DARION*100+1, 9, 17, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Say Line 6'),
(@DARION*100+1, 9, 18, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 66, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Set Orientation Home Position'),
(@DARION*100+1, 9, 19, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 41, 0, 0, 0, 0, 0, 0, 11, @ALEXANDROS, 10, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Despawn Highlord Alexandros'),
(@DARION*100+1, 9, 20, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Restore NPC Flags'),
(@DARION*100+1, 9, 21, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 3, 0, 0, 0, 0, 0, 9, @JAINA, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Restore NPC Flags'),
(@DARION*100+1, 9, 22, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @MURADIN, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Restore NPC Flags'),
(@DARION*100+1, 9, 23, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @UTHER, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Restore NPC Flags'),
(@DARION*100+1, 9, 24, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 9, @SYLVANAS, 0, 10, 0, 0, 0, 0, 'Highlord Darion Mograine - On Script - Restore NPC Flags'),
(@ALEXANDROS, 0, 0, 0, 1, 0, 100, 1, 0, 0, 0, 0, 11, 72469, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Highlord Alexandros Mograine - Out of Combat - Cast ''Alexandros Spawn''');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_03_31_00.sql 
-- -------------------------------------------------------- 
-- Fix for quest ID 5561, Kodo Roundup
-- Add the missing link between npc_text and gossip_menu_id :
DELETE FROM `gossip_menu` WHERE `entry`=3650 AND `text_id`=4449;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (3650, 4449);
-- Link the npc_text from gossip_menu:
UPDATE `creature_template` SET `gossip_menu_id`=3650 WHERE `entry`=11627;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_01_00.sql 
-- -------------------------------------------------------- 
--
DELETE FROM `smart_scripts` WHERE `entryorguid`=15274 AND `id`=2;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(15274, 0, 2, 0, 8, 0, 100, 1, 50613, 0, 0, 0, 11, 61314, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mana Wyrm - On Spellhit Arcane Torrent - Quest Credit');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_01_01.sql 
-- -------------------------------------------------------- 
-- Pathing for Silvermoon City Guardian Entry: 16222 'TDB FORMAT' 
SET @NPC := 56892;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=9797.032,`position_y`=-7418.94,`position_z`=14.02596 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,9797.032,-7418.94,14.02596,0,0,0,0,100,0), -- 20:03:32
(@PATH,2,9795.906,-7402.565,13.77626,0,0,0,0,100,0), -- 20:03:42
(@PATH,3,9795.656,-7400.315,13.77626,0,0,0,0,100,0), -- 20:03:42
(@PATH,4,9790.018,-7397.717,14.59705,0,0,0,0,100,0), -- 20:03:50
(@PATH,5,9788.768,-7397.717,14.84705,0,0,0,0,100,0), -- 20:03:50
(@PATH,6,9775.768,-7396.717,14.84705,0,0,0,0,100,0), -- 20:03:50
(@PATH,7,9770.018,-7396.217,14.84705,0,0,0,0,100,0), -- 20:03:50
(@PATH,8,9767.188,-7400.526,14.03701,0,0,0,0,100,0), -- 20:04:01
(@PATH,9,9767.188,-7402.276,14.03701,0,0,0,0,100,0), -- 20:04:01
(@PATH,10,9765.688,-7412.526,13.78701,0,0,0,0,100,0), -- 20:04:01
(@PATH,11,9758.99,-7418.586,13.32115,0,0,0,0,100,0), -- 20:04:10
(@PATH,12,9713.854,-7426.193,13.51967,0,0,0,0,100,0), -- 20:04:24
(@PATH,13,9707.104,-7429.193,13.51967,0,0,0,0,100,0), -- 20:04:24
(@PATH,14,9700.104,-7432.693,13.51967,0,0,0,0,100,0), -- 20:04:24
(@PATH,15,9687.424,-7434.536,13.54329,0,0,0,0,100,0), -- 20:04:39
(@PATH,16,9650.786,-7425.012,13.54389,0,0,0,0,100,0), -- 20:04:50
(@PATH,17,9611.484,-7423.664,13.54949,0,0,0,0,100,0), -- 20:04:59
(@PATH,18,9602.561,-7429.987,13.43456,0,0,0,0,100,0), -- 20:05:15
(@PATH,19,9602.061,-7430.737,13.93456,0,0,0,0,100,0), -- 20:05:15
(@PATH,20,9600.311,-7432.487,13.68456,0,0,0,0,100,0), -- 20:05:15
(@PATH,21,9578.125,-7435.431,15.78367,0,0,0,0,100,0), -- 20:05:23
(@PATH,22,9573.875,-7434.931,15.78367,0,0,0,0,100,0), -- 20:05:23
(@PATH,23,9570.875,-7434.681,15.78367,0,0,0,0,100,0), -- 20:05:23
(@PATH,24,9561.199,-7425.627,17.48535,0,0,0,0,100,0), -- 20:05:36
(@PATH,25,9560.949,-7424.377,17.98535,0,0,0,0,100,0), -- 20:05:36
(@PATH,26,9560.449,-7422.627,18.73535,0,0,0,0,100,0), -- 20:05:36
(@PATH,27,9560.199,-7420.877,19.48535,0,0,0,0,100,0), -- 20:05:36
(@PATH,28,9562.604,-7417.716,19.62811,0,0,0,0,100,0), -- 20:05:42
(@PATH,29,9564.242,-7407.687,19.65438,0,0,0,0,100,0), -- 20:05:44
(@PATH,30,9564.242,-7403.187,17.15438,0,0,0,0,100,0), -- 20:05:44
(@PATH,31,9555.359,-7396.585,17.08825,0,0,0,0,100,0), -- 20:05:52
(@PATH,32,9547.609,-7396.585,17.08825,0,0,0,0,100,0), -- 20:05:52
(@PATH,33,9539.609,-7396.835,17.08825,0,0,0,0,100,0), -- 20:05:52
(@PATH,34,9538.109,-7396.835,17.08825,0,0,0,0,100,0), -- 20:05:52
(@PATH,35,9536.859,-7396.835,17.08825,0,0,0,0,100,0), -- 20:05:52
(@PATH,36,9534.359,-7396.835,17.08825,0,0,0,0,100,0), -- 20:05:52
(@PATH,37,9530.609,-7397.085,17.08825,0,0,0,0,100,0), -- 20:05:52
(@PATH,38,9528.309,-7396.823,17.08099,0,0,0,0,100,0), -- 20:06:06
(@PATH,39,9515.559,-7396.823,15.08099,0,0,0,0,100,0), -- 20:06:06
(@PATH,40,9508.986,-7383.027,14.37308,0,0,0,0,100,0), -- 20:06:15
(@PATH,41,9507.986,-7368.527,14.37308,0,0,0,0,100,0), -- 20:06:15
(@PATH,42,9509.079,-7334.869,14.47609,0,0,0,0,100,0), -- 20:06:29
(@PATH,43,9509.329,-7333.369,14.47609,0,0,0,0,100,0), -- 20:06:29
(@PATH,44,9509.329,-7332.369,14.47609,0,0,0,0,100,0), -- 20:06:29
(@PATH,45,9509.579,-7330.119,14.47609,0,0,0,0,100,0), -- 20:06:29
(@PATH,46,9509.579,-7329.119,14.22609,0,0,0,0,100,0), -- 20:06:29
(@PATH,47,9510.201,-7311.316,14.32034,0,0,0,0,100,0), -- 20:06:45
(@PATH,48,9510.201,-7306.066,14.32034,0,0,0,0,100,0), -- 20:06:45
(@PATH,49,9510.451,-7298.816,14.32034,0,0,0,0,100,0), -- 20:06:45
(@PATH,50,9510.451,-7293.316,14.07034,0,0,0,0,100,0), -- 20:06:45
(@PATH,51,9510.329,-7300.647,14.09887,0,0,0,0,100,0), -- 20:06:54
(@PATH,52,9510.372,-7292.951,14.05711,0,0,0,0,100,0), -- 20:10:02
(@PATH,53,9515.844,-7278.31,14.22743,0,0,0,0,100,0), -- 20:10:09
(@PATH,54,9526.344,-7277.31,13.97743,0,0,0,0,100,0), -- 20:10:09
(@PATH,55,9537.094,-7276.31,14.22743,0,0,0,0,100,0), -- 20:10:09
(@PATH,56,9555.932,-7274.813,14.1918,0,0,0,0,100,0), -- 20:10:27
(@PATH,57,9620.014,-7275.292,14.19181,0,0,0,0,100,0), -- 20:10:43
(@PATH,58,9664.584,-7274.804,13.9807,0,0,0,0,100,0), -- 20:11:00
(@PATH,59,9667.084,-7274.804,14.2307,0,0,0,0,100,0), -- 20:11:00
(@PATH,60,9676.5,-7289.921,13.96391,0,0,0,0,100,0), -- 20:11:16
(@PATH,61,9676.75,-7291.921,14.21391,0,0,0,0,100,0), -- 20:11:16
(@PATH,62,9682.359,-7296.356,14.19385,0,0,0,0,100,0), -- 20:11:25
(@PATH,63,9693.359,-7297.106,14.44385,0,0,0,0,100,0), -- 20:11:25
(@PATH,64,9695.359,-7297.106,14.44385,0,0,0,0,100,0), -- 20:11:25
(@PATH,65,9698.109,-7297.356,14.69385,0,0,0,0,100,0), -- 20:11:25
(@PATH,66,9699.902,-7303.835,15.57155,0,0,0,0,100,0), -- 20:11:34
(@PATH,67,9699.902,-7308.335,15.32155,0,0,0,0,100,0), -- 20:11:34
(@PATH,68,9695.211,-7310.05,14.5845,0,0,0,0,100,0), -- 20:11:39
(@PATH,69,9693.211,-7310.3,14.5845,0,0,0,0,100,0), -- 20:11:39
(@PATH,70,9682.211,-7310.8,14.3345,0,0,0,0,100,0), -- 20:11:39
(@PATH,71,9676.809,-7317.146,14.0487,0,0,0,0,100,0), -- 20:11:48
(@PATH,72,9676.059,-7328.396,13.7987,0,0,0,0,100,0), -- 20:11:48
(@PATH,73,9675.559,-7335.146,12.5487,0,0,0,0,100,0), -- 20:11:48
(@PATH,74,9675.311,-7342.5,12.11859,0,0,0,0,100,0), -- 20:11:59
(@PATH,75,9675.311,-7345.75,12.11859,0,0,0,0,100,0), -- 20:11:59
(@PATH,76,9675.311,-7347.5,12.11859,0,0,0,0,100,0), -- 20:11:59
(@PATH,77,9675.311,-7348.5,12.11859,0,0,0,0,100,0), -- 20:11:59
(@PATH,78,9669.002,-7356.422,12.18507,0,0,0,0,100,0), -- 20:12:06
(@PATH,79,9668.814,-7361.282,12.18596,0,0,0,0,100,0), -- 20:12:10
(@PATH,80,9668.77,-7364.439,12.18267,0,0,0,0,100,0), -- 20:12:12
(@PATH,81,9690.079,-7381.311,12.09861,0,0,0,0,100,0), -- 20:12:22
(@PATH,82,9687.182,-7384.319,11.74371,0,0,0,0,100,0), -- 20:12:24
(@PATH,83,9686.182,-7384.319,11.74371,0,0,0,0,100,0), -- 20:12:24
(@PATH,84,9683.373,-7392.788,12.14598,0,0,0,0,100,0), -- 20:12:28
(@PATH,85,9683.373,-7394.288,12.14598,0,0,0,0,100,0), -- 20:12:28
(@PATH,86,9682.873,-7403.288,11.89598,0,0,0,0,100,0), -- 20:12:28
(@PATH,87,9682.623,-7408.288,11.89598,0,0,0,0,100,0), -- 20:12:28
(@PATH,88,9682.373,-7411.788,11.89598,0,0,0,0,100,0), -- 20:12:28
(@PATH,89,9682.123,-7420.038,13.89598,0,0,0,0,100,0), -- 20:12:28
(@PATH,90,9681.873,-7421.538,13.89598,0,0,0,0,100,0), -- 20:12:28
(@PATH,91,9682.331,-7411.838,12.38935,0,0,0,0,100,0), -- 20:12:44
(@PATH,92,9682.581,-7408.338,11.88935,0,0,0,0,100,0), -- 20:12:44
(@PATH,93,9682.581,-7403.338,12.13935,0,0,0,0,100,0), -- 20:12:44
(@PATH,94,9683.081,-7394.338,12.13935,0,0,0,0,100,0), -- 20:12:44
(@PATH,95,9683.081,-7392.838,12.13935,0,0,0,0,100,0), -- 20:12:44
(@PATH,96,9683.331,-7390.088,12.13935,0,0,0,0,100,0), -- 20:12:44
(@PATH,97,9687.128,-7384.611,11.93381,0,0,0,0,100,0), -- 20:12:58
(@PATH,98,9689.84,-7379.5,12.07366,0,0,0,0,100,0), -- 20:13:02
(@PATH,99,9689.515,-7378.112,12.14712,0,0,0,0,100,0), -- 20:13:04
(@PATH,100,9668.722,-7361.385,12.18551,0,0,0,0,100,0), -- 20:13:14
(@PATH,101,9668.566,-7356.804,12.18437,0,0,0,0,100,0), -- 20:13:18
(@PATH,102,9675.295,-7347.331,12.04761,0,0,0,0,100,0), -- 20:13:20
(@PATH,103,9675.295,-7346.081,12.04761,0,0,0,0,100,0), -- 20:13:20
(@PATH,104,9675.045,-7342.581,12.04761,0,0,0,0,100,0), -- 20:13:20
(@PATH,105,9675.82,-7328.544,13.847,0,0,0,0,100,0), -- 20:13:27
(@PATH,106,9676.07,-7317.294,14.097,0,0,0,0,100,0), -- 20:13:27
(@PATH,107,9676.32,-7315.294,14.097,0,0,0,0,100,0), -- 20:13:27
(@PATH,108,9693.324,-7310.449,14.44139,0,0,0,0,100,0), -- 20:13:38
(@PATH,109,9695.074,-7310.449,14.44139,0,0,0,0,100,0), -- 20:13:38
(@PATH,110,9697.824,-7310.199,14.69139,0,0,0,0,100,0), -- 20:13:38
(@PATH,111,9699.779,-7303.936,15.51412,0,0,0,0,100,0), -- 20:13:48
(@PATH,112,9700.029,-7299.436,15.51412,0,0,0,0,100,0), -- 20:13:48
(@PATH,113,9698.18,-7297.467,15.08991,0,0,0,0,100,0), -- 20:13:53
(@PATH,114,9695.18,-7297.467,14.58991,0,0,0,0,100,0), -- 20:13:53
(@PATH,115,9693.43,-7297.217,14.58991,0,0,0,0,100,0), -- 20:13:53
(@PATH,116,9682.43,-7296.467,14.33991,0,0,0,0,100,0), -- 20:13:53
(@PATH,117,9680.68,-7296.467,14.08991,0,0,0,0,100,0), -- 20:13:53
(@PATH,118,9676.986,-7292.785,14.18614,0,0,0,0,100,0), -- 20:14:01
(@PATH,119,9676.736,-7290.035,13.93614,0,0,0,0,100,0), -- 20:14:01
(@PATH,120,9676.486,-7284.785,14.18614,0,0,0,0,100,0), -- 20:14:01
(@PATH,121,9664.668,-7274.95,13.98935,0,0,0,0,100,0), -- 20:14:11
(@PATH,122,9641.668,-7275.45,13.98935,0,0,0,0,100,0), -- 20:14:11
(@PATH,123,9600.16,-7275.033,14.19062,0,0,0,0,100,0), -- 20:14:27
(@PATH,124,9596.66,-7275.033,14.19062,0,0,0,0,100,0), -- 20:14:27
(@PATH,125,9595.887,-7274.914,14.19024,0,0,0,0,100,0), -- 20:14:43
(@PATH,126,9526.42,-7277.001,14.22749,0,0,0,0,100,0), -- 20:14:59
(@PATH,127,9515.92,-7278.001,14.22749,0,0,0,0,100,0), -- 20:14:59
(@PATH,128,9513.92,-7278.251,14.22749,0,0,0,0,100,0), -- 20:14:59
(@PATH,129,9510.681,-7293.288,14.06322,0,0,0,0,100,0), -- 20:15:17
(@PATH,130,9510.681,-7298.788,14.31322,0,0,0,0,100,0), -- 20:15:17
(@PATH,131,9510.681,-7306.038,14.31322,0,0,0,0,100,0), -- 20:15:17
(@PATH,132,9510.431,-7311.288,14.31322,0,0,0,0,100,0), -- 20:15:17
(@PATH,133,9510.431,-7314.288,14.31322,0,0,0,0,100,0), -- 20:15:17
(@PATH,134,9509.529,-7330.155,14.47821,0,0,0,0,100,0), -- 20:15:34
(@PATH,135,9509.279,-7332.405,14.47821,0,0,0,0,100,0), -- 20:15:34
(@PATH,136,9509.279,-7333.405,14.47821,0,0,0,0,100,0), -- 20:15:34
(@PATH,137,9509.029,-7334.905,14.47821,0,0,0,0,100,0), -- 20:15:34
(@PATH,138,9507.779,-7356.905,14.47821,0,0,0,0,100,0), -- 20:15:34
(@PATH,139,9508.02,-7368.424,14.58394,0,0,0,0,100,0), -- 20:15:49
(@PATH,140,9508.77,-7382.924,14.58394,0,0,0,0,100,0), -- 20:15:49
(@PATH,141,9508.77,-7385.174,14.58394,0,0,0,0,100,0), -- 20:15:49
(@PATH,142,9528.405,-7396.765,17.07827,0,0,0,0,100,0), -- 20:16:04
(@PATH,143,9534.251,-7396.64,16.96906,0,0,0,0,100,0), -- 20:16:13
(@PATH,144,9537.001,-7396.64,16.96906,0,0,0,0,100,0), -- 20:16:13
(@PATH,145,9538.001,-7396.64,16.96906,0,0,0,0,100,0), -- 20:16:13
(@PATH,146,9539.751,-7396.64,16.96906,0,0,0,0,100,0), -- 20:16:13
(@PATH,147,9547.501,-7396.64,16.96906,0,0,0,0,100,0), -- 20:16:13
(@PATH,148,9555.251,-7396.39,16.96906,0,0,0,0,100,0), -- 20:16:13
(@PATH,149,9556.751,-7396.39,16.96906,0,0,0,0,100,0), -- 20:16:13
(@PATH,150,9564.121,-7407.653,19.65082,0,0,0,0,100,0), -- 20:16:26
(@PATH,151,9564.121,-7411.653,19.65082,0,0,0,0,100,0), -- 20:16:26
(@PATH,152,9562.407,-7417.577,19.72515,0,0,0,0,100,0), -- 20:16:34
(@PATH,153,9560.863,-7424.275,17.98024,0,0,0,0,100,0), -- 20:16:37
(@PATH,154,9561.113,-7425.525,17.48024,0,0,0,0,100,0), -- 20:16:37
(@PATH,155,9561.613,-7427.275,16.73024,0,0,0,0,100,0), -- 20:16:37
(@PATH,156,9573.232,-7434.738,15.66694,0,0,0,0,100,0), -- 20:16:43
(@PATH,157,9577.982,-7435.488,15.66694,0,0,0,0,100,0), -- 20:16:43
(@PATH,158,9581.732,-7435.988,14.16694,0,0,0,0,100,0), -- 20:16:43
(@PATH,159,9601.162,-7431.205,13.72673,0,0,0,0,100,0), -- 20:16:55
(@PATH,160,9602.662,-7430.205,13.47673,0,0,0,0,100,0), -- 20:16:55
(@PATH,161,9604.162,-7428.705,13.47673,0,0,0,0,100,0), -- 20:16:55
(@PATH,162,9611.602,-7423.619,13.54987,0,0,0,0,100,0), -- 20:17:04
(@PATH,163,9661.816,-7430.175,13.54431,0,0,0,0,100,0), -- 20:17:19
(@PATH,164,9691.869,-7434.476,13.54329,0,0,0,0,100,0), -- 20:17:29
(@PATH,165,9699.86,-7432.892,13.52494,0,0,0,0,100,0), -- 20:17:39
(@PATH,166,9707.11,-7429.142,13.52494,0,0,0,0,100,0), -- 20:17:39
(@PATH,167,9713.61,-7426.142,13.52494,0,0,0,0,100,0), -- 20:17:39
(@PATH,168,9720.36,-7422.642,13.52494,0,0,0,0,100,0), -- 20:17:39
(@PATH,169,9760.865,-7418.738,13.57886,0,0,0,0,100,0), -- 20:17:53
(@PATH,170,9766.886,-7402.463,13.85978,0,0,0,0,100,0), -- 20:18:08
(@PATH,171,9767.136,-7400.463,13.85978,0,0,0,0,100,0), -- 20:18:08
(@PATH,172,9767.636,-7397.713,14.10978,0,0,0,0,100,0), -- 20:18:08
(@PATH,173,9769.994,-7396.318,14.46496,0,0,0,0,100,0), -- 20:18:18
(@PATH,174,9775.994,-7396.568,14.96496,0,0,0,0,100,0), -- 20:18:18
(@PATH,175,9788.744,-7397.568,14.96496,0,0,0,0,100,0), -- 20:18:18
(@PATH,176,9789.994,-7397.818,14.96496,0,0,0,0,100,0), -- 20:18:18
(@PATH,177,9792.994,-7398.068,14.46496,0,0,0,0,100,0), -- 20:18:18
(@PATH,178,9795.314,-7400.193,14.02223,0,0,0,0,100,0), -- 20:18:29
(@PATH,179,9795.564,-7402.443,14.02223,0,0,0,0,100,0), -- 20:18:29
(@PATH,180,9796.064,-7407.193,14.02223,0,0,0,0,100,0), -- 20:18:29
(@PATH,181,9796.564,-7412.443,14.02223,0,0,0,0,100,0); -- 20:18:29

-- Pathing for Silvermoon City Guardian Entry: 16222 'TDB FORMAT' 
SET @NPC := 56891;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=9617.148,`position_y`=-7372.344,`position_z`=14.95278 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,9617.148,-7372.344,14.95278,0,0,0,0,100,0), -- 11:38:44
(@PATH,2,9617.634,-7381.962,14.90948,0,0,0,0,100,0), -- 11:38:50
(@PATH,3,9613.992,-7385.066,14.11522,0,0,0,0,100,0), -- 11:38:55
(@PATH,4,9611.742,-7385.566,14.11522,0,0,0,0,100,0), -- 11:38:55
(@PATH,5,9607.742,-7386.316,13.86522,0,0,0,0,100,0), -- 11:38:55
(@PATH,6,9601.742,-7387.316,13.86522,0,0,0,0,100,0), -- 11:38:55
(@PATH,7,9600.242,-7387.566,13.86522,0,0,0,0,100,0), -- 11:38:55
(@PATH,8,9595.152,-7394.241,13.41057,0,0,0,0,100,0), -- 11:39:05
(@PATH,9,9601.703,-7417.196,13.54557,0,0,0,0,100,0), -- 11:39:12
(@PATH,10,9605.453,-7422.946,13.54557,0,0,0,0,100,0), -- 11:39:12
(@PATH,11,9611.395,-7424.344,13.54924,0,0,0,0,100,0), -- 11:39:20
(@PATH,12,9669.465,-7433.946,13.54404,0,0,0,0,100,0), -- 11:39:37
(@PATH,13,9692.181,-7435.465,13.54329,0,0,0,0,100,0), -- 11:39:47
(@PATH,14,9706.28,-7428.034,13.54329,0,0,0,0,100,0), -- 11:39:57
(@PATH,15,9712.78,-7423.784,13.54329,0,0,0,0,100,0), -- 11:39:57
(@PATH,16,9755.95,-7419.636,13.33486,0,0,0,0,100,0), -- 11:40:07
(@PATH,17,9758.95,-7419.636,13.33486,0,0,0,0,100,0), -- 11:40:07
(@PATH,18,9760.95,-7419.386,13.58486,0,0,0,0,100,0), -- 11:40:07
(@PATH,19,9765.676,-7425.275,13.87941,0,0,0,0,100,0), -- 11:40:26
(@PATH,20,9766.176,-7435.275,13.87941,0,0,0,0,100,0), -- 11:40:26
(@PATH,21,9766.426,-7437.275,13.87941,0,0,0,0,100,0), -- 11:40:26
(@PATH,22,9766.676,-7440.525,14.12941,0,0,0,0,100,0), -- 11:40:26
(@PATH,23,9769.751,-7441.452,14.42178,0,0,0,0,100,0), -- 11:40:36
(@PATH,24,9775.751,-7441.702,14.92178,0,0,0,0,100,0), -- 11:40:36
(@PATH,25,9782.751,-7441.952,14.92178,0,0,0,0,100,0), -- 11:40:36
(@PATH,26,9785.001,-7441.952,14.92178,0,0,0,0,100,0), -- 11:40:36
(@PATH,27,9791.501,-7442.202,14.92178,0,0,0,0,100,0), -- 11:40:36
(@PATH,28,9793.707,-7440.501,14.62286,0,0,0,0,100,0), -- 11:40:45
(@PATH,29,9794.207,-7437.501,14.12286,0,0,0,0,100,0), -- 11:40:45
(@PATH,30,9794.707,-7435.501,14.12286,0,0,0,0,100,0), -- 11:40:45
(@PATH,31,9795.457,-7431.251,13.87286,0,0,0,0,100,0), -- 11:40:45
(@PATH,32,9796.457,-7426.001,13.87286,0,0,0,0,100,0), -- 11:40:45
(@PATH,33,9796.707,-7424.001,13.87286,0,0,0,0,100,0), -- 11:40:45
(@PATH,34,9802.908,-7420.202,13.36064,0,0,0,0,100,0), -- 11:40:55
(@PATH,35,9865.802,-7420.71,13.53932,0,0,0,0,100,0), -- 11:41:14
(@PATH,36,9880.648,-7415.182,13.51911,0,0,0,0,100,0), -- 11:41:27
(@PATH,37,9885.898,-7409.932,13.51911,0,0,0,0,100,0), -- 11:41:27
(@PATH,38,9886.761,-7404.89,13.40958,0,0,0,0,100,0), -- 11:41:33
(@PATH,39,9888.123,-7384.329,15.85358,0,0,0,0,100,0), -- 11:41:40
(@PATH,40,9888.623,-7369.579,20.85358,0,0,0,0,100,0), -- 11:41:40
(@PATH,41,9873.136,-7363.303,20.90705,0,0,0,0,100,0), -- 11:41:51
(@PATH,42,9867.386,-7362.553,19.65705,0,0,0,0,100,0), -- 11:41:51
(@PATH,43,9863.198,-7364.229,19.0695,0,0,0,0,100,0), -- 11:42:00
(@PATH,44,9859.948,-7366.479,19.0695,0,0,0,0,100,0), -- 11:42:00
(@PATH,45,9841.254,-7366.119,18.845,0,0,0,0,100,0), -- 11:42:07
(@PATH,46,9836.004,-7362.869,18.845,0,0,0,0,100,0), -- 11:42:07
(@PATH,47,9833.344,-7362.54,18.90108,0,0,0,0,100,0), -- 11:42:15
(@PATH,48,9829.344,-7362.79,20.65108,0,0,0,0,100,0), -- 11:42:15
(@PATH,49,9827.594,-7363.04,20.90108,0,0,0,0,100,0), -- 11:42:15
(@PATH,50,9821.844,-7363.54,20.90108,0,0,0,0,100,0), -- 11:42:15
(@PATH,51,9829.27,-7362.814,20.40145,0,0,0,0,100,0), -- 11:42:26
(@PATH,52,9833.27,-7362.314,19.65145,0,0,0,0,100,0), -- 11:42:26
(@PATH,53,9835.859,-7362.992,19.06359,0,0,0,0,100,0), -- 11:42:34
(@PATH,54,9841.109,-7365.992,19.06359,0,0,0,0,100,0), -- 11:42:34
(@PATH,55,9850.109,-7370.992,19.06359,0,0,0,0,100,0), -- 11:42:34
(@PATH,56,9859.49,-7366.387,18.84956,0,0,0,0,100,0), -- 11:42:41
(@PATH,57,9863.24,-7364.137,18.84956,0,0,0,0,100,0), -- 11:42:41
(@PATH,58,9873.046,-7363.469,20.91313,0,0,0,0,100,0), -- 11:42:48
(@PATH,59,9874.796,-7363.469,20.91313,0,0,0,0,100,0), -- 11:42:48
(@PATH,60,9887.834,-7384.231,15.64358,0,0,0,0,100,0), -- 11:42:58
(@PATH,61,9887.834,-7387.231,14.64358,0,0,0,0,100,0), -- 11:42:58
(@PATH,62,9886.838,-7404.881,13.63869,0,0,0,0,100,0), -- 11:43:08
(@PATH,63,9876.497,-7419.251,13.51794,0,0,0,0,100,0), -- 11:43:16
(@PATH,64,9865.785,-7420.523,13.54063,0,0,0,0,100,0), -- 11:43:22
(@PATH,65,9859.535,-7420.523,13.54063,0,0,0,0,100,0), -- 11:43:22
(@PATH,66,9801.332,-7420.127,13.66687,0,0,0,0,100,0), -- 11:43:36
(@PATH,67,9796.978,-7424.1,13.69735,0,0,0,0,100,0), -- 11:43:53
(@PATH,68,9796.728,-7425.85,13.94735,0,0,0,0,100,0), -- 11:43:53
(@PATH,69,9795.728,-7431.35,13.94735,0,0,0,0,100,0), -- 11:43:53
(@PATH,70,9794.978,-7435.35,13.69735,0,0,0,0,100,0), -- 11:43:53
(@PATH,71,9794.478,-7437.6,13.94735,0,0,0,0,100,0), -- 11:43:53
(@PATH,72,9793.978,-7440.6,14.19735,0,0,0,0,100,0), -- 11:43:53
(@PATH,73,9791.54,-7442.293,14.44685,0,0,0,0,100,0), -- 11:44:02
(@PATH,74,9785.04,-7442.043,14.94685,0,0,0,0,100,0), -- 11:44:02
(@PATH,75,9782.79,-7442.043,14.94685,0,0,0,0,100,0), -- 11:44:02
(@PATH,76,9775.54,-7441.793,14.94685,0,0,0,0,100,0), -- 11:44:02
(@PATH,77,9769.79,-7441.543,14.94685,0,0,0,0,100,0), -- 11:44:02
(@PATH,78,9766.505,-7435.237,13.99367,0,0,0,0,100,0), -- 11:44:13
(@PATH,79,9765.755,-7425.237,13.74367,0,0,0,0,100,0), -- 11:44:13
(@PATH,80,9765.755,-7423.487,13.74367,0,0,0,0,100,0), -- 11:44:13
(@PATH,81,9758.748,-7419.551,13.30211,0,0,0,0,100,0), -- 11:44:22
(@PATH,82,9755.748,-7419.551,13.30211,0,0,0,0,100,0), -- 11:44:22
(@PATH,83,9720.498,-7421.301,13.30211,0,0,0,0,100,0), -- 11:44:22
(@PATH,84,9706.665,-7428.206,13.52816,0,0,0,0,100,0), -- 11:44:41
(@PATH,85,9699.915,-7432.456,13.52816,0,0,0,0,100,0), -- 11:44:41
(@PATH,86,9688.145,-7435.444,13.54329,0,0,0,0,100,0), -- 11:44:52
(@PATH,87,9661.658,-7430.764,13.54354,0,0,0,0,100,0), -- 11:45:01
(@PATH,88,9650.658,-7426.014,13.54354,0,0,0,0,100,0), -- 11:45:01
(@PATH,89,9611.502,-7424.288,13.54895,0,0,0,0,100,0), -- 11:45:10
(@PATH,90,9601.961,-7417.239,13.55362,0,0,0,0,100,0), -- 11:45:27
(@PATH,91,9600.711,-7415.739,13.55362,0,0,0,0,100,0), -- 11:45:27
(@PATH,92,9595.22,-7392.367,13.64975,0,0,0,0,100,0), -- 11:45:36
(@PATH,93,9601.827,-7387.457,13.94776,0,0,0,0,100,0), -- 11:45:43
(@PATH,94,9607.577,-7386.457,13.94776,0,0,0,0,100,0), -- 11:45:43
(@PATH,95,9611.827,-7385.707,13.69776,0,0,0,0,100,0), -- 11:45:43
(@PATH,96,9613.827,-7385.457,13.94776,0,0,0,0,100,0), -- 11:45:43
(@PATH,97,9616.577,-7384.957,14.19776,0,0,0,0,100,0), -- 11:45:43
(@PATH,98,9617.488,-7381.918,14.6536,0,0,0,0,100,0), -- 11:45:53
(@PATH,99,9617.488,-7376.918,14.9036,0,0,0,0,100,0), -- 11:45:53
(@PATH,100,9617.238,-7372.168,14.9036,0,0,0,0,100,0), -- 11:45:53
(@PATH,101,9617.634,-7381.962,14.90948,0,0,0,0,100,0), -- 11:46:00
(@PATH,102,9613.973,-7385.057,14.11319,0,0,0,0,100,0), -- 11:46:04
(@PATH,103,9611.723,-7385.557,14.11319,0,0,0,0,100,0), -- 11:46:04
(@PATH,104,9607.723,-7386.307,13.86319,0,0,0,0,100,0), -- 11:46:04
(@PATH,105,9601.973,-7387.307,13.86319,0,0,0,0,100,0), -- 11:46:04
(@PATH,106,9600.223,-7387.557,13.86319,0,0,0,0,100,0), -- 11:46:04
(@PATH,107,9595.393,-7394.153,13.41572,0,0,0,0,100,0), -- 11:46:14
(@PATH,108,9601.696,-7417.232,13.54363,0,0,0,0,100,0), -- 11:46:21
(@PATH,109,9605.696,-7422.982,13.54363,0,0,0,0,100,0), -- 11:46:21
(@PATH,110,9611.418,-7424.264,13.54923,0,0,0,0,100,0); -- 11:46:29

DELETE FROM `creature_formations` WHERE `leaderGUID`=56878;
INSERT INTO `creature_formations` (`leaderGUID`, `memberGUID`, `dist`, `angle`, `groupAI`) VALUES
(56878, 56878, 0, 0, 2),
(56878, 56877, 3, 90, 2);

-- Pathing for Silvermoon Guardian Entry: 16221 'TDB FORMAT' 
SET @NPC := 56878;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=9102.456,`position_y`=-7482.04,`position_z`=78.35519 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,9102.456,-7482.04,78.35519,0,0,0,0,100,0), -- 19:43:23
(@PATH,2,9103.248,-7482.65,78.35519,0,0,0,0,100,0), -- 19:43:23
(@PATH,3,9104.715,-7483.78,78.2541,0,0,0,0,100,0), -- 19:43:23
(@PATH,4,9109.629,-7484.697,77.63239,0,0,0,0,100,0), -- 19:43:23
(@PATH,5,9115.527,-7485.799,77.00739,0,0,0,0,100,0), -- 19:43:23
(@PATH,6,9120.441,-7486.717,76.38239,0,0,0,0,100,0), -- 19:43:23
(@PATH,7,9124.373,-7487.451,75.75739,0,0,0,0,100,0), -- 19:43:23
(@PATH,8,9129.287,-7488.369,75.13239,0,0,0,0,100,0), -- 19:43:23
(@PATH,9,9137.15,-7489.838,74.58031,0,0,0,0,100,0), -- 19:43:23
(@PATH,10,9141.082,-7490.572,73.95531,0,0,0,0,100,0), -- 19:43:23
(@PATH,11,9142.2,-7490.788,73.90916,0,0,0,0,100,0), -- 19:43:23
(@PATH,12,9142.2,-7490.788,73.90916,0,0,0,0,100,0), -- 19:43:23
(@PATH,13,9149.529,-7476.493,69.95045,0,0,0,0,100,0), -- 19:43:29
(@PATH,14,9161.441,-7460.135,63.98,0,0,0,0,100,0), -- 19:43:35
(@PATH,15,9179.331,-7453.608,56.39117,0,0,0,0,100,0), -- 19:43:44
(@PATH,16,9198.095,-7455.712,49.39744,0,0,0,0,100,0), -- 19:43:52
(@PATH,17,9232.926,-7447.019,39.44244,0,0,0,0,100,0), -- 19:44:00
(@PATH,18,9244.291,-7433.579,36.1172,0,0,0,0,100,0), -- 19:44:16
(@PATH,19,9245.047,-7432.797,36.07084,0,0,0,0,100,0), -- 19:44:22
(@PATH,20,9258.551,-7433.484,36.12626,0,0,0,0,100,0), -- 19:44:28
(@PATH,21,9265.053,-7448.939,36.07185,0,0,0,0,100,0), -- 19:44:34
(@PATH,22,9260.291,-7466.419,35.95266,0,0,0,0,100,0), -- 19:44:41
(@PATH,23,9259.721,-7483.818,35.70003,0,0,0,0,100,0), -- 19:44:49
(@PATH,24,9260.23,-7465.513,35.81084,0,0,0,0,100,0), -- 19:44:56
(@PATH,25,9248.131,-7424.455,35.23894,0,0,0,0,100,0), -- 19:45:04
(@PATH,26,9250.974,-7376.712,29.71493,0,0,0,0,100,0), -- 19:45:17
(@PATH,27,9269.848,-7341.41,22.52634,0,0,0,0,100,0), -- 19:45:36
(@PATH,28,9290.326,-7305.721,18.07349,0,0,0,0,100,0), -- 19:45:53
(@PATH,29,9309.75,-7283.949,14.86156,0,0,0,0,100,0), -- 19:46:10
(@PATH,30,9296.104,-7297.955,16.94801,0,0,0,0,100,0), -- 19:46:23
(@PATH,31,9273.268,-7335.548,21.18934,0,0,0,0,100,0), -- 19:46:32
(@PATH,32,9252.006,-7373.276,28.924,0,0,0,0,100,0), -- 19:46:50
(@PATH,33,9247.184,-7419.163,34.14049,0,0,0,0,100,0), -- 19:47:07
(@PATH,34,9261.941,-7441.854,35.80672,0,0,0,0,100,0), -- 19:47:26
(@PATH,35,9264.699,-7445.521,36.01838,0,0,0,0,100,0), -- 19:47:37
(@PATH,36,9260.029,-7465.688,35.94786,0,0,0,0,100,0), -- 19:47:46
(@PATH,37,9259.739,-7483.936,35.70295,0,0,0,0,100,0), -- 19:47:53
(@PATH,38,9260.128,-7466.218,35.80263,0,0,0,0,100,0), -- 19:48:00
(@PATH,39,9264.906,-7448.786,36.13476,0,0,0,0,100,0), -- 19:48:08
(@PATH,40,9258.107,-7433.702,36.05851,0,0,0,0,100,0), -- 19:48:15
(@PATH,41,9235.605,-7445.421,38.64949,0,0,0,0,100,0), -- 19:48:20
(@PATH,42,9200.038,-7455.543,49.04723,0,0,0,0,100,0), -- 19:48:27
(@PATH,43,9182.41,-7453.483,55.13942,0,0,0,0,100,0), -- 19:48:43
(@PATH,44,9164.158,-7457.769,62.62826,0,0,0,0,100,0), -- 19:48:50
(@PATH,45,9151.068,-7474.071,68.90013,0,0,0,0,100,0), -- 19:48:59
(@PATH,46,9142.732,-7490.235,73.94332,0,0,0,0,100,0), -- 19:49:07
(@PATH,47,9110.125,-7484.632,77.80888,0,0,0,0,100,0), -- 19:49:15
(@PATH,48,9090.791,-7472.85,80.36261,0,0,0,0,100,0), -- 19:49:30
(@PATH,49,9082.396,-7462.425,82.50674,0,0,0,0,100,0), -- 19:49:38
(@PATH,50,9077.785,-7456.682,83.13695,0,0,0,0,100,0), -- 19:49:45
(@PATH,51,9066.736,-7450.874,83.24338,0,0,0,0,100,0), -- 19:49:50
(@PATH,52,9066.793,-7450.982,83.18549,0,0,0,0,100,0), -- 19:49:57
(@PATH,53,9067.023,-7451.012,83.33182,0,0,0,0,100,0), -- 19:50:02
(@PATH,54,9089.325,-7471.378,80.84378,0,0,0,0,100,0); -- 19:50:06
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_02_00.sql 
-- -------------------------------------------------------- 
SET @CGUID		:= 76018;

DELETE FROM `creature` WHERE `guid` BETWEEN @CGUID+0 AND @CGUID+31;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `PhaseId`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `MovementType`) VALUES
(@CGUID+0, 21664, 532, 3, 1, -11086.92, -1899.176, 220.7505, 0.7330383, 7200, 0, 0), -- 21664 (Area: 3457) (Auras: )
(@CGUID+1, 21664, 532, 3, 1, -11104.63, -1877.507, 220.7505, 0.6806784, 7200, 0, 0), -- 21664 (Area: 3457) (Auras: )
(@CGUID+2, 17469, 532, 3, 1, -11064.65, -1874.336, 220.7505, 3.839724, 7200, 0, 0), -- 17469 (Area: 3457) (Auras: )
(@CGUID+3, 17211, 532, 3, 1, -11089.7, -1887.114, 220.7505, 0.715585, 7200, 0, 0), -- 17211 (Area: 3457) (Auras: )
(@CGUID+4, 17211, 532, 3, 1, -11093.26, -1882.732, 220.7505, 0.6632251, 7200, 0, 0), -- 17211 (Area: 3457) (Auras: )
(@CGUID+5, 17211, 532, 3, 1, -11096.65, -1878.406, 220.7505, 0.6632251, 7200, 0, 0), -- 17211 (Area: 3457) (Auras: )
(@CGUID+6, 21160, 532, 3, 1, -11083.24, -1903.355, 220.7504, 0.6632251, 7200, 0, 0), -- 21160 (Area: 3457) (Auras: 32226 - 32226)
(@CGUID+7, 17211, 532, 3, 1, -11100.17, -1873.871, 220.7505, 0.6457718, 7200, 0, 0), -- 17211 (Area: 3457) (Auras: )
(@CGUID+8, 17211, 532, 3, 1, -11079.32, -1900.23, 220.7504, 0.6632251, 7200, 0, 0), -- 17211 (Area: 3457) (Auras: )
(@CGUID+9, 17211, 532, 3, 1, -11103.67, -1869.466, 220.7505, 0.6981317, 7200, 0, 0), -- 17211 (Area: 3457) (Auras: )
(@CGUID+10, 17469, 532, 3, 1, -11068.14, -1869.956, 220.7505, 3.892084, 7200, 0, 0), -- 17469 (Area: 3457) (Auras: )
(@CGUID+11, 17211, 532, 3, 1, -11082.71, -1895.736, 220.7505, 0.6632251, 7200, 0, 0), -- 17211 (Area: 3457) (Auras: )
(@CGUID+12, 17469, 532, 3, 1, -11071.65, -1865.527, 220.7505, 3.822271, 7200, 0, 0), -- 17469 (Area: 3457) (Auras: )
(@CGUID+13, 17211, 532, 3, 1, -11086.31, -1891.324, 220.7505, 0.6457718, 7200, 0, 0), -- 17211 (Area: 3457) (Auras: 32226 - 32226)
(@CGUID+14, 17469, 532, 3, 1, -11075.02, -1861.288, 220.7505, 3.735005, 7200, 0, 0), -- 17469 (Area: 3457) (Auras: )
(@CGUID+15, 17469, 532, 3, 1, -11078.52, -1856.995, 220.7505, 3.892084, 7200, 0, 0), -- 17469 (Area: 3457) (Auras: 32226 - 32226)
(@CGUID+16, 21726, 532, 3, 1, -11053.46, -1879.717, 220.7505, 4.014257, 7200, 0, 0), -- 21726 (Area: 3457) (Auras: )
(@CGUID+17, 17469, 532, 3, 1, -11057.62, -1883.089, 220.7505, 3.804818, 7200, 0, 0), -- 17469 (Area: 3457) (Auras: )
(@CGUID+18, 17469, 532, 3, 1, -11082.03, -1852.191, 220.7505, 3.804818, 7200, 0, 0), -- 17469 (Area: 3457) (Auras: )
(@CGUID+19, 21747, 532, 3, 1, -11060.35, -1870.903, 220.7505, 3.804818, 7200, 0, 0), -- 21747 (Area: 3457) (Auras: )
(@CGUID+20, 21748, 532, 3, 1, -11056.89, -1875.286, 220.7505, 3.665191, 7200, 0, 0), -- 21748 (Area: 3457) (Auras: )
(@CGUID+21, 21726, 532, 3, 1, -11077.72, -1848.789, 220.7505, 3.909538, 7200, 0, 0), -- 21726 (Area: 3457) (Auras: )
(@CGUID+22, 17469, 532, 3, 1, -11061.3, -1878.629, 220.7505, 3.909538, 7200, 0, 0), -- 17469 (Area: 3457) (Auras: )
(@CGUID+23, 21747, 532, 3, 1, -11070.86, -1857.749, 220.7505, 3.839724, 7200, 0, 0), -- 21747 (Area: 3457) (Auras: )
(@CGUID+24, 21748, 532, 3, 1, -11074.35, -1853.261, 220.7505, 3.735005, 7200, 0, 0), -- 21748 (Area: 3457) (Auras: )
(@CGUID+25, 21750, 532, 3, 1, -11067.6, -1861.944, 220.7505, 3.839724, 7200, 0, 0), -- 21750 (Area: 3457) (Auras: )
(@CGUID+26, 21752, 532, 3, 1, -11063.64, -1866.364, 220.7505, 3.822271, 7200, 0, 0), -- 21752 (Area: 3457) (Auras: 32226 - 32226)
(@CGUID+27, 21160, 532, 3, 1, -11107.91, -1873.032, 220.7505, 0.715585, 7200, 0, 0), -- 21160 (Area: 3457) (Auras: )
(@CGUID+28, 21682, 532, 3, 1, -11101.01, -1881.932, 220.7505, 0.6632251, 7200, 0, 0), -- 21682 (Area: 3457) (Auras: )
(@CGUID+29, 21683, 532, 3, 1, -11097.45, -1886.197, 220.7505, 0.7853982, 7200, 0, 0), -- 21683 (Area: 3457) (Auras: 32226 - 32226)
(@CGUID+30, 21682, 532, 3, 1, -11090.56, -1894.994, 220.7505, 0.6283185, 7200, 0, 0), -- 21682 (Area: 3457) (Auras: )
(@CGUID+31, 21684, 532, 3, 1, -11093.84, -1890.473, 220.7505, 0.6981317, 7200, 0, 0); -- 21684 (Area: 3457) (Auras: )

DELETE FROM `gossip_menu` WHERE `entry` IN(8404,8368,8354,8355,8366,8362,8367,7413,8345,8346,8347,8349,8348);
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES 
(8404,10506), -- 16816
(8404,10718), -- 16816
(8368,10442), -- 21752
(8354,10425), -- 17469
(8355,10426), -- 21726
(8366,10439), -- 21748
(8362,10434), -- 21747
(8367,10440), -- 21750
(7413,8952), -- 17211
(8345,10413), -- 21160
(8346,10414), -- 21664
(8347,10416), -- 21682
(8349,10418), -- 21684
(8348,10417); -- 21683

DELETE FROM `gossip_menu_option` WHERE `menu_id` IN(8404,8368,8354,8355,8366,8362,8367,7413,8345,8346,8347,8349,8348);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES 
(8368, 0, 0, 'Control Warchief Blackhand', 19384, 1, 1, 0, 0, 0, 0, NULL, 0),
(8349, 0, 0, 'Control King Llane', 19297, 1, 1, 0, 0, 0, 0, NULL, 0),
(8354, 0, 0, 'Control Orc Grunt', 19320, 1, 1, 0, 0, 0, 0, NULL, 0),
(8355, 0, 0, 'Control Summoned Daemon', 19323, 1, 1, 0, 0, 0, 0, NULL, 0),
(8366, 0, 0, 'Control Orc Wolf', 19375, 1, 1, 0, 0, 0, 0, NULL, 0),
(8362, 0, 0, 'Control Orc Necrolyte', 19367, 1, 1, 0, 0, 0, 0, NULL, 0),
(8367, 0, 0, 'Control Orc Warlock', 19377, 1, 1, 0, 0, 0, 0, NULL, 0),
(7413, 0, 0, 'Control Human Footman', 14008, 1, 1, 0, 0, 0, 0, NULL, 0),
(8345, 0, 0, 'Control Conjured Water Elemental.', 19285, 1, 1, 0, 0, 0, 0, NULL, 0),
(8346, 0, 0, 'Control Human Charger.', 19288, 1, 1, 0, 0, 0, 0, NULL, 0),
(8347, 0, 0, 'Control Human Cleric', 19293, 1, 1, 0, 0, 0, 0, NULL, 0),
(8348, 0, 0, 'Control Human Conjurer', 19295, 1, 1, 0, 0, 0, 0, NULL, 0);

UPDATE `creature_template` SET `gossip_menu_id`=8404 WHERE  `entry`=16816;
UPDATE `creature_template` SET `gossip_menu_id`=8368 WHERE  `entry`=21752;
UPDATE `creature_template` SET `gossip_menu_id`=8354 WHERE  `entry`=17469;
UPDATE `creature_template` SET `gossip_menu_id`=8355 WHERE  `entry`=21726;
UPDATE `creature_template` SET `gossip_menu_id`=8366 WHERE  `entry`=21748;
UPDATE `creature_template` SET `gossip_menu_id`=8362 WHERE  `entry`=21747;
UPDATE `creature_template` SET `gossip_menu_id`=8367 WHERE  `entry`=21750;
UPDATE `creature_template` SET `gossip_menu_id`=7413 WHERE  `entry`=17211;
UPDATE `creature_template` SET `gossip_menu_id`=8345 WHERE  `entry`=21160;
UPDATE `creature_template` SET `gossip_menu_id`=8346 WHERE  `entry`=21664;
UPDATE `creature_template` SET `gossip_menu_id`=8347 WHERE  `entry`=21682;
UPDATE `creature_template` SET `gossip_menu_id`=8348 WHERE  `entry`=21683;

 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_02_01.sql 
-- -------------------------------------------------------- 
UPDATE `creature_template` SET `AIName` = 'SmartAI' WHERE `entry` IN (21302, 21316, 20683, 21314, 21500, 19740, 19755, 21499, 21501, 21305);
UPDATE `creature_template_addon` SET `emote`=0 WHERE `entry`=21302;
UPDATE `creature_addon` SET `emote`=0 WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id`=21302);
UPDATE `creature_template` SET  `InhabitType`=4 WHERE  `entry`=21316;
UPDATE `creature` SET `position_x`= -3441.672363, `position_y`= 2950.212646, `position_z`= 171.877686, `orientation`= 6.151093 WHERE `guid`= 74657;

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (-74662, -74651, 21305, 21501, 21499, 19755, 19740, 21500, 21314, 21302, 20683) AND `source_type` = 0 ;
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (21316*100, 21316*100+1) AND `source_type` = 9 ;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(21302,0,0,0,25,0,100,0,0,0,0,0,11,33346,0,0,0,0,0,19,21348,15,0,0,0,0,0,'Shadow Council Warlock - On Reset - Cast Green Beam'),
(21302,0,1,0,1,0,100,1,0,0,0,0,11,33346,0,0,0,0,0,19,21348,15,0,0,0,0,0,'Shadow Council Warlock- OOC - Cast Green Beam (No repeat)'),
(21302,0,2,0,0,0,100,0,1000,2000,5000,7000,11,9613,0,0,0,0,0,2,0,0,0,0,0,0,0,'Shadow Council Warlock- IC - Cast Shadow Bolt'),
(21302,0,3,0,0,0,100,0,6000,8000,10000,12000,11,37992,0,0,0,0,0,2,0,0,0,0,0,0,0,'Shadow Council Warlock- IC - Cast Drain Life'),
(19755,0,0,0,0,0,100,0,4000,5000,15000,17000,11,36253,0,0,0,0,0,2,0,0,0,0,0,0,0,'Mo''arg Weaponsmith- IC - Chemical Flames'),
(19755,0,1,0,0,0,100,0,6000,8000,10000,12000,11,37580,0,0,0,0,0,2,0,0,0,0,0,0,0,'Mo''arg Weaponsmith - IC - Drill Armor'),
(21499,0,0,0,0,0,100,0,4000,5000,15000,17000,11,35321,0,0,0,0,0,2,0,0,0,0,0,0,0,'Overseer Ripsaw - IC - Gushing Wound'),
(21499,0,1,0,0,0,100,0,6000,8000,9000,11000,11,32735,0,0,0,0,0,2,0,0,0,0,0,0,0,'Overseer Ripsaw - IC - Saw Blade'),
(21305,0,0,0,0,0,100,0,4000,5000,10000,12000,11,37950,0,0,0,0,0,2,0,0,0,0,0,0,0,'Mutant Horror - IC - Mutant Horror'),
(21305,0,1,0,0,0,100,0,15000,15000,15000,15000,11,8599,0,0,0,0,0,2,0,0,0,0,0,0,0,'Mutant Horror - IC - Enrage'),
(19740,0,0,0,0,0,100,0,1000,2000,9000,13000,11,33799,0,0,0,0,0,2,0,0,0,0,0,0,0,'wrathwalker - IC - Cast Flame Wave'),
(20683,0,0,0,0,0,100,0,1000,2000,15000,17000,11,37629,0,0,0,0,0,2,0,0,0,0,0,0,0,'Prophetess Cavrylin - IC - Cast Melt Flesh'),
(20683,0,1,0,0,0,100,0,6000,8000,10000,12000,11,37997,0,0,0,0,0,2,0,0,0,0,0,0,0,'Prophetess Cavrylin - IC - Cast Chaos Nova'),
(21314,0,0,0,0,0,100,0,1000,2000,4000,7000,11,15496,0,0,0,0,0,2,0,0,0,0,0,0,0,'Terrormaster - IC - Cast Cleave'),
(21314,0,1,0,0,0,100,0,9000,12000,20000,25000,11,38154,0,0,0,0,0,2,0,0,0,0,0,0,0,'Terrormaster - IC - Cast Fear'), 
(21500,0,0,0,0,0,100,0,1000,2000,6000,9000,11,22859,0,0,0,0,0,2,0,0,0,0,0,0,0,'Morgroron - IC - Cast Mortal Cleave'),
(21500,0,1,0,0,0,100,0,12000,15000,20000,25000,11,38741,0,0,0,0,0,2,0,0,0,0,0,0,0,'Morgroron - IC - Rain of Fire'),
(21500,0,2,0,0,0,100,0,5000,7000,10000,13000,11,38750,0,0,0,0,0,2,0,0,0,0,0,0,0,'Morgroron - IC - War Stomp'),
(21501,0,0,0,0,0,100,0,1000,2000,8000,12000,11,11443,0,0,0,0,0,2,0,0,0,0,0,0,0,'Makazradon - IC - Cripple'),
(21501,0,1,0,0,0,100,0,4000,5000,7000,10000,11,38742,0,0,0,0,0,2,0,0,0,0,0,0,0,'Makazradon - IC - Fel Cleave'),
(21501,0,2,0,0,0,100,0,12000,15000,20000,25000,11,38741,0,0,0,0,0,2,0,0,0,0,0,0,0,'Makazradon - IC - Rain of Fire'),
(-74662,0,0,0,1,0,100,0,120000,120000,240000,240000,80,21316*100,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - OOC - Action list'),
(-74651,0,0,0,1,0,100,0,180000,180000,300000,300000,80,21316*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - OOC - Action liist'),
(-74662,0,1,0,40,0,100,0,1,7466200,0,0,54,5000,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint1 - pause wp)'),
(-74651,0,1,0,40,0,100,0,1,7465100,0,0,54,5000,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint1 - pause wp)'),
(-74662,0,2,0,40,0,100,0,2,7466200,0,0,47,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint2 - Set visible off'),
(-74651,0,2,0,40,0,100,0,2,7465100,0,0,47,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint2 - Set visible off'),
(-74662,0,3,4,40,0,100,0,3,7466200,0,0,28,36658,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint3 - Remove aura)'),   
(-74662,0,4,5,61,0,100,0,0,0,0,0,47,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint3 - Set visible on'),
(-74662,0,5,6,61,0,100,0,0,0,0,0,66,0,0,0,0,0,0,8,0,0,0,0,0,0,6.265730,'Deathforged Infernal - On waypoint3 - Set orientation'),
(-74662,0,6,7,61,0,100,0,0,0,0,0,60,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint3 - Set fly Off'),
(-74662,0,7,0,61,0,100,0,0,0,0,0,11,16245,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint3 - Cast spell'),
(-74651,0,3,4,40,0,100,0,3,7465100,0,0,28,36658,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint1 - Remove aura)'), 
(-74651,0,4,5,61,0,100,0,0,0,0,0,47,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint3 - Set visible on'),
(-74651,0,5,6,61,0,100,0,0,0,0,0,66,0,0,0,0,0,0,8,0,0,0,0,0,0,4.468040,'Deathforged Infernal - On waypoint3 - Set orientation'),
(-74651,0,6,7,61,0,100,0,0,0,0,0,60,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint3 - Set fly Off'),
(-74651,0,7,0,61,0,100,0,0,0,0,0,11,16245,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - On waypoint3 - Cast spell'),
(21316*100,9,0,0,0,0,100,0,0,0,0,0,60,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Set fly off'),  
(21316*100,9,1,0,0,0,100,0,0,0,0,0,28,16245,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Remove aura'), 
(21316*100,9,2,0,0,0,100,0,0,0,0,0,53,1,7466200,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - start wp'),
(21316*100,9,3,0,0,0,100,0,13000,13000,0,0,86,33346,0,19,20683,15,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Cross cast'), 
(21316*100,9,4,0,0,0,100,0,3000,3000,0,0,86,36656,0,19,20683,15,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Cross cast'), 
(21316*100,9,5,0,0,0,100,0,0,0,0,0,11,36658,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Cast Transform'),
(21316*100,9,6,0,0,0,100,0,0,0,0,0,60,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Set fly On'), 
(21316*100+1,9,0,0,0,0,100,0,0,0,0,0,60,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Set fly off'),  
(21316*100+1,9,1,0,0,0,100,0,0,0,0,0,28,16245,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Remove aura'), 
(21316*100+1,9,2,0,0,0,100,0,0,0,0,0,53,1,7465100,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - start wp'),
(21316*100+1,9,3,0,0,0,100,0,12000,12000,0,0,86,33346,0,19,20683,15,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Cross cast'), 
(21316*100+1,9,4,0,0,0,100,0,3000,3000,0,0,86,36656,0,19,20683,15,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Cross cast'),
(21316*100+1,9,5,0,0,0,100,0,0,0,0,0,11,36658,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Cast Transform'),  
(21316*100+1,9,6,0,0,0,100,0,0,0,0,0,60,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Deathforged Infernal - Action list - Set fly On');

DELETE FROM `waypoints` WHERE `entry` IN(7466200, 7465100);
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES
(7466200, 1, -3411.152100, 2979.670410, 169.896851, 'Deathforged Infernal'),
(7466200, 2, -3411.152100, 2979.670410, 293.973755, 'Deathforged Infernal'),
(7466200, 3, -3441.462891, 2974.701172, 171.833115, 'Deathforged Infernal'),
(7465100, 1, -3411.152100, 2979.670410, 169.896851, 'Deathforged Infernal'),
(7465100, 2, -3411.152100, 2979.670410, 293.973755, 'Deathforged Infernal'),
(7465100, 3, -3408.362061, 3007.351807, 171.597610, 'Deathforged Infernal');


UPDATE `creature` SET `spawndist`=0,`MovementType`=2 WHERE `guid`=74602;
DELETE FROM `creature_addon` WHERE `guid`=74602;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (74602,746020,0,0,4097,0, '');
DELETE FROM `waypoint_data` WHERE `id`=746020;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(746020,1,-3374.98, 3001.77, 170.893,0,0,0,0,100,0),
(746020,2,-3423.57, 3005.07, 171.273,0,0,0,0,100,0);

UPDATE `creature` SET `spawndist`=0,`MovementType`=2 WHERE `guid`=74601;
DELETE FROM `creature_addon` WHERE `guid`=74601;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (74601,746010,0,0,4097,0, '');
DELETE FROM `waypoint_data` WHERE `id`=746010;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(746010, 1, -3438.26, 2988.15, 171.462, 0, 0, 0, 0, 100, 0),
(746010, 2, -3437.38, 2950.23, 171.240, 0, 0, 0, 0, 100, 0);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_03_00.sql 
-- -------------------------------------------------------- 
--
UPDATE `creature` SET `spawnMask`=1 WHERE `map`=532;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_03_01.sql 
-- -------------------------------------------------------- 
DELETE FROM `disables` WHERE `sourceType`=1 AND `entry` IN(7181,7202,7381,7382);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) VALUES 
(1, 7181, 0, '', '', 'Deprecated quest'),
(1, 7202, 0, '', '', 'Deprecated quest'),
(1, 7381, 0, '', '', 'Deprecated quest'),
(1, 7382, 0, '', '', 'Deprecated quest');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_03_02.sql 
-- -------------------------------------------------------- 
--
UPDATE `smart_scripts` SET `event_type`=23, `event_param1`=58627, `comment`="Burning Skeleton - On has aura - Cast 'Immolation'" WHERE `entryorguid`=31048 AND `source_type`=0 AND `id`=0;
UPDATE `smart_scripts` SET  `comment`="Burning Skeleton - On has aura - Cast 'Skeleton Check Master'" WHERE `entryorguid`=31048 AND `source_type`=0 AND `id`=1;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_03_03.sql 
-- -------------------------------------------------------- 
--
UPDATE `creature_template` SET `InhabitType`=4 WHERE `entry` IN (20142, 19918, 20069, 19959);
UPDATE `creature_template` SET `scale`=1.5 WHERE `Entry`=19959;
UPDATE `creature_addon` SET `bytes2`=4097 WHERE `guid` IN (23472, 23473);
UPDATE `creature_addon` SET `auras`="34712" WHERE `guid`=23078;
UPDATE `creature` SET `modelid`=0 WHERE `guid` = 23597;
UPDATE `creature` SET `modelid`=19278 WHERE `guid` = 23459;
UPDATE `creature` SET `modelid`=19282 WHERE `guid` = 23460;
UPDATE `creature` SET `modelid`=19280 WHERE `guid` = 23461;
UPDATE `creature` SET `modelid`=19281 WHERE `guid` = 23462;
DELETE FROM `creature_addon` WHERE `guid` IN (23441, 23428, 23431, 23430, 23434, 23427);
DELETE FROM `creature` WHERE `guid` IN (23428, 23441, 23431, 23430, 23434, 23427);
UPDATE `creature` SET `spawndist`=15 WHERE `id`=19918;

SET @Andormu:= 19932;
SET @Nozari:= 19933;

UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` IN (19918, @Andormu, @Nozari);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (-23440, -23439, -23438, -23437, -23436, -23433, -23432, -23429, -23426, @Nozari, @Nozari*100, 19918*100+1, 19918*100);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Nozari,0,0,0,1,0,100,0,60000,60000,900000,900000,80,@Nozari*100,0,0,0,0,0,1,0,0,0,0,0,0,0,"Nozari - OOC - Action list"),
(@Nozari*100,9,0,0,0,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,1,0,0,0,100,0,4000,4000,0,0,1,0,0,0,0,0,0,19,@Andormu,15,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,2,0,0,0,100,0,7000,7000,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,3,0,0,0,100,0,5000,5000,0,0,1,1,0,0,0,0,0,19,@Andormu,15,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,4,0,0,0,100,0,9000,9000,0,0,1,2,0,0,0,0,0,19,@Andormu,15,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,5,0,0,0,100,0,7000,7000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,6,0,0,0,100,0,10000,10000,0,0,1,3,0,0,0,0,0,19,@Andormu,15,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,7,0,0,0,100,0,8000,8000,0,0,1,4,0,0,0,0,0,19,@Andormu,15,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,8,0,0,0,100,0,7000,7000,0,0,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,9,0,0,0,100,0,5000,5000,0,0,1,5,0,0,0,0,0,19,@Andormu,15,0,0,0,0,0,"Nozari - Action list - Talk"),
(@Nozari*100,9,10,0,0,0,100,0,10000,10000,0,0,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,"Nozari - Action list - Talk"),
(-23440,0,0,0,1,0,100,0,1200000,1200000,1200000,1200000,53,0,2344000,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC  - Start wp"),
(-23440,0,1,0,40,0,100,0,2,0,0,0,11,34699,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23440,0,2,0,58,0,100,0,6,0,0,0,80,19918*100,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(19918*100,9,0,0,0,0,100,0,0,0,0,0,47,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - Action list - Set invisible"),
(19918*100,9,1,0,0,0,100,0,5000,5000,0,0,28,34699,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - Action list - remove aura"),
(19918*100,9,2,0,0,0,100,0,180000,180000,0,0,47,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - Action list - Set visible"),
(-23440,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On"),
(-23438,0,0,0,1,0,100,0,1800000,1800000,1800000,1800000,53,0,2344000,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC  - Start wp"),
(-23438,0,1,0,40,0,100,0,2,0,0,0,11,34699,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23438,0,2,0,58,0,100,0,6,0,0,0,80,19918*100,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(-23438,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On"),
(-23436,0,0,0,1,0,100,0,2200000,2200000,2200000,2200000,53,0,2344000,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC - Start wp"),
(-23436,0,1,0,40,0,100,0,2,0,0,0,11,34699,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23436,0,2,0,58,0,100,0,6,0,0,0,80,19918*100,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(-23436,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On"),
(-23433,0,0,0,1,0,100,0,2600000,2600000,2600000,2600000,53,0,2344000,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC - Start wp"),
(-23433,0,1,0,40,0,100,0,2,0,0,0,11,34699,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23433,0,2,0,58,0,100,0,6,0,0,0,80,19918*100,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(-23433,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On"),
(-23439,0,0,0,1,0,100,0,1200000,1200000,1200000,1200000,53,1,2344001,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC - Start wp"),
(-23439,0,1,0,40,0,100,0,2,0,0,0,11,34702,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23439,0,2,0,58,0,100,0,7,0,0,0,80,19918*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(19918*100+1,9,0,0,0,0,100,0,0,0,0,0,47,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - Action list - Set invisible"),
(19918*100+1,9,1,0,0,0,100,0,5000,5000,0,0,28,34702,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - Action list - remove aura"),
(19918*100+1,9,2,0,0,0,100,0,180000,180000,0,0,47,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - Action list - Set visible"),
(-23439,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On"),
(-23437,0,0,0,1,0,100,0,1800000,1800000,1800000,1800000,53,1,2344001,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC - Start wp"),
(-23437,0,1,0,40,0,100,0,2,0,0,0,11,34702,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23437,0,2,0,58,0,100,0,7,0,0,0,80,19918*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(-23437,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On"),
(-23432,0,0,0,1,0,100,0,2200000,2200000,2200000,2200000,53,1,2344001,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC - Start wp"),
(-23432,0,1,0,40,0,100,0,2,0,0,0,11,34702,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23432,0,2,0,58,0,100,0,7,0,0,0,80,19918*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(-23432,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On"),
(-23426,0,0,0,1,0,100,0,2600000,2600000,2600000,2600000,53,1,2344001,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC - Start wp"),
(-23426,0,1,0,40,0,100,0,2,0,0,0,11,34702,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23426,0,2,0,58,0,100,0,7,0,0,0,80,19918*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(-23426,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On"),
(-23429,0,0,0,1,0,100,0,3000000,3000000,3000000,3000000,53,1,2344001,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - OOC - Start wp"),
(-23429,0,1,0,40,0,100,0,2,0,0,0,11,34702,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp2 - Cast"),
(-23429,0,2,0,58,0,100,0,7,0,0,0,80,19918*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On wp Ended - Action list"),
(-23429,0,3,0,25,0,100,0,0,0,0,0,48,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Time Watcher - On Reset - Set Active On");

DELETE FROM `waypoints` WHERE `entry` IN(2344000, 2344001);
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES
(2344000, 1, -8450.692383, -4176.048340, -199.006653, 'Time Watcher'),
(2344000, 2, -8449.390625, -4160.058105, -209.984818, 'Time Watcher'),
(2344000, 3, -8448.969727, -4158.049805, -208.871887, 'Time Watcher'), 
(2344000, 4, -8436.923828, -4107.062012, -209.587540, 'Time Watcher'),
(2344000, 5, -8393.622070, -4071.729736, -209.587540, 'Time Watcher'),
(2344000, 6, -8349.183594, -4058.919189, -209.033203, 'Time Watcher'),
(2344001, 1, -8340.623047, -4344.326172, -199.383408, 'Time Watcher'), 
(2344001, 2, -8314.343750, -4348.677246, -209.548553, 'Time Watcher'), 
(2344001, 3, -8246.214844, -4344.385742, -205.160355, 'Time Watcher'),
(2344001, 4, -8207.163086, -4307.918945, -196.205338, 'Time Watcher'),
(2344001, 5, -8186.439941, -4259.733398, -183.948654, 'Time Watcher'),
(2344001, 6, -8188.971191, -4211.268555, -174.056534, 'Time Watcher'),
(2344001, 7, -8169.538574, -4167.970703, -165.149277, 'Time Watcher');

DELETE FROM `creature_text` WHERE `entry` IN(@Nozari,@Andormu);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`, `BroadcastTextID`) VALUES
(@Nozari, 0, 0, 'Fascinating. What is it, Andormu?', 12, 0, 100, 1, 0, 0,  'Nozari',17541),
(@Nozari, 1, 0, 'Hrm, are they tampering with the timeways?', 12, 0, 100, 1, 0, 0,  'Nozari',17543),
(@Nozari, 2, 0, 'I hate it when you speak to me in that manner. I am your equal in every way, brother. Remember that the next time you are trapped amidst a temporal vortex, crying for help.', 12, 0, 100, 1, 0, 0,  'Nozari',17546),
(@Nozari, 3, 0, 'Shall we?', 12, 0, 100, 1, 0, 0,  'Nozari',17549),
(@Nozari, 4, 0, '%s nods.', 16, 0, 100, 1, 0, 0,  'Nozari',17551),
(@Andormu, 0, 0, 'Something very, very disturbing. We''re starting to see more of them across the timeways.', 12, 0, 100, 1, 0, 0,  'Andormu',17542),
(@Andormu, 1, 0, 'Nozari, I''m not talking about a greedy wizard looking to improve his station in life by making a small adjustment to the past.', 12, 0, 100, 1, 0, 0,  'Andormu',17544),
(@Andormu, 2, 0, 'These beings aren''t giving themselves the winning numbers for the Stormwind lottery, dear.', 12, 0, 100, 1, 0, 0,  'Andormu',17545),
(@Andormu, 3, 0, 'My apologies, Nozari. I have been a bit ''on edge'' as of late. These creatures are attempting to alter the timeways.', 12, 0, 100, 1, 0, 0,  'Andormu',17547),
(@Andormu, 4, 0, 'You do recall the last time that this occurred, yes? Let us hope that the master need not be involved.', 12, 0, 100, 1, 0, 0,  'Andormu',17548),
(@Andormu, 5, 0, 'Absolutely not. We must not place ourselves at risk. With the master away, we are all that is in place to keep the stream intact. Others are due to arrive any moment now. Heroes and adventurers from Azeroth...', 12, 0, 100, 1, 0, 0,  'Andormu',17550);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_03_04.sql 
-- -------------------------------------------------------- 
--
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=17584;
DELETE FROM `smart_scripts` WHERE `entryorguid`=17584 AND `source_type`=0 AND `id`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(17584, 0, 0, 0, 19, 0, 100, 0, 9625, 0, 0, 0, 11, 30781, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Torallius the Pack Handler - On Quest Accept - Cast credit spell 30781');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_03_05.sql 
-- -------------------------------------------------------- 
--
UPDATE `quest_template_addon` SET `PrevQuestId`=24429 WHERE `Id` IN (7493,7497);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_03_06.sql 
-- -------------------------------------------------------- 
-- Baelmon the Hound-Master SAI
SET @ENTRY  := 19747;
SET @SPELL1 := 39218; -- Baelmon Channeling (no duration)
SET @SPELL2 := 11443; -- Cripple
SET @SPELL3 := 31598; -- Rain of Fire
SET @SPELL4 := 39272; -- Summon Wrath Hound
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=9 AND `entryorguid` IN (@ENTRY*100,@ENTRY*100+1,@ENTRY*100+2,@ENTRY*100+3,@ENTRY*100+4);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,11,0,100,0,0,0,0,0,53,0,@ENTRY,1,0,0,2,1,0,0,0,0,0,0,0,'Baelmon the Hound-Master - On Spawn - Load Path'),
(@ENTRY,0,1,2,40,0,100,0,3,@ENTRY,0,0,54,6000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 3 - pause path'),
(@ENTRY,0,2,0,61,0,100,0,0,0,0,0,80,@ENTRY*100,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 3 - run script'),
(@ENTRY,0,3,4,40,0,100,0,6,@ENTRY,0,0,54,21000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 6 - pause path'),
(@ENTRY,0,4,0,61,0,100,0,0,0,0,0,80,@ENTRY*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 6 - run script'),
(@ENTRY,0,5,6,40,0,100,0,7,@ENTRY,0,0,54,49000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 7 - pause path'),
(@ENTRY,0,6,0,61,0,100,0,0,0,0,0,80,@ENTRY*100+2,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 7 - run script'),
(@ENTRY,0,7,8,40,0,100,0,10,@ENTRY,0,0,54,8000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 10 - pause path'),
(@ENTRY,0,8,0,61,0,100,0,0,0,0,0,80,@ENTRY*100+3,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 10 - run script'),
(@ENTRY,0,9,10,40,0,100,0,11,@ENTRY,0,0,54,300000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 11 - pause path'),
(@ENTRY,0,10,0,61,0,100,0,0,0,0,0,80,@ENTRY*100+4,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Reach wp 11 - run script'),
(@ENTRY,0,11,0,0,0,100,0,5000,9000,18000,25000,11,@SPELL2,0,0,0,0,0,2,0,0,0,0,0,0,0,'Baelmon the Hound-Master - combat - cast spell'),
(@ENTRY,0,12,0,0,0,100,0,20000,25000,25000,30000,11,@SPELL3,0,0,0,0,0,2,0,0,0,0,0,0,0,'Baelmon the Hound-Master - combat - cast spell'),
(@ENTRY,0,13,14,0,0,100,0,35000,40000,40000,45000,11,@SPELL4,0,0,0,0,0,1,0,0,0,0,0,0,0,'Baelmon the Hound-Master - combat - cast spell'),
(@ENTRY,0,14,0,61,0,100,0,0,0,0,0,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,'Baelmon the Hound-Master - combat - Say4'),
(@ENTRY*100,9,0,0,0,0,100,0,2000,2000,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Script - say 0'),
(@ENTRY*100+1,9,0,0,0,0,100,0,1000,1000,0,0,66,0,0,0,0,0,0,8,0,0,0,0,0,0,4.485496, 'Baelmon the Hound-Master - Script - turn to'),
(@ENTRY*100+1,9,1,0,0,0,100,0,5000,5000,0,0,66,0,0,0,0,0,0,8,0,0,0,0,0,0,2.513274, 'Baelmon the Hound-Master - Script - turn to'),
(@ENTRY*100+1,9,2,0,0,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Script - say 1'),
(@ENTRY*100+1,9,3,0,0,0,100,0,0,0,0,0,12,21837,8,0,0,0,0,8,0,0,0,2282.145,5380.948,148.3864,3.060301, 'Baelmon the Hound-Master - Script - summon Summoned Wrath Hound'),
(@ENTRY*100+2,9,0,0,0,0,100,0,1000,1000,0,0,66,0,0,0,0,0,0,8,0,0,0,0,0,0,5.480334, 'Baelmon the Hound-Master - Script - turn to'),
(@ENTRY*100+2,9,1,0,0,0,100,0,2000,2000,0,0,11,@SPELL1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Script - cast spell'),
(@ENTRY*100+2,9,2,0,0,0,100,0,46000,46000,0,0,28,@SPELL1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Script - Stop Channeling'),
(@ENTRY*100+3,9,0,0,0,0,100,0,2000,2000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Script - say 2'),
(@ENTRY*100+4,9,0,0,0,0,100,0,1000,1000,0,0,66,0,0,0,0,0,0,8,0,0,0,0,0,0,5.480334, 'Baelmon the Hound-Master - Script - turn to'),
(@ENTRY*100+4,9,1,0,0,0,100,0,2000,2000,0,0,11,@SPELL1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Script - cast spell'),
(@ENTRY*100+4,9,2,0,0,0,100,0,296000,296000,0,0,28,@SPELL1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Baelmon the Hound-Master - Script - Stop Channeling'),
(@ENTRY,0,15,0,4,0,100,0,0,0,0,0,1, 3,0,0,0,0,0,2,0,0,0,0,0,0,0,'Summoned Wrath Hound - on aggro - Say3');

-- waypoints for Baelmon the Hound-Master
DELETE FROM `waypoints` WHERE `entry` IN (@ENTRY); 
INSERT INTO `waypoints` (`entry`,`pointid`,`position_x`,`position_y`,`position_z`,`point_comment`) VALUES
(@ENTRY,1,2251.221,5415.110,144.5944, 'Baelmon the Hound-Master'),
(@ENTRY,2,2253.267,5422.193,144.3444, 'Baelmon the Hound-Master'),
(@ENTRY,3,2251.017,5425.398,144.3444, 'Baelmon the Hound-Master'),
(@ENTRY,4,2250.168,5418.636,144.3444, 'Baelmon the Hound-Master'),
(@ENTRY,5,2263.631,5399.851,145.9176, 'Baelmon the Hound-Master'),
(@ENTRY,6,2273.678,5402.083,146.9626, 'Baelmon the Hound-Master'),
(@ENTRY,7,2270.231,5394.729,145.4702, 'Baelmon the Hound-Master'),
(@ENTRY,8,2264.036,5400.769,146.0911, 'Baelmon the Hound-Master'),
(@ENTRY,9,2234.945,5428.808,144.3444, 'Baelmon the Hound-Master'),
(@ENTRY,10,2223.490,5426.155,144.3497, 'Baelmon the Hound-Master'),
(@ENTRY,11,2270.231,5394.729,145.4702, 'Baelmon the Hound-Master');

-- NPC talk text insert from sniff 
DELETE FROM `creature_text` WHERE `entry`=@ENTRY; 
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`, `BroadcastTextID`) VALUES 
(@ENTRY,0,0, 'Make ready the chambers, another ally will soon join our ranks!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 19416),
(@ENTRY,1,0, 'Our ally has arrived!  Clear the way to the materialization chamber!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 19417),
(@ENTRY,2,0, 'Now, proceed to the translocator. Forge Camp Wrath awaits your arrival!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 19418),
(@ENTRY,3,0, 'Prepare yourself for eternal torture, mortal!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 20366),
(@ENTRY,3,1, 'WHAT?! Who dares to disturb the Burning Legion?',14,0,100,0,0,0, 'Baelmon the Hound-Master', 20365),
(@ENTRY,3,2, 'I shall enjoy the smell of the grease from your marrow crackling over the fire!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 20368),
(@ENTRY,3,3, 'Nothing will prevent your doom!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 20367),
(@ENTRY,3,4, 'You DARE to attack me?!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 20369),
(@ENTRY,3,5, 'You will suffer slowly until the end of time for this affront!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 20370),
(@ENTRY,4,0, 'Release the hounds!',14,0,100,0,0,0, 'Baelmon the Hound-Master', 20395);

UPDATE `creature` SET `spawndist`=10, `MovementType`=1 WHERE `guid` IN (84894, 84893, 84896, 84897, 84898, 84895, 84889, 84892, 84886, 84890, 84891, 84885, 84883, 84882, 84879, 84880, 84878, 84877, 84876, 84874, 84872, 84873, 84865, 84867, 84871);

-- Summoned Wrath Hound SAI
SET @ENTRY  := 21837;
SET @SPELL1 := 22578; -- Glowy (Black)
SET @SPELL2 := 37312; -- Portal Exit Effect
SET @SPELL3 := 22911; -- Charge
SET @SPELL4 := 36406; -- Bouble Breath
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN (@ENTRY, 22499);
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`IN (@ENTRY, 22499);
DELETE FROM `smart_scripts` WHERE `source_type`=9 AND `entryorguid` IN (@ENTRY*100,@ENTRY*100+1,@ENTRY*100+2);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,1,54,0,100,0,0,0,0,0,11,@SPELL1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Summoned Wrath Hound - On summon - cast aura'),
(@ENTRY,0,1,0,61,0,100,0,0,0,0,0,53,0,@ENTRY,0,0,0,2,1,0,0,0,0,0,0,0,'Summoned Wrath Hound - On summon - Load Path'),
(@ENTRY,0,2,3,40,0,100,0,1,@ENTRY,0,0,54,5000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Reach wp 1 - pause path'),
(@ENTRY,0,3,0,61,0,100,0,0,0,0,0,80,@ENTRY*100,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Reach wp 1 - run script'),
(@ENTRY,0,4,5,40,0,100,0,7,@ENTRY,0,0,54,4000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Reach wp 7 - pause path'),
(@ENTRY,0,5,0,61,0,100,0,0,0,0,0,80,@ENTRY*100+1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Reach wp 7 - run script'),
(@ENTRY,0,6,7,40,0,100,0,22,@ENTRY,0,0,54,4000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Reach wp 22 - pause path'),
(@ENTRY,0,7,0,61,0,100,0,0,0,0,0,80,@ENTRY*100+2,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Reach wp 22 - run script'),
(@ENTRY,0,8,0,4,0,100,0,0,0,0,0,11,@SPELL3,0,0,0,0,0,1,0,0,0,0,0,0,0,'Summoned Wrath Hound - on aggro - cast spell'),
(@ENTRY,0,9,0,0,0,100,0,4000,8000,6000,10000,11,@SPELL4,0,0,0,0,0,2,0,0,0,0,0,0,0,'Summoned Wrath Hound - combat - cast spell'),
(@ENTRY*100,9,0,0,0,0,100,0,4500,4500,0,0,11,@SPELL2,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Script - cast spell'),
(@ENTRY*100+1,9,0,0,0,0,100,0,500,500,0,0,19,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Script - Set unit flags'),
(@ENTRY*100+2,9,0,0,0,0,100,0,4500,4500,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Summoned Wrath Hound - Script - despawn'),
-- Lesser Wrath Hound SAI
(22499,0,0,0,0,0,100,0,4000,8000,6000,10000,11,36406,0,0,0,0,0,2,0,0,0,0,0,0,0,'Lesser Wrath Hound- combat - cast spell'),
(22499,0,1,0,54,0,100,0,0,0,0,0,49,0,0,0,0,0,0,21,30,0,0,0,0,0,0,'Lesser Wrath Hound- Just Summoned - Start Attack');

-- waypoints for Baelmon the Hound-Master
DELETE FROM `waypoints` WHERE `entry` IN (@ENTRY); 
INSERT INTO `waypoints` (`entry`,`pointid`,`position_x`,`position_y`,`position_z`,`point_comment`) VALUES
(@ENTRY,1,2268.924,5393.452,145.4471, 'Summoned Wrath Hound'),
(@ENTRY,2,2255.472,5407.068,145.0279, 'Summoned Wrath Hound'),
(@ENTRY,3,2243.098,5420.322,144.3444, 'Summoned Wrath Hound'),
(@ENTRY,4,2226.786,5438.047,144.3497, 'Summoned Wrath Hound'),
(@ENTRY,5,2211.937,5460.007,150.8571, 'Summoned Wrath Hound'),
(@ENTRY,6,2195.829,5463.447,153.6814, 'Summoned Wrath Hound'),
(@ENTRY,7,2188.094,5477.495,155.0914, 'Summoned Wrath Hound'),
(@ENTRY,8,2195.297,5464.697,153.6814, 'Summoned Wrath Hound'),
(@ENTRY,9,2208.225,5464.172,153.5997, 'Summoned Wrath Hound'),
(@ENTRY,10,2219.882,5446.347,144.3497, 'Summoned Wrath Hound'),
(@ENTRY,11,2210.560,5435.390,144.6450, 'Summoned Wrath Hound'),
(@ENTRY,12,2170.015,5423.634,144.2937, 'Summoned Wrath Hound'),
(@ENTRY,13,2142.269,5430.440,144.8000, 'Summoned Wrath Hound'),
(@ENTRY,14,2100.592,5447.438,145.3132, 'Summoned Wrath Hound'),
(@ENTRY,15,2046.968,5449.164,145.0331, 'Summoned Wrath Hound'),
(@ENTRY,16,2031.698,5416.583,144.7770, 'Summoned Wrath Hound'),
(@ENTRY,17,2009.996,5392.866,145.6075, 'Summoned Wrath Hound'),
(@ENTRY,18,1951.617,5362.657,148.1501, 'Summoned Wrath Hound'),
(@ENTRY,19,1958.205,5340.299,153.2451, 'Summoned Wrath Hound'),
(@ENTRY,20,1950.292,5312.918,154.0889, 'Summoned Wrath Hound'),
(@ENTRY,21,1960.221,5301.518,154.0889, 'Summoned Wrath Hound'),
(@ENTRY,22,1980.815,5314.191,156.4767, 'Summoned Wrath Hound');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=39218;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 4, 39218, 0, 31, 3, 20736, 0, 0, '', 'Baelmon Channeling target');

-- Blade's Edge - Legion - Invis Bunny update
UPDATE `creature_template` SET `InhabitType`=4,`flags_extra`=`flags_extra`|128 WHERE `entry`=20736;
-- Remove spawn
DELETE FROM `creature` WHERE `guid`=76416;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_04_00.sql 
-- -------------------------------------------------------- 
-- Lil Timmy c.8666 - Stormwind
-- correct spawn point, spawntime, waypoints.
SET @GUID:= 23427; -- 1 free guid set by tc
DELETE FROM `creature_formations` WHERE `leaderGUID`=45501;
INSERT INTO `creature_formations` (`leaderGUID`,`memberGUID`,`dist`,`angle`,`groupAI`) VALUES
(45501,45501,0,0,2),
(45501,@GUID,2,0,2);

-- White Kitten - missing added
DELETE FROM `creature` WHERE `guid`=@GUID; 
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `PhaseId`, `PhaseGroup`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `curhealth`, `spawndist`, `MovementType`) VALUES
(@GUID, 7386, 0, 1, 1, 1, -8632.046875, 921.279480, 99.382813, 3.897803, 180, 42, 0, 0);

UPDATE `creature` SET `position_x` =-8634.505859, `position_y` = 918.960571, `position_z` = 99.354980, `orientation` = 3.832987, `MovementType` = 2, `spawntimesecs` = 5400 WHERE `guid` = 45501;
UPDATE `creature` SET `position_x` =-8583.879883 , `position_y` =633.127014 , `position_z` =96.338028 , `orientation` =4.989326  WHERE `guid` = 79816;
UPDATE `creature` SET `position_x` =-8582.030273 , `position_y` =633.633972 , `position_z` =96.338028 , `orientation` =4.989326  WHERE `guid` = 79815;
UPDATE `creature` SET `position_x` =-8580.509766 , `position_y` =635.107971 , `position_z` =96.338028 , `orientation` =4.989326  WHERE `guid` = 79817;

UPDATE `creature_template` SET `MovementType` = 2 WHERE `entry` = 8666;
DELETE FROM `creature_addon` WHERE `guid`=45501;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(45501,455010,0,0,1,0,'');

DELETE FROM `waypoint_data` WHERE `id`=455010;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(455010,1,-8641.4,912.342,99.1397,0,0,0,0,100,0),
(455010,2,-8661.71,894.74,97.6239,0,0,0,0,100,0),
(455010,3,-8679.15,880.967,97.0168,0,0,0,0,100,0),
(455010,4,-8681.12,877.654,97.0168,0,0,0,0,100,0),
(455010,5,-8679.29,873.082,97.0168,0,0,0,0,100,0),
(455010,6,-8659.98,849.329,97.0168,0,0,0,0,100,0),
(455010,7,-8639.96,825.073,96.6251,0,0,0,0,100,0),
(455010,8,-8636.74,813.025,96.6486,0,0,0,0,100,0),
(455010,9,-8634,793.001,96.6508,0,0,0,0,100,0),
(455010,10,-8635.94,785.58,96.6515,0,0,0,0,100,0),
(455010,11,-8651.43,775.162,96.6714,0,0,0,0,100,0),
(455010,12,-8661.39,764.974,96.6998,0,0,0,0,100,0),
(455010,13,-8662.58,758.134,96.6947,0,0,0,0,100,0),
(455010,14,-8647.73,738.576,96.6965,0,0,0,0,100,0),
(455010,15,-8630.74,726.606,96.7377,0,0,0,0,100,0),
(455010,16,-8618.88,711.997,96.7248,0,0,0,0,100,0),
(455010,17,-8614.67,709.545,96.7549,0,0,0,0,100,0),
(455010,18,-8606.13,711.345,96.7382,0,0,0,0,100,0),
(455010,19,-8598.07,712.945,96.6746,0,0,0,0,100,0),
(455010,20,-8588.25,706.887,97.0168,0,0,0,0,100,0),
(455010,21,-8566.09,678.512,97.0168,0,0,0,0,100,0),
(455010,22,-8561.86,674.735,97.0168,0,0,0,0,100,0),
(455010,23,-8556.46,676.784,97.0168,0,0,0,0,100,0),
(455010,24,-8542.79,686.774,97.6239,0,0,0,0,100,0),
(455010,25,-8536.45,686.854,97.6775,0,0,0,0,100,0),
(455010,26,-8531.64,683.194,98.4422,0,0,0,0,100,0),
(455010,27,-8524.58,673.178,102.5,0,0,0,0,100,0),
(455010,28,-8519.8,666.4,102.615,0,0,0,0,100,0),
(455010,29,-8512.94,656.648,100.901,0,0,0,0,100,0),
(455010,30,-8513.15,648.714,100.292,0,0,0,0,100,0),
(455010,31,-8518.18,642.361,100.092,0,0,0,0,100,0),
(455010,32,-8538.04,630.723,100.404,0,0,0,0,100,0),
(455010,33,-8554.03,617.81,102.053,0,0,0,0,100,0),
(455010,34,-8564.5,613.48,102.435,0,0,0,0,100,0),
(455010,35,-8576.12,601.799,103.26,0,0,0,0,100,0),
(455010,36,-8582.44,589.572,103.691,0,0,0,0,100,0),
(455010,37,-8586.68,575.605,102.985,0,0,0,0,100,0),
(455010,38,-8585.96,565.941,102.26,0,0,0,0,100,0),
(455010,39,-8578.9,545.988,101.779,0,0,0,0,100,0),
(455010,40,-8581.73,541.012,102.09,0,0,0,0,100,0),
(455010,41,-8590.09,533.912,104.76,0,0,0,0,100,0),
(455010,42,-8598.32,527.164,106.399,0,0,0,0,100,0),
(455010,43,-8605.67,520.882,105.748,0,0,0,0,100,0),
(455010,44,-8610.26,515.735,103.79,0,0,0,0,100,0),
(455010,45,-8613.43,514.684,103.401,0,0,0,0,100,0),
(455010,46,-8618.8,518.794,103.068,0,0,0,0,100,0),
(455010,47,-8635.17,535.152,99.9833,0,0,0,0,100,0),
(455010,48,-8647.39,546.721,97.8568,0,0,0,0,100,0),
(455010,49,-8655.78,552.938,96.9435,0,0,0,0,100,0),
(455010,50,-8671.86,552.874,97.2037,0,0,0,0,100,0),
(455010,51,-8679.66,549.654,97.5031,0,0,0,0,100,0),
(455010,52,-8689.63,540.268,97.828,0,0,0,0,100,0),
(455010,53,-8698.98,530.295,97.7173,0,0,0,0,100,0),
(455010,54,-8712.64,520.242,97.2398,0,0,0,0,100,0),
(455010,55,-8715.24,521.571,97.4039,0,0,0,0,100,0),
(455010,56,-8720.77,528.729,99.1496,0,0,0,0,100,0),
(455010,57,-8729.84,539.87,101.105,0,0,0,0,100,0),
(455010,58,-8735.95,547.101,100.845,0,0,0,0,100,0),
(455010,59,-8745.79,557.737,97.7107,0,0,0,0,100,0),
(455010,60,-8746.01,564.915,97.4001,0,0,0,0,100,0),
(455010,61,-8729.92,581.294,97.6775,0,0,0,0,100,0),
(455010,62,-8719.58,591.033,98.4713,0,0,0,0,100,0),
(455010,63,-8712.04,594.001,98.6079,0,0,0,0,100,0),
(455010,64,-8707.26,600.676,98.9982,0,0,0,0,100,0),
(455010,65,-8704.46,616.407,100.215,0,0,0,0,100,0),
(455010,66,-8705.6,629.078,100.477,0,0,0,0,100,0),
(455010,67,-8708.67,645.787,99.9994,0,0,0,0,100,0),
(455010,68,-8716.46,666.585,98.8681,0,0,0,0,100,0),
(455010,69,-8724.09,676.482,98.6317,0,0,0,0,100,0),
(455010,70,-8728.54,684.167,98.7324,0,0,0,0,100,0),
(455010,71,-8733.47,695.151,98.723,0,0,0,0,100,0),
(455010,72,-8743.6,709.876,98.2678,0,0,0,0,100,0),
(455010,73,-8741.08,714.561,98.9815,0,0,0,0,100,0),
(455010,74,-8734.46,720.119,101.647,0,0,0,0,100,0),
(455010,75,-8726.79,726.231,100.924,0,0,0,0,100,0),
(455010,76,-8718.09,733.687,97.9511,0,0,0,0,100,0),
(455010,77,-8716.42,737.269,97.7782,0,0,0,0,100,0),
(455010,78,-8721.01,746.752,97.9693,0,0,0,0,100,0),
(455010,79,-8730.96,759.107,98.1572,0,0,0,0,100,0),
(455010,80,-8731.99,769.385,98.0161,0,0,0,0,100,0),
(455010,81,-8724.64,778.108,98.0604,0,0,0,0,100,0),
(455010,82,-8717.55,792.762,97.1197,0,0,0,0,100,0),
(455010,83,-8717.68,798.804,97.1792,0,0,0,0,100,0),
(455010,84,-8728.3,817.744,96.9777,0,0,0,0,100,0),
(455010,85,-8726.79,830.504,96.3102,0,0,0,0,100,0),
(455010,86,-8723.42,841.35,96.0764,0,0,0,0,100,0),
(455010,87,-8709.57,857.779,96.978,0,0,0,0,100,0),
(455010,88,-8692.38,870.557,97.0284,0,0,0,0,100,0),
(455010,89,-8679.35,880.974,97.0167,0,0,0,0,100,0),
(455010,90,-8661.22,896.239,97.5968,0,0,0,0,100,0),
(455010,91,-8643.7,912.233,98.9288,0,0,0,0,100,0),
(455010,92,-8634.58,918.926,99.3551,0,0,0,0,100,0);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_04_01.sql 
-- -------------------------------------------------------- 
SET @OGUID       := 29742;
SET @CGUID       := 143505;

UPDATE `gameobject_template` SET `flags`=32 WHERE  `entry`IN (177257,177258,177259,179504,179505,179503,179506);

DELETE FROM `gameobject` where `id` IN (177257,177258,177259,179504,179505,179503,179506,179563,177219,179563,177219,177221,177211,177212,177215,179549,177217)  AND  `map`=429;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `PhaseId`, `PhaseGroup`,  `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) VALUES
(@OGUID+0, 177258, 429, 3,  1, 1,  121.2223, 429.0921, 28.45481, 0.4188786, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 177258 (Area: 0)
(@OGUID+1, 177259, 429, 3,  1, 1,  12.94134, 277.9307, -8.932899, 0, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 177259 (Area: 5915)
(@OGUID+2, 177257, 429, 3,  1, 1,  -92.34557, 442.6703, 28.54704, 0.4188786, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 177257 (Area: 5915)
(@OGUID+3, 179504, 429, 3,  1, 1,  78.13875, 737.4021, -24.62163, 0, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 179504 (Area: 5915)
(@OGUID+4, 179505, 429, 3,  1, 1,  -155.4332, 734.1661, -24.62163, 0, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 179505 (Area: 5915)
(@OGUID+5, 179503, 429, 3,  1, 1, -38.96511, 813.7094, -27.31784, 0, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 179503 (Area: 5913)
(@OGUID+6, 179506, 429, 3,  1, 1, -38.75632, 812.5867, -3.8761, 0, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 179506 (Area: 5913)
(@OGUID+7, 179563, 429, 3,  1, 1, 116.1046, 638.89, -48.46696, 4.520406, 0, 0, 0, 1, 7200, 255, 1), -- 179563 (Area: 5915)
(@OGUID+8, 177219, 429, 3,  1, 1, 385.3268, 374.2315, -1.34314, 1.570796, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 177219 (Area: 5913)
(@OGUID+9, 177221, 429, 3,  1, 1, 50.58631, 501.9406, -23.14985, 4.71239, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 177221 (Area: 5913)
(@OGUID+10, 177211, 429, 3, 1,  1, -41.8254, 159.8736, -3.448337, 0, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 177211 (Area: 5913)
(@OGUID+11, 177212, 429, 3, 1,  1, 10.72159, 159.4589, -3.448351, 3.141593, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 177212 (Area: 5913)
(@OGUID+12, 177215, 429, 3, 1,  1, 255.4084, 10.3791, -2.693807, 1.570796, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 177215 (Area: 5913)
(@OGUID+13, 179549, 429, 3, 1,  1, 351.5679, 88.67347, -36.39297, 1.570796, 0, 0, 1, -4.371139E-08, 7200, 255, 1), -- 179549 (Area: 5913)
(@OGUID+14, 177217, 429, 3, 1,  1, 491.2043, 515.1331, 29.46753, 1.570796, 0, 0, 1, -4.371139E-08, 7200, 255, 1); -- 177217 (Area: 5913)

DELETE FROM `creature` WHERE `id` IN(11480,11483,13916,13160);

INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `PhaseId`, `PhaseGroup`,  `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `MovementType`) VALUES
(@CGUID+0, 11480, 429, 3,  1, 1, 130.4702, 422.0486, 28.68438, 5.5676, 7200, 0, 0), -- 11480 (Area: 0)
(@CGUID+1, 11480, 429, 3,  1, 1, 129.8357, 437.4243, 28.68438, 0.6457718, 7200, 0, 0), -- 11480 (Area: 0)
(@CGUID+2, 11483, 429, 3,  1, 1, 112.3598, 421.3277, 28.68438, 3.979351, 7200, 0, 0), -- 11483 (Area: 0)
(@CGUID+3, 11483, 429, 3,  1, 1, 112.2285, 437.3398, 28.68438, 2.495821, 7200, 0, 0), -- 11483 (Area: 0)
(@CGUID+4, 11483, 429, 3,  1, 1, 24.50517, 278.4114, -8.422497, 0.8726646, 7200, 0, 0), -- 11483 (Area: 5915)
(@CGUID+5, 11480, 429, 3,  1, 1, 3.030073, 275.064, -8.27089, 3.036873, 7200, 0, 0), -- 11480 (Area: 5915)
(@CGUID+6, 11483, 429, 3,  1, 1, 18.33997, 262.5933, -7.229755, 5.044002, 7200, 0, 0), -- 11483 (Area: 5915)
(@CGUID+7, 11480, 429, 3,  1, 1, -99.35272, 434.2773, 28.68608, 3.926991, 7200, 0, 0), -- 11480 (Area: 5915)
(@CGUID+8, 11483, 429, 3,  1, 1, -83.95709, 435.4832, 28.68626, 5.61996, 7200, 0, 0), -- 11483 (Area: 5915)
(@CGUID+9, 11480, 429, 3,  1, 1, -100.4601, 449.052, 28.6875, 2.321288, 7200, 0, 0), -- 11480 (Area: 5915)
(@CGUID+10, 11483, 429, 3, 1, 1, -83.53941, 449.507, 28.6862, 0.9599311, 7200, 5, 1), -- 11483 (Area: 5915) (possible waypoints or random movement)
(@CGUID+11, 11483, 429, 3, 1, 1, 90.66621, 740.4608, -24.49702, 0.3490658, 7200, 0, 0), -- 11483 (Area: 5915)
(@CGUID+12, 11480, 429, 3, 1, 1, 75.12427, 748.4026, -24.49702, 1.553343, 7200, 0, 0), -- 11480 (Area: 5915)
(@CGUID+13, 11483, 429, 3, 1, 1, 81.96804, 725.6087, -24.49635, 5.166174, 7200, 5, 1), -- 11483 (Area: 5915) (Auras: ) (possible waypoints or random movement)
(@CGUID+14, 11480, 429, 3, 1, 1, 67.98151, 733.5004, -24.49676, 3.665191, 7200, 0, 0), -- 11480 (Area: 5915)
(@CGUID+15, 11480, 429, 3, 1, 1, -151.3329, 721.8649, -24.49508, 4.904375, 7200, 0, 0), -- 11480 (Area: 5915)
(@CGUID+16, 11483, 429, 3, 1, 1, -142.6175, 736.7474, -24.49702, 6.108652, 7200, 0, 0), -- 11483 (Area: 5915)
(@CGUID+17, 11483, 429, 3, 1, 1, -166.1358, 729.3408, -24.49702, 3.525565, 7200, 0, 0), -- 11483 (Area: 5915)
(@CGUID+18, 11480, 429, 3, 1, 1, -157.8231, 746.3584, -24.49702, 1.623156, 7200, 0, 0),-- 11480 (Area: 5915)
(@CGUID+19, 11480, 429, 3, 1, 1, 10.1239, 287.949, -8.78406, 1.62106, 7200, 0, 0),-- 11480 (Area: 5915)
(@CGUID+20, 13916, 429, 3, 1, 1, 13.45142, 277.6283, 1.514334, 2.286381, 7200, 0, 0), -- 13916 (Area: 5915)
(@CGUID+21, 13160, 429, 3, 1, 1, 462.8175, 251.535, 8.817377, 4.555309, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+22, 13160, 429, 3, 1, 1, 452.7207, 247.395, 11.30013, 4.642576, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+23, 13160, 429, 3, 1, 1, 448.1017, 251.626, 11.29641, 4.677482, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+24, 13160, 429, 3, 1, 1, 455.3728, 253.9155, 11.3039, 4.625123, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+25, 13160, 429, 3, 1, 1, 448.4704, 258.7166, 11.29889, 4.677482, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+26, 13160, 429, 3, 1, 1, 449.7683, 243.4786, 11.29772, 4.660029, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+27, 13160, 429, 3, 1, 1, 452.4716, 260.4895, 11.30317, 4.642576, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+28, 13160, 429, 3, 1, 1, 477.2956, 252.6243, 2.873334, 2.817438, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+29, 13160, 429, 3, 1, 1, 462.7784, 260.8503, 8.751555, 2.073882, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+30, 13160, 429, 3, 1, 1, 471.8947, 252.9371, 4.882514, 4.475661, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+31, 13160, 429, 3, 1, 1, 486.488, 313.6001, 2.936218, 4.485496, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+32, 13160, 429, 3, 1, 1, 494.2127, 319.3602, 2.936227, 4.45059, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+33, 13160, 429, 3, 1, 1, 474.5892, 316.7031, 2.936239, 4.537856, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+34, 13160, 429, 3, 1, 1, 478.2815, 324.038, 2.936251, 4.537856, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+35, 13160, 429, 3, 1, 1, 493.1786, 309.8522, 2.936205, 4.433136, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+36, 13160, 429, 3, 1, 1, 467.4713, 316.5861, 2.93625, 4.590216, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+37, 13160, 429, 3, 1, 1, 471.9471, 324.6326, 2.936259, 4.572762, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+38, 13160, 429, 3, 1, 1, 485.187, 320.7115, 2.936234, 4.485496, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+39, 13160, 429, 3, 1, 1, 490.0086, 329.4991, 2.852889, 1.889038, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+40, 13160, 429, 3, 1, 1, 484.4862, 327.5917, 2.85289, 3.664191, 7200, 5, 1), -- 13160 (Area: 0) (possible waypoints or random movement)
(@CGUID+41, 13160, 429, 3, 1, 1, 479.4313, 534.1829, -25.30033, 4.625123, 7200, 0, 0), -- 13160 (Area: 0) (Auras: )
(@CGUID+42, 13160, 429, 3, 1, 1, 494.0698, 531.7256, -25.30895, 4.590216, 7200, 0, 0), -- 13160 (Area: 0) (Auras: )
(@CGUID+43, 13160, 429, 3, 1, 1, 480.5341, 542.3371, -25.3806, 6.065704, 7200, 0, 0), -- 13160 (Area: 0) (Auras: )
(@CGUID+44, 13160, 429, 3, 1, 1, 518.8744, 532.2348, -25.39863, 2.637258, 7200, 0, 0), -- 13160 (Area: 0) (Auras: )
(@CGUID+45, 13160, 429, 3, 1, 1, 521.4735, 543.2216, -25.39985, 2.595782, 7200, 0, 0), -- 13160 (Area: 0) (Auras: )
(@CGUID+46, 13160, 429, 3, 1, 1, 532.2884, 535.4186, -25.40494, 0.08802468, 7200, 0, 0), -- 13160 (Area: 0) (Auras: )
(@CGUID+47, 13160, 429, 3, 1, 1, 507.4343, 541.1744, -25.39325, 4.851843, 7200, 0, 0), -- 13160 (Area: 0) (Auras: )
(@CGUID+48, 13160, 429, 3, 1, 1, 481.1589, 552.1213, -25.3825, 0.2103749, 7200, 0, 0), -- 13160 (Area: 0) (Auras: )
(@CGUID+49, 13160, 429, 3, 1, 1, 511.5812, 556.6573, -25.39854, 3.27448, 7200, 5, 1), -- 13160 (Area: 0) (Auras: ) (possible waypoints or random movement)
(@CGUID+50, 13160, 429, 3, 1, 1, 532.3871, 552.1505, -25.40498, 2.640144, 7200, 5, 1), -- 13160 (Area: 0) (Auras: ) (possible waypoints or random movement)
(@CGUID+51, 13160, 429, 3, 1, 1, 472.9739, -32.42187, -3.903387, 6.217165, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+52, 13160, 429, 3, 1, 1, 460.5289, -24.7918, -3.886683, 1.132865, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+53, 13160, 429, 3, 1, 1, 472.9483, -21.3704, -3.911369, 2.103442, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+54, 13160, 429, 3, 1, 1, 460.5975, -39.44626, -3.886727, 5.271253, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+55, 13160, 429, 3, 1, 1, 471.0815, -48.29583, -3.886039, 1.733511, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+56, 13160, 429, 3, 1, 1, 456.1195, -58.02909, -4.279171, 3.795121, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+57, 13160, 429, 3, 1, 1, 470.6244, -60.59518, -3.886099, 0.863512, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+58, 13160, 429, 3, 1, 1, 463.9873, -52.75054, -3.886564, 2.476561, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+59, 13160, 429, 3, 1, 1, 460.2359, -68.97313, -3.886916, 3.776749, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+60, 13160, 429, 3, 1, 1, 469.8415, -70.88819, -3.886191, 4.886696, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+61, 13160, 429, 3, 1, 1, 396.9608, -98.32198, -3.887123, 2.894927, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+62, 13160, 429, 3, 1, 1, 404.5128, -110.0931, -3.886247, 0.1595933, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+63, 13160, 429, 3, 1, 1, 445.6248, -113.9238, -3.886032, 5.102283, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+64, 13160, 429, 3, 1, 1, 414.6084, -108.6204, -3.886377, 0.0039941, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+65, 13160, 429, 3, 1, 1, 443.4119, -99.60896, -3.887109, 2.986769, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+66, 13160, 429, 3, 1, 1, 401.1027, -104.8092, -3.88664, 0.2074864, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+67, 13160, 429, 3, 1, 1, 438.3328, -108.5529, -3.886425, 5.919142, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+68, 13160, 429, 3, 1, 1, 426.5037, -100.5517, -3.887007, 2.909149, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+69, 13160, 429, 3, 1, 1, 414.977, -98.74563, -3.887123, 2.845945, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+70, 13160, 429, 3, 1, 1, 424.4419, -108.6975, -3.886389, 1.076339, 7200, 0, 0), -- 13160 (Area: 0)
(@CGUID+71, 13160, 429, 3, 1, 1, 396.8296, -98.32212, -3.803594, 1.37881, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+72, 13160, 429, 3, 1, 1, 414.977, -98.74563, -3.803658, 1.448623, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+73, 13160, 429, 3, 1, 1, 400.0482, -105.0312, -3.803111, 1.396263, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+74, 13160, 429, 3, 1, 1, 437.5299, -108.435, -3.803006, 1.553343, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+75, 13160, 429, 3, 1, 1, 424.8745, -110.3578, -3.802841, 1.500983, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+76, 13160, 429, 3, 1, 1, 460.3629, -24.53695, -3.803384, 1.658063, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+77, 13160, 429, 3, 1, 1, 403.6488, -110.2321, -3.802753, 1.413717, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+78, 13160, 429, 3, 1, 1, 427.6517, -100.8249, -3.803568, 1.500983, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+79, 13160, 429, 3, 1, 1, 414.4864, -108.6242, -3.802916, 1.448623, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+80, 13160, 429, 3, 1, 1, 456.6826, -57.28893, -4.25341, 1.623156, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+81, 13160, 429, 3, 1, 1, 471.2401, -46.80048, -3.802765, 1.710423, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+82, 13160, 429, 3, 1, 1, 473.4761, -22.86045, -3.80176, 1.745329, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+83, 13160, 429, 3, 1, 1, 470.647, -70.00087, -3.802982, 1.692969, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+84, 13160, 429, 3, 1, 1, 472.6571, -33.71276, -3.802353, 1.727876, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+85, 13160, 429, 3, 1, 1, 461.8682, -40.83654, -3.803358, 1.658063, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+86, 13160, 429, 3, 1, 1, 444.67, -99.75545, -3.803718, 1.570796, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+87, 13160, 429, 3, 1, 1, 460.2359, -68.97313, -3.803645, 1.64061, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+88, 13160, 429, 3, 1, 1, 464.6496, -53.98102, -3.803182, 1.658063, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+89, 13160, 429, 3, 1, 1, 445.7453, -113.9387, -3.802556, 1.570796, 7200, 5, 1), -- 13160 (Area: 5913) (possible waypoints or random movement)
(@CGUID+90, 13160, 429, 3, 1, 1, 470.4993, -60.37534, -3.802882, 1.692969, 7200, 5, 1); -- 13160 (Area: 5913) (possible waypoints or random movement)

 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_04_02.sql 
-- -------------------------------------------------------- 
--
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=16 AND `SourceEntry`=26813;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(16,0,26813,0,0,23,0,4163,0,0,0,0,'','Dismount player from Kor''kron War Rider when not in intended zone(Icemist Village)');

UPDATE `creature_template` SET `InhabitType`=5, `npcflag`=16777216 WHERE `entry`=26813;
DELETE FROM `creature_template_addon` WHERE `entry`=26813;
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES 
(26813, 0, 0, 33554432, 0, 0, '55971');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_04_03.sql 
-- -------------------------------------------------------- 
-- Pathing for  Entry: Entry: 21801 (Vhel'kur)
SET @NPC := 21801;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5140.628,`position_y`=537.6068,`position_z`=225.5867 WHERE `guid`=76114;
UPDATE `creature_template` SET  `InhabitType`=4 WHERE  `entry`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=76114;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(76114,@PATH,0,33554432,1,0, '55971');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5140.628,537.6068,225.5867,0,0,0,0,100,0),
(@PATH,2,-5047.682,660.9525,148.8644,0,0,0,0,100,0),
(@PATH,3,-5011.98,674.3496,148.8644,0,0,0,0,100,0),
(@PATH,4,-4981.461,669.827,148.8644,0,0,0,0,100,0),
(@PATH,5,-4959.014,632.2751,148.8644,0,0,0,0,100,0),
(@PATH,6,-4943.981,608.1472,148.8644,0,0,0,0,100,0),
(@PATH,7,-4929.495,593.4562,148.8644,0,0,0,0,100,0),
(@PATH,8,-4910.322,557.0013,148.8644,0,0,0,0,100,0),
(@PATH,9,-4886.5,522.941,148.8644,0,0,0,0,100,0),
(@PATH,10,-4860.481,492.9438,148.8644,0,0,0,0,100,0),
(@PATH,11,-4853.529,472.7724,148.8644,0,0,0,0,100,0),
(@PATH,12,-4853.905,449.3954,148.8644,0,0,0,0,100,0),
(@PATH,13,-4862.35,417.3469,148.8644,0,0,0,0,100,0),
(@PATH,14,-4904.073,347.1447,148.8644,0,0,0,0,100,0),
(@PATH,15,-4938.952,289.3222,148.8644,0,0,0,0,100,0),
(@PATH,16,-4959.224,247.5302,148.8644,0,0,0,0,100,0),
(@PATH,17,-4971.73,201.8298,148.8644,0,0,0,0,100,0),
(@PATH,18,-4979.256,156.9421,148.8644,0,0,0,0,100,0),
(@PATH,19,-4987.867,132.5271,148.8644,0,0,0,0,100,0),
(@PATH,20,-4998.63,107.5142,148.8644,0,0,0,0,100,0),
(@PATH,21,-5020.076,73.21224,148.8644,0,0,0,0,100,0),
(@PATH,22,-5061.159,55.51009,148.8644,0,0,0,0,100,0),
(@PATH,23,-5077.839,49.82715,148.8644,0,0,0,0,100,0),
(@PATH,24,-5112.819,49.43273,148.8644,0,0,0,0,100,0),
(@PATH,25,-5175.664,65.2347,163.3922,0,0,0,0,100,0),
(@PATH,26,-5198.992,107.9043,173.1977,0,0,0,0,100,0),
(@PATH,27,-5165.273,219.0713,198.2255,0,0,0,0,100,0),
(@PATH,28,-5113.013,300.7207,198.2255,0,0,0,0,100,0),
(@PATH,29,-5109.759,353.3949,226.7533,0,0,0,0,100,0),
(@PATH,30,-5113.028,378.9975,240.6144,0,0,0,0,100,0),
(@PATH,31,-5122.197,411.4973,243.2811,0,0,0,0,100,0);
-- 0x1C39A84240154A4000002100000AE735 .go -5140.628 537.6068 225.5867
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_04_04.sql 
-- -------------------------------------------------------- 
--
SET @ENTRY := 10978;
SET @Sprinkle:=7583;
SET @Quixxil:=10977;

UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry` IN (@ENTRY, @Sprinkle, @Quixxil);
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Sprinkle AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Quixxil AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,8,0,100,0,17166,0,0,0,80,1097800,2,0,0,0,0,1,0,0,0,0,0,0,0,"Legacki - On Spellhit 'Release Umi's Yeti' - Run Script"),
(@ENTRY,0,1,0,40,0,100,0,15,10978,0,0,80,@ENTRY*100+01,2,0,0,0,0,1,0,0,0,0,0,0,0,"Legacki - On Waypoint 15 Reached - Run Script"),
(@Sprinkle,0,0,0,8,0,100,0,17166,0,0,0,80,@Sprinkle*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Sprinkle - On Spellhit 'Release Umi's Yeti' - Run Script"),
(@Sprinkle,0,1,0,40,0,100,0,7,@Sprinkle,0,0,80,@Sprinkle*100+01,2,0,0,0,0,1,0,0,0,0,0,0,0,"Sprinkle - On Waypoint 7 Reached - Run Script"),
(@Quixxil,0,0,0,8,0,100,0,17166,0,0,0,80,@Quixxil*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Quixxil - On Spellhit 'Release Umi's Yeti' - Run Script"),
(@Quixxil,0,1,0,40,0,100,0,11,@Quixxil,0,0,80,@Quixxil*100+01,2,0,0,0,0,1,0,0,0,0,0,0,0,"Quixxil - On Waypoint 11 Reached - Run Script");

-- Actionlist SAI
SET @ENTRY := 1097800;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=9;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Sprinkle*100 AND `source_type`=9;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Quixxil*100 AND `source_type`=9;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,9,0,0,0,0,100,0,1000,1000,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Legacki - On Script - Say Line 0"),
(@ENTRY,9,1,0,0,0,100,0,1000,1000,0,0,53,1,10978,0,0,0,2,1,0,0,0,0,0,0,0,"Legacki -On Script - Start Waypoint"),
(@ENTRY,9,2,0,0,0,100,0,0,0,0,0,45,0,1,0,0,0,0,19,10980,20,0,0,0,0,0,"Legacki -On Script - Say Line 1"),
(@ENTRY,9,3,0,0,0,100,0,1000,1000,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Legacki -On Script - Say Line 1"),
(@ENTRY,9,4,0,0,0,100,0,0,0,0,0,64,1,0,0,0,0,0,7,0,0,0,0,0,0,0,"Legacki -On Script  - Store target"),
(@Sprinkle*100,9,0,0,0,0,100,0,1000,1000,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Sprinkle - On Script - Say Line 0"),
(@Sprinkle*100,9,1,0,0,0,100,0,1000,1000,0,0,53,1,@Sprinkle,0,0,0,2,1,0,0,0,0,0,0,0,"Sprinkle - On Script - Start Waypoint"),
(@Sprinkle*100,9,2,0,0,0,100,0,0,0,0,0,45,0,2,0,0,0,0,19,10980,20,0,0,0,0,0,"Sprinkle - On Script - Say Line 1"),
(@Sprinkle*100,9,3,0,0,0,100,0,1000,1000,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Sprinkle - On Script - Say Line 1"),
(@Sprinkle*100,9,4,0,0,0,100,0,0,0,0,0,64,1,0,0,0,0,0,7,0,0,0,0,0,0,0,"Sprinkle - On Script  - Store target"),
(@Quixxil*100,9,0,0,0,0,100,0,1000,1000,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Quixxil - On Script - Say Line 0"),
(@Quixxil*100,9,1,0,0,0,100,0,1000,1000,0,0,53,1,@Quixxil,0,0,0,2,1,0,0,0,0,0,0,0,"Quixxil - On Script - Start Waypoint"),
(@Quixxil*100,9,2,0,0,0,100,0,0,0,0,0,45,0,3,0,0,0,0,19,10980,20,0,0,0,0,0,"Quixxil - On Script - Say Line 1"),
(@Quixxil*100,9,3,0,0,0,100,0,1000,1000,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Quixxil - On Script - Say Line 1"),
(@Quixxil*100,9,4,0,0,0,100,0,4000,4000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Quixxil - On Script - Say Line 2"),
(@Quixxil*100,9,5 ,0,0,0,100,0,0,0,0,0,64,1,0,0,0,0,0,7,0,0,0,0,0,0,0,"Quixxil - On Script  - Store target");

-- Actionlist SAI
SET @ENTRY := 1097801;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=9;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Sprinkle*100+1 AND `source_type`=9;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Quixxil*100+1 AND `source_type`=9;


INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,9,0,0,0,0,100,0,2000,2000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Legacki - On Script - Say Line 2"),
(@ENTRY,9,1,0,0,0,100,0,2000,2000,0,0,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,"Legacki - On Script - Say Line 3"),
(@ENTRY,9,2,0,0,0,100,0,2000,2000,0,0,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,"Legacki - On Script - Say Line 4"),
(@ENTRY,9,3,0,0,0,100,0,2000,2000,0,0,33,10978,0,0,0,0,0,12,1,0,0,0,0,0,0,"Legacki - On Script - Credit quest"),
(@Sprinkle*100+1,9,0,0,0,0,100,0,2000,2000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Sprinkle - On Script - Say Line 2"),
(@Sprinkle*100+1,9,1,0,0,0,100,0,2000,2000,0,0,33,@Sprinkle,0,0,0,0,0,12,1,0,0,0,0,0,0,"Sprinkle - On Script - Credit quest"),
(@Quixxil*100+1,9,1,0,0,0,100,0,2000,2000,0,0,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,"Quixxil - On Script - Say Line 3"),
(@Quixxil*100+1,9,2,0,0,0,100,0,2000,2000,0,0,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,"Quixxil - On Script - Say Line 4"),
(@Quixxil*100+1,9,3,0,0,0,100,0,2000,2000,0,0,33,@Quixxil,0,0,0,0,0,12,1,0,0,0,0,0,0,"Quixxil - On Script - Credit quest");

-- Umi's Mechanical Yeti SAI
SET @ENTRY := 10980;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,38,0,100,0,0,1,0,0,29,5,10,0,0,0,0,19,10978,10,0,0,0,0,0,"Umi's Mechanical Yeti - On Just Summoned - Start Follow Closest Creature 'Legacki'"),
(@ENTRY,0,1,0,38,0,100,0,0,2,0,0,29,5,10,0,0,0,0,19,@Sprinkle,10,0,0,0,0,0,"Umi's Mechanical Yeti - On Just Summoned - Start Follow Closest Creature 'Legacki'"),
(@ENTRY,0,2,0,38,0,100,0,0,3,0,0,29,5,10,0,0,0,0,19,@Quixxil,10,0,0,0,0,0,"Umi's Mechanical Yeti - On Just Summoned - Start Follow Closest Creature 'Legacki'"),
(@ENTRY,0,3,4,54,0,100,0,0,0,0,0,1,0,1000,0,0,0,0,1,0,0,0,0,0,0,0,"Umi's Mechanical Yeti - On Just Summoned - Say Line 0"),
(@ENTRY,0,4,0,61,0,100,0,0,0,0,0,41,30000,0,0,0,0,0,1,0,0,0,0,0,0,0,"Umi's Mechanical Yeti - On Just Summoned - Despawn In 30000 ms"),
(@ENTRY,0,5,0,52,0,100,0,0,10980,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Umi's Mechanical Yeti - On Text 0 Over - Say Line 1");

DELETE FROM `creature_text` WHERE `entry` IN (10980, 10978, @Quixxil, @Sprinkle);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES 
(10978, 0, 0, '%s jumps in fright!', 16, 0, 100, 0, 0, 0, 6301, 0, 'Legacki'),
(10978, 1, 0, 'Ahhhhh!!!', 12, 0, 100, 0, 0, 0, 6302, 0, 'Legacki'),
(10978, 2, 0, 'You big meanie! Who put you up to this?', 12, 0, 100, 0, 0, 0, 6303, 0, 'Legacki'),
(10978, 3, 0, 'It was Umi, wasn''t it?! She''s always playing jokes on me, and now she''s got you in on it too!', 12, 0, 100, 0, 0, 0, 6304, 0, 'Legacki'),
(10978, 4, 0, '%s sighs.', 16, 0, 100, 0, 0, 0, 6305, 0, 'Legacki'),
(@Quixxil, 0, 0, 'Oh!!! Get that thing away from me!', 12, 0, 100, 0, 0, 0, 6314, 0, 'Quixxil'),
(@Quixxil, 1, 0, '%s runs quickly away from Umi''s Mechanical yeti.', 16, 0, 100, 0, 0, 0, 6318, 0, 'Quixxil'),
(@Quixxil, 2, 0, 'Why do you chase me, Mechanical yeti?! WHY?!', 12, 0, 100, 0, 0, 0, 6317, 0, 'Quixxil'),
(@Quixxil, 3, 0, '%s looks relieved.', 16, 0, 100, 0, 0, 0, 6322, 0, 'Quixxil'),
(@Quixxil, 4, 0, 'I''m jumpy as it is... and people insist on scaring me... Next time, though, I''ll be ready!', 12, 0, 100, 0, 0, 0, 6320, 0, 'Quixxil'),
(@Sprinkle, 0, 0, '%s jumps in fright!', 16, 0, 100, 0, 0, 0, 6306, 0, 'Legacki'),
(@Sprinkle, 1, 0, 'Ahhhhh!!! What is that thing?!', 12, 0, 100, 0, 0, 0, 6307, 0, 'Legacki'),
(@Sprinkle, 2, 0, 'Umi sent you, didn''t she? She told me to expect a surprise, but I never thought that this is what she meant!', 12, 0, 100, 0, 0, 0, 6308, 0, 'Legacki'),
(10980, 0, 0, '%s marches around, roaring and making a ruckus.', 16, 0, 100, 0, 0, 0, 6327, 0, 'Umi''s Mechanical Yeti'),
(10980, 1, 0, 'RAAAAAAAR!', 12, 0, 100, 0, 0, 0, 6329, 0, 'Umi''s Mechanical Yeti');


DELETE FROM `waypoints` WHERE `entry` IN (10978, @Quixxil, @Sprinkle);
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES 
(10978, 1,6713.694,-4682.374,721.6163, 'Legacki'),
(10978, 2,6731.523,-4675.635,721.2076, 'Legacki'),
(10978, 3,6736.806,-4672.955,721.9132, 'Legacki'),
(10978, 4,6749.889,-4662.13,725.0011, 'Legacki'),
(10978, 5,6750.157,-4661.562,724.9875, 'Legacki'),
(10978, 6,6732.859,-4641.781,722.8784, 'Legacki'),
(10978, 7,6727.719,-4641.233,721.6862, 'Legacki'),
(10978, 8,6715.982,-4643.644,721.1971, 'Legacki'),
(10978, 9,6719.307,-4652.817,721.231, 'Legacki'),
(10978, 10,6715.004,-4685.518,721.466, 'Legacki'),
(10978, 11,6718.75,-4705.675,721.9097, 'Legacki'),
(10978, 12,6712.263,-4717.947,721.5077, 'Legacki'),
(10978, 13,6701.636,-4714.284,721.6529, 'Legacki'),
(10978, 14,6701.108,-4695.846,722.3613, 'Legacki'),
(10978, 15,6705.608,-4686.181,721.9736, 'Legacki'),
(@Quixxil, 1, -6186.462402, -1096.938965, -213.828033, 'Quixxil'),
(@Quixxil, 2, -6165.831543, -1100.158203, -210.395004, 'Quixxil'),
(@Quixxil, 3, -6146.718750, -1120.232056, -212.840530, 'Quixxil'),
(@Quixxil, 4, -6160.913574, -1141.027466, -217.996246, 'Quixxil'),
(@Quixxil, 5, -6184.303711, -1125.439087, -217.041824, 'Quixxil'),
(@Quixxil, 6, -6203.271484, -1111.438354, -219.371887, 'Quixxil'),
(@Quixxil, 7, -6213.762695, -1080.857422, -211.627533, 'Quixxil'),
(@Quixxil, 8, -6184.807129, -1050.598633, -191.913223, 'Quixxil'),
(@Quixxil, 9, -6173.853516, -1054.144165, -190.152405, 'Quixxil'),
(@Quixxil, 10, -6179.203613, -1041.906372, -185.499283, 'Quixxil'),
(@Quixxil, 11, -6197.859863, -1082.170044, -209.164001, 'Quixxil'),
(@Sprinkle, 1, -7114.151855, -3811.191406, 8.390815, 'Sprinkle'),
(@Sprinkle, 2, -7155.397949, -3820.387695, 8.370462, 'Sprinkle'),
(@Sprinkle, 3, -7178.134766, -3791.506836, 8.370436, 'Sprinkle'),
(@Sprinkle, 4, -7160.750488, -3758.740723, 8.370436, 'Sprinkle'),
(@Sprinkle, 5, -7136.473145, -3747.802246, 8.407521, 'Sprinkle'),
(@Sprinkle, 6, -7109.993652, -3739.603760, 9.105528, 'Sprinkle'),
(@Sprinkle, 7, -7111.109863, -3741.780029, 8.609260, 'Sprinkle');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_04_05.sql 
-- -------------------------------------------------------- 
--
DELETE FROM `creature_text`
WHERE `entry` IN (1506, 1507, 1667);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(1506, 0, 0, 'You carry the taint of the Scourge.  Prepare to enter the Twisting Nether.', 12, 7, 25, 0, 0, 0, 2625, 0, 'Scarlet Convert 1'),
(1506, 0, 1, 'There is no escape for you.  The Crusade shall destroy all who carry the Scourge''s taint.', 12, 7, 25, 0, 0, 0, 2626, 0, 'Scarlet Convert 2'),
(1506, 0, 2, 'The Light condemns all who harbor evil.  Now you will die!', 12, 7, 25, 0, 0, 0, 2627, 0, 'Scarlet Convert 3'),
(1506, 0, 3, 'The Scarlet Crusade shall smite the wicked and drive evil from these lands!', 12, 7, 25, 0, 0, 0, 2628, 0, 'Scarlet Convert 4'),
(1507, 0, 0, 'You carry the taint of the Scourge.  Prepare to enter the Twisting Nether.', 12, 7, 25, 0, 0, 0, 2625, 0, 'Scarlet Initiate 1'),
(1507, 0, 1, 'There is no escape for you.  The Crusade shall destroy all who carry the Scourge''s taint.', 12, 7, 25, 0, 0, 0, 2626, 0, 'Scarlet Initiate 2'),
(1507, 0, 2, 'The Light condemns all who harbor evil.  Now you will die!', 12, 7, 25, 0, 0, 0, 2627, 0, 'Scarlet Initiate 3'),
(1507, 0, 3, 'The Scarlet Crusade shall smite the wicked and drive evil from these lands!', 12, 7, 25, 0, 0, 0, 2628, 0, 'Scarlet Initiate 4'),
(1667, 0, 0, 'These lands shall be cleansed!', 12, 0, 100, 5, 0, 0, 314, 0, 'Meven Korgal - Nearby aggro'),
(1667, 1, 0, 'These undead atrocities will be destroyed!', 12, 0, 100, 5, 0, 0, 316, 0, 'Meven Korgal - Spawn'),
(1667, 2, 0, 'The Scarlet Crusade shall not fail in its mission!', 12, 0, 100, 0, 0, 0, 317, 0, 'Meven Korgal - On aggro');

-- Scarlet Initiate SAI
SET @ENTRY := 1507;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,7,0,0,0,100,0,0,0,3500,5000,11,20793,64,0,0,0,0,2,0,0,0,0,0,0,0,"Scarlet Initiate - In Combat - Cast 'Fireball'"),
(@ENTRY,0,10,0,23,0,100,0,12544,0,5000,10000,11,12544,0,0,0,0,0,1,0,0,0,0,0,0,0,"Scarlet Initiate - On Has Aura 'Frost Armor' - Cast 'Frost Armor'"),
(@ENTRY,0,11,0,4,0,50,0,0,0,0,0,1,0,0,0,0,0,0,2,0,0,0,0,0,0,0,"Scarlet Initiate - On Aggro - Say Line 0");

-- Scarlet Convert SAI
SET @ENTRY := 1506;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,4,0,50,0,0,0,0,0,1,0,0,0,0,0,0,2,0,0,0,0,0,0,0,"Scarlet Convert - On Aggro - Say Line 0");

-- Meven Korgal SAI
SET @ENTRY := 1667;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,25,0,100,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,"Meven Korgal - On Reset - Say Line 1"),
(@ENTRY,0,1,0,1,0,100,0,10000,30000,45000,120000,1,0,0,0,0,0,0,17,1,30,0,0,0,0,0,"Meven Korgal - Out of Combat - Say Line 0"),
(@ENTRY,0,2,0,4,0,100,0,0,0,0,0,1,2,0,0,0,0,0,2,0,0,0,0,0,0,0,"Meven Korgal - On Aggro - Say Line 2");
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_04_06.sql 
-- -------------------------------------------------------- 
-- 
-- Valiance Keep Rifleman SAI
SET @ENTRY := 25311;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,1000,1000,6000,6000,11,45761,0,0,0,0,0,19,24921,50,0,0,0,0,0,"Valiance Keep Rifleman - Out of Combat - Cast 'Shoot Gun'");
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_05_00.sql 
-- -------------------------------------------------------- 
        SET @OGUID       := 16926;

        -- Creatures :
        SET @MOGRAINE       := 20345;
        SET @ISILLIEN      := 20346;
        SET @ABBENDIS      := 20347;
        SET @FAIRBANKS      := 20348;
        SET @TIRION      := 20349;
        SET @DOAN      := 20352;
        SET @TARGET       := 20391; -- for spells
        -- DB Guids :
        SET @MGUID       := 83685;
        SET @FGUID      := 83686;
        SET @DGUID      := 83689;
        SET @TGUID      := 83690;
        SET @IGUID      := 83691;
        SET @AGUID      := 83692;
        -- Event
        SET @TRIGG       := 4498;
        -- Default difference between events
        SET @DELAY      := 3000000;
        -- Objects
        SET @CHAIR      := 184305;
        SET @DARK      := 184306;
        SET @LIGHT      := 184307;
        SET @CHEST      := 184308;
        -- Positions
        SET @CHESTX      := 1816.698;
        SET @CHESTY      := 1031.316;
        SET @CHESTZ      := 11.84751;
        -- Spells
        SET @TBTL       := 35172;
        SET @JUDGE      := 35170;
        SET @SHOCK      := 35160;
        SET @SMITE      := 35155;
        SET @HEALS      := 35162;

        DELETE FROM `gameobject` WHERE `id` BETWEEN 22882 AND 22887 AND `map`=560;
        INSERT INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `PhaseId`, `PhaseGroup`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) VALUES
        (@OGUID+0, 22882, 560, 3, 1, 1, 1823.365, 1029.282, 11.64124, 1.439896, 0, 0, 0.7071069, 0.7071066, 7200, 255, 1),
        (@OGUID+1, 22883, 560, 3, 1, 1, 1824.24, 1031.343, 11.64124, 0.5061446, 0, 0, 0.7071069, 0.7071066, 7200, 255, 1),
        (@OGUID+2, 22884, 560, 3, 1, 1, 1822.9, 1032.183, 11.64124, 3.141593, 0, 0, 0.7071069, 0.7071066, 7200, 255, 1),
        (@OGUID+3, 22885, 560, 3, 1, 1, 1813.827, 999.6839, 12.13165, 0.1047193, 0, 0, 0.7071069, 0.7071066, 7200, 255, 1),
        (@OGUID+4, 22886, 560, 3, 1, 1, 1822.891, 1030.719, 11.64124, 4.232424, 0, 0, 0.7071069, 0.7071066, 7200, 255, 1),
        (@OGUID+5, 22887, 560, 3, 1, 1, 1812.618, 999.1037, 12.13165, 1.509709, 0, 0, 0.7071069, 0.7071066, 7200, 255, 1);

        DELETE FROM `conditions` WHERE `SourceEntry` IN (@SMITE,@SHOCK,@JUDGE,@HEALS);
        INSERT INTO `conditions`(`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
        (13,1,@SMITE,0,0,31,0,3,@TARGET,0,0,0,'','Spell casts only on Creature 20391'),
        (13,1,@SHOCK,0,0,31,0,3,@TARGET,0,0,0,'','Spell casts only on Creature 20391'),
        (13,1,@JUDGE,0,0,31,0,3,@TARGET,0,0,0,'','Spell casts only on Creature 20391'),
        (13,1,@HEALS,0,0,31,0,3,@TARGET,0,0,0,'','Spell casts only on Creature 20391');

        DELETE FROM `creature_text` WHERE `entry` IN (@MOGRAINE,@ISILLIEN,@ABBENDIS,@FAIRBANKS,@TIRION,@DOAN);
        INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `BroadcastTextId`, `comment`) VALUES
        -- Commander Mograine
        (@MOGRAINE, 0 , 0, 'Keep your voices down. There are strangers about...' ,12 ,0,100,1,18084, ' Mograine - 1'),
        (@MOGRAINE, 1 , 0, 'Brothers and sisters, I have called you here today to discuss the fate of Lordaeron.' ,12 ,0,100,1,18085, ' Mograine - 2'),
        (@MOGRAINE, 2 , 0, 'I hear things... Things that should not be.' ,12 ,0,100,1,18087, ' Mograine - 3'),
        (@MOGRAINE, 3 , 0, 'The dead rise... Undead, from the frozen northlands. Whole cities have gone missing. I...' ,12 ,0,100,1,18088, ' Mograine - 4'),
        (@MOGRAINE, 4 , 0, 'I have heard that Northrend is lost...' ,12 ,0,100,274,18089, ' Mograine - 5'),
        (@MOGRAINE, 5 , 0, 'We must stand at the ready. I have faced undead before. They are ruthless killing machines, devoid of any emotion or compassion.' ,12 ,0,100,1,18091, ' Mograine - 6'),
        (@MOGRAINE, 6 , 0, 'Propose? I propose that we prepare. That we prepare our loved ones, family and friends for the possibility of an undead holocaust.' ,12 ,0,100,6,18095, ' Mograine - 7'),
        (@MOGRAINE, 7 , 0, 'And there is this...' ,12 ,0,100,6,18096, ' Mograine - 8'),
        (@MOGRAINE, 8 , 0, 'I have had this object in my possession for 10 years. Since Blackrock Spire...' ,12 ,0,100,1,18099, ' Mograine - 9'),
        (@MOGRAINE, 9 , 0, 'I wrested it free from the remains of an orc lieutenant - a dark caster... It is from their homeworld.' ,12 ,0,100,1,18100, ' Mograine - 10'),
        (@MOGRAINE, 10 , 0, 'Do not get too close. I laid a hand upon it once... Only once and never again. The memories of that day still linger.' ,12 ,0,100,1,18101, ' Mograine - 11'),
        (@MOGRAINE, 11 , 0, 'I surmise that this object is the living embodiment of shadows... darkness... It is a manifestation. It is a void.' ,12 ,0,100,1,18103, ' Mograine - 12'),
        (@MOGRAINE, 12 , 0, 'No, old friend, it is very relevant.' ,12 ,0,100,1,18107, ' Mograine - 13'),
        (@MOGRAINE, 13 , 0, 'Let me ask you this, brothers and sisters: Can good exist without evil? Can there be light without dark?' ,12 ,0,100,1,18108, ' Mograine - 14'),
        (@MOGRAINE, 14 , 0, 'And if that answer is no, then could it be possible that because this artifact exists, its polar opposite must also exist?' ,12 ,0,100,25,18109, ' Mograine - 15'),
        (@MOGRAINE, 15 , 0, 'Could you imagine what the material manifestation of the Light could do against the undead?' ,12 ,0,100,1,18110, ' Mograine - 16'),
        (@MOGRAINE, 16 , 0, 'It consumed the Light!' ,12 ,0,100,25,18117, ' Mograine - 17'),
        (@MOGRAINE, 17 , 0, 'BY THE LIGHT! Could it be? Could this be it?' ,12 ,0,100,5,18122, ' Mograine - 18'),
        (@MOGRAINE, 18 , 0, 'I must know... I will know.' ,12 ,0,100,5,18123, ' Mograine - 19'),
        (@MOGRAINE, 19 , 0, 'I... It... It is beautiful. What I felt when I touched it... The Light coursed through me and I through it... It healed my spirit.' ,12 ,0,100,1,18125, ' Mograine - 20'),
        (@MOGRAINE, 20 , 0, 'Let us never again speak of this day. Our enemies are many. They need not know we hold such artifacts.' ,12 ,0,100,1,18128, ' Mograine - 21'),
        (@MOGRAINE, 21 , 0, 'I have seen it... From this blessed crystal we will forge a weapon. This weapon will hold inside it a piece of each of us... And when it is used against undead, it shall cast them down. And in its wake, it will leave only ashes...' ,12 ,0,100,1,18129, ' Mograine - 22'),
        (@MOGRAINE, 22 , 0, '%s lowers his voice to a whisper.' ,16,0,100,0,18086, ' Emote - 1'),
        (@MOGRAINE, 23 , 0, 'Gasps can be heard throughout the room.' ,16,0,100,0,18090, ' Emote - 2'),
        (@MOGRAINE, 25 , 0, '%s unlocks the chest.' ,16,0,100,25,18097, ' Emote - 4'),
        (@MOGRAINE, 26 , 0, '%s removes the gauntlet from his right arm and shows everyone his mangled hand.' ,16,0,100,0,18102, ' Emote - 5'),
        (@MOGRAINE, 27 , 0, '%s shakes his head.' ,16,0,100,0,18106, ' Emote - 6'),
        (@MOGRAINE, 28 , 0, 'Shock then silence overtakes the crowd.' ,16,0,100,0,18113, ' Emote - 7'),
        (@MOGRAINE, 29 , 0, '%s reaches out to touch the light crystal.' ,16,0,100,25,18124, ' Emote - 8'),
        (@MOGRAINE, 30 , 0, '%s puts the crystal back inside the chest.' ,16,0,100,0,18127, ' Emote - 9'),
        -- Isillien
        (@ISILLIEN, 0 , 0, 'I do not see how this evil artifact is relevant to the undead. We must destroy it!' ,12 ,0,100,5,18104, ' Isillien - 1'),
        (@ISILLIEN, 1 , 0, 'Nonsense, Mograine! It must be destroyed!' ,12 ,0,100,5,18112, ' Isillien - 2'),
        (@ISILLIEN, 2 , 0, 'Your hand! It is healed!' ,12 ,0,100,25,18126, ' Isillien - 3'),
        (@ISILLIEN, 3 , 0, 'The Ashbringer...' ,12 ,0,100,66,18130, ' Isillien - 4'),
        -- Abbendis
        (@ABBENDIS, 0 , 0, 'By the Light! What is it?' ,12 ,0,100,25,18098, ' Abbendis - 1'),
        (@ABBENDIS, 1 , 0, 'The Ashbringer...' ,12 ,0,100,66,18130, ' Abbendis - 2'),
        -- Fairbanks
        (@FAIRBANKS, 0 , 0, 'Is... Is it getting lighter? Its coloration... It is changing.' ,12 ,0,100,1,18121, ' Fairbanks - 1'),
        (@FAIRBANKS, 1 , 0, 'The Ashbringer...' ,12 ,0,100,66,18130, ' Fairbanks - 2'),
        -- Tirion Fordring
        (@TIRION, 0 , 0, 'Aye, I''ve battled them as well. We are ill-prepared as a kingdom to withstand such an assault.' ,12 ,0,100,1,18093, ' Tirion - 1'),
        (@TIRION, 1 , 0, 'Impossible!' ,12 ,0,100,5,18118, ' Tirion - 2'),
        (@TIRION, 2 , 0, 'The Ashbringer...' ,12 ,0,100,66,18130, ' Tirion - 3'),
        (@TIRION, 3 , 0, '%s nods.' ,16,0,100,0,18092, ' Emote - 3'),
        -- Archanist Doan
        (@DOAN, 0 , 0, 'What do you propose, Mograine?' ,12 ,0,100,6,18094, ' Doan - 1'),
        (@DOAN, 1 , 0, 'The Ashbringer...' ,12 ,0,100,66,18130, ' Doan - 2');

        UPDATE `creature` SET `position_x`=1818.420044, `position_y`=1031.170044, `position_z`=11.0651, `orientation`=3.14155 WHERE  `guid`=@MGUID;
        UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN (@MOGRAINE,@TIRION,@ABBENDIS,@FAIRBANKS,@ISILLIEN);
        UPDATE `creature_template` SET `scale` = 1.5 WHERE `entry` = @TARGET;

        DELETE FROM `creature_addon` WHERE `guid` = @MGUID;
        INSERT INTO `creature_addon` (`guid`,`bytes1`) VALUES (@MGUID,4);

        DELETE FROM `areatrigger_scripts` WHERE `entry` = @TRIGG;
        INSERT INTO `areatrigger_scripts`(`entry`, `ScriptName`) VALUES
        (@TRIGG,'SmartTrigger');

        DELETE FROM `smart_scripts` WHERE `entryorguid` IN (@TRIGG,@MOGRAINE,@MOGRAINE*100,@TIRION,@TIRION*100,@ABBENDIS,@ABBENDIS*100,@ISILLIEN,@ISILLIEN*100,@FAIRBANKS,@FAIRBANKS*100);
        INSERT INTO `smart_scripts`(`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
        -- Trigger sets data on involved NPCs
        (@TRIGG,2,0,1,46,0,100,0,@TRIGG,0,0,0,45,1,1,0,0,0,0,10,@MGUID,@MOGRAINE,0,0,0,0,0,"Set data - On Enter inn"),
        (@TRIGG,2,1,2,61,0,100,0,@TRIGG,0,0,0,45,1,1,0,0,0,0,10,@TGUID,@TIRION,0,0,0,0,0,"Set data - On Enter inn"),
        (@TRIGG,2,2,3,61,0,100,0,@TRIGG,0,0,0,45,1,1,0,0,0,0,10,@AGUID,@ABBENDIS,0,0,0,0,0,"Set data - On Enter inn"),
        (@TRIGG,2,3,4,61,0,100,0,@TRIGG,0,0,0,45,1,1,0,0,0,0,10,@IGUID,@ISILLIEN,0,0,0,0,0,"Set data - On Enter inn"),
        (@TRIGG,2,4,5,61,0,100,0,@TRIGG,0,0,0,45,1,1,0,0,0,0,10,@FGUID,@FAIRBANKS,0,0,0,0,0,"Set data - On Enter inn"),
        -- NPCs run scripts
        (@MOGRAINE,0,0,1,38,0,100,0,1,1,@DELAY,@DELAY,80,@MOGRAINE*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - On set data - Run Script"),
        (@ISILLIEN,0,0,1,38,0,100,0,1,1,@DELAY,@DELAY,80,@ISILLIEN*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Isillien - On set data - Run Script"),
        (@TIRION,0,0,1,38,0,100,0,1,1,@DELAY,@DELAY,80,@TIRION*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tirion - On set data - Run Script"),
        (@FAIRBANKS,0,0,1,38,0,100,0,1,1,@DELAY,@DELAY,80,@FAIRBANKS*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fairbanks - On set data - Run Script"),
        (@ABBENDIS,0,0,1,38,0,100,0,1,1,@DELAY,@DELAY,80,@ABBENDIS*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Abbendis - On set data - Run Script"),
        -- Mograine (main) script
        (@MOGRAINE*100,9,0,0,0,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 1"),-- 56:34.5
        (@MOGRAINE*100,9,1,0,0,0,100,0,5000,5000,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 2"),-- 39
        (@MOGRAINE*100,9,2,0,0,0,100,0,7000,7000,0,0,1,22,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote 1"), -- 46
        (@MOGRAINE*100,9,3,0,0,0,100,0,1000,1000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 3"),-- 47
        (@MOGRAINE*100,9,4,0,0,0,100,0,4000,4000,0,0,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 4"),-- 51
        (@MOGRAINE*100,9,5,0,0,0,100,0,9000,9000,0,0,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 5"), -- 00
        (@MOGRAINE*100,9,6,0,0,0,100,0,6000,6000,0,0,1,23,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote 2"), -- 06
        (@MOGRAINE*100,9,7,0,0,0,100,0,2000,2000,0,0,1,5,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 6"), -- 08
        (@MOGRAINE*100,9,8,0,0,0,100,0,6000,6000,0,0,1,3,0,0,0,0,0,10,@TGUID,@TIRION,0,0,0,0,0,"Tirion - Script - Emote 3"), -- 14
        (@MOGRAINE*100,9,9,0,0,0,100,0,3000,3000,0,0,1,0,0,0,0,0,0,10,@TGUID,@TIRION,0,0,0,0,0,"Tirion - Script - Say Line 1"), -- 17
        (@MOGRAINE*100,9,10,0,0,0,100,0,6000,6000,0,0,1,0,0,0,0,0,0,10,@DGUID,@DOAN,0,0,0,0,0,"Doan - Say Line 1"), -- 23
        (@MOGRAINE*100,9,11,0,0,0,100,0,5000,5000,0,0,1,6,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 7"), -- 28
        (@MOGRAINE*100,9,12,0,0,0,100,0,7000,7000,0,0,1,7,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 8"), -- 35
        (@MOGRAINE*100,9,13,0,0,0,100,0,4000,4000,0,0,50,@CHEST,155,0,0,0,0,8,0,0,0,@CHESTX,@CHESTY,@CHESTZ,0,"Mograine - Script - Spawn chest"), -- 39
        (@MOGRAINE*100,9,14,0,0,0,100,0,2000,2000,0,0,1,25,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote 4"), -- 41
        (@MOGRAINE*100,9,15,0,0,0,100,0,1000,1000,0,0,9,0,0,0,0,0,0,15,@CHEST,10,0,0,0,0,0,"Mograine - Script - Open Chest"), -- 42
        (@MOGRAINE*100,9,16,0,0,0,100,0,0,0,0,0,12,@TARGET,1,120000,0,0,0,8,0,0,0,1816.784,1031.313,12.4703,6.265732,"Mograine - Script - Spawn dummy"), -- 42
        (@MOGRAINE*100,9,17,0,0,0,100,0,0,0,0,0,50,@DARK,110,0,0,0,0,8,0,0,0,@CHESTX,@CHESTY,@CHESTZ,0,"Mograine - Script - Spawn crystal"), -- 42
        (@MOGRAINE*100,9,18,0,0,0,100,0,2000,2000,0,0,5,34,0,0,0,0,0,10,@AGUID,@ABBENDIS,0,0,0,0,0,"Abbendis - Script - Emote"), -- 44
        (@MOGRAINE*100,9,19,0,0,0,100,0,2000,2000,0,0,1,0,0,0,0,0,0,10,@AGUID,@ABBENDIS,0,0,0,0,0,"Abbendis - Script - Say Line 1"), -- 46
        (@MOGRAINE*100,9,20,0,0,0,100,0,5000,5000,0,0,1,8,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 9"), -- 50
        (@MOGRAINE*100,9,21,0,0,0,100,0,4000,4000,0,0,1,9,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 10"), -- 54
        (@MOGRAINE*100,9,22,0,0,0,100,0,6000,6000,0,0,1,10,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 11"), -- 00
        (@MOGRAINE*100,9,23,0,0,0,100,0,3000,3000,0,0,1,26,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote 5"),
        (@MOGRAINE*100,9,24,0,0,0,100,0,4000,4000,0,0,1,11,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 12"),
        (@MOGRAINE*100,9,25,0,0,0,100,0,9000,9000,0,0,1,0,0,0,0,0,0,10,@IGUID,@ISILLIEN,0,0,0,0,0,"Isillien - Script - Say Line 1"),
        (@MOGRAINE*100,9,26,0,0,0,100,0,5000,5000,0,0,1,27,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote 6"),
        (@MOGRAINE*100,9,27,0,0,0,100,0,1000,1000,0,0,1,12,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 13"),
        (@MOGRAINE*100,9,28,0,0,0,100,0,4000,4000,0,0,1,13,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 14"),
        (@MOGRAINE*100,9,29,0,0,0,100,0,3000,3000,0,0,5,6,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote"),
        (@MOGRAINE*100,9,30,0,0,0,100,0,3000,3000,0,0,1,14,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 15"),
        (@MOGRAINE*100,9,31,0,0,0,100,0,2000,2000,0,0,5,6,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote"),
        (@MOGRAINE*100,9,32,0,0,0,100,0,2000,2000,0,0,1,15,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 16"),
        (@MOGRAINE*100,9,33,0,0,0,100,0,4000,4000,0,0,5,6,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote"),
        (@MOGRAINE*100,9,34,0,0,0,100,0,4000,4000,0,0,1,1,0,0,0,0,0,10,@IGUID,@ISILLIEN,0,0,0,0,0,"Isillien - Script - Say Line 2"),
        (@MOGRAINE*100,9,35,0,0,0,100,0,3000,3000,0,0,5,25,0,0,0,0,0,10,@IGUID,@ISILLIEN,0,0,0,0,0,"Isillien - Script - Emote"),
        (@MOGRAINE*100,9,36,0,0,0,100,0,5000,5000,0,0,1,28,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote 7"),
        (@MOGRAINE*100,9,37,0,0,0,100,0,0,0,0,0,5,5,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote"), -- 52
        (@MOGRAINE*100,9,38,0,0,0,100,0,2000,2000,0,0,91,@MGUID,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Set unit bytes"),
        (@MOGRAINE*100,9,39,0,0,0,100,0,0,0,0,0,69,0,0,0,0,0,0,8,0,0,0,1818.665,1029.883,11.09751,2.553278,"Mograine - Script - Start WP"),
        (@MOGRAINE*100,9,40,0,0,0,100,0,1000,1000,0,0,66,0,0,0,0,0,0,15,@CHEST,50,0,0,0,0,0,"Mograine - Script - Set orientation"),
        (@MOGRAINE*100,9,41,0,0,0,100,0,4000,4000,0,0,1,16,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 17"), -- 58
        (@MOGRAINE*100,9,42,0,0,0,100,0,2000,2000,0,0,5,5,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote"), -- 00
        (@MOGRAINE*100,9,43,0,0,0,100,0,3000,3000,0,0,1,1,0,0,0,0,0,10,@TGUID,@TIRION,0,0,0,0,0,"Tirion - Say Line 2"), -- 03
        (@MOGRAINE*100,9,44,0,0,0,100,0,9000,9000,0,0,1,0,0,0,0,0,0,10,@FGUID,@FAIRBANKS,0,0,0,0,0,"Fairbanks - Say Line 2"), -- 11
        (@MOGRAINE*100,9,45,0,0,0,100,0,20000,20000,0,0,50,@LIGHT,32,0,0,0,0,8,0,0,0,1816.681,1031.386,12.78501,0,"Mograine - Script - Spawn crystal"),
        (@MOGRAINE*100,9,46,0,0,0,100,0,3000,3000,0,0,1,17,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 18"), -- 34
        (@MOGRAINE*100,9,47,0,0,0,100,0,2000,2000,0,0,5,25,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote"), -- 36
        (@MOGRAINE*100,9,48,0,0,0,100,0,2000,2000,0,0,1,18,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 19"), -- 38
        (@MOGRAINE*100,9,49,0,0,0,100,0,4000,4000,0,0,1,29,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote 8"), -- 42
        (@MOGRAINE*100,9,50,0,0,0,100,0,1500,1500,0,0,5,16,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote"),
        (@MOGRAINE*100,9,51,0,0,0,100,0,0,0,0,0,11,@TBTL,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Cast TBLT"),
        (@MOGRAINE*100,9,52,0,0,0,100,0,4000,4000,0,0,5,18,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote"),
        (@MOGRAINE*100,9,53,0,0,0,100,0,7000,7000,0,0,1,19,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 20"),
        (@MOGRAINE*100,9,54,0,0,0,100,0,7000,7000,0,0,1,2,0,0,0,0,0,10,@IGUID,@ISILLIEN,0,0,0,0,0,"Isillien - Script - Say Line 3"),
        (@MOGRAINE*100,9,55,0,0,0,100,0,4000,4000,0,0,1,30,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Emote 9"),
        (@MOGRAINE*100,9,56,0,0,0,100,0,0,0,0,0,32,0,0,0,0,0,0,15,@CHEST,10,0,0,0,0,0,"Mograine - Script - Close Chest"),
        (@MOGRAINE*100,9,57,0,0,0,100,0,4000,4000,0,0,1,20,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 21"),
        (@MOGRAINE*100,9,58,0,0,0,100,0,0,0,0,0,69,0,0,0,0,0,0,8,0,0,0,1818.346,1031.224,11.09751,3.124123,"Mograine - Script - Start WP"),
        (@MOGRAINE*100,9,59,0,0,0,100,0,500,500,0,0,50,@CHAIR,5000,0,0,0,0,8,0,0,0,1818.346,1031.224,11.09751,3.124123,"Mograine - Script - Spawn chair"),
        (@MOGRAINE*100,9,60,0,0,0,100,0,1000,1000,0,0,66,0,0,0,0,0,0,10,@TGUID,@TIRION,0,0,0,0,0,"Mograine - Script - Set orientation"),
        (@MOGRAINE*100,9,61,0,0,0,100,0,0,0,0,0,90,4,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Set unit bytes"),
        (@MOGRAINE*100,9,62,0,0,0,100,0,8000,8000,0,0,1,21,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mograine - Script - Say Line 22"),
        (@MOGRAINE*100,9,63,0,0,0,100,0,11000,11000,0,0,1,2,0,0,0,0,0,10,@TGUID,@TIRION,0,0,0,0,0,"Tirion - Say Line 3"),
        (@MOGRAINE*100,9,64,0,0,0,100,0,3000,3000,0,0,1,3,0,0,0,0,0,10,@IGUID,@ISILLIEN,0,0,0,0,0,"Isillien - Script - Say line 4"),
        (@MOGRAINE*100,9,65,0,0,0,100,0,0,0,0,0,1,1,0,0,0,0,0,10,@DGUID,@DOAN,0,0,0,0,0,"Doan - Script - Say line 2"),
        (@MOGRAINE*100,9,66,0,0,0,100,0,0,0,0,0,1,1,0,0,0,0,0,10,@FGUID,@FAIRBANKS,0,0,0,0,0,"Fairbanks - Script - Say line 2"),
        (@MOGRAINE*100,9,67,0,0,0,100,0,0,0,0,0,1,1,0,0,0,0,0,10,@AGUID,@ABBENDIS,0,0,0,0,0,"Abbendis - Script - Say line 2"),
        -- Tirion script
        (@TIRION*100,9,0,0,0,0,100,0,0,0,0,0,50,@CHAIR,215,0,0,0,0,8,0,0,0,1818.346,1031.224,11.09751,3.124123,"Mograine - Script - Spawn chair"),
        (@TIRION*100,9,1,0,0,0,100,0,152000,152000,0,0,11,@SHOCK,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tirion - Script - Cast Holy Shock"),
        (@TIRION*100,9,2,0,0,0,100,0,12500,12500,0,0,11,@SHOCK,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tirion - Script - Cast Holy Shock"),
        (@TIRION*100,9,3,0,0,0,100,0,3500,3500,0,0,11,@JUDGE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tirion - Script - Cast Judgement"),
        (@TIRION*100,9,4,0,0,0,100,0,2000,2000,0,0,11,@JUDGE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tirion - Script - Cast Judgement"),
        (@TIRION*100,9,5,0,0,0,100,0,2500,2500,0,0,11,@HEALS,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tirion - Script - Cast Heal"),
        (@TIRION*100,9,6,0,0,0,100,0,4000,4000,0,0,11,@SHOCK,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tirion - Script - Cast Holy Shock"),
        -- Fairbanks script
        (@FAIRBANKS*100,9,0,0,0,0,100,0,155000,155000,0,0,66,0,0,0,0,0,0,15,@CHEST,50,0,0,0,0,0,"Fairbanks - Script - Set orientation"),
        (@FAIRBANKS*100,9,1,0,0,0,100,0,6000,6000,0,0,11,@HEALS,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fairbanks - Script - Cast Heal"),
        (@FAIRBANKS*100,9,2,0,0,0,100,0,5000,5000,0,0,11,@SMITE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fairbanks - Script - Cast Smite"),
        (@FAIRBANKS*100,9,3,0,0,0,100,0,2500,2500,0,0,11,@HEALS,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fairbanks - Script - Cast Heal"),
        (@FAIRBANKS*100,9,4,0,0,0,100,0,2500,2500,0,0,11,@SMITE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fairbanks - Script - Cast Smite"),
        (@FAIRBANKS*100,9,5,0,0,0,100,0,2500,2500,0,0,11,@SMITE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fairbanks - Script - Cast Smite"),
        (@FAIRBANKS*100,9,6,0,0,0,100,0,2500,2500,0,0,11,@HEALS,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fairbanks - Script - Cast Heal"),
        (@FAIRBANKS*100,9,7,0,0,0,100,0,70000,70000,0,0,66,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fairbanks - Script - Set orientation"),
        -- Abbendis script
        (@ABBENDIS*100,9,0,0,0,0,100,0,68500,68500,0,0,66,0,0,0,0,0,0,15,@CHEST,50,0,0,0,0,0,"Abbendis - Script - Set orientation"),
        (@ABBENDIS*100,9,1,0,0,0,100,0,96000,96000,0,0,11,@SHOCK,2,0,0,0,0,1,0,0,0,0,0,0,0,"Abbendis - Script - Cast Holy Shock"),
        (@ABBENDIS*100,9,2,0,0,0,100,0,3500,3500,0,0,11,@JUDGE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Abbendis - Script - Cast Judgement"),
        (@ABBENDIS*100,9,3,0,0,0,100,0,3500,3500,0,0,11,@SHOCK,2,0,0,0,0,1,0,0,0,0,0,0,0,"Abbendis - Script - Cast Holy Shock"),
        (@ABBENDIS*100,9,4,0,0,0,100,0,5000,5000,0,0,11,@HEALS,2,0,0,0,0,1,0,0,0,0,0,0,0,"Abbendis - Script - Cast Heal"),
        (@ABBENDIS*100,9,5,0,0,0,100,0,2500,2500,0,0,11,@SHOCK,2,0,0,0,0,1,0,0,0,0,0,0,0,"Abbendis - Script - Cast Holy Shock"),
        (@ABBENDIS*100,9,6,0,0,0,100,0,67000,67000,0,0,66,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Abbendis - Script - Set orientation"),
        -- Isillien script
        (@ISILLIEN*100,9,0,0,0,0,100,0,68000,68000,0,0,66,0,0,0,0,0,0,15,@CHEST,50,0,0,0,0,0,"Isillien - Script - Set orientation"),
        (@ISILLIEN*100,9,1,0,0,0,100,0,66000,66000,0,0,11,@SMITE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Isillien - Script - Cast Smite"),
        (@ISILLIEN*100,9,2,0,0,0,100,0,32000,32000,0,0,11,@SMITE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Isillien - Script - Cast Smite"),
        (@ISILLIEN*100,9,3,0,0,0,100,0,2500,2500,0,0,11,@HEALS,2,0,0,0,0,1,0,0,0,0,0,0,0,"Isillien - Script - Cast Heal"),
        (@ISILLIEN*100,9,4,0,0,0,100,0,2500,2500,0,0,11,@SMITE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Isillien - Script - Cast Smite"),
        (@ISILLIEN*100,9,5,0,0,0,100,0,2500,2500,0,0,11,@SMITE,2,0,0,0,0,1,0,0,0,0,0,0,0,"Isillien - Script - Cast Smite"),
        (@ISILLIEN*100,9,6,0,0,0,100,0,2500,2500,0,0,11,@HEALS,2,0,0,0,0,1,0,0,0,0,0,0,0,"Isillien - Script - Cast Heal"),
        (@ISILLIEN*100,9,7,0,0,0,100,0,70000,70000,0,0,66,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Isillien - Script - Set orientation");
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_05_01.sql 
-- -------------------------------------------------------- 
UPDATE `creature_template` SET `AIName`='SmartAI',`ScriptName`='' WHERE `entry` IN(12858,12859);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN(12858,12859) AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid` IN(1285800) AND `source_type`=9;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES

(12858, 0, 0, 0, 11, 0, 100, 0, 0, 0, 0, 0, 2, 113, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Torek - On Spawn - Set Faction'),
(12858, 0, 1, 2, 19, 0, 100, 0, 6544, 0, 0, 0, 64, 1, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Torek - On Quest Accept - Store Targetlist'),
(12858, 0, 2, 0, 61, 0, 100, 0, 0, 0, 0, 0, 80, 1285800, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Torek - On Quest Accept - Run Script'),
(12858, 0, 3, 0, 40, 0, 100, 1, 1, 12858, 0, 0, 1, 1, 0, 0, 0, 0, 0, 12, 1, 0, 0, 0, 0, 0, 0, 'Torek - On Reached WP1 - Say Line 1'),
(12858, 0, 4, 0, 40, 0, 100, 1, 8, 12858, 0, 0, 1, 2, 0, 0, 0, 0, 0, 12, 1, 0, 0, 0, 0, 0, 0, 'Torek - On Reached WP8 - Say Line 2'),
(12858, 0, 5, 0, 40, 0, 100, 1, 19, 12858, 0, 0, 107, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Torek - On Reached WP19 - Summon Group'),
(12858, 0, 6, 7, 40, 0, 100, 1, 20, 12858, 0, 0, 1, 3, 0, 0, 0, 0, 0, 12, 1, 0, 0, 0, 0, 0, 0, 'Torek - On Reached WP20 - Say Line 3'),
(12858, 0, 7, 0, 61, 0, 100, 0, 0, 0, 0, 0, 15, 6544, 0, 0, 0, 0, 0, 12, 1, 0, 0, 0, 0, 0, 0, 'Torek - On Reached WP20 - Complete Quest'),
(12858, 0, 8, 9, 40, 0, 100, 1, 21, 12858, 0, 0, 1, 4, 0, 0, 0, 0, 0, 12, 1, 0, 0, 0, 0, 0, 0, 'Torek - On Reached WP21 - Say Line 4'),
(12858, 0, 9, 0, 61, 0, 100, 0, 0, 0, 0, 0, 54, 60000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Torek - On Reached WP21 - Pause WP'),
(12858, 0, 10, 11, 40, 0, 100, 0, 22, 12858, 0, 0, 45, 2, 2, 0, 0, 0, 0, 9, 12859, 0, 100, 0, 0, 0, 0, 'Torek - On Reached WP22 - Set Data on Splintertree Raider'),
(12858, 0, 11, 0, 61, 0, 100, 0, 0, 0, 0, 0, 41, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Torek - On Reached WP22 - Despawn'),
(12858, 0, 12, 13, 6, 0, 100, 0, 0, 0, 0, 0, 45, 2, 2, 0, 0, 0, 0, 9, 12859, 0, 200, 0, 0, 0, 0, 'Torek - On Death - Set Data on Splintertree Raider'),
(12858, 0, 13, 0, 61, 0, 100, 0, 0, 0, 0, 0, 6, 6544, 0, 0, 0, 0, 0, 12, 1, 0, 0, 0, 0, 0, 0, 'Torek - On Death - Fail Quest'),
(12858, 0, 14, 0, 9, 0, 100, 0, 0, 5, 15000, 20000, 11, 11977, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 'Torek - IC - Cast Rend'),
(12858, 0, 15, 0, 0, 0, 100, 0, 0, 5000, 20000, 30000, 11, 8078, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 'Torek - IC - Cast Thunderclap'),
(12858, 0, 16, 0, 40, 0, 100, 0, 0, 0, 0, 0, 101, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'On reached WP - Set Home Position'),

(12859, 0, 0, 0, 11, 0, 100, 0, 0, 0, 0, 0, 2, 113, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Splintertree Raider - On Spawn - Set Faction'),
(12859, 0, 1, 2, 38, 0, 100, 0, 1, 1, 0, 0, 2, 83, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Splintertree Raider - On Data Set 1 1 - Set Faction'),
(12859, 0, 2, 3, 61, 0, 100, 0, 0, 0, 0, 0, 91, 8, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Splintertree Raider - On Data Set 1 1 - Set Bytes 1'),
(12859, 0, 3, 4, 61, 0, 100, 0, 0, 0, 0, 0, 8, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Splintertree Raider - On Data Set 1 1 - Set Aggresive'),

(12859, 0, 4, 0, 61, 0, 100, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 19, 12858, 0, 0, 0, 0, 0, 0, 'Splintertree Raider - On Data Set 1 1 - Follow Torek'),
(12859, 0, 5, 0, 7, 0, 100, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 19, 12858, 0, 0, 0, 0, 0, 0, 'Splintertree Raider - On Evade - Follow Torek'),
(12859, 0, 6, 0, 38, 0, 100, 0, 2, 2, 0, 0, 41, 1000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Splintertree Raider - On Data Set 2 2 - Despawn'),
(12859, 0, 7, 0, 1, 0, 100, 0, 1000, 1000, 1000, 1000, 101, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Splintertree Raider - OOC - Set Home Position'),

(1285800, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Torek - Script - Set NPC Flags'),
(1285800, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 2, 83, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Torek - Script - Set Faction'),
(1285800, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 12, 1, 0, 0, 0, 0, 0, 0, 'Torek - Script - Say Line 0'),
(1285800, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 45, 1, 1, 0, 0, 0, 0, 9, 12859, 0, 200, 0, 0, 0, 0, 'Torek - Script - Set Data on Splintertree Raider'),
(1285800, 9, 4, 0, 0, 0, 100, 0, 0, 0, 0, 0, 53, 0, 12858, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 'Torek - Script - Start WP');

DELETE FROM `creature_summon_groups` WHERE `summonerId`=12858;
INSERT INTO `creature_summon_groups` (`summonerId`, `summonerType`, `groupId`, `entry`, `position_x`, `position_y`, `position_z`, `orientation`, `summonType`, `summonTime`) VALUES 
(12858, 0, 0, 12860, 1776.73, -2049.06, 109.83, 1.54, 4, 25000),
(12858, 0, 0, 12896, 1774.64, -2049.41, 109.83, 1.40, 4, 25000),
(12858, 0, 0, 12897, 1778.73, -2049.50, 109.83, 1.67, 4, 25000);

DELETE FROM `script_waypoint` WHERE `entry`=12858;
DELETE FROM `waypoints` WHERE `entry`=12858;
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES 
 (12858, 1, 1788.88, -2240.17, 111.71, ''),
 (12858, 2, 1797.49, -2238.11, 112.31, ''),
 (12858, 3, 1803.83, -2232.77, 111.22, ''),
 (12858, 4, 1806.65, -2217.83, 107.36, ''),
 (12858, 5, 1811.81, -2208.01, 107.45, ''),
 (12858, 6, 1820.85, -2190.82, 100.49, ''),
 (12858, 7, 1829.6, -2177.49, 96.44, ''),
 (12858, 8, 1837.98, -2164.19, 96.71, 'prepare'),
 (12858, 9, 1839.99, -2149.29, 96.78, ''),
 (12858, 10, 1835.14, -2134.98, 96.8, ''),
 (12858, 11, 1823.57, -2118.27, 97.43, ''),
 (12858, 12, 1814.99, -2110.35, 98.38, ''),
 (12858, 13, 1806.6, -2103.09, 99.19, ''),
 (12858, 14, 1798.27, -2095.77, 100.04, ''),
 (12858, 15, 1783.59, -2079.92, 100.81, ''),
 (12858, 16, 1776.79, -2069.48, 101.77, ''),
 (12858, 17, 1776.82, -2054.59, 109.82, ''),
 (12858, 18, 1776.88, -2047.56, 109.83, ''),
 (12858, 19, 1776.86, -2036.55, 109.83, ''),
 (12858, 20, 1776.9, -2024.56, 109.83,  'win'),
 (12858, 21, 1776.87, -2028.31, 109.83, 'stay'),
 (12858, 22, 1776.9, -2028.3, 109.83, '');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_05_02.sql 
-- -------------------------------------------------------- 
-- Add Quest rewards for quest  Teron Gorefiend, I am...
UPDATE `quest_template` SET `RewardChoiceItemId1`=31104, `RewardChoiceItemId2`=31110, `RewardChoiceItemId3`=31109, `RewardChoiceItemId4`=31107, `RewardChoiceItemId5`=31106, `RewardChoiceItemId6`=31105 WHERE `Id` IN (10645,10639);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_05_03.sql 
-- -------------------------------------------------------- 
--
UPDATE `creature` SET `spawnMask`=1 WHERE `map`=429;
UPDATE `gameobject` SET `spawnMask`=1 WHERE `map`=429;
-- UPDATE `quest_template` SET `RewardChoiceItemCount1`=1, `RewardChoiceItemCount2`=1, `RewardChoiceItemCount3`=1, `RewardChoiceItemCount4`=1, `RewardChoiceItemCount5`=1, `RewardChoiceItemCount6`=1 WHERE `Id` IN (10645,10639);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_05_04.sql 
-- -------------------------------------------------------- 
UPDATE `creature_template` SET `ainame`='SmartAI', `scriptname`='' WHERE `entry` IN(12256,12255,12254,12253,12252,12251,12249,12244);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN(12256,12255,12254,12253,12252,12251,12249,12244) AND `source_type`=0;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(12256, 0, 0, 1, 8, 0, 100, 0, 19250, 0, 120000, 120000, 33, 12247, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Kill Credit'),
(12255, 0, 0, 1, 8, 0, 100, 0, 19250, 0, 120000, 120000, 33, 12247, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Kill Credit'),
(12254, 0, 0, 1, 8, 0, 100, 0, 19250, 0, 120000, 120000, 33, 12247, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Kill Credit'),
(12253, 0, 0, 1, 8, 0, 100, 0, 19250, 0, 120000, 120000, 33, 12247, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Kill Credit'),
(12252, 0, 0, 1, 8, 0, 100, 0, 19250, 0, 120000, 120000, 33, 12247, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Kill Credit'),
(12251, 0, 0, 1, 8, 0, 100, 0, 19250, 0, 120000, 120000, 33, 12247, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Kill Credit'),
(12249, 0, 0, 1, 8, 0, 100, 0, 19250, 0, 120000, 120000, 33, 12247, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Kill Credit'),
(12244, 0, 0, 1, 8, 0, 100, 0, 19250, 0, 120000, 120000, 33, 12247, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Kill Credit'),
(12256, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 120, 0, 0, 0, 0, 0, 20, 177668, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Despawn Mark of Detonation'),
(12255, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 120, 0, 0, 0, 0, 0, 20, 177668, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Despawn Mark of Detonation'),
(12254, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 120, 0, 0, 0, 0, 0, 20, 177668, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Despawn Mark of Detonation'),
(12253, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 120, 0, 0, 0, 0, 0, 20, 177668, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Despawn Mark of Detonation'),
(12252, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 120, 0, 0, 0, 0, 0, 20, 177668, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Despawn Mark of Detonation'),
(12251, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 120, 0, 0, 0, 0, 0, 20, 177668, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Despawn Mark of Detonation'),
(12249, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 120, 0, 0, 0, 0, 0, 20, 177668, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Despawn Mark of Detonation'),
(12244, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 120, 0, 0, 0, 0, 0, 20, 177668, 0, 0, 0, 0, 0, 0, 'Mark of Detonation - On Spellhit - Despawn Mark of Detonation');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=19250;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(13, 1, 19250, 0, 0, 31, 0, 3, 12256, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Mark of Detonation'),
(13, 1, 19250, 0, 1, 31, 0, 3, 12255, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Mark of Detonation'),
(13, 1, 19250, 0, 2, 31, 0, 3, 12254, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Mark of Detonation'),
(13, 1, 19250, 0, 3, 31, 0, 3, 12253, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Mark of Detonation'),
(13, 1, 19250, 0, 4, 31, 0, 3, 12252, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Mark of Detonation'),
(13, 1, 19250, 0, 5, 31, 0, 3, 12251, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Mark of Detonation'),
(13, 1, 19250, 0, 6, 31, 0, 3, 12249, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Mark of Detonation'),
(13, 1, 19250, 0, 7, 31, 0, 3, 12244, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Mark of Detonation'),
(13, 4, 19250, 0, 0, 31, 0, 3, 12247, 0, 0, 0, 0, '', 'Place Smokeys Mixture targets Scourge Structure');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_05_05.sql 
-- -------------------------------------------------------- 
UPDATE `creature_template` SET `ainame`='SmartAI', `scriptname`='' WHERE `entry` =29217;

DELETE FROM `smart_scripts` WHERE `entryorguid` =29217 AND `source_type`=0;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(29217, 0, 0, 0, 0, 0, 100, 2, 0, 2000, 2000, 4000, 11, 53617, 64, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 'Anub ar Venomancer - On Range - Cast Poison Bolt (Normal)'),
(29217, 0, 1, 0, 0, 0, 100, 4, 0, 2000, 2000, 4000, 11, 59359, 64, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 'Anub ar Venomancer - On Range - Cast Poison Bolt (Heroic)');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_05_07.sql 
-- -------------------------------------------------------- 
-- Pathing for Kor'kron Defender Entry: 19362 'TDB FORMAT' 
SET @NPC := 69082;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-2973.116,`position_y`=2556.282,`position_z`=105.8032 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-2973.116,2556.282,105.8032,0,0,0,0,100,0), -- 13:50:20
(@PATH,2,-2962.696,2547.778,105.8032,0,0,0,0,100,0), -- 13:50:24
(@PATH,3,-2953.723,2551.618,105.8032,0,0,0,0,100,0), -- 13:50:29
(@PATH,4,-2954.077,2563.904,105.8032,0,0,0,0,100,0), -- 13:50:33
(@PATH,5,-2967.127,2567.316,105.8032,0,0,0,0,100,0), -- 13:50:38
(@PATH,6,-2968.788,2567.15,105.8032,0,0,0,0,100,0); -- 13:50:43
-- 0x1C09FC424012E88000001A00019B696A .go -2973.116 2556.282 105.8032

-- Pathing for Kor'kron Defender Entry: 19362 'TDB FORMAT' 
SET @NPC := 69087;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-3089.103,`position_y`=2508.026,`position_z`=83.78547 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-3089.103,2508.026,83.78547,0,0,0,0,100,0), -- 13:47:23
(@PATH,2,-3087.197,2507.859,83.85033,0,0,0,0,100,0), -- 13:47:23
(@PATH,3,-3081.65,2516.463,84.05411,0,0,0,0,100,0), -- 13:47:26
(@PATH,4,-3090.028,2524.237,84.04036,0,0,0,0,100,0), -- 13:47:30
(@PATH,5,-3094.658,2522.566,84.06168,0,0,0,0,100,0), -- 13:47:33
(@PATH,6,-3096.908,2520.316,83.81168,0,0,0,0,100,0), -- 13:47:33
(@PATH,7,-3098.908,2518.066,84.06168,0,0,0,0,100,0), -- 13:47:33
(@PATH,8,-3099.196,2517.561,84.04745,0,0,0,0,100,0), -- 13:47:37
(@PATH,9,-3097.946,2514.811,84.04745,0,0,0,0,100,0), -- 13:47:37
(@PATH,10,-3096.946,2512.061,84.04745,0,0,0,0,100,0); -- 13:47:37
-- 0x1C09FC424012E88000001A00009B6969 .go -3089.103 2508.026 83.78547

DELETE FROM `creature_formations` WHERE `leaderGUID`=69091;
INSERT INTO `creature_formations` (`leaderGUID`, `memberGUID`, `dist`, `angle`, `groupAI`) VALUES
(69091, 69091, 0, 0, 2),
(69091, 69092, 3, 270, 2);

-- Pathing for Kor'kron Defender Entry: 19362 'TDB FORMAT' 
SET @NPC := 69091;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-3146.969,`position_y`=2563.808,`position_z`=61.37704 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-3146.969,2563.808,61.37704,0,0,0,0,100,0), -- 13:54:58
(@PATH,2,-3120.494,2564.844,61.75181,0,0,0,0,100,0), -- 13:54:58
(@PATH,3,-3120.494,2564.844,61.75181,0,0,0,0,100,0), -- 13:54:58
(@PATH,4,-3120.175,2565.024,61.98759,0,0,0,0,100,0), -- 13:55:06
(@PATH,5,-3105.344,2573.74,61.804,0,0,0,0,100,0), -- 13:55:12
(@PATH,6,-3091.726,2572.44,62.09174,0,0,0,0,100,0), -- 13:55:18
(@PATH,7,-3088.27,2558.042,62.11543,0,0,0,0,100,0), -- 13:55:24
(@PATH,8,-3100.597,2545.807,62.11351,0,0,0,0,100,0), -- 13:55:32
(@PATH,9,-3094.525,2537.95,61.9211,0,0,0,0,100,0), -- 13:55:35
(@PATH,10,-3079.098,2537.322,62.08109,0,0,0,0,100,0), -- 13:55:43
(@PATH,11,-3062.423,2502.734,63.59535,0,0,0,0,100,0), -- 13:55:52
(@PATH,12,-3063.66,2507.985,63.10339,0,0,0,0,100,0), -- 13:56:00
(@PATH,13,-3065.486,2517.504,62.46205,0,0,0,0,100,0), -- 13:56:04
(@PATH,14,-3079.187,2537.478,62.18378,0,0,0,0,100,0), -- 13:56:14
(@PATH,15,-3094.9,2537.977,62.13345,0,0,0,0,100,0), -- 13:56:20
(@PATH,16,-3100.354,2544.985,61.87681,0,0,0,0,100,0), -- 13:56:30
(@PATH,17,-3100.963,2545.778,61.87681,0,0,0,0,100,0), -- 13:56:30
(@PATH,18,-3088.325,2558.04,61.854,0,0,0,0,100,0), -- 13:56:30
(@PATH,19,-3088.325,2558.04,61.854,0,0,0,0,100,0), -- 13:56:30
(@PATH,20,-3088.377,2558.176,62.10567,0,0,0,0,100,0), -- 13:56:32
(@PATH,21,-3091.65,2572.547,62.05435,0,0,0,0,100,0), -- 13:56:38
(@PATH,22,-3105.591,2573.866,62.01907,0,0,0,0,100,0), -- 13:56:43
(@PATH,23,-3120.379,2565.042,61.81473,0,0,0,0,100,0), -- 13:56:50
(@PATH,24,-3120.494,2564.844,61.75181,0,0,0,0,100,0); -- 13:57:02
-- 0x1C09FC424012E88000001A00039B696A .go -3146.969 2563.808 61.37704

-- Remove too many spawns
DELETE FROM `creature` WHERE `guid` IN (74221,74220);

-- Pathing for Kor'kron Wyvern Rider Entry: 21153 'TDB FORMAT' 
SET @NPC := 74219;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-3014.283,`position_y`=2567.434,`position_z`=141.6225 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,17722,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-3014.283,2567.434,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,2,-2997.179,2547.184,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,3,-2975.056,2527.355,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,4,-2944.816,2526.003,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,5,-2925.329,2546.514,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,6,-2919.246,2565.872,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,7,-2920.081,2589.365,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,8,-2931.993,2608.762,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,9,-2955.626,2618.87,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,10,-2977.497,2619.666,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,11,-3014.667,2609.543,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,12,-3055.036,2590.46,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,13,-3085.538,2571.631,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,14,-3114.716,2555.922,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,15,-3143.012,2552.032,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,16,-3176.688,2560.458,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,17,-3190.569,2587.816,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,18,-3187.101,2615.266,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,19,-3164.5,2634.338,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,20,-3137.512,2639.929,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,21,-3099.443,2633.703,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,22,-3074.344,2622.916,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,23,-3057.397,2608.769,141.6225,0,0,1,0,100,0), -- 13:51:11
(@PATH,24,-3045.574,2592.941,140.8448,0,0,1,0,100,0), -- 13:51:11
(@PATH,25,-3046.095,2591.678,139.9281,0,0,1,0,100,0), -- 13:51:11
(@PATH,26,-3028.975,2582.002,141.6225,0,0,1,0,100,0); -- 13:51:11
-- 0x1C09FC424014A84000001A00001B6969 .go -3014.283 2567.434 141.6225

DELETE FROM `creature_formations` WHERE `leaderGUID`=69079;
INSERT INTO `creature_formations` (`leaderGUID`, `memberGUID`, `dist`, `angle`, `groupAI`) VALUES
(69079, 69079, 0, 0, 2),
(69079, 69081, 3, 270, 2);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_06_00.sql 
-- -------------------------------------------------------- 
-- Pathing for Barash the Den Mother Entry: 23269 'TDB FORMAT' 
SET @NPC := 40645;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5028.414,`position_y`=385.6584,`position_z`=171.0149 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5028.414,385.6584,171.0149,0,0,1,0,100,0), -- 20:05:15
(@PATH,2,-5037.621,373.1704,171.0173,0,0,1,0,100,0), -- 20:05:18
(@PATH,3,-5051.44,373.3133,171.0181,0,0,1,0,100,0), -- 20:05:20
(@PATH,4,-5055.748,366.6401,171.0181,0,0,1,0,100,0), -- 20:05:21
(@PATH,5,-5058.255,357.087,171.0151,0,0,1,0,100,0), -- 20:05:23
(@PATH,6,-5055.19,345.9996,170.9724,0,0,1,0,100,0), -- 20:05:25
(@PATH,7,-5034.827,346.2221,171.0174,0,0,1,0,100,0), -- 20:05:27
(@PATH,8,-5025.357,353.0934,170.6913,0,0,1,0,100,0), -- 20:05:31
(@PATH,9,-5019.042,361.9999,170.704,0,0,1,0,100,0), -- 20:05:33
(@PATH,10,-5018.177,373.4781,170.964,0,0,1,0,100,0); -- 20:05:34

-- Update position Ja'y Nosliw <Skybreaker General> 22433
UPDATE `creature` SET `position_x`=-5144.404, `position_y`=600.9089, `position_z`=82.75489, `orientation`=6.021386 WHERE  `guid`=78787;

-- Update position Taskmaster Varkule Dragonbreath 23140
UPDATE `creature` SET `position_x`=-5114.439, `position_y`=588.4843, `position_z`=85.87241, `orientation`=3.036873 WHERE  `guid`=51876;

-- Pathing for Arvoar the Rapacious Entry: 23267 'TDB FORMAT' 
SET @NPC := 40619;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5115.436,`position_y`=144.5614,`position_z`=130.1606 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5115.436,144.5614,130.1606,0,0,0,0,100,0), -- 07:19:51
(@PATH,2,-5127.696,139.5475,130.1854,0,0,0,0,100,0), -- 07:19:53
(@PATH,3,-5122.23,120.3736,129.9845,0,0,0,0,100,0), -- 07:19:56
(@PATH,4,-5117.039,117.6734,129.9086,0,0,0,0,100,0), -- 07:19:58
(@PATH,5,-5107.681,117.8557,129.8407,0,0,0,0,100,0), -- 07:19:59
(@PATH,6,-5100.675,122.349,130.0891,0,0,0,0,100,0), -- 07:20:01
(@PATH,7,-5105.304,139.7458,130.1348,0,0,0,0,100,0); -- 07:20:04

-- Update fly speed of Dragonmaw Skybreakers
UPDATE `waypoint_data` SET `move_type`=1 WHERE  `id` IN (782940, 782970, 782910, 782920, 782930, 782940, 782950, 782960, 782970, 782980, 782990, 783000);

-- Pathing for Dragonmaw Transporter Entry: 23188 'TDB FORMAT' 
SET @NPC := 132814;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-4584.821,`position_y`=56.05914,`position_z`=260.3116 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,16314,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-4584.821,56.05914,260.3116,0,0,1,0,100,0), -- 07:28:22
(@PATH,2,-4554.921,74.39236,250.4782,0,0,1,0,100,0), -- 07:28:22
(@PATH,3,-4532.374,72.2015,243.3948,0,0,1,0,100,0), -- 07:28:22
(@PATH,4,-4508.559,67.28505,241.6727,0,0,1,0,100,0), -- 07:28:22
(@PATH,5,-4487.302,56.6492,242.2004,0,0,1,0,100,0), -- 07:28:22
(@PATH,6,-4470.297,28.84614,244.756,0,0,1,0,100,0), -- 07:28:22
(@PATH,7,-4461.818,-8.348633,247.9227,0,0,1,0,100,0), -- 07:28:22
(@PATH,8,-4477.97,-44.38118,242.3393,0,0,1,0,100,0), -- 07:28:22
(@PATH,9,-4516.678,-59.03483,240.2283,0,0,1,0,100,0), -- 07:28:22
(@PATH,10,-4543.176,-60.92849,240.2838,0,0,1,0,100,0), -- 07:28:22
(@PATH,11,-4566.225,-51.06413,247.3116,0,0,1,0,100,0), -- 07:28:22
(@PATH,12,-4581.653,-29.58301,254.2004,0,0,1,0,100,0), -- 07:28:22
(@PATH,13,-4591.782,0.532661,260.3116,0,0,1,0,100,0), -- 07:28:22
(@PATH,14,-4594.208,28.59668,260.3116,0,0,1,0,100,0); -- 07:28:22

-- Pathing for Dragonmaw Transporter Entry: 23188 'TDB FORMAT' 
SET @NPC := 132818;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-4735.987,`position_y`=122.2487,`position_z`=106.433 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,16314,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-4735.987,122.2487,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,2,-4744.92,149.3799,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,3,-4751.057,182.3161,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,4,-4767.275,216.0688,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,5,-4776.866,248.6194,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,6,-4781.172,280.7835,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,7,-4783.174,319.2545,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,8,-4752.483,324.3357,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,9,-4729.643,316.453,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,10,-4715.97,309.7697,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,11,-4709.104,290.6015,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,12,-4710.651,264.7013,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,13,-4709.572,256.6543,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,14,-4706.147,218.4914,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,15,-4709.459,193.1469,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,16,-4704.721,157.6649,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,17,-4695.498,116.758,106.433,0,0,1,0,100,0), -- 07:15:27
(@PATH,18,-4713.938,87.04405,101.2085,0,0,1,0,100,0), -- 07:15:27
(@PATH,19,-4726.667,100.212,106.433,0,0,1,0,100,0); -- 07:15:27
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_06_01.sql 
-- -------------------------------------------------------- 
DELETE FROM `page_text` WHERE `ID` IN (3587, 3588, 3589, 3590, 3591, 3592, 3593, 3581, 3582, 3584, 3585, 3586, 3583);
INSERT INTO `page_text` (`ID`, `Text`, `NextPageID`, `VerifiedBuild`) VALUES
(3587, 'How to date a Dwarven woman:

1. Ask her to buy you a drink.', 0, 19831), -- 3587
(3588, 'Let''s be honest. Since the end of the Third War, Night Elf girls have heard it all.  In fact, they''d already heard it all long before you or I was born. 

If you want to engage the mind of a Night Elf girl, you''re going to have to stand out. Sure, we''ve all heard the tales of Night Elf lasses dancing on mailboxes and stripping to pay for Nightsaber training. True or not, if you want to light that lovely lady''s lips up with a smile, you gotta be unique, memorable and confident. 

Start off by showing that you''re looking for more than a gal with looks. Sure, she can bounce, she can dance, but can she hold a decent conversation?  Does she even understand the proper use of a samophlange? Does she know how to have a fun time?

There''s nothing worse than bringing a Night Elf to a party, only to watch her stand awkwardly by herself, breaking conversation only to lament the loss of her Highborne sister during the War of the Ancients.', 0, 19831), -- 3588
(3589, 'There''s nothing like wooing the heart of a human girl. Infinitely forgiving, endlessly caring and fantastically fun, human girls have been the downfall of countless heroes throughout the ages. (See Chapter 3: "Jaina Proudmoore and the men who loved her")

However, generation after generation has proven it takes something more than just money, looks or an epic suit of armor to attract the woman of your dreams. Not even the power of Gnomish invention can help you here. 

To truly charm the heart of another, you should possess these qualities.

* Be Fun & Friendly
* Be a Challenge
* Be a Man

', 3590, 19831), -- 3589
(3590, 'Be Fun & Friendly

Ever have at friend who shows up at your house and brings everybody down? Yep. Everyone does. Does he get invited back to the parties? Not unless he''s bringing the ale. If you want to be an attractive person, live an attractive lifestyle. 

You''ll find that the more you enjoy socializing with others, the more they will enjoy socializing with you! 

There''s no faster way to ruin a girl''s night than bringing in that needy, apologetic vibe. Let it go, embrace the fun and your confidence will soar.', 3591, 19831), -- 3590
(3591, 'Be a Challenge

Too many Gnomes these days walk with their head slung low, shuffling along sadly from tavern to tavern, hopelessly holding on these limiting beliefs that no human girl would ever want them. They rush into the tavern, shower her with compliments and free drinks, then go home dejected. 

Well, let me be the first to tell you, friend, when you give your self away so cheaply, you diminsh the value of your unique, exquisite personality. You''ve been told by society that you are not the prize, that women will never acknowledge you, that you must beg for love and attention.

They are wrong. Don''t give your affection away so freely. Instead of asking yourself, "Does she like me?" ask yourself instead, "Do I like her?" 

Challenge her - show her you''re looking for a girl who offers more than a pretty face. If she can''t keep up with your life, move on. If she shows she''s got something to offer, you''re at the beginning of a beautiful thing. ', 3592, 19831), -- 3591
(3592, 'Be a Man

In an age where we''ve been banished from our homes, forced to fight for our very survival and faced down horrors never before known to Azeroth, you would think that the nature of manhood would be better understood. 

Sadly, the art of masculinity has been lost, washed away in the glitz and glamor of battle. However, all is not lost! With practice and confidence, you can come back in touch with yourself. 

Once you''ve met the human girl of your dreams and she''s shown herself to be worthy of your affections:

* Take the lead - show her everything that is beautiful about your world
* Hold her hand - develop a close, affectionate relationship
* Show respect - for yourself, for her and for the world around you

Above all:

* Be responsible
', 3593, 19831), -- 3592
(3593, 'Troubleshooting

While everything doesn''t always go the way you expect, that''s what make life unpredictable and exciting. However, there''s a few tips to help you a long way in improving your life. Here''s some common blunders:

* Don''t chase
* Don''t be needy
* Don''t get stuck on someone who dislikes you

These all stem from a core belief that women you must have the woman you''re talking to right now. Break free - there''s many women out there in this beautiful world and if one doesn''t work out, let go. You''ll find you become more attractive the less clingy you are. 
', 0, 19831), -- 3593
(3581, '"Roleplaying"

Good roleplaying skills are essential. No Genius Gnomish gal wants a giant bore. Regale her with tales of your future cross-continental adventures:

  "You and me, babe, we''re gonna fly to Kalimdor, etch our names into the side of Teldrassil and spend the rest of our lives swinging from the trees in Un''goro Crater."

"Storytelling"

Share stores of your exciting future together! The more implausible, the better. Nothing gets a Gnomish girl excited like an ambitious plan. It also makes for great conversation starters!

  "With our brilliant minds combined, we could retake Gnomeregan. ... why haven''t we retaken Gnomeregan anyways?"', 3582, 19831), -- 3581
(3582, '"Teasing"

Teasing is the art of making fun of a woman in a humorous way. Be careful, you can easily go too far. Calling her a "Goblin Ganking Gnat" will leave you walking home wearing that Green Gordok Grog you just bought. Try something a bit more subtle. 

If she acts childish and refuses to stop jumping onto tables in the middle of the bar try:

"I hear there''s an opening for star entertainer at the Stormwind orphanage."

If she won''t stop talking about herself playfully try:

"Where''s your off switch?"

IMPORTANT NOTE: Gnomish girls do NOT have an off switch. Attempting to find it may result in the loss of life, limbs or wallet.', 0, 19831), -- 3582
(3584, 'I''d just arrived off the boat to Azuremyst when I stopped a group of Draenei dames around the ripe young age of 230. They were laughing and having a great time. What luck, still on the docks and I''d found exactly the type of fun, energetic Draenei women I wanted to meet on this trip. 

At first a bit anxious, I breathed deeply and reminded myself, "they too are here in Azeroth on vacation, looking to meet new people and have a great time."

Sure enough, they were delighted to see one devilishly charming Gnome like myself in the Exodar. It even turned out we were both staying near the Vault of Lights. We exchanged deep, penetrating glances into each others eyes, promising to meet again near A''dal later that night. ', 3585, 19831), -- 3584
(3585, 'The next day, I met another beautiful Draenei woman - the gleam of her horns gave only the gentlest of glimpses into her refined tastes. I asked why she was visiting the Exodar, when in shock, she told me she wasn''t visiting - she lived here. 

The words I spoke to myself before returned to my mind: 

"She is on vacation, looking to meet amazing people and have a great time."  

I had it all wrong, she wasn''t on vacation at all! Then suddenly, the grinding gears of my mind clicked:  I''d been claiming to myself that they were so receptive because they were on vacation. What a gnollish excuse! 

I''m a fun, interesting guy who any sensible minded girl would love to group with, on vacation or not! Now when I meet Draenei girls, I remind myself of the simple truth:

"She too is looking to meet people and have a great time."', 3586, 19831), -- 3585
(3586, 'The following 497 pages of this dictionary consistent entirely of oddly angled pictures taken from a Super Snapper FX 2000. ', 0, 19831), -- 3586
(3583, 'Are your tastes more exotic? 
Do you desire someone a little out of this world? 
Are hooves your thing, but succubi a little too much for you?

Read on, my friend...', 3584, 19831); -- 3583

UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid` IN(20349,20345,20346,20347,20348) AND `source_type`=0 AND `id`=0 AND `link`=1;
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=4498 AND `source_type`=2 AND `id`=4 AND `link`=5;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_06_02.sql 
-- -------------------------------------------------------- 
UPDATE `creature_template` SET `AIName` = 'SmartAI' WHERE `entry` = 16222;

DELETE FROM `smart_scripts` WHERE `entryorguid` = 16222;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
-- Eye (Paladin's only)
(16222,0,0,1,22,0,100,0,38,5000,5000,0,66,0,0,0,0,0,0,7,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Receive Emote Eye (Paladin Only) - Set Orientation Invoker'),
(16222,0,1,15,61,0,100,0,0,0,0,0,67,1,1000,1000,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Event Link - Create Timed Event 1'),
(16222,0,2,0,59,0,100,0,1,0,0,0,5,16,0,0,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Timed Event - Play Emote Kneel'),
-- Kiss
(16222,0,3,4,22,0,100,0,58,5000,5000,0,66,0,0,0,0,0,0,7,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Receive Emote Kiss - Set Orientation Invoker'),
(16222,0,4,15,61,0,100,0,0,0,0,0,67,2,1000,1000,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Event Link - Create Timed Event 2'),
(16222,0,5,0,59,0,100,0,2,0,0,0,5,2,0,0,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Timed Event - Play Emote Bow'),
-- Salute
(16222,0,6,7,22,0,100,0,78,5000,5000,0,66,0,0,0,0,0,0,7,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Receive Emote Salute - Set Orientation Invoker'),
(16222,0,7,15,61,0,100,0,0,0,0,0,67,3,1000,1000,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Event Link - Create Timed Event 3'),
(16222,0,8,0,59,0,100,0,3,0,0,0,5,66,0,0,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Timed Event - Play Emote Salute'),
-- Rude
(16222,0,9,10,22,0,100,0,77,5000,5000,0,66,0,0,0,0,0,0,7,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Receive Emote Rude - Set Orientation Invoker'),
(16222,0,10,15,61,0,100,0,0,0,0,0,67,4,1000,1000,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Event Link - Create Timed Event 4'),
(16222,0,11,0,59,0,100,0,4,0,0,0,5,25,0,0,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Timed Event - Play Emote Poinit'),
-- Shy
(16222,0,12,13,22,0,100,0,84,5000,5000,0,66,0,0,0,0,0,0,7,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Receive Emote Shy - Set Orientation Invoker'),
(16222,0,13,15,61,0,100,0,0,0,0,0,67,5,1000,1000,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Event Link - Create Timed Event 5'),
(16222,0,14,0,59,0,100,0,5,0,0,0,5,23,0,0,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Timed Event - Play Emote Flex'),
-- Clean up
(16222,0,15,0,61,0,100,0,0,0,0,0,67,6,5000,5000,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Event Link - Create Timed Event 6'),
(16222,0,16,0,59,0,100,0,6,0,0,0,66,0,0,0,0,0,100,1,0,0,0,0,0,0,0,'Silvermoon City Guardian - On Timed Event - Reset Orientation');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 22 AND `SourceEntry` = 16222;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(22, 1, 16222, 0, 0, 15, 0, 2, 0, 0, 0, 0, 0, '', 'Silvermoon Guardian - Only Kneel to Paladins');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_06_03.sql 
-- -------------------------------------------------------- 
UPDATE `gameobject_template` SET `AIName`='', `ScriptName`='', `data2`=13685, `data3`=3000, `data5`=1 WHERE `entry`=184729;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=21319;

DELETE FROM `event_scripts` WHERE id IN (13685);
INSERT INTO `event_scripts` (`id`, `delay`, `command`, `datalong`, `datalong2`, `dataint`, `x`, `y`, `z`, `o`) VALUES
(13685, 1, 10, 21319, 90000, 0, 1316.469, 6686.669, -18.59028, 1.072638);

DELETE FROM `smart_scripts` WHERE `entryorguid`=184729 AND `source_type`=1;
DELETE FROM `smart_scripts` WHERE `entryorguid`=21319 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(21319, 0, 0, 1, 63, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 21, 20, 0, 0, 0, 0, 0,0,'Gor Grimgut- Just Summoned - Talk'),
(21319, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 0, 0, 21, 20, 0, 0, 0, 0, 0,0,'Gor Grimgut- Just Summoned - Attack'),
(21319, 0, 2, 0, 0, 0, 100, 0, 3000, 5000, 7000, 10000, 75, 35492, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0,0,'Gor Grimgut - In Combat - Cast Exhaustion'),
(21319, 0, 3, 0, 0, 0, 100, 0, 10000, 12000, 12000, 15000, 11, 35491, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0,0,'Gor Grimgut - In Combat - Cast Furious Rage');

DELETE FROM `creature_text` WHERE `entry` IN (21319);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES 
(21319, 0, 0, 'Puny $r cannot lift spear.  Gor lift spear!', 12, 0, 100, 0, 0, 0, 18980, 0, 'Gor Grimgut'),
(21319, 0, 1, 'Hah!  The Thunderspike is mine.  Die!', 12, 0, 100, 0, 0, 0, 18979, 0, 'Gor Grimgut');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_06_05.sql 
-- -------------------------------------------------------- 
SET @OGUID     := 5596; -- 1 free guid set by TC
SET @QUEST     := 12326;
SET @TANK      := 27587; -- Alliance Steam Tank
SET @ELITE     := 27588; -- 7th Legion Elite
SET @ENGI      := 27163; -- 7th Legion Siege Engineer
SET @SPELL_1   := 49315; -- Ice Cannon
SET @SPELL_2   := 49333; -- Ice Prison
SET @SPELL_3   := 49109; -- Drop Off Gnome
SET @SPELL_4   := 49081; -- Drop Off Soldier

DELETE FROM `gameobject` WHERE `guid` = @OGUID;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `PhaseId`, `PhaseGroup`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) VALUES
(@OGUID+0, 189331, 571, 1, 1, 1, 3707.753418, -1182.235840, 120.745689, 1.24532, 0, 0, 0, 1, 120, 0, 1);

UPDATE `creature_template` SET `npcflag`=16777216, `unit_flags`=32768, `spell1`=49315, `spell2`=49333, `spell3`=49109, `spell4`=49081 WHERE `entry`=@TANK;
UPDATE `creature_template` SET `faction` = 1975 WHERE `entry`=27335;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` IN (@ENGI, 27607, @TANK);

DELETE FROM `npc_spellclick_spells` WHERE `npc_entry`=@TANK;
INSERT INTO `npc_spellclick_spells`(`npc_entry`,`spell_id`,`cast_flags`,`user_type`) VALUES
(@TANK,49078,1,0), -- player
(@TANK,46598,1,1); -- npc

DELETE FROM `vehicle_template_accessory` WHERE `entry`=@TANK;
INSERT INTO `vehicle_template_accessory`(`entry`,`accessory_entry`,`seat_id`,`minion`,`description`,`summontype`,`summontimer`) VALUES
(@TANK,@ENGI,1,1,"7th Legion Siege Engineer Rides Alliance Steam Tank",5,0),
(@TANK,@ELITE,2,1,"7th Legion Elite Rides Alliance Steam Tank",5,0),
(@TANK,@ELITE,3,1,"7th Legion Elite Rides Alliance Steam Tank",5,0);
DELETE FROM `creature` WHERE `id` IN (27163,27588);

DELETE FROM `conditions` WHERE (`SourceTypeOrReferenceId`=16 AND `SourceEntry`=@TANK) OR (`SourceTypeOrReferenceId`=13 AND `SourceEntry` IN (@SPELL_2,@SPELL_3))  OR (`SourceTypeOrReferenceId`=18 AND `SourceEntry` IN (46598,49078));
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(16,0,@TANK,0,0,23,0,4246,0,0,0,0,'','Dismount player when not in intended zone'),
(16,0,@TANK,0,1,23,0,4190,0,0,0,0,'','Dismount player when not in intended zone'),
(16,0,@TANK,0,2,23,0,4188,0,0,0,0,'','Dismount player when not in intended zone'),
(13,1,@SPELL_2,0,0,31,0,3,27288,0,0,0,'','Ice Prison can target Death Knight Champion'),
(13,1,@SPELL_2,0,1,31,0,3,27410,0,0,0,'','Ice Prison can target Scourge SeigeSmith'),
(13,1,@SPELL_2,0,2,31,0,3,27286,0,0,0,'','Ice Prison can target Dreadbone Invader'),
(13,1,@SPELL_2,0,3,31,0,3,27283,0,0,0,'','Ice Prison can target Risen Winterguarde Mage'),
(13,2,@SPELL_3,0,0,31,0,3,@ENGI,0,0,0,'','Drop Off Gnome target 7th Legion Siege Engineer'),
(18,@TANK,46598,0,0,31,0,3,0,0,0,0,'','Only npc for spellclick'),
(18,@TANK,49078,0,0,9,0,@QUEST,0,0,0,0,'','Required quest active for spellclick');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=22 AND `SourceGroup`=3 AND `SourceEntry`=@TANK;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`, `ConditionTarget`, `ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`, `ErrorTextId`,`ScriptName`,`Comment`) VALUES
(22,3,@TANK,0,1,1,49078,0,0,0,'','event require aura 49078');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=22 AND `SourceGroup`=1 AND `SourceEntry`=@ENGI;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`, `ConditionTarget`, `ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`, `ErrorTextId`,`ScriptName`,`Comment`) VALUES
(22,1,@ENGI,0,1,29,27607,14,0,0,'','event require creature distance');

DELETE FROM `creature_template_addon` WHERE `entry` IN (@ENGI,@ELITE);
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(@ENGI,0,0,0,257,133,NULL),
(@ELITE,0,0,0,257,333,NULL);

DELETE FROM `smart_scripts` WHERE `entryorguid` = 27607  AND `source_type` = 0;
DELETE FROM `smart_scripts` WHERE `entryorguid` = 27607*100  AND `source_type` = 9;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @TANK  AND `source_type` = 0;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @TANK*100  AND `source_type` = 9;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @ENGI  AND `source_type` = 0;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @ENGI*100  AND `source_type` = 9;
DELETE FROM `smart_scripts` WHERE `entryorguid` = @ENGI*100+1  AND `source_type` = 9;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(@ENGI, 0, 0, 0, 8, 0, 100, 0, @SPELL_3, 0, 0, 0, 80, @ENGI*100, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - On spell hit - action list'),
(@ENGI*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Set visible off'),
(@ENGI*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 12, @ENGI, 3, 18000, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Summon'),
(@ENGI*100, 9, 2, 0, 0, 0, 100, 0, 15000, 15000, 0, 0, 47, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Set visible on'),
(@ENGI, 0, 1, 0, 23, 0, 100, 1, 46598, 0, 0, 0, 80, @ENGI*100+1, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - Has aura - action list'),
(@ENGI*100+1, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Talk'),
(@ENGI*100+1, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - React passif'),
(@ENGI*100+1, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 29, 3, 0, 0, 0, 0, 0, 19, 27607, 15, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - follow'),
(@ENGI*100+1, 9, 3, 0, 0, 0, 100, 0, 3000, 3000, 0, 0, 11, 49114, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Cast to summon a GO'),
(@ENGI*100+1, 9, 4, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Talk'),
(@ENGI*100+1, 9, 5, 0, 0, 0, 100, 0, 5000, 5000, 0, 0, 75, 49215, 0, 0, 0, 0, 0, 19, 27607, 15, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Add aura'),
(@ENGI*100+1, 9, 6, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Talk'),
(@ENGI*100+1, 9, 7, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 29, 1, 0, 0, 0, 0, 0, 19, @TANK, 15, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - follow'),
(@TANK,0,0,0,38,0,100,0,0,1,0,0,11,49122,0,0,0,0,0,23,0,0,0,0,0,0,0,"Tank - On data set- cast spell credit"),
(@TANK,0,1,0,25,0,100,0,0,0,0,0,8,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tank - On reset- react passif"),
(@TANK,0,2,0,1,0,100,0,30000,30000,50000,50000,1,1,0,0,0,0,0,19,@ELITE,10,0,0,0,0,0,'Tank - OOC - Talk'),
(@TANK,0,4,0,25,0,100,0,0,0,0,0,59,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tank - On reset- Set Run on"),
(@TANK, 0, 3, 0, 8, 0, 100, 0, @SPELL_4, 0, 0, 0, 80, @TANK*100, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - On spell hit - action list'),
(@TANK*100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 11, @ELITE, 10, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Set visible off'),
(@TANK*100, 9, 1, 0, 0, 0, 100, 0, 4000, 4000, 0, 0, 1, 2, 0, 0, 0, 0, 0, 11, @ELITE, 10, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Summon'),
(@TANK*100, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 41, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '7th Legion Siege Engineer - action list - Set visible on'),
(27607, 0, 1, 0, 23, 0, 100, 1, 49215, 1, 0, 0, 80, 27607*100, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Plague Wagon - Has aura - action list'),
(27607*100, 9, 0, 0, 0, 0, 100, 0, 11000, 11000, 0, 0, 45, 0, 1, 0, 0, 0, 0, 19, @TANK, 30, 0, 0, 0, 0, 0, 'Plague Wagon - action list - Set data'),
(27607*100, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 41, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Plague Wagon - action list - despawn');

DELETE FROM `creature_text` WHERE `entry` IN (@ELITE, @ENGI);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES 
(@ELITE, 0, 0, 'Without you we''d be lost. Thanks for the ride!', 12, 7, 100, 0, 0, 0, 26844, 0, '7th Legion Elite'),
(@ELITE, 0, 1, 'For the Alliance and Lord Fordragon!', 12, 7, 100, 0, 0, 0, 26841, 0, '7th Legion Elite'),
(@ELITE, 0, 2, 'HOO-WAH! The cavalry has arrived!', 12, 7, 100, 0, 0, 0, 26842, 0, '7th Legion Elite'),
(@ELITE, 0, 3, 'Great driving, soldier! Not a scratch on us!', 12, 7, 100, 0, 0, 0, 26843, 0, '7th Legion Elite'),
(@ELITE, 1, 0, 'I think I see one of their plague wagons!', 12, 0, 100, 0, 0, 0, 26971, 0, '7th Legion Elite'),
(@ELITE, 1, 1, 'Did I ever tell you soldiers about the time I destroyed the Thandol Span? Yep, weren''t no dark irons destroyed that bridge! Was me!', 12, 7, 100, 0, 0, 0, 26972, 0, '7th Legion Elite'),
(@ELITE, 1, 2, 'It doesn''t make any sense. Why don''t they just fly Naxxramas over Wintergarde Keep and blow it up? I mean, that''s what I would do if I were Kel''Thuzad.', 12, 7, 100, 0, 0, 0, 26973, 0, '7th Legion Elite'),
(@ELITE, 1, 3, 'I wonder if we''ll ever solve the mystery of the strange ore. There''s gotta be some use for it!', 12, 0, 100, 0, 0, 0, 26974, 0, '7th Legion Elite'),
(@ELITE, 1, 4, 'Hey, do any of you know McGoyver over at Valgarde? He''s my uncle. You know what his title is? Pro. Yea, just "Pro." I want to be a pro too.', 12, 0, 100, 0, 0, 0, 26975, 0, '7th Legion Elite'),
(@ELITE, 1, 5, 'Something straight up stinks in here! It''s definitely not me. Gnomes smell like butter and sunshine. Not like those dwarves that smell like they were born from a trogg''s armpit! None of you are dwarves, are you?', 12, 0, 100, 0, 0, 0, 26976, 0, '7th Legion Elite'),
(@ELITE, 1, 6, 'I used to know a dwarf that claimed gnomes evolved from beneath the earth. That we all started out as sand gnomes. What a load of nonsense! Sand gnomes? PUH-LEASE!', 12, 0, 100, 0, 0, 0, 26977, 0, '7th Legion Elite'),
(@ELITE, 1, 7, 'I''ve never seen destruction like this...', 12, 0, 100, 0, 0, 0, 26978, 0, '7th Legion Elite'),
(@ELITE, 1, 8, 'Keep the chatter down, people. We need to stay alert!', 12, 0, 100, 0, 0, 0, 26979, 0, '7th Legion Elite'),
(@ELITE, 1, 9, 'The smell of death covers every inch of this place.', 12, 0, 100, 0, 0, 0, 26980, 0, '7th Legion Elite'),
(@ELITE, 1, 10, 'Driver, I hear you single handedly airlifted our villagers out of this hell-hole. Is that true?', 12, 0, 100, 0, 0, 0, 26981, 0, '7th Legion Elite'),
(@ELITE, 1, 11, 'So does anyone know anything about Thel''zan? Who is he? How did he come to leading the Scourge armies on the ground?', 12, 0, 100, 0, 0, 0, 26982, 0, '7th Legion Elite'),
(@ELITE, 1, 12, 'Look at this mess... The Scourge will pay for this!', 12, 0, 100, 0, 0, 0, 26983, 0, '7th Legion Elite'),
(@ELITE, 1, 13, 'Where are all the corpses? What have they done to our people?', 12, 0, 100, 0, 0, 0, 26984, 0, '7th Legion Elite'),
(@ELITE, 1, 14, 'I hope I''m alive to see Arthas get what''s coming to him.', 12, 0, 100, 0, 0, 0, 26985, 0, '7th Legion Elite'),
(@ELITE, 2, 0, 'Reporting for duty, sir!', 12, 7, 100, 0, 0, 0, 26954, 0, '7th Legion Elite'),
(@ENGI, 0, 1, 'Keep ''em off me for about 15 seconds and this thing is as good as dead.', 12, 7, 100, 0, 0, 0, 26854, 0, '7th Legion Siege Engineer'),
(@ENGI, 0, 2, 'Cover me!', 12, 7, 100, 0, 0, 0, 26852, 0, '7th Legion Siege Engineer'),
(@ENGI, 0, 3, 'When I''m done with this plague wagon it''ll look like a goblin built it! Keep me safe!', 12, 7, 100, 0, 0, 0, 26855, 0, '7th Legion Siege Engineer'),
(@ENGI, 0, 4, 'Keep the bad guys off me!', 12, 7, 100, 0, 0, 0, 26853, 0, '7th Legion Siege Engineer'),
(@ENGI, 1, 0, '%s deftly assembles a strange machine.', 16, 7, 100, 0, 0, 0, 26856, 0, '7th Legion Siege Engineer'),
(@ENGI, 2, 0, 'That oughta do it! Just a few more seconds now.', 12, 7, 100, 0, 0, 0, 26858, 0, '7th Legion Siege Engineer');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_07_00.sql 
-- -------------------------------------------------------- 
UPDATE `creature_template` SET `gossip_menu_id`=5708, `npcflag`=1 WHERE  `entry`=14353;
UPDATE `creature_template` SET `gossip_menu_id`=5733, `npcflag`=1 WHERE  `entry`=14323;
UPDATE `creature_template` SET `gossip_menu_id`=5746, `npcflag`=1 WHERE  `entry`=11441;
UPDATE `creature_template` SET `gossip_menu_id`=5735, `npcflag`=1 WHERE  `entry`=14326;

DELETE FROM `gossip_menu` WHERE `entry` IN(5708,5715,5740,5733,5746,5735);
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES 
(5708, 6876), 
(5708, 6895), 
(5715, 6882), 
(5740, 6916), 
(5733, 6905), 
(5746, 6922), 
(5735, 6907); 

DELETE FROM `gossip_menu_option` WHERE `menu_id` IN(5708,5715,5740,5733,5746,5735);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES 
(5708, 0, 0, 'I''m the new king?  What are you talking about?', 9365, 1, 1, 5715, 0, 0, 0, NULL, 0), -- Entry: 14353
(5708, 1, 0, 'Henchmen?  Tribute?', 9440, 1, 1, 5740, 0, 0, 0, NULL, 0), -- Entry: 14353
(5715, 0, 0, 'It''s good to be the king!  Now, let''s get back to what you were talking about before...', 9441, 1, 1, 0, 0, 0, 0, NULL, 0), -- Entry: 14353
(5740, 0, 0, 'Well then... show me the tribute!', 9367, 1, 1, 0, 0, 0, 0, NULL, 0); -- Entry: 14353
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_07_02.sql 
-- -------------------------------------------------------- 
-- Cleanup on Dragonmaw Ascendant!
DELETE FROM `creature` WHERE `guid` IN (52283, 52284, 52285, 52286, 52287, 52288, 52289, 52290, 52291, 52292, 52293, 52294, 52295, 52296, 52297, 52298, 52299, 52300, 52301, 52302, 52303, 52304, 52305, 52306, 52307, 52308, 52309, 52310, 52311, 52312, 52313, 52314, 52315, 52316, 52317, 52318, 52319, 52320, 52321, 52322, 52323, 52324, 52325, 52326, 52327, 52328, 52329, 52332, 52333);

-- Update Pos
UPDATE `creature` SET `position_x`=-4932.889, `position_y`=22.40549, `position_z`=62.24448, `orientation`=3.612832, `MovementType`=0 WHERE  `guid`=52330;
UPDATE `creature` SET `position_x`=-4941.292, `position_y`=35.20237, `position_z`=62.73532, `orientation`=3.612832, `MovementType`=0 WHERE  `guid`=52331;

-- Pathing for Dragonmaw Ascendant Entry: 22253 'TDB FORMAT' 
SET @NPC := 52275;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5195.857,`position_y`=90.12917,`position_z`=70.22656 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5195.857,90.12917,70.22656,0,0,0,0,100,0), -- 20:28:25
(@PATH,2,-5191.49,106.8146,71.93172,0,0,0,0,100,0), -- 20:28:37
(@PATH,3,-5195.716,138.8907,71.36693,0,0,0,0,100,0), -- 20:28:39
(@PATH,4,-5200.492,147.7713,70.40874,0,0,0,0,100,0), -- 20:28:47
(@PATH,5,-5209.126,186.8668,72.33409,0,0,0,0,100,0), -- 20:28:51
(@PATH,6,-5208.013,200.6249,73.10587,0,0,0,0,100,0), -- 20:28:59
(@PATH,7,-5201.791,256.0194,71.98375,0,0,0,0,100,0), -- 20:29:08
(@PATH,8,-5188.366,291.6975,73.16975,0,0,0,0,100,0), -- 20:29:12
(@PATH,9,-5182.603,302.7376,73.64639,0,0,0,0,100,0), -- 20:29:21
(@PATH,10,-5176.891,356.321,72.7616,0,0,0,0,100,0), -- 20:29:28
(@PATH,11,-5182.551,367.8651,72.34415,0,0,0,0,100,0), -- 20:29:34
(@PATH,12,-5180.885,365.6959,72.57543,0,0,0,0,100,0), -- 20:29:42
(@PATH,13,-5179.66,364.4311,72.66841,0,0,0,0,100,0), -- 20:29:46
(@PATH,14,-5174.673,325.8635,73.72121,0,0,0,0,100,0), -- 20:29:53
(@PATH,15,-5198.33,272.533,72.0965,0,0,0,0,100,0), -- 20:30:00
(@PATH,16,-5200.696,243.5709,72.67114,0,0,0,0,100,0), -- 20:30:07
(@PATH,17,-5201.835,233.1574,73.37646,0,0,0,0,100,0), -- 20:30:13
(@PATH,18,-5210.627,167.8349,70.08221,0,0,0,0,100,0), -- 20:30:20
(@PATH,19,-5198.43,145.466,70.45454,0,0,0,0,100,0), -- 20:30:29
(@PATH,20,-5195.288,134.2268,72.13242,0,0,0,0,100,0), -- 20:30:34
(@PATH,21,-5195.829,90.17783,70.22305,0,0,0,0,100,0); -- 20:30:41
-- 0x1C09FC424015BB4000001A00002191E8 .go -5195.857 90.12917 70.22656

-- Pathing for Dragonmaw Ascendant Entry: 22253 'TDB FORMAT' 
SET @NPC := 52276;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5218.906,`position_y`=565.3663,`position_z`=50.85233 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5218.906,565.3663,50.85233,0,0,0,0,100,0), -- 06:57:51
(@PATH,2,-5208.237,539.7261,64.97557,0,0,0,0,100,0), -- 06:58:07
(@PATH,3,-5207.74,528.3329,71.45394,0,0,0,0,100,0), -- 06:58:12
(@PATH,4,-5203.943,505.6842,74.86707,0,0,0,0,100,0), -- 06:58:15
(@PATH,5,-5201.894,495.4175,74.75183,0,0,0,0,100,0), -- 06:58:22
(@PATH,6,-5200.353,477.0113,74.51066,0,0,0,0,100,0), -- 06:58:25
(@PATH,7,-5196.964,442.6382,74.37379,0,0,0,0,100,0), -- 06:58:31
(@PATH,8,-5206.846,402.5792,75.1168,0,0,0,0,100,0), -- 06:58:37
(@PATH,9,-5195.471,385.1235,72.39382,0,0,0,0,100,0), -- 06:58:41
(@PATH,10,-5175.989,405.5393,74.1407,0,0,0,0,100,0), -- 06:58:46
(@PATH,11,-5170.418,417.7219,75.94295,0,0,0,0,100,0), -- 06:58:52
(@PATH,12,-5166.691,432.0739,76.86201,0,0,0,0,100,0), -- 06:58:55
(@PATH,13,-5163.314,446.5266,77.07475,0,0,0,0,100,0), -- 06:59:00
(@PATH,14,-5123.884,478.5325,80.71776,0,0,0,0,100,0), -- 06:59:04
(@PATH,15,-5110.585,482.2175,83.02201,0,0,0,0,100,0), -- 06:59:11
(@PATH,16,-5099.746,484.6776,83.24095,0,0,0,0,100,0), -- 06:59:16
(@PATH,17,-5067.211,481.675,84.28523,0,0,0,0,100,0), -- 06:59:20
(@PATH,18,-5042.09,468.0539,84.99848,0,0,0,0,100,0), -- 06:59:25
(@PATH,19,-5020.597,458.8485,87.52278,0,0,0,0,100,0), -- 06:59:28
(@PATH,20,-5014.573,456.4386,88.06664,0,0,0,0,100,0), -- 06:59:34
(@PATH,21,-4981.055,419.6735,87.5862,0,0,0,0,100,0), -- 06:59:39
(@PATH,22,-4971.752,413.9452,86.63638,0,0,0,0,100,0), -- 06:59:46
(@PATH,23,-4981.293,436.6361,87.37029,0,0,0,0,100,0), -- 06:59:48
(@PATH,24,-5000.16,465.5633,87.46753,0,0,0,0,100,0), -- 06:59:57
(@PATH,25,-5003.161,469.669,87.71191,0,0,0,0,100,0), -- 07:00:01
(@PATH,26,-5024.007,485.0092,86.62108,0,0,0,0,100,0), -- 07:00:05
(@PATH,27,-5052.383,491.6175,85.6012,0,0,0,0,100,0), -- 07:00:10
(@PATH,28,-5089.974,495.1535,84.65902,0,0,0,0,100,0), -- 07:00:15
(@PATH,29,-5131.202,491.5916,82.67003,0,0,0,0,100,0), -- 07:00:23
(@PATH,30,-5104.979,495.0316,84.09554,0,0,0,0,100,0), -- 07:00:24
(@PATH,31,-5136.438,490.7238,82.10683,0,0,0,0,100,0), -- 07:00:32
(@PATH,32,-5148.315,486.5536,80.46145,0,0,0,0,100,0), -- 07:00:36
(@PATH,33,-5176.314,497.1033,78.8902,0,0,0,0,100,0), -- 07:00:40
(@PATH,34,-5191.643,517.5717,76.7062,0,0,0,0,100,0), -- 07:00:46
(@PATH,35,-5209.542,551.1528,59.1912,0,0,0,0,100,0), -- 07:00:52
(@PATH,36,-5218.914,591.9361,58.3474,0,0,0,0,100,0), -- 07:01:01
(@PATH,37,-5216.641,622.2191,67.05342,0,0,0,0,100,0), -- 07:01:09
(@PATH,38,-5219.429,635.3921,68.57317,0,0,0,0,100,0), -- 07:01:16
(@PATH,39,-5219.883,633.0313,68.81996,0,0,0,0,100,0), -- 07:01:19
(@PATH,40,-5220.871,599.2662,61.02921,0,0,0,0,100,0), -- 07:01:21
(@PATH,41,-5218.937,565.3672,50.85265,0,0,0,0,100,0); -- 07:01:27
-- 0x1C09FC424015BB40000019000022140B .go -5218.906 565.3663 50.85233

-- Pathing for Dragonmaw Ascendant Entry: 22253 'TDB FORMAT' 
SET @NPC := 52277;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-4898.732,`position_y`=216.78,`position_z`=54.74985 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-4898.732,216.78,54.74985,0,0,0,0,100,0), -- 07:15:21
(@PATH,2,-4885.031,236.3208,52.61034,0,0,0,0,100,0), -- 07:15:29
(@PATH,3,-4889.695,229.7686,52.92027,0,0,0,0,100,0), -- 07:15:32
(@PATH,4,-4887.056,233.156,52.68503,0,0,0,0,100,0), -- 07:15:40
(@PATH,5,-4858.93,265.0389,48.16708,0,0,0,0,100,0), -- 07:15:41
(@PATH,6,-4851.827,284.8839,47.34936,0,0,0,0,100,0), -- 07:15:48
(@PATH,7,-4846.079,316.0757,55.46541,0,0,0,0,100,0), -- 07:15:54
(@PATH,8,-4843.175,349.7769,58.46518,0,0,0,0,100,0), -- 07:16:01
(@PATH,9,-4845.812,366.7672,60.97961,0,0,0,0,100,0), -- 07:16:07
(@PATH,10,-4856.089,402.1339,59.56307,0,0,0,0,100,0), -- 07:16:12
(@PATH,11,-4857.438,423.075,61.22576,0,0,0,0,100,0), -- 07:16:18
(@PATH,12,-4859.081,457.2928,64.90662,0,0,0,0,100,0), -- 07:16:25
(@PATH,13,-4846.12,505.6395,50.40051,0,0,0,0,100,0), -- 07:16:33
(@PATH,14,-4851.98,549.8719,48.70653,0,0,0,0,100,0), -- 07:16:40
(@PATH,15,-4878.309,576.0837,60.99225,0,0,0,0,100,0), -- 07:16:54
(@PATH,16,-4877.338,605.8702,68.93106,0,0,0,0,100,0), -- 07:17:02
(@PATH,17,-4879.686,577.7703,61.86221,0,0,0,0,100,0), -- 07:17:09
(@PATH,18,-4853.525,555.9883,49.47371,0,0,0,0,100,0), -- 07:17:14
(@PATH,19,-4845.978,509.1451,49.54256,0,0,0,0,100,0), -- 07:17:22
(@PATH,20,-4846.321,510.5673,48.53417,0,0,0,0,100,0), -- 07:17:35
(@PATH,21,-4846,508.4746,49.31151,0,0,0,0,100,0), -- 07:17:42
(@PATH,22,-4859.021,476.5473,64.16699,0,0,0,0,100,0), -- 07:17:43
(@PATH,23,-4857.8,436.0128,60.95083,0,0,0,0,100,0), -- 07:17:50
(@PATH,24,-4857.147,413.6607,60.26784,0,0,0,0,100,0), -- 07:17:58
(@PATH,25,-4848.22,378.149,60.79633,0,0,0,0,100,0), -- 07:18:04
(@PATH,26,-4843.683,354.1476,59.12819,0,0,0,0,100,0), -- 07:18:10
(@PATH,27,-4844.921,324.3129,56.56282,0,0,0,0,100,0), -- 07:18:15
(@PATH,28,-4848.563,297.3202,46.17633,0,0,0,0,100,0), -- 07:18:21
(@PATH,29,-4856.53,270.7361,47.4738,0,0,0,0,100,0), -- 07:18:28
(@PATH,30,-4881.715,240.4737,51.88027,0,0,0,0,100,0); -- 07:18:34
-- 0x1C09FC424015BB4000001900002230F5 .go -4898.732 216.78 54.74985

-- Pathing for Dragonmaw Ascendant Entry: 22253 'TDB FORMAT' 
SET @NPC := 52278;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5004.888,`position_y`=702.8807,`position_z`=82.04395 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5004.888,702.8807,82.04395,0,0,0,0,100,0), -- 07:09:09
(@PATH,2,-5011.663,699.3098,81.89692,0,0,0,0,100,0), -- 07:09:11
(@PATH,3,-5015.312,687.5909,82.03319,0,0,0,0,100,0), -- 07:09:13
(@PATH,4,-4996.903,700.0056,82.65056,0,0,0,0,100,0), -- 07:09:17
(@PATH,5,-4992.215,702.6887,83.10714,0,0,0,0,100,0), -- 07:09:19
(@PATH,6,-4969.132,707.9535,83.95985,0,0,0,0,100,0), -- 07:09:23
(@PATH,7,-4964.693,690.9923,83.32875,0,0,0,0,100,0), -- 07:09:25
(@PATH,8,-4954.07,677.088,77.76302,0,0,0,0,100,0), -- 07:09:29
(@PATH,9,-4949.576,659.408,77.40804,0,0,0,0,100,0), -- 07:09:34
(@PATH,10,-4960.746,641.1125,76.67274,0,0,0,0,100,0), -- 07:09:37
(@PATH,11,-4961.855,639.7071,77.21989,0,0,0,0,100,0), -- 07:09:43
(@PATH,12,-4956.266,612.0739,75.91876,0,0,0,0,100,0), -- 07:09:47
(@PATH,13,-4954.515,605.1003,75.18854,0,0,0,0,100,0), -- 07:09:51
(@PATH,14,-4956.737,589.0807,74.53506,0,0,0,0,100,0), -- 07:09:54
(@PATH,15,-4969.977,557.0493,76.5106,0,0,0,0,100,0), -- 07:09:59
(@PATH,16,-4988.75,549.4487,83.39151,0,0,0,0,100,0), -- 07:10:01
(@PATH,17,-4988.618,539.1655,83.00819,0,0,0,0,100,0), -- 07:10:05
(@PATH,18,-4981.907,532.507,78.12585,0,0,0,0,100,0), -- 07:10:07
(@PATH,19,-4959.939,531.2402,73.84925,0,0,0,0,100,0), -- 07:10:10
(@PATH,20,-4946.729,533.6745,66.42971,0,0,0,0,100,0), -- 07:10:15
(@PATH,21,-4926.818,538.8672,65.0467,0,0,0,0,100,0), -- 07:10:18
(@PATH,22,-4911.734,530.0341,55.14276,0,0,0,0,100,0), -- 07:10:22
(@PATH,23,-4898.571,547.2958,47.75196,0,0,0,0,100,0), -- 07:10:27
(@PATH,24,-4890.054,557.7857,57.1432,0,0,0,0,100,0), -- 07:10:33
(@PATH,25,-4876.696,566.1863,57.93503,0,0,0,0,100,0), -- 07:10:38
(@PATH,26,-4862.715,575.6901,52.22014,0,0,0,0,100,0), -- 07:10:40
(@PATH,27,-4858.512,594.1106,60.68968,0,0,0,0,100,0), -- 07:10:45
(@PATH,28,-4856.906,612.0919,65.71946,0,0,0,0,100,0), -- 07:10:49
(@PATH,29,-4854.059,628.2389,66.45804,0,0,0,0,100,0), -- 07:10:53
(@PATH,30,-4849.357,642.4513,59.23316,0,0,0,0,100,0), -- 07:10:56
(@PATH,31,-4839.23,658.6438,54.41743,0,0,0,0,100,0), -- 07:11:01
(@PATH,32,-4840.304,673.9675,51.38053,0,0,0,0,100,0), -- 07:11:05
(@PATH,33,-4849.471,676.6252,54.26706,0,0,0,0,100,0), -- 07:11:08
(@PATH,34,-4862.514,682.0897,59.79738,0,0,0,0,100,0), -- 07:11:10
(@PATH,35,-4880.908,688.1571,66.10199,0,0,0,0,100,0), -- 07:11:13
(@PATH,36,-4863.878,682.674,59.87488,0,0,0,0,100,0), -- 07:11:15
(@PATH,37,-4880.722,688.0911,66.16837,0,0,0,0,100,0), -- 07:11:22
(@PATH,38,-4896.763,688.6787,70.71627,0,0,0,0,100,0), -- 07:11:24
(@PATH,39,-4905.12,669.2037,74.99036,0,0,0,0,100,0), -- 07:11:28
(@PATH,40,-4907.025,659.6029,77.51676,0,0,0,0,100,0), -- 07:11:33
(@PATH,41,-4917.011,663.6454,73.60014,0,0,0,0,100,0), -- 07:11:35
(@PATH,42,-4927.575,673.5298,75.48451,0,0,0,0,100,0), -- 07:11:38
(@PATH,43,-4938.423,682.1601,78.2296,0,0,0,0,100,0), -- 07:11:41
(@PATH,44,-4954.518,695.4416,84.32332,0,0,0,0,100,0), -- 07:11:45
(@PATH,45,-4969.985,709.3942,83.46222,0,0,0,0,100,0), -- 07:11:51
(@PATH,46,-4971.432,710.7948,83.43773,0,0,0,0,100,0), -- 07:11:55
(@PATH,47,-4986.938,716.7153,82.9008,0,0,0,0,100,0); -- 07:11:58
-- 0x1C09FC424015BB4000001900002230F7 .go -5004.888 702.8807 82.04395

-- Pathing for Dragonmaw Ascendant Entry: 22253 'TDB FORMAT' 
SET @NPC := 52279;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5022.057,`position_y`=479.9312,`position_z`=87.00283 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5022.057,479.9312,87.00283,0,0,0,0,100,0), -- 20:22:29
(@PATH,2,-5014.553,472.7912,88.03943,0,0,0,0,100,0), -- 20:22:39
(@PATH,3,-4999.62,456.4922,87.46286,0,0,0,0,100,0), -- 20:22:47
(@PATH,4,-4967.545,406.6447,86.09462,0,0,0,0,100,0), -- 20:22:56
(@PATH,5,-4963.545,398.6447,85.84462,0,0,0,0,100,0), -- 20:22:56
(@PATH,6,-4966.199,380.3893,84.58515,0,0,0,0,100,0), -- 20:23:05
(@PATH,7,-4969.449,369.8893,84.33515,0,0,0,0,100,0), -- 20:23:05
(@PATH,8,-4971.571,363.7757,84.01563,0,0,0,0,100,0), -- 20:23:11
(@PATH,9,-4981.193,315.1064,82.50433,0,0,0,0,100,0), -- 20:23:18
(@PATH,10,-4982.943,308.6064,82.00433,0,0,0,0,100,0), -- 20:23:18
(@PATH,11,-4985.527,270.3568,80.93662,0,0,0,0,100,0), -- 20:23:31
(@PATH,12,-4984.912,261.6664,80.87724,0,0,0,0,100,0), -- 20:23:39
(@PATH,13,-4984.194,254.0232,80.98643,0,0,0,0,100,0), -- 20:23:41
(@PATH,14,-4983.671,226.0687,80.50131,0,0,0,0,100,0), -- 20:23:46
(@PATH,15,-5002.183,198.7367,80.73076,0,0,0,0,100,0), -- 20:23:54
(@PATH,16,-4997.445,147.227,79.09719,0,0,0,0,100,0), -- 20:24:01
(@PATH,17,-4996.195,142.477,78.59719,0,0,0,0,100,0), -- 20:24:01
(@PATH,18,-4996.078,134.5595,77.47847,0,0,0,0,100,0), -- 20:24:06
(@PATH,19,-4996.328,129.5595,76.97847,0,0,0,0,100,0), -- 20:24:06
(@PATH,20,-4996.578,121.8095,76.72847,0,0,0,0,100,0), -- 20:24:06
(@PATH,21,-4997.078,112.0595,75.97847,0,0,0,0,100,0), -- 20:24:06
(@PATH,22,-5003.599,85.19402,76.17677,0,0,0,0,100,0), -- 20:24:13
(@PATH,23,-5000.918,93.57802,75.89561,0,0,0,0,100,0), -- 20:24:18
(@PATH,24,-4996.32,127.891,76.83911,0,0,0,0,100,0), -- 20:24:21
(@PATH,25,-4996.07,133.891,77.33911,0,0,0,0,100,0), -- 20:24:21
(@PATH,26,-4995.82,139.891,78.08911,0,0,0,0,100,0), -- 20:24:21
(@PATH,27,-4997.385,146.7997,78.94779,0,0,0,0,100,0), -- 20:24:29
(@PATH,28,-4998.385,151.2997,79.44779,0,0,0,0,100,0), -- 20:24:29
(@PATH,29,-4999.885,157.0497,79.94779,0,0,0,0,100,0), -- 20:24:29
(@PATH,30,-5000.678,159.7069,80.21076,0,0,0,0,100,0), -- 20:24:32
(@PATH,31,-5002.248,199.0518,80.65829,0,0,0,0,100,0), -- 20:24:41
(@PATH,32,-4983.636,226.4841,80.61127,0,0,0,0,100,0), -- 20:24:47
(@PATH,33,-4986.27,283.0035,81.57057,0,0,0,0,100,0), -- 20:24:53
(@PATH,34,-4980.688,317.2732,82.73731,0,0,0,0,100,0), -- 20:25:02
(@PATH,35,-4979.188,322.0232,83.23731,0,0,0,0,100,0), -- 20:25:02
(@PATH,36,-4976.569,330.6879,83.80838,0,0,0,0,100,0), -- 20:25:09
(@PATH,37,-4964.715,384.6398,85.10558,0,0,0,0,100,0), -- 20:25:15
(@PATH,38,-4969.031,409.8725,86.30847,0,0,0,0,100,0), -- 20:25:22
(@PATH,39,-4975.031,422.3725,87.05847,0,0,0,0,100,0), -- 20:25:22
(@PATH,40,-5003.537,460.994,88.00166,0,0,0,0,100,0), -- 20:25:31
(@PATH,41,-5022.101,479.8282,87.23804,0,0,0,0,100,0), -- 20:25:39
(@PATH,42,-5029.75,486.7083,86.9541,0,0,0,0,100,0); -- 20:25:54
-- 0x1C09FC424015BB4000001A0000219932 .go -5022.057 479.9312 87.00283

-- Pathing for Dragonmaw Ascendant Entry: 22253 'TDB FORMAT' 
SET @NPC := 52280;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5210.222,`position_y`=418.9368,`position_z`=73.92104 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5210.222,418.9368,73.92104,0,0,0,0,100,0), -- 07:02:01
(@PATH,2,-5194.546,459.6038,74.90173,0,0,0,0,100,0), -- 07:02:06
(@PATH,3,-5198.185,476.6735,74.56579,0,0,0,0,100,0), -- 07:02:16
(@PATH,4,-5205.193,489.9176,74.52521,0,0,0,0,100,0), -- 07:02:20
(@PATH,5,-5205.922,514.9041,74.24827,0,0,0,0,100,0), -- 07:02:25
(@PATH,6,-5197.391,548.4199,71.05492,0,0,0,0,100,0), -- 07:02:30
(@PATH,7,-5202.769,580.7711,57.59827,0,0,0,0,100,0), -- 07:02:35
(@PATH,8,-5207.66,619.1582,66.18312,0,0,0,0,100,0), -- 07:02:42
(@PATH,9,-5203.323,643.3531,69.50911,0,0,0,0,100,0), -- 07:02:50
(@PATH,10,-5209.102,642.1717,68.62796,0,0,0,0,100,0), -- 07:02:54
(@PATH,11,-5223.636,616.6403,64.28975,0,0,0,0,100,0), -- 07:02:59
(@PATH,12,-5236.8,572.5868,43.20554,0,0,0,0,100,0), -- 07:03:04
(@PATH,13,-5230.435,558.9042,52.97427,0,0,0,0,100,0), -- 07:03:14
(@PATH,14,-5236.423,533.6864,54.42358,0,0,0,0,100,0), -- 07:03:25
(@PATH,15,-5257.124,507.9855,45.40817,0,0,0,0,100,0), -- 07:03:31
(@PATH,16,-5275.066,487.7511,39.84113,0,0,0,0,100,0), -- 07:03:38
(@PATH,17,-5276.403,465.4868,50.2813,0,0,0,0,100,0), -- 07:03:44
(@PATH,18,-5260.017,431.7862,58.83994,0,0,0,0,100,0), -- 07:03:49
(@PATH,19,-5264.36,398.4801,59.6331,0,0,0,0,100,0), -- 07:03:58
(@PATH,20,-5247.507,387.5706,65.87028,0,0,0,0,100,0), -- 07:04:05
(@PATH,21,-5218.22,394.0266,74.48222,0,0,0,0,100,0), -- 07:04:10
(@PATH,22,-5197.534,385.7352,72.49537,0,0,0,0,100,0), -- 07:04:17
(@PATH,23,-5194.736,383.3604,72.18273,0,0,0,0,100,0), -- 07:04:22
(@PATH,24,-5180.763,356.1179,72.43773,0,0,0,0,100,0), -- 07:04:28
(@PATH,25,-5175.956,317.8983,74.14841,0,0,0,0,100,0), -- 07:04:33
(@PATH,26,-5192.369,287.6161,72.80084,0,0,0,0,100,0), -- 07:04:36
(@PATH,27,-5215.688,268.9493,70.65274,0,0,0,0,100,0), -- 07:04:43
(@PATH,28,-5234.928,266.7101,71.72028,0,0,0,0,100,0), -- 07:04:50
(@PATH,29,-5261.104,267.6116,66.5541,0,0,0,0,100,0), -- 07:04:53
(@PATH,30,-5269.087,272.336,70.12495,0,0,0,0,100,0), -- 07:04:59
(@PATH,31,-5286.044,309.7126,70.90472,0,0,0,0,100,0), -- 07:05:03
(@PATH,32,-5281.2,352.0699,63.69888,0,0,0,0,100,0), -- 07:05:09
(@PATH,33,-5272.806,362.6875,62.99657,0,0,0,0,100,0), -- 07:05:19
(@PATH,34,-5246.281,400.4454,59.19219,0,0,0,0,100,0), -- 07:05:23
(@PATH,35,-5225.892,410.2588,65.93465,0,0,0,0,100,0); -- 07:05:31
-- 0x1C09FC424015BB4000001A0000216370 .go -5210.222 418.9368 73.92104

-- Pathing for Dragonmaw Ascendant Entry: 22253 'TDB FORMAT' 
SET @NPC := 52281;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5285.737,`position_y`=354.3048,`position_z`=58.63031 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5285.737,354.3048,58.63031,0,0,0,0,100,0), -- 19:31:06
(@PATH,2,-5294.725,316.1119,67.75787,0,0,0,0,100,0), -- 19:31:14
(@PATH,3,-5301.147,281.1097,59.07014,0,0,0,0,100,0), -- 19:31:23
(@PATH,4,-5304.806,249.8802,62.29091,0,0,0,0,100,0), -- 19:31:30
(@PATH,5,-5314.573,202.0623,63.99271,0,0,0,0,100,0), -- 19:31:37
(@PATH,6,-5314.016,154.8896,48.03545,0,0,0,0,100,0), -- 19:31:47
(@PATH,7,-5299.288,117.1861,34.29665,0,0,0,0,100,0), -- 19:31:57
(@PATH,8,-5306.702,135.3764,38.50175,0,0,0,0,100,0), -- 19:32:03
(@PATH,9,-5299.126,117.1948,34.29574,0,0,0,0,100,0), -- 19:32:10
(@PATH,10,-5283.754,81.68874,36.04304,0,0,0,0,100,0), -- 19:32:14
(@PATH,11,-5272.062,57.64223,42.34082,0,0,0,0,100,0), -- 19:32:21
(@PATH,12,-5264.497,27.00165,48.20856,0,0,0,0,100,0), -- 19:32:27
(@PATH,13,-5250.226,-6.593673,46.53293,0,0,0,0,100,0), -- 19:32:36
(@PATH,14,-5242.217,-36.79307,41.91159,0,0,0,0,100,0), -- 19:32:43
(@PATH,15,-5227.656,-66.75511,59.73492,0,0,0,0,100,0), -- 19:32:49
(@PATH,16,-5240.584,-40.47152,44.62254,0,0,0,0,100,0), -- 19:32:58
(@PATH,17,-5247.443,-12.82116,46.7327,0,0,0,0,100,0), -- 19:33:05
(@PATH,18,-5261.27,18.16552,48.98127,0,0,0,0,100,0), -- 19:33:12
(@PATH,19,-5270.131,51.50296,42.63126,0,0,0,0,100,0), -- 19:33:19
(@PATH,20,-5282.171,78.36816,34.01592,0,0,0,0,100,0), -- 19:33:27
(@PATH,21,-5295.439,108.8647,33.0154,0,0,0,0,100,0), -- 19:33:33
(@PATH,22,-5313.281,152.3893,47.28299,0,0,0,0,100,0), -- 19:33:41
(@PATH,23,-5315.114,198.1515,63.51984,0,0,0,0,100,0), -- 19:33:50
(@PATH,24,-5307.13,236.8966,62.42439,0,0,0,0,100,0), -- 19:34:00
(@PATH,25,-5301.88,277.6213,59.22523,0,0,0,0,100,0), -- 19:34:11
(@PATH,26,-5295.718,311.9755,66.89128,0,0,0,0,100,0), -- 19:34:18
(@PATH,27,-5300.28,287.5018,64.20296,0,0,0,0,100,0), -- 19:34:20
(@PATH,28,-5295.666,312.1795,67.10269,0,0,0,0,100,0), -- 19:34:28
(@PATH,29,-5286.51,351.3247,58.38956,0,0,0,0,100,0), -- 19:34:31
(@PATH,30,-5281.85,390.6465,55.83143,0,0,0,0,100,0), -- 19:34:41
(@PATH,31,-5273.968,437.0849,54.09414,0,0,0,0,100,0), -- 19:34:48
(@PATH,32,-5280.505,398.1873,55.13199,0,0,0,0,100,0); -- 19:35:01
-- 0x1C09FC424015BB4000001A0000240362 .go -5285.737 354.3048 58.63031

-- Pathing for Dragonmaw Ascendant Entry: 22253 'TDB FORMAT' 
SET @NPC := 52282;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5026.082,`position_y`=30.3253,`position_z`=78.81156 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5026.082,30.3253,78.81156,0,0,0,0,100,0), -- 19:59:18
(@PATH,2,-5044.674,33.37571,80.91895,0,0,0,0,100,0), -- 19:59:19
(@PATH,3,-5100.418,46.22721,80.36517,0,0,0,0,100,0), -- 19:59:25
(@PATH,4,-5120.707,43.68564,79.09943,0,0,0,0,100,0), -- 19:59:35
(@PATH,5,-5168.205,29.74817,77.34432,0,0,0,0,100,0), -- 19:59:41
(@PATH,6,-5187.383,4.068243,74.23724,0,0,0,0,100,0), -- 19:59:48
(@PATH,7,-5192.171,-27.60443,74.06621,0,0,0,0,100,0), -- 19:59:54
(@PATH,8,-5187.181,-39.73703,74.54796,0,0,0,0,100,0), -- 20:00:02
(@PATH,9,-5162.415,-82.17906,72.73651,0,0,0,0,100,0), -- 20:00:09
(@PATH,10,-5132.202,-120.9739,65.05319,0,0,0,0,100,0), -- 20:00:16
(@PATH,11,-5108.869,-123.2847,60.21478,0,0,0,0,100,0), -- 20:00:23
(@PATH,12,-5065.221,-116.5905,61.09086,0,0,0,0,100,0), -- 20:00:31
(@PATH,13,-5032.96,-83.59184,63.18661,0,0,0,0,100,0), -- 20:00:39
(@PATH,14,-5018.273,-72.75426,67.76162,0,0,0,0,100,0), -- 20:00:48
(@PATH,15,-5003.304,-38.36306,73.39491,0,0,0,0,100,0), -- 20:00:51
(@PATH,16,-4982.256,-5.512026,72.68662,0,0,0,0,100,0), -- 20:01:01
(@PATH,17,-4970.602,8.782578,71.2607,0,0,0,0,100,0), -- 20:01:08
(@PATH,18,-4956.99,19.62778,67.81738,0,0,0,0,100,0), -- 20:01:12
(@PATH,19,-4965.795,7.546585,69.8514,0,0,0,0,100,0), -- 20:01:16
(@PATH,20,-4973.835,-2.793668,71.32764,0,0,0,0,100,0), -- 20:01:19
(@PATH,21,-4995.399,-29.66144,72.97089,0,0,0,0,100,0), -- 20:01:23
(@PATH,22,-5000.069,-61.59894,66.50253,0,0,0,0,100,0), -- 20:01:30
(@PATH,23,-5007.126,-33.22968,75.18976,0,0,0,0,100,0), -- 20:01:36
(@PATH,24,-5014.227,5.722984,77.4781,0,0,0,0,100,0); -- 20:01:44
-- 0x1C09FC424015BB4000001A0000241AC7 .go -5026.082 30.3253 78.81156
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_08_00.sql 
-- -------------------------------------------------------- 
-- Kill some database errors
DELETE FROM `creature_addon` WHERE `guid` IN (52306, 52317, 52312, 52303, 52296, 127436, 127437);
UPDATE `creature` SET `spawndist`=0 WHERE `guid`=52331;
UPDATE `creature` SET `spawndist`=0 WHERE `guid`=52330;
UPDATE `creature` SET `spawndist`=0 WHERE `guid`=120692;
DELETE FROM `linked_respawn` WHERE  `guid`=127436 AND `linkType`=0;
DELETE FROM `linked_respawn` WHERE  `guid`=127437 AND `linkType`=0;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_08_01.sql 
-- -------------------------------------------------------- 
-- Dragonmaw Tower Controller
-- Actionlist SAI
SET @ENTRY := 23370;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` IN (@ENTRY);
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY, 0, 0,0,10,0,100,0,1,20,15000,15000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Dragonmaw Tower Controller - ooc los - Say Line 0");

-- Add text
DELETE FROM `creature_text` WHERE `entry`=23370;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`, `BroadcastTextID`) VALUES
(23370, 0, 0, 'MULVERICK!', 14, 0, 100, 22, 0, 0, 'Dragonmaw Tower Controller', 21429);
       
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=22 AND `SourceGroup`=1 AND `SourceEntry`=@ENTRY;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`, `ConditionTarget`, `ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`, `ErrorTextId`,`ScriptName`,`Comment`) VALUES
(22,1,@ENTRY,0,0,31,3,22274,0,0,'','event require creature 22274');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_09_00.sql 
-- -------------------------------------------------------- 
--
UPDATE `smart_scripts` SET `action_type`=85 WHERE `entryorguid` IN (27226, 27224, 27225, 27229) AND `source_type`=0 AND `id`=0;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_09_01.sql 
-- -------------------------------------------------------- 
--
UPDATE `creature` SET `position_x`=3592.494, `position_y`=191.1563, `position_z`=-113.6729, `orientation`=1.22173, `VerifiedBuild`=12340 WHERE `guid`=131056;
UPDATE `creature` SET `position_x`=3611.361, `position_y`=204.7483, `position_z`=-113.6808, `orientation`=3.665191, `VerifiedBuild`=12340 WHERE `guid`=131059;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_09_02.sql 
-- -------------------------------------------------------- 
-- Deathbound Wards should be taunt immune
UPDATE `creature_template` SET `flags_extra` = `flags_extra` | 256 WHERE `entry` IN (37007, 38031);
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_09_03.sql 
-- -------------------------------------------------------- 

UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=19380;
DELETE FROM `smart_scripts` WHERE `entryorguid`=19380 AND `source_type`=0;
DELETE FROM `creature_text` WHERE `entry` IN(19380,19383);

INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextID`, `comment`) VALUES
(19383, 0, 0, 'I don''t understand you!', 12, 7, 100, 18, 0, 0, 16540, 'Captured Gnome to Guard Untula'),
(19383, 0, 1, 'Please don''t beat me!', 12, 7, 100, 33, 0, 0, 16541, 'Captured Gnome to Guard Untula'),
(19380, 0, 0, 'Hurry up with that axe, you! I want it sharp enough to cut the wind!', 12, 1, 100, 60, 0, 1040, 16532, 'Guard Untula to 0'),
(19380, 0, 1, 'If you''re no good at fixing, we''ll see if you''re good for eating. Now WORK!', 12, 1, 100, 5, 0, 0, 16538, 'Guard Untula to 0');

INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(19380, 0, 0, 1, 1, 0, 100, 0, 15000, 45000, 120000, 180000, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,0,'Guard Untula - OOC - Say Line 0'),
(19380, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 19, 19383, 0, 0, 0, 0, 0,0,'Guard Untula - OOC - Say Line 0 on Captured Gnome');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_10_00.sql 
-- -------------------------------------------------------- 
--
UPDATE `smart_scripts` SET `event_flags`=1 WHERE  `entryorguid` IN (23625,23626,23624,23623) AND `source_type`=0 AND `id`=1 AND `link`=0;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_10_01.sql 
-- -------------------------------------------------------- 
DELETE FROM `smart_scripts` WHERE `entryorguid`=19995 AND `source_type`=0 AND `id`IN(3,4,5,6);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN(1999500,1999501) AND `source_type`=9;

UPDATE `creature_template` SET `AIName`='SmartAI',`ScriptName`='' WHERE `entry` IN(21241);
DELETE FROM `smart_scripts` WHERE `entryorguid`=21241 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(19995, 0, 4, 5, 75, 1, 100, 0, 0, 21241, 5, 5000, 22, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - On Creature Range - Set Phase 2'),
(19995, 0, 5, 0, 61, 0, 100, 0, 0, 0, 0, 0, 80, 1999500, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - On Creature Range - Run Script'),
(19995, 0, 6, 0, 38, 0, 100, 0, 1, 1, 0, 0, 80, 1999501, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - On Data Set 1 1 - Run Script 2'),
(21241, 0, 0, 1, 54, 0, 100, 0, 0, 0, 0, 0, 64, 1, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Bloodmaul Brutebane Stout Trigger - On Just Summoned - Store Targetlist'),
(21241, 0, 1, 2, 61, 0, 100, 0, 0, 0, 0, 0, 100, 1, 0, 0, 0, 0, 0, 19, 19995, 0, 0, 0, 0, 0, 0, 'Bloodmaul Brutebane Stout Trigger - On Just Summoned - Send Target List to Bladespire Brute'),
(21241, 0, 2, 0, 61, 0, 100, 0, 0, 0, 0, 0, 45, 1, 1, 0, 0, 0, 0, 19, 19995, 0, 0, 0, 0, 0, 0, 'Bloodmaul Brutebane Stout Trigger - On Just Summoned - Set Data to Bladespire Brute'),
(1999500, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 41, 0, 0, 0, 0, 0, 0, 19, 21241, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Despawn Trigger'),
(1999500, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 5, 16, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotKneel (16)'),
(1999500, 9, 2, 0, 0, 0, 100, 0, 2000, 2000, 0, 0, 70, 300, 0, 0, 0, 0, 0, 20, 184315, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Despawn GO'),
(1999500, 9, 3, 0, 0, 0, 100, 0, 0, 0, 0, 0, 71, 0, 0, 2703, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Update Virtual Item Slot ID 1'),
(1999500, 9, 4, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Say Line 4'),
(1999500, 9, 5, 0, 0, 0, 100, 0, 2000, 2000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 6, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 7, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 8, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 9, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 10, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 11, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 12, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 13, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 5, 92, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Play Emote OneShotEatNoSheathe'),
(1999500, 9, 14, 0, 0, 0, 100, 0, 2000, 2000, 0, 0, 11, 35240, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Cast Bloodmaul Intoxication'),
(1999500, 9, 15, 0, 0, 0, 100, 0, 0, 0, 0, 0, 71, 0, 0, 14874, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Update Virtual Item Slot ID 1'),
(1999500, 9, 16, 0, 0, 0, 100, 0, 0, 0, 0, 0, 17, 93, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Set Emote State 93'),
(1999500, 9, 17, 0, 0, 0, 100, 0, 0, 0, 0, 0, 33, 21241, 0, 0, 0, 0, 0, 12, 1, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Kill Credit'),
(1999500, 9, 18, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Say Line 5'),
(1999500, 9, 19, 0, 0, 0, 100, 0, 30000, 30000, 0, 0, 17, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Set Emote State 93'),
(1999500, 9, 20, 0, 0, 0, 100, 0, 0, 0, 0, 0, 22, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Set Phase 1'),
(1999500, 9, 21, 0, 0, 0, 100, 0, 0, 0, 0, 0, 8, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Set Aggresive'),
(1999500, 9, 22, 0, 0, 0, 100, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script - Evade'),
(1999501, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 8, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script 2 - Set Defensive'),
(1999501, 9, 1, 0, 0, 0, 100, 0, 0, 0, 0, 0, 22, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script 2 - Set Set Phase 1'),
(1999501, 9, 2, 0, 0, 0, 100, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 0, 0, 19, 21241, 0, 0, 0, 0, 0, 0, 'Bladespire Brute - Script 2 - Move to Position');

DELETE FROM `creature_text` WHERE `entry`=19995 AND `groupid` IN(4,5);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextID`, `comment`) VALUES
(19995, 4, 0, 'Ohh, look! Bloodmaul Brew! Mmmm...', 12, 0, 100, 16, 0, 0, 18170, 'Bladespire Brute to 21241'),
(19995, 4, 1, 'Bloodmaul Brew? Me favorite!', 12, 0, 100, 16, 0, 0, 18171, 'Bladespire Brute to 21241'),
(19995, 4, 2, 'Ohh, look! Bloodmaul Brew! Mmmm...', 12, 0, 100, 16, 0, 398, 18170, 'Bladespire Brute to 21241'),
(19995, 4, 3, 'Mmmm, Bloodmaul Brew!', 12, 0, 100, 16, 0, 0, 17844, 'Bladespire Brute to 21241'),
(19995, 5, 0, 'Ugh... Me not feel so guud.', 12, 0, 100, 92, 0, 0, 18197, 'Bladespire Brute to 21241');
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_11_00.sql 
-- -------------------------------------------------------- 
DELETE FROM `creature` WHERE `guid` IN (52283, 52284, 52285, 52286, 69717, 69718, 69719, 69720);

SET @GUID          := 69717; -- 4 creature guid Set by TC
SET @OGUID         := 5510; -- 1 GOB guid set by TC
DELETE FROM `creature` WHERE `id`=23142;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `PhaseId`, `PhaseGroup`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`,`curhealth`) VALUES
(@GUID+0, 23142, 530, 1, 1, 1, -5121.06, 598.424, 84.7603, 0.0581088, 600, 6542),
(@GUID+1, 23142, 530, 1, 1, 1, -5119.60, 601.834, 84.8180, 5.1726200, 600, 6542),
(@GUID+2, 23142, 530, 1, 1, 1, -5115.02, 601.537, 85.0292, 4.0204400, 600, 6542),
(@GUID+3, 23142, 530, 1, 1, 1, -5114.25, 597.062, 85.1574, 2.7049000, 600, 6542);

DELETE FROM `gameobject` WHERE `guid` IN (@OGUID);
INSERT INTO `gameobject` (`guid`,`id`,`map`,`spawnMask`,`PhaseId`, `PhaseGroup`,`position_x`,`position_y`,`position_z`,`orientation`,`rotation0`,`rotation1`,`rotation2`,`rotation3`,`spawntimesecs`,`animprogress`,`state`) VALUES
(@OGUID,50983,530,1,1, 1, -5116.788574, 631.188660, 85.055522,0.949811,0,0,0.457254, 0.889336, 300, 0, 1);

DELETE FROM `creature_equip_template` WHERE `entry`=23146;
INSERT INTO `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`, `VerifiedBuild`) VALUES (23146, 1, 31603, 0, 0, 0);
UPDATE `creature` SET `position_x`= -5158.237305, `position_y`= 584.302612, `position_z`= 81.074142, `orientation`= 5.954358 WHERE `guid`= 78136;
UPDATE `creature` SET `position_x`= -5156.172363, `position_y`= 590.695251, `position_z`= 80.769630, `orientation`= 5.502757 WHERE `guid`= 78135;
UPDATE `creature` SET `position_x`= -5152.365723, `position_y`= 592.303040, `position_z`= 81.361931, `orientation`= 4.705578 WHERE `guid`= 78137;
UPDATE `creature` SET `position_x`= -5113.345703, `position_y`= 623.850159, `position_z`= 86.736343, `orientation`= 1.033840 WHERE `guid`= 78160;
UPDATE `creature` SET `position_x`= -5123.593750, `position_y`= 626.638916, `position_z`= 86.629669, `orientation`= 1.395123 WHERE `guid`= 78159;
UPDATE `creature` SET `position_x`= -5165.503418, `position_y`= 568.109131, `position_z`= 80.523895, `orientation`= 2.707489 WHERE `guid`= 52107;
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_11_01.sql 
-- -------------------------------------------------------- 
-- Add Gug
SET @ENTRY := 23144;
SET @GUID := 23428;
DELETE FROM `creature` WHERE `guid`=@GUID;
INSERT INTO `creature` (`guid`, `id`, `map`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `curhealth`) VALUES 
(@GUID, @ENTRY, 530, -5119.803, 624.7665, 86.82748, 1.256637, 300, 3858);
DELETE FROM `creature_template_addon` WHERE `entry`=@ENTRY;
INSERT INTO `creature_template_addon` (`entry`, `bytes2`, `emote`) VALUES (@ENTRY, 4097, 133);

-- Add spawns
SET @CGUID := 143596;
DELETE FROM `creature` WHERE `guid` BETWEEN @CGUID+0 AND @CGUID+297;
INSERT INTO `creature` (`guid`, `id`, `map`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `curhealth`) VALUES 
(@CGUID+0, 23146, 530, -5143.491, 515.507, 84.50488, 4.834562, 300, 88860), -- Dragonmaw Enforcer
(@CGUID+1, 23146, 530, -5162.25, 534.2803, 82.78862, 5.183628, 300, 88860),
(@CGUID+2, 23146, 530, -5160.024, 561.3785, 82.47646, 0.05235988, 300, 88860),
(@CGUID+3, 23289, 530, -5141.078, 637.8817, 36.18917, 0, 300, 4299), -- 143599 -- Mine Car
(@CGUID+4, 23289, 530, -5158.637, 727.454, 45.57201, 0, 300, 4299), -- 143600
(@CGUID+5, 23287, 530, -5229.827, 626.3266, 48.93023, 4.223697, 300, 4299), -- 23287 (Area: 3965) -- Murkblood Miner
(@CGUID+6, 23287, 530, -5160.776, 616.7452, 36.24147, 5.044002, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+7, 23287, 530, -5160.416, 658.8326, 38.73607, 3.106686, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+8, 23287, 530, -5163.108, 651.5301, 37.39709, 2.460914, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+9, 23287, 530, -5160.553, 678.9923, 41.02322, 2.792527, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+10, 23287, 530, -5161.212, 668.8508, 40.50122, 3.281219, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+11, 23287, 530, -5152.85, 621.4561, 35.87352, 5.166174, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+12, 23287, 530, -5157.459, 707.9813, 42.97822, 2.75762, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+13, 23287, 530, -5156.376, 687.0754, 41.27747, 2.80998, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+14, 23287, 530, -5138.036, 646.6555, 36.65028, 0.2443461, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+15, 23287, 530, -5144.963, 630.1486, 35.9286, 5.61996, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+16, 23287, 530, -5158.104, 699.1263, 42.52824, 3.473205, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+17, 23287, 530, -5139.44, 637.2499, 36.16368, 5.689773, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+18, 23287, 530, -5131.758, 701.3122, 41.26741, 0.5235988, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+19, 23287, 530, -5156.701, 715.8916, 43.45729, 3.106686, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+20, 23287, 530, -5121.366, 693.678, 38.92881, 1.361357, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+21, 23287, 530, -5097.448, 680.2379, 32.88943, 3.001966, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+22, 23287, 530, -5135.893, 721.6864, 44.35764, 0.08726646, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+23, 23287, 530, -5159.912, 725.4135, 45.55035, 3.333579, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+24, 23287, 530, -5140.923, 729.5199, 45.31025, 0.8726646, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+25, 23287, 530, -5079.597, 669.6569, 33.3003, 3.124139, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+26, 23287, 530, -5133.994, 712.6088, 42.61035, 0.2268928, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+27, 23287, 530, -5063.482, 624.7257, 27.56281, 2.338741, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+28, 23287, 530, -5080.878, 644.967, 32.68097, 2.426008, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+29, 23287, 530, -5100.418, 654.4422, 33.49445, 2.303835, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+30, 23287, 530, -5164.475, 734.2371, 47.03968, 3.438299, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+31, 23287, 530, -5058.134, 647.3507, 29.36781, 2.80998, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+32, 23287, 530, -5219.054, 621.868, 47.97065, 4.310963, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+33, 23287, 530, -5040.079, 627.7264, 19.75634, 2.373648, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+34, 23287, 530, -5018.824, 584.2097, 22.70727, 2.059489, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+35, 23287, 530, -4983.617, 632.3187, 20.58379, 3.054326, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+36, 23287, 530, -5019.396, 638.6277, 23.41907, 3.281219, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+37, 23287, 530, -5043.952, 595.3922, 18.24016, 2.094395, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+38, 23287, 530, -4969.858, 654.9359, 20.92751, 3.420845, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+39, 23287, 530, -4959.944, 591.9269, 12.99098, 1.989675, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+40, 23287, 530, -4968.715, 620.0629, 16.17488, 2.670354, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+41, 23287, 530, -4942.414, 582.1096, 9.914608, 2.129302, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+42, 23287, 530, -4959.944, 591.9269, 12.99098, 1.989675, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+43, 23287, 530, -4944.878, 561.5024, 7.636816, 2.094395, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+44, 23287, 530, -4921.915, 547.6068, 6.501626, 2.042035, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+45, 23287, 530, -4940.023, 535.2883, 7.127902, 1.675516, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+46, 23287, 530, -4904.557, 530.2072, 4.563097, 2.129302, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+47, 23287, 530, -4921.108, 528.4734, 6.568361, 2.042035, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+48, 23287, 530, -4887.938, 531.7214, 0.6247723, 3.054326, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+49, 23287, 530, -4965.987, 483.2187, 4.143906, 0.03490658, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+50, 23287, 530, -4918.072, 502.0591, 2.48006, 1.570796, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+51, 23287, 530, -4926.752, 455.5083, 1.527722, 1.396263, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+52, 23287, 530, -4981.036, 460.0726, 4.207428, 0.1745329, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+53, 23287, 530, -4937.384, 438.7227, 2.34031, 1.291544, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+54, 23287, 530, -4907.827, 475.5659, 0.8901013, 2.042035, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+55, 23287, 530, -4882.042, 472.4888, -3.117535, 3.228859, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+56, 23287, 530, -4941.028, 469.1831, 0.2023413, 1.064651, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+57, 23287, 530, -4950.465, 451.1373, 0.7255403, 0.9424778, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+58, 23287, 530, -4904.538, 420.0294, -5.088545, 2.443461, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+59, 23287, 530, -4959.418, 393.7061, -0.9294897, 4.520403, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+60, 23287, 530, -4956.983, 356.4414, -2.264346, 4.468043, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+61, 23287, 530, -4919.892, 307.3166, -13.13219, 3.403392, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+62, 23287, 530, -4970.338, 322.2038, -2.636889, 3.909538, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+63, 23287, 530, -4969.645, 298.882, -3.503148, 3.420845, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+64, 23287, 530, -4941.545, 294.6626, -8.814796, 3.263766, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+65, 23287, 530, -4994.479, 281.7764, -3.49516, 2.687807, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+66, 23287, 530, -5014.456, 279.3217, 0.06473933, 2.356194, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+67, 23287, 530, -4981.001, 267.0449, -6.299074, 2.565634, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+68, 23287, 530, -4966.653, 279.1325, -5.876964, 2.844887, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+69, 23287, 530, -4991.896, 300.7972, -1.304715, 3.281219, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+70, 23287, 530, -5009.957, 296.281, 2.063254, 2.792527, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+71, 23287, 530, -4973.297, 220.8183, -10.63328, 2.216568, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+72, 23287, 530, -4977.839, 252.8167, -8.2034, 2.408554, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+73, 23287, 530, -5011.099, 222.092, -10.51172, 1.815142, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+74, 23287, 530, -4995.983, 254.4959, -4.699264, 2.216568, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+75, 23287, 530, -5023.841, 241.114, -4.392029, 1.32645, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+76, 23287, 530, -5044.643, 201.4753, -11.86366, 0.8726646, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+77, 23287, 530, -5069.865, 170.6837, -9.401138, 0.8203048, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+78, 23287, 530, -5009.812, 160.8324, -14.7209, 2.75762, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+79, 23287, 530, -5067.464, 192.5109, -8.89424, 0.6457718, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+80, 23287, 530, -5041.76, 179.2009, -14.23692, 0.9599311, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+81, 23287, 530, -5051.51, 155.655, -12.63313, 1.396263, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+82, 23287, 530, -5090.02, 180.6597, -8.299232, 0.3490658, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+83, 23287, 530, -5085.621, 154.5083, -10.88531, 0.9075712, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+84, 23287, 530, -5056.444, 120.8188, -17.15018, 2.059489, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+85, 23287, 530, -5068.56, 141.5665, -13.4759, 1.082104, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+86, 23287, 530, -5115.295, 147.5993, -11.40529, 0.6457718, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+87, 23287, 530, -5109.351, 129.4899, -13.61424, 0.6283185, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+88, 23287, 530, -5107.625, 169.4426, -8.387082, 0.3490658, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+89, 23287, 530, -5118.663, 105.1847, -12.59805, 1.256637, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+90, 23287, 530, -5149.458, 152.029, -12.39146, 6.178465, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+91, 23287, 530, -5174.084, 155.2752, -12.23319, 6.021386, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+92, 23287, 530, -5198.479, 157.0766, -13.41456, 0.4712389, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+93, 23287, 530, -5178.485, 174.8304, -11.48918, 5.340707, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+94, 23287, 530, -5198.593, 202.2049, -13.6357, 5.113815, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+95, 23287, 530, -5189.779, 135.4848, -12.0408, 0.6806784, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+96, 23287, 530, -5229.695, 136.8613, -13.63386, 0.5585054, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+97, 23287, 530, -5218.991, 222.2088, -11.34154, 5.218534, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+98, 23287, 530, -5220.694, 199.7151, -12.80523, 5.951573, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+99, 23287, 530, -5247.149, 212.4612, -13.82043, 5.637414, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+100, 23287, 530, -5241.209, 231.7721, -11.93106, 5.51524, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+101, 23287, 530, -5023.841, 241.114, -4.392029, 1.32645, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+102, 23287, 530, -4937.384, 438.7227, 2.34031, 1.291544, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+103, 23287, 530, -4940.023, 535.2883, 7.127902, 1.675516, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+104, 23287, 530, -4959.944, 591.9269, 12.99098, 1.989675, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+105, 23287, 530, -4950.465, 451.1373, 0.7255403, 0.9424778, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+106, 23287, 530, -4991.896, 300.7972, -1.304715, 3.281219, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+107, 23287, 530, -4970.338, 322.2038, -2.636889, 3.909538, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+108, 23287, 530, -4969.645, 298.882, -3.503148, 3.420845, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+109, 23287, 530, -5069.865, 170.6837, -9.401138, 0.8203048, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+110, 23287, 530, -4959.418, 393.7061, -0.9294897, 4.520403, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+111, 23287, 530, -4926.752, 455.5083, 1.527722, 1.396263, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+112, 23287, 530, -4940.023, 535.2883, 7.127902, 1.675516, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+113, 23287, 530, -4921.108, 528.4734, 6.568361, 2.042035, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+114, 23287, 530, -4904.557, 530.2072, 4.563097, 2.129302, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+115, 23287, 530, -4983.617, 632.3187, 20.58379, 3.054326, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+116, 23287, 530, -4956.983, 356.4414, -2.264346, 4.468043, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+117, 23287, 530, -4965.987, 483.2187, 4.143906, 0.03490658, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+118, 23287, 530, -4941.028, 469.1831, 0.2023413, 1.064651, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+119, 23287, 530, -4940.023, 535.2883, 7.127902, 1.675516, 300, 4299), -- 23287 (Area: 3965) (Auras: 31261 - 31261)
(@CGUID+120, 23287, 530, -5178.181, 642.9128, 39.79438, 1.919862, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+121, 23287, 530, -5187.549, 638.7712, 41.74369, 1.954769, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+122, 23287, 530, -5170.539, 614.4111, 37.59125, 4.921828, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+123, 23287, 530, -5186.184, 617.5848, 42.23409, 4.34587, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+124, 23287, 530, -5219.594, 636.9407, 48.11805, 1.605703, 300, 4299), -- 23287 (Area: 3965
(@CGUID+125, 23287, 530, -5209.468, 636.0561, 45.94989, 1.780236, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+126, 23287, 530, -5209.135, 619.5105, 46.42957, 4.502949, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+127, 23287, 530, -5197.959, 636.2016, 44.08671, 1.64061, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+128, 23287, 530, -5195.912, 618.5502, 44.35371, 4.555309, 300, 4299), -- 23287 (Area: 3965)
(@CGUID+129, 23169, 530, -5009.558, 687.8375, 18.86654, 1.480325, 300, 4299), -- 23169 (Area: 3965) -- Nethermine Flayer
(@CGUID+130, 23169, 530, -4967.988, 620.9315, 15.97108, 4.676374, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+131, 23169, 530, -4958.076, 598.2208, 13.32545, 5.52459, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+132, 23169, 530, -4979.914, 633.551, 20.30668, 2.572264, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+133, 23169, 530, -5013.305, 639.0559, 23.55437, 1.450203, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+134, 23169, 530, -4966.186, 655.6455, 20.22517, 2.583087, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+135, 23169, 530, -5010.128, 689.9114, 18.92688, 5.894914, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+136, 23169, 530, -4979.914, 633.551, 20.30668, 2.465721, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+137, 23169, 530, -5013.305, 639.0559, 23.55437, 1.212913, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+138, 23169, 530, -4958.076, 598.2208, 13.32545, 5.791277, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+139, 23169, 530, -4979.914, 633.551, 20.30668, 5.372581, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+140, 23169, 530, -5010.128, 689.9114, 18.92688, 6.151822, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+141, 23169, 530, -4966.186, 655.6455, 20.22517, 5.770682, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+142, 23169, 530, -4958.076, 598.2208, 13.32545, 5.80118, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+143, 23169, 530, -5013.305, 639.0559, 23.55437, 1.466077, 300, 4299), -- 23169 (Area: 3965
(@CGUID+144, 23169, 530, -4919.305, 584.1595, 5.835848, 5.860526, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+145, 23169, 530, -4945.825, 567.2396, 8.357429, 4.963535, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+146, 23169, 530, -4920.444, 554.3483, 6.81108, 1.592739, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+147, 23169, 530, -4885.112, 529.7742, -0.07188316, 3.104875, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+148, 23169, 530, -4905.904, 531.3935, 4.766143, 4.255386, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+149, 23169, 530, -4958.076, 598.2208, 13.32545, 2.345144, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+150, 23169, 530, -4946.532, 565.8182, 8.39564, 2.171958, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+151, 23169, 530, -4971.019, 481.9048, 4.255131, 5.742048, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+152, 23169, 530, -4978.048, 461.4908, 3.580604, 1.284639, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+153, 23169, 530, -4910.448, 466.246, 1.150314, 4.61044, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+154, 23169, 530, -4922.486, 452.6082, 1.741542, 1.848747, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+155, 23169, 530, -4883.248, 474.9959, -2.790574, 0.4947965, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+156, 23169, 530, -4954.734, 439.9607, 2.19193, 0.7067229, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+157, 23169, 530, -4917.937, 430.3383, -1.619577, 5.612211, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+158, 23169, 530, -4958.768, 351.7107, -2.247269, 1.772254, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+159, 23169, 530, -5038.031, 350.875, 2.62308, 3.72062, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+160, 23169, 530, -4973.916, 319.2121, -2.417306, 1.525316, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+161, 23169, 530, -4931.086, 298.9532, -11.71306, 3.185381, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+162, 23169, 530, -5005.922, 290.7715, 0.05161196, 0.369805, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+163, 23169, 530, -5039.163, 319.1835, -3.561619, 3.754901, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+164, 23169, 530, -5010.779, 274.0312, -1.596922, 4.01841, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+165, 23169, 530, -4954.379, 295.0413, -5.898067, 2.82695, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+166, 23169, 530, -4991.504, 250.8047, -5.723885, 4.988063, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+167, 23169, 530, -5062.854, 293.8755, -8.149835, 3.967818, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+168, 23169, 530, -5018.055, 224.3272, -10.22529, 0.6886171, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+169, 23169, 530, -4976.179, 227.3799, -10.6006, 0.5956746, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+170, 23169, 530, -4935.262, 463.8304, 0.3223694, 0.1625037, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+171, 23169, 530, -4922.94, 501.7582, 2.849531, 5.342018, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+172, 23169, 530, -4975.554, 269.0946, -6.799523, 5.499429, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+173, 23169, 530, -4960.398, 355.0888, -1.787407, 5.133213, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+174, 23169, 530, -5016.044, 224.2127, -10.37248, 2.795499, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+175, 23169, 530, -5060.153, 197.7174, -9.609101, 3.258459, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+176, 23169, 530, -5049.03, 166.6831, -12.72408, 2.162152, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+177, 23169, 530, -4991.019, 249.8632, -5.817747, 2.753308, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+178, 23169, 530, -4977.148, 226.7941, -10.30185, 2.419013, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+179, 23169, 530, -5002.327, 291.3423, -0.9251177, 3.278219, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+180, 23169, 530, -5074.881, 159.3353, -10.2591, 1.89717, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+181, 23169, 530, -5094.841, 177.7091, -8.408723, 4.038786, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+182, 23169, 530, -5059.071, 126.2977, -16.5597, 5.170018, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+183, 23169, 530, -5109.454, 127.173, -13.81656, 1.71806, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+184, 23169, 530, -4954.41, 294.5933, -5.898048, 0.8626862, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+185, 23169, 530, -5116.198, 103.0558, -12.65989, 4.638427, 300, 4299), -- 23169 (Area: 3965) (Auras: )
(@CGUID+186, 23169, 530, -5143.03, 145.8997, -12.47764, 2.741143, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+187, 23169, 530, -5110.538, 154.916, -10.57434, 1.115047, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+188, 23169, 530, -5047.887, 169.4041, -12.75727, 4.620065, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+189, 23169, 530, -5003.257, 159.8983, -14.73863, 0.2256795, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+190, 23169, 530, -5009.199, 275.662, -1.798863, 0.7142927, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+191, 23169, 530, -4975.545, 227.9456, -10.74416, 0.6228198, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+192, 23169, 530, -4991.768, 250.8438, -5.681248, 2.223132, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+193, 23169, 530, -5062.847, 293.5673, -8.190778, 4.098309, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+194, 23169, 530, -5001.911, 292.2764, -0.9095231, 1.151772, 300, 4299), -- 23169 (Area: 3965
(@CGUID+195, 23169, 530, -4971.963, 318.408, -2.593545, 1.286586, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+196, 23169, 530, -5037.165, 349.7794, 2.602906, 3.60786, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+197, 23169, 530, -4932.7, 299.3089, -11.32172, 1.003917, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+198, 23169, 530, -5040.047, 317.9085, -4.035919, 4.635794, 300, 4299), -- 23169 (Area: 396
(@CGUID+199, 23169, 530, -4957.847, 405.2784, 0.9240213, 3.046554, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+200, 23169, 530, -4948.438, 460.1876, 0.1027715, 0.9935466, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+201, 23169, 530, -4917.532, 431.2552, -1.466718, 2.644402, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+202, 23169, 530, -4955.563, 438.1111, 2.453238, 3.585394, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+203, 23169, 530, -4924.495, 453.2117, 1.67316, 0.4023848, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+204, 23169, 530, -4978.226, 457.8618, 3.579613, 5.989014, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+205, 23169, 530, -4910.379, 470.4026, 0.8641118, 1.141848, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+206, 23169, 530, -4970.949, 480.7429, 4.06482, 5.434553, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+207, 23169, 530, -4885.628, 472.7194, -2.342252, 6.117043, 300, 4299), -- 23169 (Area: 396
(@CGUID+208, 23169, 530, -4919.86, 502.4582, 2.580096, 3.676699, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+209, 23169, 530, -4919.363, 552.8414, 6.88634, 6.262745, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+210, 23169, 530, -4946.429, 566.189, 8.320451, 0.8319092, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+211, 23169, 530, -4884.078, 530.2368, -0.2521183, 4.288206, 300, 4299), -- 23169 (Area: 396
(@CGUID+212, 23169, 530, -4934.582, 535.1161, 6.564591, 4.536118, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+213, 23169, 530, -4906.604, 532.5593, 4.957226, 1.968214, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+214, 23169, 530, -4920.163, 587.0939, 6.468598, 2.24315, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+215, 23169, 530, -4959.686, 596.4736, 13.12449, 5.048675, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+216, 23169, 530, -4978.36, 633.9875, 19.98329, 0.7612703, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+217, 23169, 530, -4962.661, 656.2208, 19.60664, 2.322476, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+218, 23169, 530, -5015.241, 641.3285, 23.3768, 3.365274, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+219, 23169, 530, -5010.128, 689.9114, 18.78944, 2.978981, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+220, 23169, 530, -5077.739, 333.02, 5.179652, 2.509581, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+221, 23169, 530, -5019.065, 222.1603, -10.62667, 4.953171, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+222, 23169, 530, -4979.019, 269.4832, -6.478584, 3.350721, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+223, 23169, 530, -5058.351, 196.9229, -9.941554, 5.702285, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+224, 23169, 530, -5092.194, 181.7484, -8.519144, 6.063996, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+225, 23169, 530, -5071.979, 158.8044, -10.67218, 0.01778779, 300, 4299), -- 23169 (Area: 3965)
(@CGUID+226, 23326, 530, -5001.687, 643.4487, 22.84591, 5.311926, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732) -- nethermine ravager
(@CGUID+227, 23326, 530, -5016.762, 689.4977, 18.98941, 4.254198, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+228, 23326, 530, -4948.338, 574.87, 9.780287, 5.182978, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+229, 23326, 530, -4978.119, 622.5638, 18.00215, 2.614223, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+230, 23326, 530, -4967.506, 658.9974, 20.74481, 3.170801, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+231, 23326, 530, -4957.431, 584.6068, 12.20244, 1.852743, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+232, 23326, 530, -5005.486, 644.3996, 22.76456, 1.884956, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+233, 23326, 530, -5015.393, 692.0869, 18.86917, 2.075273, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+234, 23326, 530, -4967.506, 658.9974, 20.74481, 0.5888432, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+235, 23326, 530, -4978.119, 622.5638, 18.00215, 1.621879, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+236, 23326, 530, -5005.486, 644.3996, 22.76456, 1.646531, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+237, 23326, 530, -5015.393, 692.0869, 18.86917, 3.379317, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+238, 23326, 530, -4951.823, 581.4465, 11.22198, 2.676563, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+239, 23326, 530, -4946.557, 579.6539, 10.15224, 1.13619, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+240, 23326, 530, -4928.474, 480.4054, -0.1783632, 0.9677349, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+241, 23326, 530, -4918.411, 483.8331, 0.07626688, 2.36952, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+242, 23326, 530, -4921.181, 439.6729, 0.8384005, 0.5438383, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+243, 23326, 530, -4952.597, 401.4749, -0.3538051, 3.418738, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+244, 23326, 530, -4868.084, 531.6934, -1.683346, 3.018919, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+245, 23326, 530, -4965.881, 349.554, -1.536497, 4.615863, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+246, 23326, 530, -4971.478, 304.6561, -3.048606, 5.414115, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+247, 23326, 530, -4953.476, 284.0535, -6.819366, 4.314682, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+248, 23326, 530, -5014.135, 288.1924, 1.793531, 5.620615, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+249, 23326, 530, -4962.295, 381.2368, -1.744818, 4.602769, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+250, 23326, 530, -4995.37, 236.0123, -7.475551, 5.915942, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+251, 23326, 530, -5046.238, 204.7434, -11.17455, 0.3013434, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+252, 23326, 530, -5015.046, 264.2135, -0.9630037, 4.380797, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+253, 23326, 530, -5054.073, 191.3642, -11.12945, 4.913056, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+254, 23326, 530, -5068.295, 161.4686, -10.65775, 3.185342, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+255, 23326, 530, -5084.805, 170.5898, -8.363562, 2.470629, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+256, 23326, 530, -5118.899, 113.8509, -12.95813, 1.053266, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+257, 23326, 530, -5121.38, 140.2108, -12.24173, 4.717645, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+258, 23326, 530, -5028.928, 162.4327, -14.69674, 4.598915, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+259, 23326, 530, -5013.326, 287.5868, 1.554583, 2.353504, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+260, 23326, 530, -5162.3, 147.2595, -13.68486, 2.213196, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+261, 23326, 530, -5195.16, 185.9774, -12.47411, 2.135337, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+262, 23326, 530, -5185.331, 154.8984, -12.49722, 5.532097, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+263, 23326, 530, -5028.03, 163.2786, -14.87766, 0.7552983, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+264, 23326, 530, -5055.308, 191.3719, -10.81202, 0.5039104, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+265, 23326, 530, -4995.011, 236.3476, -7.462594, 6.063029, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+266, 23326, 530, -5037.676, 207.097, -11.84669, 3.400416, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+267, 23326, 530, -5038.441, 207.0967, -11.75817, 3.849093, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732
(@CGUID+268, 23326, 530, -4949.182, 294.4462, -6.966339, 0.678574, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+269, 23326, 530, -4973.03, 308.0056, -2.714836, 1.892263, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+270, 23326, 530, -4967.495, 349.8932, -1.395857, 2.333905, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+271, 23326, 530, -4950.731, 403.7043, -0.1075567, 0.8233353, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+272, 23326, 530, -4921.817, 439.4337, 0.8760551, 3.996498, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+273, 23326, 530, -4906.549, 472.265, 0.7405176, 2.369046, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+274, 23326, 530, -4942.342, 455.2948, 0.785726, 3.048413, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+275, 23326, 530, -4927.634, 480.4459, -0.2140773, 3.812036, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+276, 23326, 530, -4917.617, 533.3819, 6.377127, 4.497816, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+277, 23326, 530, -4947.289, 577.6053, 9.956731, 4.047926, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+278, 23326, 530, -4916.31, 586.3352, 5.467452, 0.6311421, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+279, 23326, 530, -4979.853, 626.7228, 18.89969, 2.537099, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+280, 23326, 530, -4968.971, 659.8459, 20.95211, 0.4729249, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+281, 23326, 530, -5002.798, 646.2766, 22.85282, 0.4368325, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+282, 23326, 530, -5084.679, 170.6154, -8.364773, 2.919591, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+283, 23326, 530, -5069.651, 161.7928, -10.45241, 3.358221, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+284, 23326, 530, -5122.319, 139.8094, -12.27753, 4.208749, 300, 3224), -- 23326 (Area: 3965) (Auras: 32732 - 32732)
(@CGUID+285, 23309, 530, -4860.765, 468.7788, -5.74204, 4.24115, 300, 4299), -- 23309 (Area: 3965) -- murkblood overseer
(@CGUID+286, 23324, 530, -5196.179, 166.8186, -12.59387, 4.930552, 300, 4299), -- 23324 (Area: 3965) (Auras: 40743 - 40743) Crazed Murkblood Miner
(@CGUID+287, 23324, 530, -5210.658, 146.5081, -14.06982, 3.909939, 300, 4299), -- 23324 (Area: 3965) (Auras: 40743 - 40743)
(@CGUID+288, 23324, 530, -5178.911, 173.927, -11.54059, 1.130439, 300, 4299), -- 23324 (Area: 3965) (Auras: 40743 - 40743) (possible waypoints or random movement)
(@CGUID+289, 23324, 530, -5192.216, 133.6122, -12.00781, 2.232028, 300, 4299), -- 23324 (Area: 3965) (possible waypoints or random movement)
(@CGUID+290, 23324, 530, -5179.031, 152.351, -12.46348, 4.376995, 300, 4299), -- 23324 (Area: 3965) (possible waypoints or random movement)
(@CGUID+291, 23324, 530, -5236.018, 198.761, -13.93909, 1.100167, 300, 4299), -- 23324 (Area: 3965) (Auras: 40743 - 40743)
(@CGUID+292, 23324, 530, -5210.276, 207.8238, -12.79146, 1.512519, 300, 4299), -- 23324 (Area: 3965) (Auras: 40743 - 40743) (possible waypoints or random movement)
(@CGUID+293, 23324, 530, -5230.436, 213.0719, -12.7647, 4.868175, 300, 4299), -- 23324 (Area: 3965) (Auras: 40743 - 40743) (possible waypoints or random movement)
(@CGUID+294, 23324, 530, -5254.496, 210.4873, -13.75615, 5.228326, 300, 4299), -- 23324 (Area: 3965) (Auras: 40743 - 40743) (possible waypoints or random movement)
(@CGUID+295, 23324, 530, -5224.815, 227.1456, -10.97935, 5.068825, 300, 4299), -- 23324 (Area: 3965) (possible waypoints or random movement)
(@CGUID+296, 23305, 530, -5223.799, 139.1362, -13.6459, 5.91905, 300, 4299), -- 23305 (Area: 3965) (Auras: 40743 - 40743) (possible waypoints or random movement) Crazed Murkblood Foreman
(@CGUID+297, 23305, 530, -5247.993, 226.5564, -12.5696, 3.970754, 300, 4299); -- 23305 (Area: 3965) (Auras: 40743 - 40743)

-- Remove dubble spawns
DELETE FROM `creature` WHERE `guid` IN (143638, 143631, 143641, 143643, 143642, 143714, 143707, 143701, 143649, 143706, 143656, 143658, 143659, 143665, 48234, 48235, 48236, 48237, 48241, 48240, 48239, 48238, 48242, 48243, 48229, 48232, 48233, 48231, 48230, 48245, 48244, 48251, 48246, 48248, 48247, 48253, 48252, 48256, 48255, 48254, 48249, 48250, 143880, 143820, 143878, 143820, 143776, 143820, 143847, 143860, 143784, 143771, 143862, 143817, 143770, 143774, 143765, 143773, 143772, 143786, 143855, 143758, 143775, 143818, 143761, 143793, 143842, 143791, 143866, 143754, 143867, 143752, 143797, 143868, 143750, 143749, 143803, 143836, 143748, 143747, 143804, 143744, 143807, 143805, 143740, 143741, 143806, 143824, 143873, 143835, 143727, 143738, 143734, 143825, 143831, 143728, 143812, 143835, 143730, 143813, 143826, 143828, 143739, 143814, 143729, 143823, 143829, 143725, 143736, 143815, 143832, 143830, 143732, 143811, 52032, 143792, 143759, 143789, 52021, 52023, 52022, 52020, 52024, 52016, 52017, 52018, 52015, 52014,52019, 48257, 52013, 52011, 52012, 48258, 52030, 52031, 143762, 143846);

-- update spawn distance
UPDATE `creature` SET `spawndist`=4 WHERE `id` IN (23326, 23169);
UPDATE `creature` SET `MovementType`=1 WHERE `id` IN (23326, 23169);

-- Change weapons are sheathed for 23376
SET @ENTRY := 23376;
DELETE FROM `creature_template_addon` WHERE `entry`=@ENTRY;
INSERT INTO `creature_template_addon` (`entry`) VALUES (@ENTRY);

-- Remove existing mine cars
DELETE FROM `creature` WHERE `guid` IN (370643, 52334, 52335);

-- Make Murkblood Miner spawn dead
DELETE FROM `creature_template_addon` WHERE `entry`=23287;
INSERT INTO `creature_template_addon` (`entry`, `bytes1`, `bytes2`, `auras`) VALUES (23287, 0, 1, 31261);

-- Add aura to nethermine ravager
DELETE FROM `creature_template_addon` WHERE `entry`=23326;
INSERT INTO `creature_template_addon` (`entry`, `bytes1`, `bytes2`, `auras`) VALUES (23326, 0, 0, 32732);

-- Mine Car SAI
SET @ENTRY := 23289;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,25,0,100,0,0,0,0,0,11,40684,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mine Car - On Reset - Cast 'Mine Cart'"),
(@ENTRY,0,1,0,38,0,100,0,1,1,0,0,103,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Mine Car - On Data Set 1 1 - Set Rooted On"),
(@ENTRY,0,2,0,38,0,100,0,1,0,0,0,65,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"Mine Car - On Data Set 1 0 - Resume Waypoint");

-- Dragonmaw Foreman SAI
SET @ENTRY := 23376;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,0,0,45,1,1,0,0,0,0,10,143599,23289,0,0,0,0,0,"Dragonmaw Foreman - In Combat - Set Data 1 1"),
(@ENTRY,0,1,0,25,0,100,0,0,0,0,0,45,1,0,0,0,0,0,10,143599,23289,0,0,0,0,0,"Dragonmaw Foreman - On Reset - Set Data 1 0"),
(@ENTRY,0,2,0,25,0,100,0,0,0,0,0,45,1,0,0,0,0,0,10,143600,23289,0,0,0,0,0,"Dragonmaw Foreman - On Reset - Set Data 1 0"),
(@ENTRY,0,3,0,0,0,100,0,0,0,0,0,45,1,1,0,0,0,0,10,143600,23289,0,0,0,0,0,"Dragonmaw Foreman - In Combat - Set Data 1 1");

-- Crazed Murkblood Miner SAI
SET @ENTRY := 23324;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,60000,120000,60000,120000,11,40743,0,0,0,0,0,1,0,0,0,0,0,0,0,"Crazed Murkblood Miner - Out of Combat - Cast 'Frenzy'");

-- Crazed Murkblood Foreman SAI
SET @ENTRY := 23305;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,60000,120000,60000,120000,11,40743,0,0,0,0,0,1,0,0,0,0,0,0,0,"Crazed Murkblood Foreman - Out of Combat - Cast 'Frenzy'");

-- Add creature addon
DELETE FROM `creature_addon` WHERE `guid` IN (143601, 143628, 143720, 143721, 143724, 143719, 143718, 143602, 143607, 143611, 143613, 143610, 143616, 143614, 143622, 143618, 143620, 143626, 143619, 143615, 143612, 143609, 1436058, 143606, 143603, 143604, 143716, 143717, 143723, 143605, 143722);
INSERT INTO `creature_addon` (`guid`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES
(143601, 0, 1, 233, NULL),
(143628, 0, 1, 233, NULL),
(143720, 0, 1, 233, NULL),
(143721, 0, 1, 233, NULL),
(143724, 0, 1, 233, NULL),
(143719, 0, 1, 233, NULL),
(143718, 0, 1, 233, NULL),
(143602, 0, 1, 233, NULL),
(143607, 0, 1, 233, NULL),
(143611, 0, 1, 233, NULL),
(143613, 0, 1, 233, NULL),
(143610, 0, 1, 233, NULL),
(143616, 0, 1, 233, NULL),
(143614, 0, 1, 233, NULL),
(143622, 0, 1, 233, NULL),
(143618, 0, 1, 233, NULL),
(143620, 0, 1, 233, NULL),
(143626, 0, 1, 233, NULL),
(143619, 0, 1, 233, NULL),
(143615, 0, 1, 233, NULL),
(143612, 0, 1, 233, NULL),
(143609, 0, 1, 233, NULL),
(143605, 0, 1, 233, NULL),
(143606, 0, 1, 233, NULL),
(143603, 0, 1, 233, NULL),
(143604, 0, 1, 233, NULL),
(143716, 0, 1, 233, NULL),
(143717, 0, 1, 233, NULL),
(143723, 0, 1, 233, NULL),
(143722, 0, 1, 233, NULL);

-- Update position 
UPDATE `creature` SET `position_x`=-5165.002, `position_y`=534.0313, `position_z`=82.90166, `orientation`=3.857178 WHERE  `guid`=52106;
UPDATE `creature` SET `position_x`=-5146.481, `position_y`=517.6528, `position_z`=85.1688, `orientation`=3.351032 WHERE  `guid`=52109;
UPDATE `creature` SET `position_x`=-5166.383, `position_y`=563.9937, `position_z`=80.58907, `orientation`=3.211406 WHERE  `guid`=52107;
UPDATE `creature` SET `position_x`=-5128.689, `position_y`=579.8379, `position_z`=85.25567, `orientation`=0.2617994 WHERE  `guid`=52112;
UPDATE `creature` SET `position_x`=-5133.728, `position_y`=591.6107, `position_z`=84.34083, `orientation`=0.08726646 WHERE  `guid`=52111;
UPDATE `creature` SET `position_x`=-5103.687, `position_y`=585.6875, `position_z`=85.79736, `orientation`=2.111848 WHERE  `guid`=52114;
UPDATE `creature` SET `position_x`=-5079.457, `position_y`=538.1425, `position_z`=86.41579, `orientation`=5.5676 WHERE  `guid`=52231;
UPDATE `creature` SET `position_x`=-5094.562, `position_y`=596.6859, `position_z`=86.2102, `orientation`=2.617994 WHERE  `guid`=52115;
UPDATE `creature` SET `position_x`=-5102.299, `position_y`=623.4721, `position_z`=86.07585, `orientation`=1.413717 WHERE  `guid`=52113;
UPDATE `creature` SET `position_x`=-5089.073, `position_y`=616.179, `position_z`=85.79301, `orientation`=1.082104 WHERE  `guid`=52230;
UPDATE `creature` SET `position_x`=-5162.11, `position_y`=629.679, `position_z`=79.66261, `orientation`=2.199115 WHERE  `guid`=52108;
UPDATE `creature` SET `position_x`=-5146.516, `position_y`=639.3836, `position_z`=81.87501, `orientation`=2.321288 WHERE  `guid`=52110;

-- Update position Ronag the slave driver
UPDATE `creature` SET `position_x`=-5161.059, `position_y`=288.1674, `position_z`=-27.11326, `orientation`=5.078908 WHERE  `guid`=40729;

-- Update mistress of the mine positions
UPDATE `creature` SET `position_x`=-5254.184, `position_y`=628.4734, `position_z`=48.99072, `orientation`=2.617994 WHERE  `guid`=88898;

-- Update toranaku position
UPDATE `creature` SET `position_x`=-5133.719, `position_y`=227.1979, `position_z`=-26.26821, `orientation`=2.059489 WHERE  `guid`=40740;

-- Update murkblood overseer positions
UPDATE `creature` SET `position_x`=-4898.184, `position_y`=411.9311, `position_z`=-5.810406, `orientation`=3.368485 WHERE  `guid`=43919;
UPDATE `creature` SET `position_x`=-4907.272, `position_y`=314.5579, `position_z`=-12.17146, `orientation`=1.518436 WHERE  `guid`=40742;
UPDATE `creature` SET `position_x`=-4961.051, `position_y`=211.6252, `position_z`=-10.97874, `orientation`=4.031711 WHERE  `guid`=42566;
UPDATE `creature` SET `position_x`=-4979.739, `position_y`=161.6144, `position_z`=-15.42497, `orientation`=2.670354 WHERE  `guid`=48228;

-- Add missing mistress of the mine
SET @ENTRY := 23149;
SET @GUID := 23430;
DELETE FROM `creature` WHERE `guid`=@GUID;
INSERT INTO `creature` (`guid`, `id`, `map`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `curhealth`) VALUES 
(@GUID, @ENTRY, 530, -5165.22, 757.3012, 49.79086, 2.932153, 25, 104790);

DELETE FROM `creature_addon` WHERE `guid` IN (143846, 143847, 52283, 23144);

DELETE FROM `creature_formations` WHERE `leaderGUID`=143599;
INSERT INTO `creature_formations` (`leaderGUID`, `memberGUID`, `dist`, `angle`, `groupAI`) VALUES
(143599, 143599, 0, 0, 1),
(143599, 52345, 2, 0, 2);

-- Pathing for Mine Car Entry: 23289 'TDB FORMAT' 
SET @NPC := 143599;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5141.078,`position_y`=637.8817,`position_z`=36.18917 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5141.078,637.8817,36.18917,0,0,0,0,100,0), -- 17:08:25
(@PATH,2,-5140.013,643.7603,36.68985,0,0,0,0,100,0), -- 17:08:26
(@PATH,3,-5139.763,645.5103,36.93985,0,0,0,0,100,0), -- 17:08:26
(@PATH,4,-5139.761,650.6669,37.334,0,0,0,0,100,0), -- 17:08:30
(@PATH,5,-5140.261,653.6669,37.834,0,0,0,0,100,0), -- 17:08:30
(@PATH,6,-5140.511,657.9169,38.084,0,0,0,0,100,0), -- 17:08:30
(@PATH,7,-5147.932,667.6057,41.11486,0,0,0,0,100,0), -- 17:08:36
(@PATH,8,-5151.682,670.6057,41.36486,0,0,0,0,100,0), -- 17:08:36
(@PATH,9,-5154.71,670.9447,41.20504,0,0,0,0,100,0), -- 17:08:42
(@PATH,10,-5157.904,664.1409,40.12909,0,0,0,0,100,0), -- 17:08:44
(@PATH,11,-5157.904,660.6409,39.87909,0,0,0,0,100,0), -- 17:08:44
(@PATH,12,-5161.058,651.3033,37.78137,0,0,0,0,100,0), -- 17:08:49
(@PATH,13,-5164.057,648.1979,37.81743,0,0,0,0,100,0), -- 17:08:51
(@PATH,14,-5165.057,647.4479,37.56743,0,0,0,0,100,0), -- 17:08:51
(@PATH,15,-5167.557,645.1979,37.81743,0,0,0,0,100,0), -- 17:08:51
(@PATH,16,-5170.843,644.0839,38.46222,0,0,0,0,100,0), -- 17:08:55
(@PATH,17,-5172.093,643.3339,38.71222,0,0,0,0,100,0), -- 17:08:55
(@PATH,18,-5173.593,642.8339,38.96222,0,0,0,0,100,0), -- 17:08:55
(@PATH,19,-5186.051,637.8183,41.7608,0,0,0,0,100,0), -- 17:09:00
(@PATH,20,-5189.714,636.5399,42.51512,0,0,0,0,100,0), -- 17:09:04
(@PATH,21,-5191.214,636.2899,42.76512,0,0,0,0,100,0), -- 17:09:04
(@PATH,22,-5197.214,634.7899,44.26512,0,0,0,0,100,0), -- 17:09:04
(@PATH,23,-5203.103,634.7686,45.08968,0,0,0,0,100,0), -- 17:09:09
(@PATH,24,-5206.103,634.7686,45.58968,0,0,0,0,100,0), -- 17:09:09
(@PATH,25,-5209.806,634.6367,46.42096,0,0,0,0,100,0), -- 17:09:13
(@PATH,26,-5213.186,634.7311,46.78786,0,0,0,0,100,0), -- 17:09:16
(@PATH,27,-5213.186,634.7311,46.78786,3.057303,13000,0,0,100,0), -- 17:09:16
(@PATH,28,-5220.281,635.3306,48.28771,0,0,0,0,100,0), -- 17:09:23
(@PATH,29,-5225.046,631.6472,48.62878,0,0,0,0,100,0), -- 17:09:24
(@PATH,30,-5226.796,630.3972,48.62878,0,0,0,0,100,0), -- 17:09:24
(@PATH,31,-5228.296,629.1472,48.87878,0,0,0,0,100,0), -- 17:09:24
(@PATH,32,-5229.272,628.4141,49.11567,0,0,0,0,100,0), -- 17:09:29
(@PATH,33,-5212.051,622.3498,47.22783,0,0,0,0,100,0), -- 17:09:34
(@PATH,34,-5206.287,621.1569,46.21485,0,0,0,0,100,0), -- 17:09:39
(@PATH,35,-5186.77,619.8926,42.72266,0,0,0,0,100,0), -- 17:09:44
(@PATH,36,-5183.428,619.211,41.72144,0,0,0,0,100,0), -- 17:09:47
(@PATH,37,-5180.678,618.461,40.97144,0,0,0,0,100,0), -- 17:09:47
(@PATH,38,-5177.428,617.961,39.97144,0,0,0,0,100,0), -- 17:09:47
(@PATH,39,-5163.798,617.6891,36.96609,0,0,0,0,100,0), -- 17:09:53
(@PATH,40,-5157.38,620.9669,36.305,0,0,0,0,100,0), -- 17:09:58
(@PATH,41,-5155.63,622.2169,36.305,0,0,0,0,100,0), -- 17:09:58
(@PATH,42,-5151.773,625.8492,36.2674,0,0,0,0,100,0), -- 17:10:02
(@PATH,43,-5147.023,630.5992,36.0174,0,0,0,0,100,0), -- 17:10:02
(@PATH,44,-5151.613,626.0107,36.01761,0,0,0,0,100,0), -- 17:10:04
(@PATH,45,-5151.613,626.0107,36.01761,0.7864318,13000,0,0,100,0), -- 17:10:04
(@PATH,46,-5147.015,630.6146,36.24972,0,0,0,0,100,0), -- 17:10:12
(@PATH,47,-5146.276,631.5181,36.09008,0,0,0,0,100,0), -- 17:10:13
(@PATH,48,-5139.943,643.661,36.66342,0,0,0,0,100,0), -- 17:10:17
(@PATH,49,-5139.693,645.661,36.91342,0,0,0,0,100,0), -- 17:10:17
(@PATH,50,-5139.456,647.1063,37.25185,0,0,0,0,100,0), -- 17:10:20
(@PATH,51,-5139.706,650.6063,37.25185,0,0,0,0,100,0), -- 17:10:20
(@PATH,52,-5140.206,653.6063,37.75185,0,0,0,0,100,0), -- 17:10:20
(@PATH,53,-5140.706,657.8563,38.25185,0,0,0,0,100,0), -- 17:10:20
(@PATH,54,-5148.056,667.8259,41.13531,0,0,0,0,100,0), -- 17:10:26
(@PATH,55,-5151.806,670.5759,41.38531,0,0,0,0,100,0), -- 17:10:26
(@PATH,56,-5154.813,670.973,41.23766,0,0,0,0,100,0), -- 17:10:34
(@PATH,57,-5157.935,667.3557,40.72045,0,0,0,0,100,0), -- 17:10:35
(@PATH,58,-5157.935,664.1057,40.22045,0,0,0,0,100,0), -- 17:10:35
(@PATH,59,-5157.935,660.6057,39.72045,0,0,0,0,100,0), -- 17:10:35
(@PATH,60,-5158.109,661.8198,39.61253,0,0,0,0,100,0), -- 17:10:40
(@PATH,61,-5158.109,661.8198,39.61253,4.732054,13000,0,0,100,0), -- 17:10:40
(@PATH,62,-5158.071,660.382,39.43446,0,0,0,0,100,0), -- 17:10:47
(@PATH,63,-5161.05,651.4003,37.68608,0,0,0,0,100,0), -- 17:10:48
(@PATH,64,-5164.23,648.3284,37.75933,0,0,0,0,100,0), -- 17:10:51
(@PATH,65,-5164.98,647.5784,37.75933,0,0,0,0,100,0), -- 17:10:51
(@PATH,66,-5167.73,645.3284,37.75933,0,0,0,0,100,0), -- 17:10:51
(@PATH,67,-5170.907,644.0687,38.51237,0,0,0,0,100,0), -- 17:10:54
(@PATH,68,-5172.157,643.5687,38.76237,0,0,0,0,100,0), -- 17:10:54
(@PATH,69,-5173.657,642.8187,39.01237,0,0,0,0,100,0), -- 17:10:54
(@PATH,70,-5186.038,637.9409,41.76183,0,0,0,0,100,0), -- 17:10:58
(@PATH,71,-5189.85,636.6265,42.57893,0,0,0,0,100,0), -- 17:11:03
(@PATH,72,-5191.35,636.3765,42.82893,0,0,0,0,100,0), -- 17:11:03
(@PATH,73,-5197.35,634.8765,44.32893,0,0,0,0,100,0), -- 17:11:03
(@PATH,74,-5202.985,634.672,45.20214,0,0,0,0,100,0), -- 17:11:07
(@PATH,75,-5206.235,634.672,45.45214,0,0,0,0,100,0), -- 17:11:07
(@PATH,76,-5209.681,634.6359,46.49954,0,0,0,0,100,0), -- 17:11:12
(@PATH,77,-5225.227,631.6835,48.47477,0,0,0,0,100,0), -- 17:11:16
(@PATH,78,-5226.727,630.4335,48.72477,0,0,0,0,100,0), -- 17:11:16
(@PATH,79,-5228.477,629.1835,48.97477,0,0,0,0,100,0), -- 17:11:16
(@PATH,80,-5229.123,628.397,49.13941,0,0,0,0,100,0), -- 17:11:21
(@PATH,81,-5212.127,622.4219,47.1857,0,0,0,0,100,0), -- 17:11:26
(@PATH,82,-5206.309,621.397,46.11539,0,0,0,0,100,0), -- 17:11:29
(@PATH,83,-5200.812,620.8071,45.10049,0,0,0,0,100,0), -- 17:11:34
(@PATH,84,-5200.812,620.8071,45.10049,6.201494,13000,0,0,100,0), -- 17:11:34
(@PATH,85,-5196.44,620.4492,44.38279,0,0,0,0,100,0), -- 17:11:42
(@PATH,86,-5186.816,619.8555,42.8981,0,0,0,0,100,0), -- 17:11:43
(@PATH,87,-5183.516,619.1785,41.63834,0,0,0,0,100,0), -- 17:11:46
(@PATH,88,-5180.766,618.4285,40.88834,0,0,0,0,100,0), -- 17:11:46
(@PATH,89,-5177.516,617.9285,39.88834,0,0,0,0,100,0), -- 17:11:46
(@PATH,90,-5163.909,617.6057,37.10681,0,0,0,0,100,0), -- 17:11:53
(@PATH,91,-5157.449,621.0334,36.26365,0,0,0,0,100,0), -- 17:11:57
(@PATH,92,-5155.699,622.2834,36.26365,0,0,0,0,100,0), -- 17:11:57
(@PATH,93,-5154.149,623.4669,36.08303,0,0,0,0,100,0), -- 17:12:00
(@PATH,94,-5152.149,625.7169,36.08303,0,0,0,0,100,0), -- 17:12:00
(@PATH,95,-5146.899,630.7169,36.08303,0,0,0,0,100,0), -- 17:12:00
(@PATH,96,-5146.199,631.3462,36.08583,0,0,0,0,100,0), -- 17:12:05
(@PATH,97,-5139.907,643.7687,36.67455,0,0,0,0,100,0), -- 17:12:08
(@PATH,98,-5139.657,645.5187,36.92455,0,0,0,0,100,0), -- 17:12:08
(@PATH,99,-5139.875,650.5488,37.28083,0,0,0,0,100,0), -- 17:12:12
(@PATH,100,-5140.125,653.5488,37.78083,0,0,0,0,100,0), -- 17:12:12
(@PATH,101,-5140.625,657.7988,38.28083,0,0,0,0,100,0), -- 17:12:12
(@PATH,102,-5148.108,667.7523,40.98037,0,0,0,0,100,0), -- 17:12:18
(@PATH,103,-5151.608,670.7523,41.48037,0,0,0,0,100,0), -- 17:12:18
(@PATH,104,-5141.205,661.6332,38.87123,0,0,0,0,100,0), -- 17:12:19
(@PATH,105,-5141.205,661.6332,38.87123,1.692706,13000,0,0,100,0), -- 17:12:19
(@PATH,106,-5151.783,670.6835,41.29935,0,0,0,0,100,0), -- 17:12:27
(@PATH,107,-5154.538,671.0463,41.41257,0,0,0,0,100,0), -- 17:12:31
(@PATH,108,-5157.866,664.149,40.19033,0,0,0,0,100,0), -- 17:12:34
(@PATH,109,-5157.866,660.649,39.69033,0,0,0,0,100,0), -- 17:12:34
(@PATH,110,-5159.319,655.634,38.41046,0,0,0,0,100,0), -- 17:12:39
(@PATH,111,-5161.069,651.384,37.91046,0,0,0,0,100,0), -- 17:12:39
(@PATH,112,-5164.453,648.3511,37.68428,0,0,0,0,100,0), -- 17:12:42
(@PATH,113,-5164.953,647.6011,37.68428,0,0,0,0,100,0), -- 17:12:42
(@PATH,114,-5167.703,645.3511,37.93428,0,0,0,0,100,0), -- 17:12:42
(@PATH,115,-5171.067,643.7227,38.57786,0,0,0,0,100,0), -- 17:12:46
(@PATH,116,-5172.317,643.4727,38.57786,0,0,0,0,100,0), -- 17:12:46
(@PATH,117,-5173.567,642.9727,39.07786,0,0,0,0,100,0), -- 17:12:46
(@PATH,118,-5186.047,637.7537,41.88425,0,0,0,0,100,0), -- 17:12:50
(@PATH,119,-5177.467,641.3476,39.69143,0,0,0,0,100,0), -- 17:12:51
(@PATH,120,-5177.467,641.3476,39.69143,3.542526,13000,0,0,100,0), -- 17:12:51
(@PATH,121,-5186.172,637.7469,41.72967,0,0,0,0,100,0), -- 17:12:58
(@PATH,122,-5189.829,636.4236,42.40816,0,0,0,0,100,0), -- 17:13:02
(@PATH,123,-5191.579,636.1736,42.90816,0,0,0,0,100,0), -- 17:13:02
(@PATH,124,-5197.329,634.9236,44.40816,0,0,0,0,100,0), -- 17:13:02
(@PATH,125,-5203.01,634.6678,45.16574,0,0,0,0,100,0), -- 17:13:05
(@PATH,126,-5206.01,634.6678,45.41574,0,0,0,0,100,0), -- 17:13:05
(@PATH,127,-5209.636,634.6382,46.55074,0,0,0,0,100,0), -- 17:13:10
(@PATH,128,-5225.139,631.7184,48.56197,0,0,0,0,100,0), -- 17:13:15
(@PATH,129,-5226.889,630.4684,48.56197,0,0,0,0,100,0), -- 17:13:15
(@PATH,130,-5228.389,629.2184,48.81197,0,0,0,0,100,0), -- 17:13:15
(@PATH,131,-5229.226,628.3779,48.93416,0,0,0,0,100,0), -- 17:13:20
(@PATH,132,-5212.188,622.4875,47.14201,0,0,0,0,100,0), -- 17:13:25
(@PATH,133,-5206.112,621.292,46.29193,0,0,0,0,100,0), -- 17:13:28
(@PATH,134,-5186.827,619.9382,42.81411,0,0,0,0,100,0), -- 17:13:33
(@PATH,135,-5193.917,620.2497,43.88791,0,0,0,0,100,0), -- 17:13:36
(@PATH,136,-5193.917,620.2497,43.88791,6.204306,13000,0,0,100,0), -- 17:13:36
(@PATH,137,-5186.842,619.9473,42.7837,0,0,0,0,100,0), -- 17:13:43
(@PATH,138,-5183.527,619.1399,41.53972,0,0,0,0,100,0), -- 17:13:46
(@PATH,139,-5180.777,618.3899,40.78972,0,0,0,0,100,0), -- 17:13:46
(@PATH,140,-5177.527,617.8899,40.03972,0,0,0,0,100,0), -- 17:13:46
(@PATH,141,-5163.918,617.7509,36.96921,0,0,0,0,100,0), -- 17:13:51
(@PATH,142,-5158.669,620.3835,36.38764,0,0,0,0,100,0), -- 17:13:55
(@PATH,143,-5157.419,621.1335,36.38764,0,0,0,0,100,0), -- 17:13:55
(@PATH,144,-5155.669,622.3835,36.38764,0,0,0,0,100,0), -- 17:13:55
(@PATH,145,-5151.717,625.7791,36.29499,0,0,0,0,100,0), -- 17:13:59
(@PATH,146,-5146.967,630.5291,36.04499,0,0,0,0,100,0), -- 17:13:59
(@PATH,147,-5146.07,631.4751,36.08121,0,0,0,0,100,0), -- 17:14:04
(@PATH,148,-5140.069,643.69,36.68766,0,0,0,0,100,0), -- 17:14:07
(@PATH,149,-5139.569,645.69,36.93766,0,0,0,0,100,0), -- 17:14:07
(@PATH,150,-5139.775,650.586,37.32704,0,0,0,0,100,0), -- 17:14:11
(@PATH,151,-5140.275,653.586,37.82704,0,0,0,0,100,0), -- 17:14:11
(@PATH,152,-5140.525,657.836,38.07704,0,0,0,0,100,0), -- 17:14:11
(@PATH,153,-5139.893,650.9125,36.97102,0,0,0,0,100,0), -- 17:14:13
(@PATH,154,-5139.893,650.9125,36.97102,1.692638,13000,0,0,100,0), -- 17:14:13
(@PATH,155,-5140.585,657.8174,38.23678,0,0,0,0,100,0), -- 17:14:21
(@PATH,156,-5147.919,667.7463,41.09047,0,0,0,0,100,0), -- 17:14:24
(@PATH,157,-5151.669,670.7463,41.34047,0,0,0,0,100,0), -- 17:14:24
(@PATH,158,-5154.635,670.886,41.44817,0,0,0,0,100,0), -- 17:14:30
(@PATH,159,-5157.795,664.1933,40.14152,0,0,0,0,100,0), -- 17:14:33
(@PATH,160,-5157.795,660.6933,39.89152,0,0,0,0,100,0), -- 17:14:33
(@PATH,161,-5161.06,651.3977,37.80369,0,0,0,0,100,0), -- 17:14:38
(@PATH,162,-5164.019,648.2813,37.58054,0,0,0,0,100,0), -- 17:14:40
(@PATH,163,-5165.019,647.5313,37.58054,0,0,0,0,100,0), -- 17:14:40
(@PATH,164,-5167.769,645.2813,37.83054,0,0,0,0,100,0), -- 17:14:40
(@PATH,165,-5171.026,643.8893,38.4648,0,0,0,0,100,0), -- 17:14:44
(@PATH,166,-5172.276,643.3893,38.7148,0,0,0,0,100,0), -- 17:14:44
(@PATH,167,-5173.776,642.8893,38.9648,0,0,0,0,100,0), -- 17:14:44
(@PATH,168,-5185.981,637.8439,41.74332,0,0,0,0,100,0), -- 17:14:49
(@PATH,169,-5189.904,636.5654,42.50515,0,0,0,0,100,0), -- 17:14:52
(@PATH,170,-5191.154,636.3154,42.75515,0,0,0,0,100,0), -- 17:14:52
(@PATH,171,-5197.154,634.8154,44.50515,0,0,0,0,100,0), -- 17:14:52
(@PATH,172,-5186.579,637.4847,41.6531,0,0,0,0,100,0), -- 17:14:54
(@PATH,173,-5186.579,637.4847,41.6531,3.542525,13000,0,0,100,0), -- 17:14:54
(@PATH,174,-5197.299,634.7244,44.4835,0,0,0,0,100,0), -- 17:15:01
(@PATH,175,-5203.041,634.7999,45.31844,0,0,0,0,100,0), -- 17:15:04
(@PATH,176,-5206.041,634.7999,45.56844,0,0,0,0,100,0), -- 17:15:04
(@PATH,177,-5209.731,634.6368,46.40524,0,0,0,0,100,0), -- 17:15:09
(@PATH,178,-5225.014,631.6444,48.62695,0,0,0,0,100,0), -- 17:15:13
(@PATH,179,-5226.764,630.3944,48.62695,0,0,0,0,100,0), -- 17:15:13
(@PATH,180,-5228.514,629.1444,48.87695,0,0,0,0,100,0), -- 17:15:13
(@PATH,181,-5229.223,628.2013,49.11297,0,0,0,0,100,0), -- 17:15:18
(@PATH,182,-5212.107,622.3733,47.2334,0,0,0,0,100,0), -- 17:15:23
(@PATH,183,-5210.747,621.8912,46.56211,0,0,0,0,100,0), -- 17:15:28
(@PATH,184,-5210.747,621.8912,46.56211,6.052472,13000,0,0,100,0), -- 17:15:28
(@PATH,185,-5208.978,621.4757,46.32497,0,0,0,0,100,0), -- 17:15:35
(@PATH,186,-5206.209,621.2124,46.10388,0,0,0,0,100,0), -- 17:15:36
(@PATH,187,-5186.642,619.9025,42.74264,0,0,0,0,100,0), -- 17:15:40
(@PATH,188,-5183.583,619.2233,41.50294,0,0,0,0,100,0), -- 17:15:43
(@PATH,189,-5180.833,618.4733,41.00294,0,0,0,0,100,0), -- 17:15:43
(@PATH,190,-5177.333,617.9733,40.00294,0,0,0,0,100,0), -- 17:15:43
(@PATH,191,-5164.016,617.6649,37.10645,0,0,0,0,100,0), -- 17:15:51
(@PATH,192,-5157.269,621.1954,36.32607,0,0,0,0,100,0), -- 17:15:54
(@PATH,193,-5155.769,622.1954,36.32607,0,0,0,0,100,0), -- 17:15:54
(@PATH,194,-5151.916,625.7462,36.27628,0,0,0,0,100,0), -- 17:15:58
(@PATH,195,-5146.916,630.4962,36.02628,0,0,0,0,100,0), -- 17:15:58
(@PATH,196,-5146.043,631.5037,36.0778,0,0,0,0,100,0), -- 17:16:03
(@PATH,197,-5143.677,634.6373,36.08821,0,0,0,0,100,0), -- 17:16:05
(@PATH,198,-5143.677,634.6373,36.08821,0.8953364,13000,0,0,100,0); -- 17:16:05
-- 0x1C09FC424016BE4000001900001FD78F .go -5141.078 637.8817 36.18917

DELETE FROM `creature_formations` WHERE `leaderGUID`=143600;
INSERT INTO `creature_formations` (`leaderGUID`, `memberGUID`, `dist`, `angle`, `groupAI`) VALUES
(143600, 143600, 0, 0, 1),
(143600, 52344, 2, 0, 2);

-- Pathing for Mine Car Entry: 23289 'TDB FORMAT' 
SET @NPC := 143600;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5158.637,`position_y`=727.454,`position_z`=45.57201 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5158.637,727.454,45.57201,0,0,0,0,100,0), -- 17:09:07
(@PATH,2,-5155.599,718.3636,44.29898,0,0,0,0,100,0), -- 17:09:09
(@PATH,3,-5154.974,714.7401,43.48943,0,0,0,0,100,0), -- 17:09:11
(@PATH,4,-5155.224,712.9901,43.48943,0,0,0,0,100,0), -- 17:09:11
(@PATH,5,-5155.724,705.7401,43.23943,0,0,0,0,100,0), -- 17:09:11
(@PATH,6,-5155.974,700.4901,42.98943,0,0,0,0,100,0), -- 17:09:11
(@PATH,7,-5155.34,696.4957,42.45358,0,0,0,0,100,0), -- 17:09:18
(@PATH,8,-5154.84,693.2457,42.20358,0,0,0,0,100,0), -- 17:09:18
(@PATH,9,-5157.657,679.5804,41.46281,0,0,0,0,100,0), -- 17:09:23
(@PATH,10,-5156.473,678.8619,41.37924,0,0,0,0,100,0), -- 17:09:27
(@PATH,11,-5153.473,678.8619,41.62924,0,0,0,0,100,0), -- 17:09:27
(@PATH,12,-5149.723,678.6119,41.37924,0,0,0,0,100,0), -- 17:09:27
(@PATH,13,-5146.723,678.6119,41.62924,0,0,0,0,100,0), -- 17:09:27
(@PATH,14,-5142.223,678.3619,41.37924,0,0,0,0,100,0), -- 17:09:27
(@PATH,15,-5139.723,678.3619,41.37924,0,0,0,0,100,0), -- 17:09:27
(@PATH,16,-5129.718,680.8803,40.1742,0,0,0,0,100,0), -- 17:09:38
(@PATH,17,-5123.968,688.6303,39.4242,0,0,0,0,100,0), -- 17:09:38
(@PATH,18,-5122.718,690.3803,39.4242,0,0,0,0,100,0), -- 17:09:38
(@PATH,19,-5123.011,691.2361,39.35996,0,0,0,0,100,0), -- 17:09:45
(@PATH,20,-5124.511,690.9861,39.60996,0,0,0,0,100,0), -- 17:09:45
(@PATH,21,-5127.261,690.4861,40.35996,0,0,0,0,100,0), -- 17:09:45
(@PATH,22,-5129.767,692.5929,40.48572,2.039433,13000,0,0,100,0), -- 17:09:50
(@PATH,23,-5132.671,699.1042,41.48306,0,0,0,0,100,0), -- 17:09:57
(@PATH,24,-5134.28,703.5355,41.8651,0,0,0,0,100,0), -- 17:10:00
(@PATH,25,-5134.28,705.0355,42.1151,0,0,0,0,100,0), -- 17:10:00
(@PATH,26,-5135.78,711.5355,42.6151,0,0,0,0,100,0), -- 17:10:00
(@PATH,27,-5136.954,718.392,44.09182,0,0,0,0,100,0), -- 17:10:04
(@PATH,28,-5140.083,725.0063,45.31725,0,0,0,0,100,0), -- 17:10:08
(@PATH,29,-5146.963,732.4551,46.1476,0,0,0,0,100,0), -- 17:10:12
(@PATH,30,-5151.213,736.2051,46.3976,0,0,0,0,100,0), -- 17:10:12
(@PATH,31,-5156.23,735.8439,46.26311,0,0,0,0,100,0), -- 17:10:17
(@PATH,32,-5159.73,735.3439,46.26311,0,0,0,0,100,0), -- 17:10:17
(@PATH,33,-5161.024,735.1243,46.05744,3.305536,13000,0,0,100,0), -- 17:10:21
(@PATH,34,-5162.643,734.8564,46.45791,0,0,0,0,100,0), -- 17:10:29
(@PATH,35,-5158.645,727.4257,45.9726,0,0,0,0,100,0), -- 17:10:30
(@PATH,36,-5157.451,725.0515,45.61901,0,0,0,0,100,0), -- 17:10:32
(@PATH,37,-5156.701,722.0515,44.86901,0,0,0,0,100,0), -- 17:10:32
(@PATH,38,-5155.451,718.5515,44.36901,0,0,0,0,100,0), -- 17:10:32
(@PATH,39,-5155.159,714.8245,43.45316,0,0,0,0,100,0), -- 17:10:36
(@PATH,40,-5155.159,712.8245,43.45316,0,0,0,0,100,0), -- 17:10:36
(@PATH,41,-5155.659,705.8245,43.20316,0,0,0,0,100,0), -- 17:10:36
(@PATH,42,-5156.159,700.5745,43.20316,0,0,0,0,100,0), -- 17:10:36
(@PATH,43,-5155.351,696.5886,42.4476,0,0,0,0,100,0), -- 17:10:43
(@PATH,44,-5154.851,693.3386,42.1976,0,0,0,0,100,0), -- 17:10:43
(@PATH,45,-5157.878,679.6798,41.45091,0,0,0,0,100,0), -- 17:10:48
(@PATH,46,-5156.542,678.974,41.3766,0,0,0,0,100,0), -- 17:10:52
(@PATH,47,-5153.542,678.724,41.6266,0,0,0,0,100,0), -- 17:10:52
(@PATH,48,-5150.326,678.3993,41.2695,6.244667,13000,0,0,100,0), -- 17:10:57
(@PATH,49,-5139.679,678.0468,41.18186,0,0,0,0,100,0), -- 17:11:04
(@PATH,50,-5129.634,680.8771,40.41336,0,0,0,0,100,0), -- 17:11:10
(@PATH,51,-5124.134,688.6271,39.66336,0,0,0,0,100,0), -- 17:11:10
(@PATH,52,-5122.634,690.3771,39.41336,0,0,0,0,100,0), -- 17:11:10
(@PATH,53,-5123.205,691.313,39.34991,0,0,0,0,100,0), -- 17:11:17
(@PATH,54,-5124.455,691.063,39.59991,0,0,0,0,100,0), -- 17:11:17
(@PATH,55,-5127.455,690.563,40.34991,0,0,0,0,100,0), -- 17:11:17
(@PATH,56,-5132.663,698.9817,41.54927,0,0,0,0,100,0), -- 17:11:20
(@PATH,57,-5134.165,703.3019,41.89793,0,0,0,0,100,0), -- 17:11:25
(@PATH,58,-5134.415,705.0519,42.14793,0,0,0,0,100,0), -- 17:11:25
(@PATH,59,-5135.665,711.5519,42.64793,0,0,0,0,100,0), -- 17:11:25
(@PATH,60,-5137.015,718.4344,44.11918,0,0,0,0,100,0), -- 17:11:29
(@PATH,61,-5140.138,725.0566,45.36108,0,0,0,0,100,0), -- 17:11:33
(@PATH,62,-5146.898,732.4678,46.15642,0,0,0,0,100,0), -- 17:11:37
(@PATH,63,-5151.148,736.2178,46.40642,0,0,0,0,100,0), -- 17:11:37
(@PATH,64,-5156.207,735.7999,46.2761,0,0,0,0,100,0), -- 17:11:42
(@PATH,65,-5159.707,735.2999,46.2761,0,0,0,0,100,0), -- 17:11:42
(@PATH,66,-5161.614,733.5115,46.41948,0,0,0,0,100,0), -- 17:11:45
(@PATH,67,-5159.614,729.7615,45.91948,0,0,0,0,100,0), -- 17:11:45
(@PATH,68,-5158.364,727.2615,45.66948,0,0,0,0,100,0), -- 17:11:45
(@PATH,69,-5157.527,724.9576,45.59089,0,0,0,0,100,0), -- 17:11:50
(@PATH,70,-5156.777,721.9576,44.84089,0,0,0,0,100,0), -- 17:11:50
(@PATH,71,-5155.527,718.4576,44.34089,0,0,0,0,100,0), -- 17:11:50
(@PATH,72,-5155.046,714.7384,43.62848,0,0,0,0,100,0), -- 17:11:54
(@PATH,73,-5155.11,713.8632,43.32977,4.644619,13000,0,0,100,0), -- 17:11:56
(@PATH,74,-5155.853,700.3505,42.94897,0,0,0,0,100,0), -- 17:12:03
(@PATH,75,-5155.375,696.4696,42.43354,0,0,0,0,100,0), -- 17:12:08
(@PATH,76,-5154.875,693.2196,42.18354,0,0,0,0,100,0), -- 17:12:08
(@PATH,77,-5157.808,679.5703,41.4225,0,0,0,0,100,0), -- 17:12:13
(@PATH,78,-5156.457,678.8987,41.37038,0,0,0,0,100,0), -- 17:12:17
(@PATH,79,-5153.457,678.8987,41.62038,0,0,0,0,100,0), -- 17:12:17
(@PATH,80,-5149.707,678.6487,41.37038,0,0,0,0,100,0), -- 17:12:17
(@PATH,81,-5146.707,678.3987,41.62038,0,0,0,0,100,0), -- 17:12:17
(@PATH,82,-5142.207,678.3987,41.37038,0,0,0,0,100,0), -- 17:12:17
(@PATH,83,-5139.707,678.1487,41.37038,0,0,0,0,100,0), -- 17:12:17
(@PATH,84,-5129.718,680.8611,40.35927,0,0,0,0,100,0), -- 17:12:28
(@PATH,85,-5123.968,688.6111,39.60927,0,0,0,0,100,0), -- 17:12:28
(@PATH,86,-5122.117,691.4048,39.21569,0,0,0,0,100,0), -- 17:12:34
(@PATH,87,-5124.367,691.1548,39.71569,0,0,0,0,100,0), -- 17:12:34
(@PATH,88,-5127.367,690.6548,40.21569,0,0,0,0,100,0), -- 17:12:34
(@PATH,89,-5132.795,699.1715,41.39445,0,0,0,0,100,0), -- 17:12:37
(@PATH,90,-5133.877,703.4885,42.01696,0,0,0,0,100,0), -- 17:12:41
(@PATH,91,-5134.377,704.7385,42.01696,0,0,0,0,100,0), -- 17:12:41
(@PATH,92,-5135.627,711.4885,42.76696,0,0,0,0,100,0), -- 17:12:41
(@PATH,93,-5136.911,716.5974,43.53514,1.753501,13000,0,0,100,0), -- 17:12:50
(@PATH,94,-5137.376,718.6188,44.1994,0,0,0,0,100,0), -- 17:12:57
(@PATH,95,-5139.936,725.0667,45.3965,0,0,0,0,100,0), -- 17:12:58
(@PATH,96,-5145.418,730.9333,45.83433,0,0,0,0,100,0), -- 17:13:01
(@PATH,97,-5146.918,732.4333,46.08433,0,0,0,0,100,0), -- 17:13:01
(@PATH,98,-5151.168,736.1833,46.33433,0,0,0,0,100,0), -- 17:13:01
(@PATH,99,-5156.112,735.9399,46.28493,0,0,0,0,100,0), -- 17:13:06
(@PATH,100,-5159.612,735.1899,46.28493,0,0,0,0,100,0), -- 17:13:06
(@PATH,101,-5161.833,733.4753,46.65749,0,0,0,0,100,0), -- 17:13:10
(@PATH,102,-5159.583,729.7253,45.90749,0,0,0,0,100,0), -- 17:13:10
(@PATH,103,-5158.583,727.2253,45.65749,0,0,0,0,100,0), -- 17:13:10
(@PATH,104,-5161.203,732.0187,46.14994,5.182036,13000,0,0,100,0); -- 17:13:31
-- 0x1C09FC424016BE4000001900009FD78F .go -5158.637 727.454 45.57201

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52061;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5041.84,`position_y`=389.8085,`position_z`=-12.50461 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5041.84,389.8085,-12.50461,0,0,0,0,100,0), -- 18:10:14
(@PATH,2,-5030.222,418.1729,-10.5541,0,0,0,0,100,0), -- 18:10:30
(@PATH,3,-5025.323,437.285,-9.860763,0,0,0,0,100,0), -- 18:10:42
(@PATH,4,-5018.703,470.1522,-7.756811,0,0,0,0,100,0), -- 18:10:53
(@PATH,5,-5001.123,509.9958,-5.460767,0,0,0,0,100,0), -- 18:11:05
(@PATH,6,-4988.681,532.6278,-6.128869,0,0,0,0,100,0), -- 18:11:20
(@PATH,7,-5005.017,551.2161,-4.296617,0,0,0,0,100,0), -- 18:11:31
(@PATH,8,-4989.901,535.1989,-5.819852,0,0,0,0,100,0), -- 18:11:45
(@PATH,9,-4997.614,515.9099,-5.108593,0,0,0,0,100,0), -- 18:11:55
(@PATH,10,-5015.447,481.5337,-7.364,0,0,0,0,100,0), -- 18:12:06
(@PATH,11,-5022.034,451.5206,-8.870285,0,0,0,0,100,0), -- 18:12:21
(@PATH,12,-5027.991,425.6957,-10.12456,0,0,0,0,100,0); -- 18:12:33
-- 0x1C09FC424016BD4000001A000024139A .go -5041.84 389.8085 -12.50461

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52086;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5145.625,`position_y`=491.5528,`position_z`=-13.89787 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5145.625,491.5528,-13.89787,0,0,0,0,100,0), -- 18:23:20
(@PATH,2,-5132.041,468.5541,-13.29704,0,0,0,0,100,0), -- 18:23:25
(@PATH,3,-5110.197,448.1371,-10.01901,0,0,0,0,100,0), -- 18:23:38
(@PATH,4,-5126.933,460.5002,-11.57316,0,0,0,0,100,0), -- 18:23:49
(@PATH,5,-5145.124,490.6877,-13.89787,0,0,0,0,100,0); -- 18:25:03
-- 0x1C09FC424016BD4000001A00002414A1 .go -5145.625 491.5528 -13.89787

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52084;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5012.641,`position_y`=527.0723,`position_z`=-4.429356 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5012.641,527.0723,-4.429356,0,0,0,0,100,0), -- 17:53:03
(@PATH,2,-5013.526,533.334,-4.454225,0,0,0,0,100,0), -- 17:53:03
(@PATH,3,-5016.438,541.4922,-4.275333,0,0,0,0,100,0), -- 17:53:03
(@PATH,4,-5017.101,543.3494,-4.186446,0,0,0,0,100,0), -- 17:53:03
(@PATH,5,-5019.346,549.6407,-3.932599,0,0,0,0,100,0), -- 17:53:03
(@PATH,6,-5019.346,549.6407,-3.932599,0,0,0,0,100,0), -- 17:53:03
(@PATH,7,-5013.449,533.623,-4.201852,0,0,0,0,100,0), -- 17:53:08
(@PATH,8,-5013.044,507.5332,-5.770618,0,0,0,0,100,0), -- 17:53:15
(@PATH,9,-5013.313,503.3087,-6.890712,0,0,0,0,100,0), -- 17:53:38
(@PATH,10,-5013.553,499.3347,-7.32141,0,0,0,0,100,0), -- 17:55:36
(@PATH,11,-5013.988,492.2674,-7.856032,0,0,0,0,100,0), -- 17:55:36
(@PATH,12,-5014.288,487.3993,-7.717823,0,0,0,0,100,0), -- 17:55:36
(@PATH,13,-5014.653,481.4716,-7.740697,0,0,0,0,100,0), -- 17:55:36
(@PATH,14,-5014.934,476.9202,-7.658531,0,0,0,0,100,0); -- 17:55:36
-- 0x1C09FC424016BD4000001A000024163A .go -5012.641 527.0723 -4.429356

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52054;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5097.479,`position_y`=383.4904,`position_z`=-13.35386 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5097.479,383.4904,-13.35386,0,0,0,0,100,0), -- 18:09:28
(@PATH,2,-5105.978,382.3359,-12.87995,0,0,0,0,100,0), -- 18:09:33
(@PATH,3,-5140.72,361.3568,-18.36172,0,0,0,0,100,0), -- 18:09:39
(@PATH,4,-5153.877,344.0262,-19.23454,0,0,0,0,100,0), -- 18:09:54
(@PATH,5,-5146.007,355.4137,-18.91095,0,0,0,0,100,0), -- 18:12:22
(@PATH,6,-5145.439,356.2371,-18.91095,0,0,0,0,100,0), -- 18:12:22
(@PATH,7,-5144.646,357.3877,-18.79898,0,0,0,0,100,0), -- 18:12:22
(@PATH,8,-5143.475,359.0869,-18.80667,0,0,0,0,100,0), -- 18:12:22
(@PATH,9,-5141.084,360.9994,-18.86554,0,0,0,0,100,0), -- 18:12:22
(@PATH,10,-5138.581,363.001,-18.69712,0,0,0,0,100,0), -- 18:12:22
(@PATH,11,-5134.565,366.2131,-17.01051,0,0,0,0,100,0), -- 18:12:22
(@PATH,12,-5131.742,368.4709,-16.21124,0,0,0,0,100,0), -- 18:12:22
(@PATH,13,-5130.425,369.5245,-15.74924,0,0,0,0,100,0), -- 18:12:22
(@PATH,14,-5127.633,371.7576,-15.0621,0,0,0,0,100,0), -- 18:12:22
(@PATH,15,-5124.102,374.582,-14.24547,0,0,0,0,100,0), -- 18:12:22
(@PATH,16,-5117.077,380.2002,-12.9527,0,0,0,0,100,0); -- 18:12:22
-- 0x1C09FC424016BD4000001A0000241848 .go -5097.479 383.4904 -13.35386

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52083;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5142.954,`position_y`=424.2722,`position_z`=-10.0708 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5142.954,424.2722,-10.0708,0,0,0,0,100,0), -- 18:09:46
(@PATH,2,-5166.739,417.5524,-10.24546,0,0,0,0,100,0), -- 18:09:57
(@PATH,3,-5174.002,422.4801,-10.30893,0,0,0,0,100,0), -- 18:10:04
(@PATH,4,-5170.584,419.3948,-10.41781,0,0,0,0,100,0), -- 18:12:22
(@PATH,5,-5169.813,418.7582,-10.41781,0,0,0,0,100,0), -- 18:12:22
(@PATH,6,-5168.172,417.4039,-10.38332,0,0,0,0,100,0), -- 18:12:22
(@PATH,7,-5167.688,417.0048,-10.36478,0,0,0,0,100,0), -- 18:12:22
(@PATH,8,-5159.976,420.4402,-10.22841,0,0,0,0,100,0), -- 18:12:22
(@PATH,9,-5157.938,421.3475,-10.24158,0,0,0,0,100,0), -- 18:12:22
(@PATH,10,-5154.572,422.8471,-10.04827,0,0,0,0,100,0), -- 18:12:22
(@PATH,11,-5151.838,424.0645,-10.09287,0,0,0,0,100,0), -- 18:12:22
(@PATH,12,-5123.059,424.4661,-11.98086,3.589592,6000,0,0,100,0); -- 18:12:39
-- 0x1C09FC424016BD4000001A0000241877 .go -5142.954 424.2722 -10.0708

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52086;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5100.122,`position_y`=472.3237,`position_z`=-8.886364 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5100.122,472.3237,-8.886364,0,0,0,0,100,0), -- 18:10:53
(@PATH,2,-5117.763,467.3499,-11.16238,0,0,0,0,100,0), -- 18:11:04
(@PATH,3,-5131.067,444.1735,-10.89796,0,0,0,0,100,0), -- 18:11:35
(@PATH,4,-5132.473,440.0923,-10.73675,0,0,0,0,100,0), -- 18:11:35
(@PATH,5,-5130.348,437.1649,-10.71613,0,0,0,0,100,0), -- 18:11:35
(@PATH,6,-5127.024,432.5856,-11.19165,0,0,0,0,100,0), -- 18:11:35
(@PATH,7,-5118.549,420.9109,-12.51914,0,0,0,0,100,0), -- 18:11:35
(@PATH,8,-5088.664,403.9553,-12.69664,0,0,0,0,100,0), -- 18:11:36
(@PATH,9,-5070.439,406.9981,-11.93173,0,0,0,0,100,0), -- 18:11:49
(@PATH,10,-5062.364,426.8455,-11.17576,0,0,0,0,100,0), -- 18:11:58
(@PATH,11,-5063.076,458.8389,-11.47571,0,0,0,0,100,0), -- 18:12:09
(@PATH,12,-5067.343,465.1718,-10.02644,0,0,0,0,100,0), -- 18:12:18
(@PATH,13,-5100.08,472.3515,-8.843109,0,0,0,0,100,0); -- 18:12:26
-- 0x1C09FC424016BD4000001A00002418CF .go -5100.122 472.3237 -8.886364

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52085;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5082.33,`position_y`=474.2946,`position_z`=-7.784039 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5082.33,474.2946,-7.784039,0,0,0,0,100,0), -- 18:09:27
(@PATH,2,-5091.409,496.4387,-10.14975,0,0,0,0,100,0), -- 18:09:40
(@PATH,3,-5094.309,513.0909,-11.32626,0,0,0,0,100,0), -- 18:09:50
(@PATH,4,-5105.632,522.7634,-10.2628,0,0,0,0,100,0), -- 18:09:56
(@PATH,5,-5107.463,534.7469,-10.20918,0,0,0,0,100,0), -- 18:10:04
(@PATH,6,-5108.773,528.9429,-10.17007,0,0,0,0,100,0), -- 18:10:10
(@PATH,7,-5103.452,521.2954,-10.52877,0,0,0,0,100,0), -- 18:10:12
(@PATH,8,-5093.061,503.7095,-11.10897,0,0,0,0,100,0); -- 18:10:21
-- 0x1C09FC424016BD4000001A0000241940 .go -5082.33 474.2946 -7.784039

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52061;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5015.721,`position_y`=393.4803,`position_z`=-13.56352 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5015.721,393.4803,-13.56352,0,0,0,0,100,0), -- 17:55:20
(@PATH,2,-5014.953,396.891,-13.21174,0,0,0,0,100,0), -- 17:55:20
(@PATH,3,-5013.08,405.213,-12.17083,0,0,0,0,100,0), -- 17:55:20
(@PATH,4,-5012.828,406.3322,-11.95596,0,0,0,0,100,0), -- 17:55:20
(@PATH,5,-5010.532,416.5321,-10.48194,0,0,0,0,100,0), -- 17:55:20
(@PATH,6,-5008.649,424.8955,-9.222077,0,0,0,0,100,0), -- 17:55:20
(@PATH,7,-5002.781,442.213,-7.196595,0,0,0,0,100,0), -- 17:55:28
(@PATH,8,-5001.61,460.2058,-6.495255,0,0,0,0,100,0), -- 17:55:37
(@PATH,9,-5001.797,445.3661,-6.927022,0,0,0,0,100,0), -- 17:55:47
(@PATH,10,-5008.122,426.1274,-8.655727,0,0,0,0,100,0), -- 17:55:55
(@PATH,11,-5014.418,397.9655,-12.76799,0,0,0,0,100,0); -- 17:56:03
-- 0x1C09FC424016BD4000001A00002419A9 .go -5015.721 393.4803 -13.56352

-- Pathing for Nethermine Burster Entry: 23285 'TDB FORMAT' 
SET @NPC := 52046;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5153.807,`position_y`=343.9828,`position_z`=-19.23604 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5153.807,343.9828,-19.23604,0,0,0,0,100,0), -- 18:17:49
(@PATH,2,-5144.604,357.4449,-18.70733,0,0,0,0,100,0), -- 18:17:59
(@PATH,3,-5116.975,380.3215,-12.69622,0,0,0,0,100,0), -- 18:18:06
(@PATH,4,-5102.355,382.6835,-13.05555,0,0,0,0,100,0), -- 18:18:21
(@PATH,5,-5105.978,382.3359,-12.87995,0,0,0,0,100,0), -- 18:18:30
(@PATH,6,-5140.741,361.2797,-18.35986,0,0,0,0,100,0); -- 18:18:37
-- 0x1C09FC424016BD4000001A0000241F37 .go -5153.807 343.9828 -19.23604

-- Pathing for Nethermine Ravager Entry: 23326 'TDB FORMAT' 
SET @NPC := 143827;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-4963.239,`position_y`=605.275,`position_z`=15.15399 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-4963.239,605.275,15.15399,0,0,1,0,100,0), -- 17:52:17
(@PATH,2,-4975.903,623.7806,17.61637,0,0,1,0,100,0), -- 17:52:20
(@PATH,3,-4992.053,634.3859,22.33038,0,0,1,0,100,0), -- 17:52:23
(@PATH,4,-5011.415,637.7054,23.62663,0,0,1,0,100,0), -- 17:52:26
(@PATH,5,-4998.898,635.9115,22.77191,0,0,1,0,100,0), -- 17:52:31
(@PATH,6,-4979.864,627.016,19.21811,0,0,1,0,100,0), -- 17:52:34
(@PATH,7,-4965.36,609.7169,16.62768,0,0,1,0,100,0), -- 17:52:37
(@PATH,8,-4958.603,589.0837,12.97569,0,0,1,0,100,0), -- 17:52:40
(@PATH,9,-4936.146,573.9352,7.7713,0,0,1,0,100,0), -- 17:52:44
(@PATH,10,-4913.703,588.2379,5.505109,0,0,1,0,100,0), -- 17:52:48
(@PATH,11,-4930.457,576.1113,6.944055,0,0,1,0,100,0), -- 17:52:53
(@PATH,12,-4955.026,583.3227,12.02873,0,0,1,0,100,0); -- 17:52:55
-- 0x1C09FC424016C78000001A0000241948 .go -4963.239 605.275 15.15399

-- Pathing for Nethermine Ravager Entry: 23326 'TDB FORMAT' 
SET @NPC := 143837;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-4926.302,`position_y`=510.8232,`position_z`=5.912571 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-4926.302,510.8232,5.912571,0,0,1,0,100,0), -- 17:52:59
(@PATH,2,-4928.601,533.3176,6.820917,0,0,1,0,100,0), -- 17:53:03
(@PATH,3,-4939.765,552.8948,6.603627,0,0,1,0,100,0), -- 17:53:05
(@PATH,4,-4955.232,575.6724,11.41519,0,0,1,0,100,0), -- 17:53:09
(@PATH,5,-4946.245,562.309,8.260891,0,0,1,0,100,0), -- 17:53:14
(@PATH,6,-4933.914,544.2485,6.190574,0,0,1,0,100,0), -- 17:53:16
(@PATH,7,-4927.376,523.0281,7.014862,0,0,1,0,100,0), -- 17:53:20
(@PATH,8,-4926.589,498.706,3.313521,0,0,1,0,100,0), -- 17:53:23
(@PATH,9,-4907.33,473.4201,1.108172,0,0,1,0,100,0), -- 17:53:27
(@PATH,10,-4885.657,466.1158,-2.15189,0,0,1,0,100,0), -- 17:53:31
(@PATH,11,-4898.996,472.0482,0.2873805,0,0,1,0,100,0), -- 17:53:36
(@PATH,12,-4925.863,491.4807,1.155461,0,0,1,0,100,0); -- 17:53:39
-- 0x1C09FC424016C78000001A0000241973 .go -4926.302 510.8232 5.912571

-- Pathing for Nethermine Ravager Entry: 23326 'TDB FORMAT' 
SET @NPC := 143848;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5009.851,`position_y`=293.1676,`position_z`=1.581649 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5009.851,293.1676,1.581649,0,0,1,0,100,0), -- 17:55:04
(@PATH,2,-4985.909,303.3185,-1.474064,0,0,1,0,100,0), -- 17:55:10
(@PATH,3,-4977.48,319.7932,-1.799831,0,0,1,0,100,0), -- 17:55:14
(@PATH,4,-4965.886,351.0247,-1.16063,0,0,1,0,100,0), -- 17:55:16
(@PATH,5,-4961.489,388.3174,-1.142369,0,0,1,0,100,0), -- 17:55:21
(@PATH,6,-4955.736,422.718,3.013297,0,0,1,0,100,0), -- 17:55:25
(@PATH,7,-4943.146,446.6736,1.949694,0,0,1,0,100,0), -- 17:55:32
(@PATH,8,-4918.688,445.8087,1.470013,0,0,1,0,100,0), -- 17:55:36
(@PATH,9,-4907.826,422.7741,-4.288939,0,0,1,0,100,0), -- 17:55:39
(@PATH,10,-4914.044,443.4701,0.7917011,0,0,1,0,100,0), -- 17:55:45
(@PATH,11,-4940.444,449.0392,1.663293,0,0,1,0,100,0), -- 17:55:48
(@PATH,12,-4953.445,428.6053,3.195693,0,0,1,0,100,0), -- 17:55:51
(@PATH,13,-4959.889,397.1684,-0.2070236,0,0,1,0,100,0), -- 17:55:55
(@PATH,14,-4965.057,353.1042,-1.062231,0,0,1,0,100,0), -- 17:56:01
(@PATH,15,-4974.901,325.7972,-1.889486,0,0,1,0,100,0), -- 17:56:06
(@PATH,16,-4983.667,306.4418,-1.94701,0,0,1,0,100,0); -- 17:56:10
-- 0x1C09FC424016C78000001A00002419DB .go -5009.851 293.1676 1.581649

-- Pathing for Nethermine Ravager Entry: 23326 'TDB FORMAT' 
SET @NPC := 143840;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-4869.946,`position_y`=530.6263,`position_z`=-1.553467 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-4869.946,530.6263,-1.553467,0,0,1,0,100,0), -- 18:06:45
(@PATH,2,-4893.954,534.9226,2.516688,0,0,1,0,100,0), -- 18:06:50
(@PATH,3,-4909.314,518.1718,4.493078,0,0,1,0,100,0), -- 18:06:54
(@PATH,4,-4901.04,531.7859,4.01936,0,0,1,0,100,0), -- 18:06:55
(@PATH,5,-4909.218,518.321,4.623864,0,0,1,0,100,0), -- 18:07:02
(@PATH,6,-4915.15,493.46,1.49952,0,0,1,0,100,0), -- 18:07:03
(@PATH,7,-4928.039,454.6728,1.627107,0,0,1,0,100,0), -- 18:07:07
(@PATH,8,-4947.842,455.957,0.743868,0,0,1,0,100,0), -- 18:07:13
(@PATH,9,-4970.903,473.2809,3.325165,0,0,1,0,100,0), -- 18:07:16
(@PATH,10,-4956.071,459.8573,0.461098,0,0,1,0,100,0), -- 18:07:22
(@PATH,11,-4930.157,454.3083,1.40765,0,0,1,0,100,0), -- 18:07:25
(@PATH,12,-4917.665,485.7419,0.5451692,0,0,1,0,100,0), -- 18:07:29
(@PATH,13,-4910.57,514.0978,4.176438,0,0,1,0,100,0), -- 18:07:34
(@PATH,14,-4898.185,533.2109,3.269632,0,0,1,0,100,0); -- 18:07:39
-- 0x1C09FC424016C78000001A0000241B47 .go -4869.946 530.6263 -1.553467

-- Pathing for Nethermine Ravager Entry: 23326 'TDB FORMAT' 
SET @NPC := 143843;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-4922.489,`position_y`=300.9247,`position_z`=-12.97113 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-4922.489,300.9247,-12.97113,0,0,1,0,100,0), -- 18:05:49
(@PATH,2,-4940.971,300.3912,-8.783532,0,0,1,0,100,0), -- 18:05:52
(@PATH,3,-4955.332,289.6572,-5.669093,0,0,1,0,100,0), -- 18:05:55
(@PATH,4,-4966.683,308.9685,-3.354346,0,0,1,0,100,0), -- 18:05:57
(@PATH,5,-4964.716,331.7885,-2.618268,0,0,1,0,100,0), -- 18:06:01
(@PATH,6,-4955.161,358.4415,-2.649262,0,0,1,0,100,0), -- 18:06:04
(@PATH,7,-4951.747,387.423,-2.672494,0,0,1,0,100,0), -- 18:06:08
(@PATH,8,-4960.425,414.4815,2.96782,0,0,1,0,100,0), -- 18:06:13
(@PATH,9,-4964.489,436.2759,3.192766,0,0,1,0,100,0), -- 18:06:16
(@PATH,10,-4961.489,417.7344,3.296109,0,0,1,0,100,0), -- 18:06:22
(@PATH,11,-4952.684,397.0981,-0.8431122,0,0,1,0,100,0), -- 18:06:25
(@PATH,12,-4953.893,364.0373,-2.409762,0,0,1,0,100,0), -- 18:06:28
(@PATH,13,-4962.485,340.8231,-2.176575,0,0,1,0,100,0), -- 18:06:33
(@PATH,14,-4966.377,318.3596,-2.773224,0,0,1,0,100,0), -- 18:06:37
(@PATH,15,-4960.039,292.8176,-4.731999,0,0,1,0,100,0), -- 18:13:52
(@PATH,16,-4942.955,299.4413,-8.193456,0,0,1,0,100,0); -- 18:13:54
-- 0x1C09FC424016C78000001A0000241B51 .go -4922.489 300.9247 -12.97113

-- Pathing for Nethermine Ravager Entry: 23326 'TDB FORMAT' 
SET @NPC := 143850;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5061.087,`position_y`=118.9844,`position_z`=-16.32363 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5061.087,118.9844,-16.32363,0,0,1,0,100,0), -- 18:05:17
(@PATH,2,-5067.978,134.1589,-14.4941,0,0,1,0,100,0), -- 18:05:24
(@PATH,3,-5078.728,174.4905,-8.200466,0,0,1,0,100,0), -- 18:05:27
(@PATH,4,-5070.809,196.4065,-8.396107,0,0,1,0,100,0), -- 18:05:32
(@PATH,5,-5051.048,203.3381,-10.27266,0,0,1,0,100,0), -- 18:05:36
(@PATH,6,-5030.57,208.88,-11.94103,0,0,1,0,100,0), -- 18:05:39
(@PATH,7,-4996.729,232.7692,-7.642292,0,0,1,0,100,0), -- 18:05:43
(@PATH,8,-4983.488,232.4408,-8.859201,0,0,1,0,100,0), -- 18:05:48
(@PATH,9,-4990.174,233.7981,-7.773879,0,0,1,0,100,0), -- 18:05:52
(@PATH,10,-5009.648,220.7942,-10.2113,0,0,1,0,100,0), -- 18:05:55
(@PATH,11,-5009.046,223.3424,-9.788197,0,0,1,0,100,0), -- 18:05:58
(@PATH,12,-5018.983,212.046,-11.68377,0,0,1,0,100,0), -- 18:06:06
(@PATH,13,-5031.654,208.8046,-11.78926,0,0,1,0,100,0), -- 18:06:07
(@PATH,14,-5076.621,183.3736,-8.145866,0,0,1,0,100,0), -- 18:13:57
(@PATH,15,-5072.223,144.1562,-12.73046,0,0,1,0,100,0), -- 18:13:58
(@PATH,16,-5061.066,119.1013,-16.51242,0,0,1,0,100,0); -- 18:14:04
-- 0x1C09FC424016C78000001A0000241C27 .go -5061.087 118.9844 -16.32363

-- Pathing for Nethermine Ravager Entry: 23326 'TDB FORMAT' 
SET @NPC := 143859;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-5052.252,`position_y`=176.8827,`position_z`=-11.95231 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES (@NPC,@PATH,0,0,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_type`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-5052.252,176.8827,-11.95231,0,0,1,0,100,0), -- 18:05:07
(@PATH,2,-5021.2,157.3062,-14.11822,0,0,1,0,100,0), -- 18:05:11
(@PATH,3,-5001.049,161.8789,-14.45323,0,0,1,0,100,0), -- 18:05:15
(@PATH,4,-5016.269,157.2713,-14.45587,0,0,1,0,100,0), -- 18:05:21
(@PATH,5,-5046.272,166.7962,-12.9523,0,0,1,0,100,0), -- 18:05:23
(@PATH,6,-5051.284,187.3221,-11.84302,0,0,1,0,100,0), -- 18:05:29
(@PATH,7,-5035.34,209.7333,-11.41766,0,0,1,0,100,0), -- 18:05:32
(@PATH,8,-5024.802,236.1987,-6.399595,0,0,1,0,100,0), -- 18:05:35
(@PATH,9,-5015.809,258.2852,-1.012123,0,0,1,0,100,0), -- 18:05:40
(@PATH,10,-5023.006,240.032,-4.77902,0,0,1,0,100,0), -- 18:05:45
(@PATH,11,-5030.113,221.3788,-9.224751,0,0,1,0,100,0), -- 18:05:47
(@PATH,12,-5049.056,197.9744,-11.1133,0,0,1,0,100,0); -- 18:05:52
-- 0x1C09FC424016C78000001A0000241C47 .go -5052.252 176.8827 -11.95231
 
 
-- -------------------------------------------------------- 
-- 2015_04_17_01_world_2015_04_11_02.sql 
-- -------------------------------------------------------- 
UPDATE `smart_scripts` SET `event_flags`=0 WHERE `entryorguid`=25310 AND `source_type`=0 AND `id`=0;
UPDATE `smart_scripts` SET `target_type`=1 WHERE `entryorguid` IN (24009, 24010) AND `source_type`=0 AND `id`=1;
 
 
-- -------------------------------------------------------- 
-- 2015_04_18_00_world.sql 
-- -------------------------------------------------------- 
DELETE FROM `trinity_string` WHERE `entry` IN (1204, 1205);
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
(1204, 'You have entered areatrigger %u.'),
(1205, 'You have left areatrigger %u.');
 
 
-- -------------------------------------------------------- 
-- 2015_04_18_01_world_6x.sql 
-- -------------------------------------------------------- 
--
DELETE FROM `gossip_menu` WHERE `entry`=10144 AND `text_id`=14088;
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (10144, 14088);

DELETE FROM `gossip_menu_option` WHERE `menu_id` = 10144;
INSERT INTO `gossip_menu_option` (`menu_id`, id, option_icon, `option_text`, `OptionBroadcastTextID`, `option_id`, `npc_option_npcflag`, `action_menu_id`) VALUES 
(10144, 0, 0, 'I''m interested.', 32566, 1, 1, 0),
(10144, 1, 0, 'I''m interested.', 32566, 1, 1, 0);

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=14 AND `SourceGroup`=10144;
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=10144;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`ErrorTextId`,`ScriptName`,`Comment`,`NegativeCondition`) VALUES
(15,10144,0,0,27,77,3,0,0,'','The player must be level 77 or higher', 0),
(15,10144,0,0,25,54197,0,0,0,'','Show the gossip if the player doesn''t learn Cold Weather Flying', 1),
(15,10144,0,0,2,44221,1,1,0,'','Show the gossip if the player doesn''t have the item', 1),
(15,10144,0,0,16,18875469,0,0,0,'','Show the gossip if the player is from the alliance', 0),
(15,10144,1,0,27,77,3,0,0,'','The player must be level 77 or higher', 0),
(15,10144,1,0,25,54197,0,0,0,'','Show the gossip if the player doesn''t learn Cold Weather Flying', 1),
(15,10144,1,0,2,44229,1,1,0,'','Show the gossip if the player doesn''t have the item', 1),
(15,10144,1,0,16,33555378,0,0,0,'','Show the gossip if the player is from the horde', 0),
(14,10144,14088,0,27,77,3,0,0,'','Show the text if the player is level 77 or higher', 0),
(14,10144,14088,0,25,54197,0,0,0,'','Show the text if the player doesn''t learn Cold Weather Flying', 1),
(14,10144,14088,0,2,44221,1,1,0,'','Show the text if the player doesn''t have the item', 1),
(14,10144,14088,0,2,44229,1,1,0,'','Show the text if the player doesn''t have the item', 1);

UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=30464;
DELETE FROM `smart_scripts` WHERE `entryorguid` = 30464  AND `source_type` = 0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(30464, 0, 0, 1, 62, 0, 100, 0, 10144, 0, 0, 0, 85, 60126, 1, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, '"Honest" Max  - On Gossip select - additem'),
(30464, 0, 1, 0, 61, 0, 100, 0, 10144, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, '"Honest" Max  - On Gossip select - close gossip'),
(30464, 0, 2, 3, 62, 0, 100, 0, 10144, 1, 0, 0, 85, 60128, 1, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, '"Honest" Max  - On Gossip select - additem'),
(30464, 0, 3, 0, 61, 0, 100, 0, 10144, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, '"Honest" Max  - On Gossip select - close gossip');
 
 
-- -------------------------------------------------------- 
-- 2015_04_26_00_world.sql 
-- -------------------------------------------------------- 
-- Vendors Elwynn Forest (19865)

UPDATE `npc_vendor` SET `VerifiedBuild` = 19865 WHERE `entry` IN (6367, 74, 1213, 151, 295, 2046, 844, 1650, 384, 3935, 3937, 190);

DELETE FROM `npc_vendor` WHERE `entry` IN (78, 955, 152, 958, 959, 894, 896, 1645, 1249, 1250, 54, 1198, 465, 66);
INSERT INTO `npc_vendor` (`entry`, `item`, `slot`, `maxcount`, `ExtendedCost`, `Type`, `VerifiedBuild`) VALUES
-- 78 (Janos Hammerknuckle)
(78, 2131, 1, 0, 0, 1, 19865),
(78, 1194, 2, 0, 0, 1, 19865),
(78, 2134, 3, 0, 0, 1, 19865),
(78, 2479, 4, 0, 0, 1, 19865),
(78, 2130, 5, 0, 0, 1, 19865),
(78, 2480, 6, 0, 0, 1, 19865),
(78, 2139, 7, 0, 0, 1, 19865),
(78, 2132, 8, 0, 0, 1, 19865),
-- 955 (Sergeant De Vries)
(955, 2723, 1, 0, 0, 1, 19865),
(955, 2593, 2, 0, 0, 1, 19865),
(955, 2596, 3, 0, 0, 1, 19865),
(955, 2594, 4, 0, 0, 1, 19865),
(955, 2595, 5, 0, 0, 1, 19865),
(955, 83095, 6, 0, 0, 1, 19865),
(955, 82343, 7, 0, 0, 1, 19865),
(955, 83094, 8, 0, 0, 1, 19865),
(955, 82344, 9, 0, 0, 1, 19865),
(955, 159, 10, 0, 0, 1, 19865),
(955, 1179, 11, 0, 0, 1, 19865),
(955, 1205, 12, 0, 0, 1, 19865),
(955, 1708, 13, 0, 0, 1, 19865),
(955, 1645, 14, 0, 0, 1, 19865),
(955, 8766, 15, 0, 0, 1, 19865),
-- 152 (Brother Danil)
(152, 4540, 1, 0, 0, 1, 19865),
(152, 159, 2, 0, 0, 1, 19865),
(152, 4496, 3, 0, 0, 1, 19865),
-- 958
(958, 64670, 1, 0, 0, 1, 19865),
(958, 63388, 2, 0, 0, 1, 19865),
(958, 79249, 3, 0, 0, 1, 19865),
(958, 3371, 4, 0, 0, 1, 19865),
(958, 2455, 608, 2, 0, 1, 19865),
(958, 3013, 609, 3, 0, 1, 19865),
(958, 3012, 615, 1, 0, 1, 19865),
(958, 1181, 610, 2, 0, 1, 19865),
(958, 955, 611, 2, 0, 1, 19865),
(958, 1180, 612, 2, 0, 1, 19865),
(958, 954, 613, 2, 0, 1, 19865),
(958, 858, 614, 3, 0, 1, 19865),
-- 959
(959, 795, 1, 0, 0, 1, 19865),
(959, 3602, 2, 0, 0, 1, 19865),
(959, 794, 3, 0, 0, 1, 19865),
(959, 792, 4, 0, 0, 1, 19865),
(959, 3603, 5, 0, 0, 1, 19865),
(959, 793, 6, 0, 0, 1, 19865),
-- 894 (Homer Stonefield)
(894, 4536, 1, 0, 0, 1, 19865),
(894, 4537, 2, 0, 0, 1, 19865),
(894, 4538, 3, 0, 0, 1, 19865),
(894, 4539, 4, 0, 0, 1, 19865),
(894, 4602, 5, 0, 0, 1, 19865),
(894, 8953, 6, 0, 0, 1, 19865),
-- 896 (Veldan Lightfoot)
(896, 799, 1, 0, 0, 1, 19865),
(896, 1839, 2, 0, 0, 1, 19865),
(896, 798, 3, 0, 0, 1, 19865),
(896, 796, 4, 0, 0, 1, 19865),
(896, 1840, 5, 0, 0, 1, 19865),
(896, 797, 6, 0, 0, 1, 19865),
(896, 846, 7, 0, 0, 1, 19865),
(896, 1843, 8, 0, 0, 1, 19865),
(896, 845, 9, 0, 0, 1, 19865),
(896, 843, 10, 0, 0, 1, 19865),
(896, 1844, 11, 0, 0, 1, 19865),
(896, 844, 12, 0, 0, 1, 19865),
-- 1645 (Quartermaster Hicks)
(1645, 2520, 1, 0, 0, 1, 19865),
(1645, 2521, 2, 0, 0, 1, 19865),
(1645, 2522, 3, 0, 0, 1, 19865),
(1645, 2523, 4, 0, 0, 1, 19865),
(1645, 2524, 5, 0, 0, 1, 19865),
(1645, 2525, 6, 0, 0, 1, 19865),
(1645, 2526, 7, 0, 0, 1, 19865),
(1645, 2527, 8, 0, 0, 1, 19865),
-- 1249 (Quartermaster Hudson)
(1249, 2392, 1, 0, 0, 1, 19865),
(1249, 2393, 2, 0, 0, 1, 19865),
(1249, 2394, 3, 0, 0, 1, 19865),
(1249, 2395, 4, 0, 0, 1, 19865),
(1249, 2396, 5, 0, 0, 1, 19865),
(1249, 2397, 6, 0, 0, 1, 19865),
(1249, 17186, 7, 0, 0, 1, 19865),
(1249, 1201, 8, 0, 0, 1, 19865),
-- 1250
(1250, 159, 1, 0, 0, 1, 19865),
(1250, 4540, 2, 0, 0, 1, 19865),
(1250, 4496, 3, 0, 0, 1, 19865),
(1250, 4498, 4, 0, 0, 1, 19865),
(1250, 4470, 5, 0, 0, 1, 19865),
(1250, 5042, 6, 0, 0, 1, 19865),
(1250, 7005, 7, 0, 0, 1, 19865),
(1250, 2901, 8, 0, 0, 1, 19865),
(1250, 85663, 9, 0, 0, 1, 19865),
(1250, 5956, 10, 0, 0, 1, 19865),
(1250, 6256, 11, 0, 0, 1, 19865),
(1250, 6217, 12, 0, 0, 1, 19865),
(1250, 2678, 13, 0, 0, 1, 19865),
(1250, 30817, 14, 0, 0, 1, 19865),
(1250, 2320, 15, 0, 0, 1, 19865),
(1250, 2880, 16, 0, 0, 1, 19865),
(1250, 3371, 17, 0, 0, 1, 19865),
(1250, 4289, 18, 0, 0, 1, 19865),
(1250, 6529, 19, 0, 0, 1, 19865),
(1250, 2604, 20, 0, 0, 1, 19865),
(1250, 2324, 21, 0, 0, 1, 19865),
(1250, 6260, 22, 0, 0, 1, 19865),
(1250, 39354, 23, 0, 0, 1, 19865),
(1250, 20815, 24, 0, 0, 1, 19865),
(1250, 39505, 25, 0, 0, 1, 19865),
(1250, 6272, 204, 1, 0, 1, 19865),
-- 54 (Corina Steele)
(54, 2489, 1, 0, 0, 1, 19865),
(54, 2492, 2, 0, 0, 1, 19865),
(54, 2491, 3, 0, 0, 1, 19865),
(54, 2494, 4, 0, 0, 1, 19865),
(54, 2495, 5, 0, 0, 1, 19865),
(54, 2488, 6, 0, 0, 1, 19865),
(54, 2490, 7, 0, 0, 1, 19865),
(54, 2493, 8, 0, 0, 1, 19865),
-- 1198
(1198, 2506, 1, 0, 0, 1, 19865),
(1198, 2507, 2, 0, 0, 1, 19865),
-- 465 (Barkeep Dobbins)
(465, 1939, 1, 0, 0, 1, 19865),
(465, 159, 2, 0, 0, 1, 19865),
(465, 1179, 3, 0, 0, 1, 19865),
(465, 1205, 4, 0, 0, 1, 19865),
(465, 1708, 5, 0, 0, 1, 19865),
(465, 1645, 6, 0, 0, 1, 19865),
(465, 8766, 7, 0, 0, 1, 19865),
(465, 2723, 8, 0, 0, 1, 19865),
(465, 2593, 9, 0, 0, 1, 19865),
(465, 2596, 10, 0, 0, 1, 19865),
(465, 2594, 11, 0, 0, 1, 19865),
(465, 2595, 12, 0, 0, 1, 19865),
(465, 83095, 13, 0, 0, 1, 19865),
(465, 82343, 14, 0, 0, 1, 19865),
(465, 83094, 15, 0, 0, 1, 19865),
(465, 82344, 16, 0, 0, 1, 19865),
(465, 2070, 17, 0, 0, 1, 19865),
(465, 414, 18, 0, 0, 1, 19865),
(465, 422, 19, 0, 0, 1, 19865),
(465, 1707, 20, 0, 0, 1, 19865),
(465, 3927, 21, 0, 0, 1, 19865),
(465, 8932, 22, 0, 0, 1, 19865),
-- 66 (Tharynn Bouden)
(66, 7005, 1, 0, 0, 1, 19865),
(66, 2901, 2, 0, 0, 1, 19865),
(66, 85663, 3, 0, 0, 1, 19865),
(66, 5956, 4, 0, 0, 1, 19865),
(66, 6256, 5, 0, 0, 1, 19865),
(66, 6217, 6, 0, 0, 1, 19865),
(66, 2678, 7, 0, 0, 1, 19865),
(66, 30817, 8, 0, 0, 1, 19865),
(66, 2320, 9, 0, 0, 1, 19865),
(66, 2880, 10, 0, 0, 1, 19865),
(66, 3371, 11, 0, 0, 1, 19865),
(66, 4289, 12, 0, 0, 1, 19865),
(66, 6529, 13, 0, 0, 1, 19865),
(66, 2604, 14, 0, 0, 1, 19865),
(66, 2324, 15, 0, 0, 1, 19865),
(66, 6260, 16, 0, 0, 1, 19865),
(66, 39354, 17, 0, 0, 1, 19865),
(66, 20815, 18, 0, 0, 1, 19865),
(66, 39505, 19, 0, 0, 1, 19865),
(66, 6325, 20, 0, 0, 1, 19865),
(66, 6328, 21, 0, 0, 1, 19865),
(66, 6270, 184, 1, 0, 1, 19865);

UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `item`=6270;
UPDATE `npc_vendor` SET `incrtime`=7200 WHERE `item`=6272;
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=958 AND `item` IN (2455, 3013, 1181, 955, 1180, 954, 858, 3012);
 
 
-- -------------------------------------------------------- 
-- 2015_04_26_01_world.sql 
-- -------------------------------------------------------- 
-- Vendors Stormwind (19865)

UPDATE `npc_vendor` SET `VerifiedBuild`=19865 WHERE `entry` IN (56925, 49701, 53641, 1285, 1287, 1291, 5193, 1294, 1295, 1297, 1303, 1309, 1310, 1312, 1314, 1315, 1319, 1320, 1321, 1323, 1324, 1333, 1339, 1341, 1349, 1350, 43694, 44252, 49893, 340, 50480, 5483, 11069, 5503, 5510, 12781, 12783, 50669, 55684, 12805, 5565);

DELETE FROM `npc_vendor` WHERE `entry` IN (52358, 44583, 87501, 6740, 5081, 30730, 1257, 43034, 1275, 1286, 1289, 1298, 1299, 1301, 1302, 1304, 1305, 1307, 1308, 1311, 1313, 1316, 1318, 1325, 1326, 1327, 1328, 73190, 52029, 52030, 52031, 44235, 1347, 44236, 44237, 1348, 1351, 44245, 44246, 277, 69974, 69975, 5494, 483, 5509, 28347, 5512, 5514, 4981, 12784, 12785);
INSERT INTO `npc_vendor` (`entry`, `item`, `slot`, `maxcount`, `ExtendedCost`, `Type`, `VerifiedBuild`) VALUES
-- 52358 (Craggle Wobbletop)
(52358, 104323, 5, 0, 0, 1, 19865), 
(52358, 104324, 6, 0, 0, 1, 19865), 
(52358, 95621, 7, 0, 0, 1, 19865), 
(52358, 95481, 8, 0, 0, 1, 19865), 
(52358, 95482, 9, 0, 0, 1, 19865), 
(52358, 46725, 10, 0, 0, 1, 19865), 
(52358, 48601, 11, 0, 0, 1, 19865), 
(52358, 54436, 12, 0, 0, 1, 19865), 
(52358, 54343, 13, 0, 0, 1, 19865), 
(52358, 44481, 14, 0, 0, 1, 19865), 
(52358, 34498, 15, 0, 0, 1, 19865), 
(52358, 54437, 16, 0, 0, 1, 19865), 
(52358, 54438, 17, 0, 0, 1, 19865), 
(52358, 44482, 18, 0, 0, 1, 19865), 
(52358, 44601, 19, 0, 0, 1, 19865), 
(52358, 44599, 20, 0, 0, 1, 19865), 
(52358, 45057, 21, 0, 0, 1, 19865), 
(52358, 44606, 22, 0, 0, 1, 19865), 
-- 44583 (Terrance Denman)
(44583, 20815, 1, 0, 0, 1, 19865), 
(44583, 52188, 2, 0, 0, 1, 19865), 
(44583, 52196, 3, 0, 3232, 1, 19865), 
(44583, 52381, 4, 0, 2972, 1, 19865), 
(44583, 52449, 5, 0, 2972, 1, 19865), 
(44583, 52447, 6, 0, 2972, 1, 19865), 
(44583, 52448, 7, 0, 2972, 1, 19865), 
(44583, 52459, 8, 0, 2972, 1, 19865), 
(44583, 52457, 9, 0, 2972, 1, 19865), 
(44583, 52450, 10, 0, 2972, 1, 19865), 
(44583, 52458, 11, 0, 2972, 1, 19865), 
(44583, 52454, 12, 0, 2972, 1, 19865), 
(44583, 52456, 13, 0, 2972, 1, 19865), 
(44583, 52451, 14, 0, 2972, 1, 19865), 
(44583, 52452, 15, 0, 2972, 1, 19865), 
(44583, 52453, 16, 0, 2972, 1, 19865), 
(44583, 52455, 17, 0, 2972, 1, 19865), 
(44583, 52398, 18, 0, 2970, 1, 19865), 
(44583, 52396, 19, 0, 2970, 1, 19865), 
(44583, 52397, 20, 0, 2970, 1, 19865), 
(44583, 52395, 21, 0, 2970, 1, 19865), 
(44583, 52394, 22, 0, 2970, 1, 19865), 
(44583, 52409, 23, 0, 2970, 1, 19865), 
(44583, 52401, 24, 0, 2970, 1, 19865), 
(44583, 52405, 25, 0, 2970, 1, 19865), 
(44583, 52408, 26, 0, 2970, 1, 19865), 
(44583, 52406, 27, 0, 2970, 1, 19865), 
(44583, 52403, 28, 0, 2970, 1, 19865), 
(44583, 52407, 29, 0, 2970, 1, 19865), 
(44583, 52404, 30, 0, 2970, 1, 19865), 
(44583, 52400, 31, 0, 2970, 1, 19865), 
(44583, 52399, 32, 0, 2970, 1, 19865), 
(44583, 52402, 33, 0, 2970, 1, 19865), 
(44583, 52428, 34, 0, 2970, 1, 19865), 
(44583, 52425, 35, 0, 2970, 1, 19865), 
(44583, 52429, 36, 0, 2970, 1, 19865), 
(44583, 52424, 37, 0, 2970, 1, 19865), 
(44583, 52426, 38, 0, 2970, 1, 19865), 
(44583, 52430, 39, 0, 2970, 1, 19865), 
(44583, 52423, 40, 0, 2970, 1, 19865), 
(44583, 52432, 41, 0, 2970, 1, 19865), 
(44583, 52427, 42, 0, 2970, 1, 19865), 
(44583, 68742, 43, 0, 2970, 1, 19865), 
(44583, 52431, 44, 0, 2970, 1, 19865), 
(44583, 52419, 45, 0, 2970, 1, 19865), 
(44583, 52421, 46, 0, 2970, 1, 19865), 
(44583, 52417, 47, 0, 2970, 1, 19865), 
(44583, 52413, 48, 0, 2970, 1, 19865), 
(44583, 52416, 49, 0, 2970, 1, 19865), 
(44583, 52415, 50, 0, 2970, 1, 19865), 
(44583, 52420, 51, 0, 2970, 1, 19865), 
(44583, 52412, 52, 0, 2970, 1, 19865), 
(44583, 52422, 53, 0, 2970, 1, 19865), 
(44583, 68360, 54, 0, 2970, 1, 19865), 
(44583, 52410, 55, 0, 2970, 1, 19865), 
(44583, 52414, 56, 0, 2970, 1, 19865), 
(44583, 52418, 57, 0, 2970, 1, 19865), 
(44583, 52411, 58, 0, 2970, 1, 19865), 
(44583, 68361, 59, 0, 2970, 1, 19865), 
(44583, 68359, 60, 0, 2970, 1, 19865), 
(44583, 52362, 61, 0, 2970, 1, 19865), 
(44583, 52387, 62, 0, 2970, 1, 19865), 
(44583, 52380, 63, 0, 2970, 1, 19865), 
(44583, 52384, 64, 0, 2970, 1, 19865), 
(44583, 52389, 65, 0, 2970, 1, 19865), 
(44583, 52393, 66, 0, 2970, 1, 19865), 
(44583, 52390, 67, 0, 2970, 1, 19865), 
(44583, 52391, 68, 0, 2970, 1, 19865), 
(44583, 52392, 69, 0, 2970, 1, 19865), 
(44583, 52437, 70, 0, 2971, 1, 19865), 
(44583, 52435, 71, 0, 2971, 1, 19865), 
(44583, 52434, 72, 0, 2971, 1, 19865), 
(44583, 52441, 73, 0, 2971, 1, 19865), 
(44583, 52438, 74, 0, 2971, 1, 19865), 
(44583, 52439, 75, 0, 2971, 1, 19865), 
(44583, 52443, 76, 0, 2971, 1, 19865), 
(44583, 52436, 77, 0, 2971, 1, 19865), 
(44583, 52433, 78, 0, 2971, 1, 19865), 
(44583, 52445, 79, 0, 2971, 1, 19865), 
(44583, 52444, 80, 0, 2971, 1, 19865), 
(44583, 52442, 81, 0, 2971, 1, 19865), 
(44583, 52440, 82, 0, 2971, 1, 19865), 
(44583, 69853, 83, 0, 2973, 1, 19865), 
(44583, 52460, 84, 0, 2973, 1, 19865), 
(44583, 52461, 85, 0, 2973, 1, 19865), 
(44583, 52463, 86, 0, 2973, 1, 19865), 
(44583, 52462, 87, 0, 2973, 1, 19865), 
(44583, 52467, 88, 0, 2973, 1, 19865), 
(44583, 52465, 89, 0, 2973, 1, 19865), 
(44583, 52464, 90, 0, 2973, 1, 19865), 
(44583, 52466, 91, 0, 2973, 1, 19865), 
-- 87501 (Paulie)
(87501, 116789, 1, 0, 5834, 1, 19865), 
-- 6740 (Innkeeper Allison)
(6740, 4540, 1, 0, 0, 1, 19865), 
(6740, 4541, 2, 0, 0, 1, 19865), 
(6740, 4542, 3, 0, 0, 1, 19865), 
(6740, 4544, 4, 0, 0, 1, 19865), 
(6740, 4601, 5, 0, 0, 1, 19865), 
(6740, 8950, 6, 0, 0, 1, 19865), 
(6740, 27855, 7, 0, 0, 1, 19865), 
(6740, 33449, 8, 0, 0, 1, 19865), 
(6740, 35950, 9, 0, 0, 1, 19865), 
(6740, 58260, 10, 0, 0, 1, 19865), 
(6740, 58261, 11, 0, 0, 1, 19865), 
(6740, 82450, 12, 0, 0, 1, 19865), 
(6740, 82451, 13, 0, 0, 1, 19865), 
(6740, 4536, 14, 0, 0, 1, 19865), 
(6740, 4537, 15, 0, 0, 1, 19865), 
(6740, 4538, 16, 0, 0, 1, 19865), 
(6740, 4539, 17, 0, 0, 1, 19865), 
(6740, 4602, 18, 0, 0, 1, 19865), 
(6740, 8953, 19, 0, 0, 1, 19865), 
(6740, 27856, 20, 0, 0, 1, 19865), 
(6740, 35949, 21, 0, 0, 1, 19865), 
(6740, 35948, 22, 0, 0, 1, 19865), 
(6740, 58264, 23, 0, 0, 1, 19865), 
(6740, 58265, 24, 0, 0, 1, 19865), 
(6740, 81919, 25, 0, 0, 1, 19865), 
(6740, 81920, 26, 0, 0, 1, 19865), 
(6740, 159, 27, 0, 0, 1, 19865), 
(6740, 1179, 28, 0, 0, 1, 19865), 
(6740, 1205, 29, 0, 0, 1, 19865), 
(6740, 1708, 30, 0, 0, 1, 19865), 
(6740, 1645, 31, 0, 0, 1, 19865), 
(6740, 8766, 32, 0, 0, 1, 19865), 
(6740, 28399, 33, 0, 0, 1, 19865), 
(6740, 58274, 34, 0, 0, 1, 19865), 
(6740, 33444, 35, 0, 0, 1, 19865), 
(6740, 33445, 36, 0, 0, 1, 19865), 
(6740, 58256, 37, 0, 0, 1, 19865), 
(6740, 58257, 38, 0, 0, 1, 19865), 
(6740, 81924, 39, 0, 0, 1, 19865), 
(6740, 81923, 40, 0, 0, 1, 19865), 
-- 5081 (Connor Rivers)
(5081, 159, 2, 0, 0, 1, 19865), 
(5081, 30817, 3, 0, 0, 1, 19865), 
(5081, 2678, 4, 0, 0, 1, 19865), 
-- 30730 (Stanly McCormick)
(30730, 39469, 9, 0, 5485, 1, 19865), 
(30730, 39774, 10, 0, 5485, 1, 19865), 
(30730, 43116, 11, 0, 5485, 1, 19865), 
(30730, 43118, 12, 0, 5485, 1, 19865), 
(30730, 43120, 13, 0, 5485, 1, 19865), 
(30730, 43122, 14, 0, 5485, 1, 19865), 
(30730, 43124, 15, 0, 5485, 1, 19865), 
(30730, 43126, 16, 0, 5485, 1, 19865), 
(30730, 61978, 17, 0, 5485, 1, 19865), 
(30730, 79254, 18, 0, 5485, 1, 19865), 
(30730, 43127, 19, 0, 5486, 1, 19865), 
(30730, 61981, 20, 0, 5486, 1, 19865), 
(30730, 79255, 21, 0, 5486, 1, 19865), 
(30730, 64670, 5358, 0, 0, 1, 19865), 
(30730, 63388, 5359, 0, 0, 1, 19865), 
(30730, 79249, 5360, 0, 0, 1, 19865), 
(30730, 39354, 5361, 0, 0, 1, 19865), 
(30730, 39505, 5362, 0, 0, 1, 19865), 
(30730, 39489, 5363, 3, 0, 1, 19865), 
(30730, 1515, 5364, 0, 0, 1, 19865), 
(30730, 79740, 5365, 0, 0, 1, 19865), 
-- 1257 (Keldric Boucher)
(1257, 64670, 1, 0, 0, 1, 19865), 
(1257, 63388, 2, 0, 0, 1, 19865), 
(1257, 79249, 3, 0, 0, 1, 19865), 
(1257, 3371, 4, 0, 0, 1, 19865), 
(1257, 858, 113, 2, 0, 1, 19865), 
-- 43034 (Colin Field)
(43034, 2723, 1, 0, 0, 1, 19865), 
(43034, 2593, 2, 0, 0, 1, 19865), 
(43034, 2596, 3, 0, 0, 1, 19865), 
(43034, 2594, 4, 0, 0, 1, 19865), 
(43034, 2595, 5, 0, 0, 1, 19865), 
(43034, 83095, 6, 0, 0, 1, 19865), 
(43034, 82343, 7, 0, 0, 1, 19865), 
(43034, 83094, 8, 0, 0, 1, 19865), 
(43034, 82344, 9, 0, 0, 1, 19865), 
(43034, 159, 10, 0, 0, 1, 19865), 
(43034, 1179, 11, 0, 0, 1, 19865), 
(43034, 1205, 12, 0, 0, 1, 19865), 
(43034, 1708, 13, 0, 0, 1, 19865), 
(43034, 1645, 14, 0, 0, 1, 19865), 
(43034, 8766, 15, 0, 0, 1, 19865), 
(43034, 28399, 16, 0, 0, 1, 19865), 
(43034, 58274, 17, 0, 0, 1, 19865), 
(43034, 33444, 18, 0, 0, 1, 19865), 
(43034, 33445, 19, 0, 0, 1, 19865), 
(43034, 58256, 20, 0, 0, 1, 19865), 
(43034, 58257, 21, 0, 0, 1, 19865), 
(43034, 81924, 22, 0, 0, 1, 19865), 
(43034, 81923, 23, 0, 0, 1, 19865), 
-- 1275 (Kyra Boucher)
(1275, 64670, 1, 0, 0, 1, 19865), 
(1275, 63388, 2, 0, 0, 1, 19865), 
(1275, 79249, 3, 0, 0, 1, 19865), 
-- 1286 (Edna Mullby)
(1286, 2901, 2, 0, 0, 1, 19865), 
(1286, 7005, 3, 0, 0, 1, 19865), 
(1286, 85663, 4, 0, 0, 1, 19865), 
(1286, 6256, 5, 0, 0, 1, 19865), 
(1286, 6217, 6, 0, 0, 1, 19865), 
(1286, 5956, 7, 0, 0, 1, 19865), 
(1286, 2320, 8, 0, 0, 1, 19865), 
(1286, 2321, 9, 0, 0, 1, 19865), 
(1286, 4291, 10, 0, 0, 1, 19865), 
(1286, 8343, 11, 0, 0, 1, 19865), 
(1286, 14341, 12, 0, 0, 1, 19865), 
(1286, 2678, 13, 0, 0, 1, 19865), 
(1286, 2880, 14, 0, 0, 1, 19865), 
(1286, 3466, 15, 0, 0, 1, 19865), 
(1286, 3857, 16, 0, 0, 1, 19865), 
(1286, 2604, 17, 0, 0, 1, 19865), 
(1286, 2325, 18, 0, 0, 1, 19865), 
(1286, 4341, 19, 0, 0, 1, 19865), 
(1286, 4342, 20, 0, 0, 1, 19865), 
(1286, 4340, 21, 0, 0, 1, 19865), 
(1286, 3371, 22, 0, 0, 1, 19865), 
(1286, 4289, 23, 0, 0, 1, 19865), 
(1286, 4399, 24, 0, 0, 1, 19865), 
(1286, 4400, 25, 0, 0, 1, 19865), 
(1286, 6530, 26, 0, 0, 1, 19865), 
(1286, 6532, 27, 0, 0, 1, 19865), 
(1286, 20815, 28, 0, 0, 1, 19865), 
(1286, 39505, 29, 0, 0, 1, 19865), 
(1286, 20856, 700, 1, 0, 1, 19865), 
-- 1289 (Gunther Weller)
(1289, 851, 1, 0, 0, 1, 19865), 
(1289, 1198, 2, 0, 0, 1, 19865), 
(1289, 853, 3, 0, 0, 1, 19865), 
(1289, 1196, 4, 0, 0, 1, 19865), 
(1289, 852, 5, 0, 0, 1, 19865), 
(1289, 1197, 6, 0, 0, 1, 19865), 
(1289, 2207, 7, 0, 0, 1, 19865), 
(1289, 854, 8, 0, 0, 1, 19865), 
(1289, 2027, 9, 0, 0, 1, 19865), 
(1289, 2024, 10, 0, 0, 1, 19865), 
(1289, 2029, 11, 0, 0, 1, 19865), 
(1289, 2025, 12, 0, 0, 1, 19865), 
(1289, 2028, 13, 0, 0, 1, 19865), 
(1289, 2026, 14, 0, 0, 1, 19865), 
(1289, 2208, 15, 0, 0, 1, 19865), 
(1289, 2030, 16, 0, 0, 1, 19865), 
(1289, 923, 17, 0, 0, 1, 19865), 
(1289, 922, 18, 0, 0, 1, 19865), 
(1289, 927, 19, 0, 0, 1, 19865), 
(1289, 926, 20, 0, 0, 1, 19865), 
(1289, 925, 21, 0, 0, 1, 19865), 
(1289, 924, 22, 0, 0, 1, 19865), 
(1289, 2209, 23, 0, 0, 1, 19865), 
(1289, 928, 24, 0, 0, 1, 19865), 
(1289, 15810, 25, 0, 0, 1, 19865), 
(1289, 15811, 26, 0, 0, 1, 19865), 
-- 1298 (Frederick Stover)
(1298, 3026, 1, 0, 0, 1, 19865), 
(1298, 3027, 2, 0, 0, 1, 19865), 
(1298, 11303, 58, 2, 0, 1, 19865), 
(1298, 11306, 59, 1, 0, 1, 19865), 
-- 1299 (Lisbeth Schneider)
(1299, 3428, 1, 0, 0, 1, 19865), 
(1299, 3426, 2, 0, 0, 1, 19865), 
(1299, 3427, 3, 0, 0, 1, 19865), 
(1299, 13897, 352, 0, 0, 1, 19865), 
(1299, 13896, 353, 0, 0, 1, 19865), 
(1299, 13898, 354, 0, 0, 1, 19865), 
-- 1301 (Julia Gallina)
(1301, 2723, 1, 0, 0, 1, 19865), 
(1301, 2593, 2, 0, 0, 1, 19865), 
(1301, 2596, 3, 0, 0, 1, 19865), 
(1301, 2594, 4, 0, 0, 1, 19865), 
(1301, 2595, 5, 0, 0, 1, 19865), 
(1301, 83095, 6, 0, 0, 1, 19865), 
(1301, 82343, 7, 0, 0, 1, 19865), 
(1301, 83094, 8, 0, 0, 1, 19865), 
(1301, 82344, 9, 0, 0, 1, 19865), 
-- 1302 (Bernard Gump)
(1302, 3421, 1, 0, 0, 1, 19865), 
(1302, 3419, 2, 0, 0, 1, 19865), 
(1302, 3422, 3, 0, 0, 1, 19865), 
(1302, 3420, 4, 0, 0, 1, 19865), 
(1302, 3423, 5, 0, 0, 1, 19865), 
(1302, 3424, 6, 0, 0, 1, 19865), 
(1302, 3356, 256, 1, 0, 1, 19865), 
-- 1304 (Darian Singh)
(1304, 5740, 1, 0, 0, 1, 19865), 
(1304, 18649, 156, 1, 0, 1, 19865), 
-- 1305 (Jarel Moor)
(1305, 2723, 1, 0, 0, 1, 19865), 
(1305, 2593, 2, 0, 0, 1, 19865), 
(1305, 2596, 3, 0, 0, 1, 19865), 
(1305, 2594, 4, 0, 0, 1, 19865), 
(1305, 2595, 5, 0, 0, 1, 19865), 
(1305, 83095, 6, 0, 0, 1, 19865), 
(1305, 82343, 7, 0, 0, 1, 19865), 
(1305, 83094, 8, 0, 0, 1, 19865), 
(1305, 82344, 9, 0, 0, 1, 19865), 
-- 1307 (Charys Yserian)
(1307, 64670, 1, 0, 0, 1, 19865), 
(1307, 63388, 2, 0, 0, 1, 19865), 
(1307, 79249, 3, 0, 0, 1, 19865), 
(1307, 4827, 184, 1, 0, 1, 19865), 
(1307, 4828, 185, 1, 0, 1, 19865), 
(1307, 4826, 186, 1, 0, 1, 19865), 
(1307, 929, 187, 2, 0, 1, 19865), 
(1307, 3385, 188, 3, 0, 1, 19865), 
-- 1308 (Owen Vaughn)
(1308, 64670, 1, 0, 0, 1, 19865), 
(1308, 63388, 2, 0, 0, 1, 19865), 
(1308, 79249, 3, 0, 0, 1, 19865), 
-- 1311 (Joachim Brenlow)
(1311, 2723, 1, 0, 0, 1, 19865), 
(1311, 2593, 2, 0, 0, 1, 19865), 
(1311, 2596, 3, 0, 0, 1, 19865), 
(1311, 2594, 4, 0, 0, 1, 19865), 
(1311, 2595, 5, 0, 0, 1, 19865), 
(1311, 83095, 6, 0, 0, 1, 19865), 
(1311, 82343, 7, 0, 0, 1, 19865), 
(1311, 83094, 8, 0, 0, 1, 19865), 
(1311, 82344, 9, 0, 0, 1, 19865), 
-- 1313 (Maria Lumere)
(1313, 3371, 1, 0, 0, 1, 19865), 
(1313, 785, 519, 2, 0, 1, 19865), 
(1313, 2453, 520, 2, 0, 1, 19865), 
(1313, 3357, 521, 1, 0, 1, 19865), 
(1313, 9301, 522, 1, 0, 1, 19865), 
-- 1316 (Adair Gilroy)
(1316, 10647, 669, 0, 0, 1, 19865), 
(1316, 39354, 670, 0, 0, 1, 19865), 
(1316, 3012, 671, 3, 0, 1, 19865), 
(1316, 954, 672, 1, 0, 1, 19865), 
(1316, 1711, 673, 2, 0, 1, 19865), 
(1316, 3013, 674, 3, 0, 1, 19865), 
(1316, 955, 675, 1, 0, 1, 19865), 
(1316, 1180, 676, 1, 0, 1, 19865), 
-- 1318 (Jessara Cordell)
(1318, 67312, 11, 0, 3326, 1, 19865), 
(1318, 64411, 13, 0, 3016, 1, 19865), 
(1318, 64412, 14, 0, 3016, 1, 19865), 
(1318, 52738, 15, 0, 3016, 1, 19865), 
(1318, 64413, 16, 0, 3016, 1, 19865), 
(1318, 52740, 17, 0, 3016, 1, 19865), 
(1318, 52739, 18, 0, 3016, 1, 19865), 
(1318, 52737, 19, 0, 3016, 1, 19865), 
(1318, 64414, 20, 0, 3016, 1, 19865), 
(1318, 64415, 21, 0, 3016, 1, 19865), 
(1318, 52736, 22, 0, 3017, 1, 19865), 
(1318, 52733, 23, 0, 3017, 1, 19865), 
(1318, 52735, 24, 0, 3017, 1, 19865), 
(1318, 38682, 2335, 0, 0, 1, 19865), 
(1318, 6217, 2336, 0, 0, 1, 19865), 
(1318, 10940, 2337, 4, 0, 1, 19865), 
(1318, 10938, 2338, 2, 0, 1, 19865), 
(1318, 4470, 2339, 0, 0, 1, 19865), 
(1318, 11291, 2340, 0, 0, 1, 19865), 
(1318, 20758, 2341, 0, 0, 1, 19865), 
(1318, 20752, 2342, 0, 0, 1, 19865), 
(1318, 20753, 2343, 0, 0, 1, 19865), 
(1318, 22307, 2344, 0, 0, 1, 19865), 
-- 1325 (Jasper Fel)
(1325, 4565, 1, 0, 0, 1, 19865), 
-- 1326 (Sloan McCoy)
(1326, 4565, 1, 0, 0, 1, 19865), 
-- 1327 (Reese Langston)
(1327, 2723, 1, 0, 0, 1, 19865), 
(1327, 2593, 2, 0, 0, 1, 19865), 
(1327, 2596, 3, 0, 0, 1, 19865), 
(1327, 2594, 4, 0, 0, 1, 19865), 
(1327, 2595, 5, 0, 0, 1, 19865), 
(1327, 83095, 6, 0, 0, 1, 19865), 
(1327, 82343, 7, 0, 0, 1, 19865), 
(1327, 83094, 8, 0, 0, 1, 19865), 
(1327, 82344, 9, 0, 0, 1, 19865), 
(1327, 159, 10, 0, 0, 1, 19865), 
(1327, 1179, 11, 0, 0, 1, 19865), 
(1327, 1205, 12, 0, 0, 1, 19865), 
(1327, 1708, 13, 0, 0, 1, 19865), 
(1327, 1645, 14, 0, 0, 1, 19865), 
(1327, 8766, 15, 0, 0, 1, 19865), 
(1327, 28399, 16, 0, 0, 1, 19865), 
(1327, 58274, 17, 0, 0, 1, 19865), 
(1327, 33444, 18, 0, 0, 1, 19865), 
(1327, 33445, 19, 0, 0, 1, 19865), 
(1327, 58256, 20, 0, 0, 1, 19865), 
(1327, 58257, 21, 0, 0, 1, 19865), 
(1327, 81924, 22, 0, 0, 1, 19865), 
(1327, 81923, 23, 0, 0, 1, 19865), 
-- 1328 (Elly Langston)
(1328, 2723, 1, 0, 0, 1, 19865), 
(1328, 2593, 2, 0, 0, 1, 19865), 
(1328, 2596, 3, 0, 0, 1, 19865), 
(1328, 2594, 4, 0, 0, 1, 19865), 
(1328, 2595, 5, 0, 0, 1, 19865), 
(1328, 83095, 6, 0, 0, 1, 19865), 
(1328, 82343, 7, 0, 0, 1, 19865), 
(1328, 83094, 8, 0, 0, 1, 19865), 
(1328, 82344, 9, 0, 0, 1, 19865), 
(1328, 159, 10, 0, 0, 1, 19865), 
(1328, 1179, 11, 0, 0, 1, 19865), 
(1328, 1205, 12, 0, 0, 1, 19865), 
(1328, 1708, 13, 0, 0, 1, 19865), 
(1328, 1645, 14, 0, 0, 1, 19865), 
(1328, 8766, 15, 0, 0, 1, 19865), 
-- 73190 (Necrolord Sipe)
(73190, 102514, 1, 0, 5214, 1, 19865), 
(73190, 70909, 2, 0, 5214, 1, 19865), 
(73190, 116777, 3, 0, 5214, 1, 19865),
-- 52029 (Edlan Halsing)
(52029, 52722, 1, 0, 3407, 1, 19865), 
(52029, 52555, 2, 0, 3338, 1, 19865), 
(52029, 52719, 3, 0, 3339, 1, 19865), 
(52029, 52721, 4, 0, 3340, 1, 19865), 
(52029, 52976, 5, 0, 3341, 1, 19865), 
(52029, 53010, 6, 0, 3344, 1, 19865), 
(52029, 52185, 7, 0, 3342, 1, 19865), 
(52029, 68813, 8, 0, 3343, 1, 19865), 
-- 52030 (Liliana Emberfrost)
(52030, 122371, 2, 0, 5802, 1, 19865), 
(52030, 122530, 3, 0, 5802, 1, 19865), 
(52030, 122373, 4, 0, 5804, 1, 19865), 
(52030, 122372, 5, 0, 5804, 1, 19865), 
(52030, 122374, 6, 0, 5804, 1, 19865), 
(52030, 122375, 7, 0, 5804, 1, 19865), 
(52030, 122377, 8, 0, 5804, 1, 19865), 
(52030, 122376, 9, 0, 5804, 1, 19865), 
(52030, 122378, 10, 0, 5804, 1, 19865), 
(52030, 122366, 11, 0, 5803, 1, 19865), 
(52030, 122364, 12, 0, 5804, 1, 19865), 
(52030, 122368, 13, 0, 5803, 1, 19865), 
(52030, 122367, 14, 0, 5804, 1, 19865), 
(52030, 122369, 15, 0, 5804, 1, 19865), 
(52030, 122365, 16, 0, 5803, 1, 19865), 
(52030, 122338, 17, 0, 5561, 1, 19865), 
(52030, 122339, 18, 0, 5560, 1, 19865), 
-- 52031 (Sarana Damir)
(52031, 39469, 1, 0, 5485, 1, 19865), 
(52031, 39774, 2, 0, 5485, 1, 19865), 
(52031, 43116, 3, 0, 5485, 1, 19865), 
(52031, 43118, 4, 0, 5485, 1, 19865), 
(52031, 43120, 5, 0, 5485, 1, 19865), 
(52031, 43122, 6, 0, 5485, 1, 19865), 
(52031, 43124, 7, 0, 5485, 1, 19865), 
(52031, 43126, 8, 0, 5485, 1, 19865), 
(52031, 61978, 9, 0, 5485, 1, 19865), 
(52031, 79254, 10, 0, 5485, 1, 19865), 
(52031, 43127, 11, 0, 5486, 1, 19865), 
(52031, 61981, 12, 0, 5486, 1, 19865), 
(52031, 79255, 13, 0, 5486, 1, 19865), 
-- 44235 (Thaegra Tillstone)
(44235, 4540, 1, 0, 0, 1, 19865), 
(44235, 4541, 2, 0, 0, 1, 19865), 
(44235, 4542, 3, 0, 0, 1, 19865), 
(44235, 4544, 4, 0, 0, 1, 19865), 
(44235, 4601, 5, 0, 0, 1, 19865), 
(44235, 8950, 6, 0, 0, 1, 19865), 
(44235, 27855, 7, 0, 0, 1, 19865), 
(44235, 33449, 8, 0, 0, 1, 19865), 
(44235, 35950, 9, 0, 0, 1, 19865), 
(44235, 58260, 10, 0, 0, 1, 19865), 
(44235, 58261, 11, 0, 0, 1, 19865), 
(44235, 82450, 12, 0, 0, 1, 19865), 
(44235, 82451, 13, 0, 0, 1, 19865), 
(44235, 4604, 14, 0, 0, 1, 19865), 
(44235, 4605, 15, 0, 0, 1, 19865), 
(44235, 4606, 16, 0, 0, 1, 19865), 
(44235, 4607, 17, 0, 0, 1, 19865), 
(44235, 4608, 18, 0, 0, 1, 19865), 
(44235, 8948, 19, 0, 0, 1, 19865), 
(44235, 27859, 20, 0, 0, 1, 19865), 
(44235, 33452, 21, 0, 0, 1, 19865), 
(44235, 35947, 22, 0, 0, 1, 19865), 
(44235, 58266, 23, 0, 0, 1, 19865), 
(44235, 58267, 24, 0, 0, 1, 19865), 
(44235, 81889, 25, 0, 0, 1, 19865), 
(44235, 81916, 26, 0, 0, 1, 19865), 
(44235, 159, 27, 0, 0, 1, 19865), 
(44235, 1179, 28, 0, 0, 1, 19865), 
(44235, 1205, 29, 0, 0, 1, 19865), 
(44235, 1708, 30, 0, 0, 1, 19865), 
(44235, 1645, 31, 0, 0, 1, 19865), 
(44235, 8766, 32, 0, 0, 1, 19865), 
(44235, 28399, 33, 0, 0, 1, 19865), 
(44235, 58274, 34, 0, 0, 1, 19865), 
(44235, 33444, 35, 0, 0, 1, 19865), 
(44235, 33445, 36, 0, 0, 1, 19865), 
(44235, 58256, 37, 0, 0, 1, 19865), 
(44235, 58257, 38, 0, 0, 1, 19865), 
(44235, 81924, 39, 0, 0, 1, 19865), 
(44235, 81923, 40, 0, 0, 1, 19865), 
-- 1347 (Alexandra Bolero)
(1347, 2320, 1, 0, 0, 1, 19865), 
(1347, 2321, 2, 0, 0, 1, 19865), 
(1347, 4291, 3, 0, 0, 1, 19865), 
(1347, 8343, 4, 0, 0, 1, 19865), 
(1347, 14341, 5, 0, 0, 1, 19865), 
(1347, 38426, 6, 0, 0, 1, 19865), 
(1347, 2324, 7, 0, 0, 1, 19865), 
(1347, 2604, 8, 0, 0, 1, 19865), 
(1347, 6260, 9, 0, 0, 1, 19865), 
(1347, 2605, 10, 0, 0, 1, 19865), 
(1347, 4341, 11, 0, 0, 1, 19865), 
(1347, 4340, 12, 0, 0, 1, 19865), 
(1347, 6261, 13, 0, 0, 1, 19865), 
(1347, 2325, 14, 0, 0, 1, 19865), 
(1347, 4342, 15, 0, 0, 1, 19865), 
(1347, 10290, 16, 0, 0, 1, 19865), 
(1347, 68199, 18, 0, 2988, 1, 19865), 
(1347, 54600, 19, 0, 2988, 1, 19865), 
(1347, 54599, 20, 0, 2988, 1, 19865), 
(1347, 54593, 21, 0, 2988, 1, 19865), 
(1347, 54594, 22, 0, 2988, 1, 19865), 
(1347, 54595, 23, 0, 2988, 1, 19865), 
(1347, 54596, 24, 0, 2988, 1, 19865), 
(1347, 54597, 25, 0, 2988, 1, 19865), 
(1347, 54598, 26, 0, 2988, 1, 19865), 
(1347, 54604, 27, 0, 2989, 1, 19865), 
(1347, 54601, 28, 0, 2989, 1, 19865), 
(1347, 54603, 29, 0, 2989, 1, 19865), 
(1347, 54602, 30, 0, 2989, 1, 19865), 
(1347, 54605, 31, 0, 2989, 1, 19865), 
(1347, 10325, 426, 1, 0, 1, 19865), 
-- 44236 (Myrla Stoneround)
(44236, 2723, 1, 0, 0, 1, 19865), 
(44236, 2593, 2, 0, 0, 1, 19865), 
(44236, 2596, 3, 0, 0, 1, 19865), 
(44236, 2594, 4, 0, 0, 1, 19865), 
(44236, 2595, 5, 0, 0, 1, 19865), 
(44236, 83095, 6, 0, 0, 1, 19865), 
(44236, 82343, 7, 0, 0, 1, 19865), 
(44236, 83094, 8, 0, 0, 1, 19865), 
(44236, 82344, 9, 0, 0, 1, 19865), 
(44236, 159, 10, 0, 0, 1, 19865), 
(44236, 1179, 11, 0, 0, 1, 19865), 
(44236, 1205, 12, 0, 0, 1, 19865), 
(44236, 1708, 13, 0, 0, 1, 19865), 
(44236, 1645, 14, 0, 0, 1, 19865), 
(44236, 8766, 15, 0, 0, 1, 19865), 
-- 44237 (Maegan Tillman)
(44237, 4540, 1, 0, 0, 1, 19865), 
(44237, 4541, 2, 0, 0, 1, 19865), 
(44237, 4542, 3, 0, 0, 1, 19865), 
(44237, 4544, 4, 0, 0, 1, 19865), 
(44237, 4601, 5, 0, 0, 1, 19865), 
(44237, 8950, 6, 0, 0, 1, 19865), 
(44237, 27855, 7, 0, 0, 1, 19865), 
(44237, 33449, 8, 0, 0, 1, 19865), 
(44237, 35950, 9, 0, 0, 1, 19865), 
(44237, 58260, 10, 0, 0, 1, 19865), 
(44237, 58261, 11, 0, 0, 1, 19865), 
(44237, 82450, 12, 0, 0, 1, 19865), 
(44237, 82451, 13, 0, 0, 1, 19865), 
(44237, 4604, 14, 0, 0, 1, 19865), 
(44237, 4605, 15, 0, 0, 1, 19865), 
(44237, 4606, 16, 0, 0, 1, 19865), 
(44237, 4607, 17, 0, 0, 1, 19865), 
(44237, 4608, 18, 0, 0, 1, 19865), 
(44237, 8948, 19, 0, 0, 1, 19865), 
(44237, 27859, 20, 0, 0, 1, 19865), 
(44237, 33452, 21, 0, 0, 1, 19865), 
(44237, 35947, 22, 0, 0, 1, 19865), 
(44237, 58266, 23, 0, 0, 1, 19865), 
(44237, 58267, 24, 0, 0, 1, 19865), 
(44237, 81889, 25, 0, 0, 1, 19865), 
(44237, 81916, 26, 0, 0, 1, 19865), 
(44237, 159, 27, 0, 0, 1, 19865), 
(44237, 1179, 28, 0, 0, 1, 19865), 
(44237, 1205, 29, 0, 0, 1, 19865), 
(44237, 1708, 30, 0, 0, 1, 19865), 
(44237, 1645, 31, 0, 0, 1, 19865), 
(44237, 8766, 32, 0, 0, 1, 19865), 
(44237, 28399, 33, 0, 0, 1, 19865), 
(44237, 58274, 34, 0, 0, 1, 19865), 
(44237, 33444, 35, 0, 0, 1, 19865), 
(44237, 33445, 36, 0, 0, 1, 19865), 
(44237, 58256, 37, 0, 0, 1, 19865), 
(44237, 58257, 38, 0, 0, 1, 19865), 
(44237, 81924, 39, 0, 0, 1, 19865), 
(44237, 81923, 40, 0, 0, 1, 19865), 
-- 1348 (Gregory Ardus)
(1348, 2492, 1, 0, 0, 1, 19865), 
(1348, 2493, 2, 0, 0, 1, 19865), 
(1348, 852, 3, 0, 0, 1, 19865), 
(1348, 1197, 4, 0, 0, 1, 19865), 
(1348, 854, 5, 0, 0, 1, 19865), 
(1348, 2030, 6, 0, 0, 1, 19865), 
(1348, 2028, 7, 0, 0, 1, 19865), 
(1348, 2026, 8, 0, 0, 1, 19865), 
(1348, 925, 9, 0, 0, 1, 19865), 
(1348, 928, 10, 0, 0, 1, 19865), 
(1348, 924, 11, 0, 0, 1, 19865), 
-- 1351 (Brother Cassius)
(1351, 64670, 1, 0, 0, 1, 19865), 
(1351, 63388, 2, 0, 0, 1, 19865), 
(1351, 79249, 3, 0, 0, 1, 19865), 
-- 44245 (Faldren Tillsdale)
(44245, 77091, 1, 0, 0, 1, 19865), 
(44245, 77090, 2, 0, 0, 1, 19865), 
(44245, 77092, 3, 0, 0, 1, 19865), 
(44245, 77089, 4, 0, 0, 1, 19865), 
(44245, 77088, 5, 0, 0, 1, 19865), 
(44245, 77111, 6, 0, 0, 1, 19865), 
(44245, 77112, 7, 0, 0, 1, 19865), 
(44245, 77110, 8, 0, 0, 1, 19865), 
(44245, 77109, 9, 0, 0, 1, 19865), 
(44245, 77108, 10, 0, 0, 1, 19865), 
(44245, 77115, 11, 0, 0, 1, 19865), 
(44245, 77116, 12, 0, 0, 1, 19865), 
(44245, 77113, 13, 0, 0, 1, 19865), 
(44245, 77114, 14, 0, 0, 1, 19865), 
(44245, 77117, 15, 0, 0, 1, 19865), 
(44245, 77095, 16, 0, 0, 1, 19865), 
(44245, 77099, 17, 0, 0, 1, 19865), 
(44245, 77097, 18, 0, 0, 1, 19865), 
(44245, 77096, 19, 0, 0, 1, 19865), 
(44245, 77098, 20, 0, 0, 1, 19865), 
(44245, 77146, 21, 0, 0, 1, 19865), 
(44245, 77147, 22, 0, 0, 1, 19865), 
(44245, 77187, 23, 0, 0, 1, 19865), 
(44245, 77179, 24, 0, 0, 1, 19865), 
(44245, 77177, 25, 0, 0, 1, 19865), 
(44245, 77176, 26, 0, 0, 1, 19865), 
(44245, 77323, 27, 0, 0, 1, 19865), 
(44245, 77324, 28, 0, 0, 1, 19865), 
(44245, 77157, 29, 0, 0, 1, 19865), 
(44245, 77159, 30, 0, 0, 1, 19865), 
(44245, 77121, 31, 0, 0, 1, 19865), 
(44245, 77122, 32, 0, 0, 1, 19865), 
(44245, 77148, 33, 0, 0, 1, 19865), 
(44245, 77149, 34, 0, 0, 1, 19865), 
(44245, 77126, 35, 0, 0, 1, 19865), 
(44245, 77180, 36, 0, 0, 1, 19865), 
(44245, 77181, 37, 0, 0, 1, 19865), 
(44245, 77173, 38, 0, 0, 1, 19865), 
(44245, 77172, 39, 0, 0, 1, 19865), 
(44245, 77322, 40, 0, 0, 1, 19865), 
(44245, 77320, 41, 0, 0, 1, 19865), 
(44245, 77161, 42, 0, 0, 1, 19865), 
(44245, 77160, 43, 0, 0, 1, 19865), 
(44245, 77127, 44, 0, 0, 1, 19865), 
(44245, 77150, 45, 0, 0, 1, 19865), 
(44245, 77151, 46, 0, 0, 1, 19865), 
(44245, 77124, 47, 0, 0, 1, 19865), 
(44245, 77125, 48, 0, 0, 1, 19865), 
(44245, 77182, 49, 0, 0, 1, 19865), 
(44245, 77183, 50, 0, 0, 1, 19865), 
(44245, 77175, 51, 0, 0, 1, 19865), 
(44245, 77174, 52, 0, 0, 1, 19865), 
(44245, 77321, 53, 0, 0, 1, 19865), 
(44245, 77319, 54, 0, 0, 1, 19865), 
(44245, 77162, 55, 0, 0, 1, 19865), 
(44245, 77163, 56, 0, 0, 1, 19865), 
(44245, 77156, 57, 0, 0, 1, 19865), 
(44245, 77155, 58, 0, 0, 1, 19865), 
(44245, 77153, 59, 0, 0, 1, 19865), 
(44245, 77120, 60, 0, 0, 1, 19865), 
(44245, 77119, 61, 0, 0, 1, 19865), 
(44245, 77123, 62, 0, 0, 1, 19865), 
(44245, 77186, 63, 0, 0, 1, 19865), 
(44245, 77185, 64, 0, 0, 1, 19865), 
(44245, 77184, 65, 0, 0, 1, 19865), 
(44245, 77171, 66, 0, 0, 1, 19865), 
(44245, 77170, 67, 0, 0, 1, 19865), 
(44245, 77169, 68, 0, 0, 1, 19865), 
(44245, 77318, 69, 0, 0, 1, 19865), 
(44245, 77317, 70, 0, 0, 1, 19865), 
(44245, 77316, 71, 0, 0, 1, 19865), 
(44245, 77166, 72, 0, 0, 1, 19865), 
(44245, 77165, 73, 0, 0, 1, 19865), 
(44245, 77164, 74, 0, 0, 1, 19865), 
(44245, 77078, 75, 0, 0, 1, 19865), 
(44245, 77079, 76, 0, 0, 1, 19865), 
(44245, 78778, 127, 0, 3834, 1, 19865), 
(44245, 78795, 128, 0, 3831, 1, 19865), 
(44245, 78814, 129, 0, 3833, 1, 19865), 
(44245, 78823, 130, 0, 3832, 1, 19865), 
(44245, 78842, 131, 0, 3835, 1, 19865), 
(44245, 78777, 132, 0, 3834, 1, 19865), 
(44245, 78798, 133, 0, 3831, 1, 19865), 
(44245, 78817, 134, 0, 3833, 1, 19865), 
(44245, 78826, 135, 0, 3832, 1, 19865), 
(44245, 78845, 136, 0, 3835, 1, 19865), 
(44245, 76357, 222, 0, 3844, 1, 19865), 
(44245, 76358, 223, 0, 3843, 1, 19865), 
(44245, 76359, 224, 0, 3842, 1, 19865), 
(44245, 76360, 225, 0, 3845, 1, 19865), 
(44245, 76361, 226, 0, 3841, 1, 19865), 
(44245, 76348, 227, 0, 3844, 1, 19865), 
(44245, 76347, 228, 0, 3843, 1, 19865), 
(44245, 76346, 229, 0, 3842, 1, 19865), 
(44245, 76345, 230, 0, 3845, 1, 19865), 
(44245, 76344, 231, 0, 3841, 1, 19865), 
(44245, 78683, 317, 0, 3864, 1, 19865), 
(44245, 78700, 318, 0, 3863, 1, 19865), 
(44245, 78719, 319, 0, 3865, 1, 19865), 
(44245, 78728, 320, 0, 3862, 1, 19865), 
(44245, 78747, 321, 0, 3866, 1, 19865), 
(44245, 78682, 322, 0, 3864, 1, 19865), 
(44245, 78703, 323, 0, 3863, 1, 19865), 
(44245, 78722, 324, 0, 3865, 1, 19865), 
(44245, 78731, 325, 0, 3862, 1, 19865), 
(44245, 78750, 326, 0, 3866, 1, 19865), 
-- 44246 (Magatha Silverton)
(44246, 71210, 1, 0, 0, 1, 19865), 
(44246, 70940, 2, 0, 0, 1, 19865), 
(44246, 71208, 3, 0, 0, 1, 19865), 
(44246, 71211, 4, 0, 0, 1, 19865), 
(44246, 71209, 5, 0, 0, 1, 19865), 
(44246, 71213, 6, 0, 0, 1, 19865), 
(44246, 71214, 7, 0, 0, 1, 19865), 
(44246, 71129, 8, 0, 0, 1, 19865), 
(44246, 71212, 9, 0, 0, 1, 19865), 
(44246, 70935, 10, 0, 0, 1, 19865), 
(44246, 71264, 11, 0, 0, 1, 19865), 
(44246, 71260, 12, 0, 0, 1, 19865), 
(44246, 71263, 13, 0, 0, 1, 19865), 
(44246, 70937, 14, 0, 0, 1, 19865), 
(44246, 71265, 15, 0, 0, 1, 19865), 
(44246, 71266, 16, 0, 0, 1, 19865), 
(44246, 71130, 17, 0, 0, 1, 19865), 
(44246, 71261, 18, 0, 0, 1, 19865), 
(44246, 71262, 19, 0, 0, 1, 19865), 
(44246, 71150, 20, 0, 0, 1, 19865), 
(44246, 71151, 21, 0, 0, 1, 19865), 
(44246, 71271, 72, 0, 0, 1, 19865), 
(44246, 71272, 73, 0, 0, 1, 19865), 
(44246, 71273, 74, 0, 0, 1, 19865), 
(44246, 71274, 75, 0, 0, 1, 19865), 
(44246, 71275, 76, 0, 0, 1, 19865), 
(44246, 71276, 77, 0, 0, 1, 19865), 
(44246, 71277, 78, 0, 0, 1, 19865), 
(44246, 71278, 79, 0, 0, 1, 19865), 
(44246, 71279, 80, 0, 0, 1, 19865), 
(44246, 71280, 81, 0, 0, 1, 19865), 
(44246, 71527, 167, 0, 3605, 1, 19865), 
(44246, 71528, 168, 0, 3561, 1, 19865), 
(44246, 71529, 169, 0, 3586, 1, 19865), 
(44246, 71530, 170, 0, 3617, 1, 19865), 
(44246, 71531, 171, 0, 3558, 1, 19865), 
(44246, 71532, 172, 0, 3608, 1, 19865), 
(44246, 71533, 173, 0, 3561, 1, 19865), 
(44246, 71534, 174, 0, 3587, 1, 19865), 
(44246, 71535, 175, 0, 3618, 1, 19865), 
(44246, 71536, 176, 0, 3558, 1, 19865), 
-- 277 (Roberto Pupellyverbos)
(277, 2723, 1, 0, 0, 1, 19865), 
(277, 2593, 2, 0, 0, 1, 19865), 
(277, 2596, 3, 0, 0, 1, 19865), 
(277, 2594, 4, 0, 0, 1, 19865), 
(277, 2595, 5, 0, 0, 1, 19865), 
(277, 83095, 6, 0, 0, 1, 19865), 
(277, 82343, 7, 0, 0, 1, 19865), 
(277, 83094, 8, 0, 0, 1, 19865), 
(277, 82344, 9, 0, 0, 1, 19865), 
(277, 1941, 10, 0, 0, 1, 19865), 
-- 69974 (Knight-Lieutenant T'Maire Sydes)
(69974, 68774, 1, 0, 1005, 1, 19865), 
(69974, 68772, 2, 0, 1005, 1, 19865), 
(69974, 68773, 3, 0, 1005, 1, 19865), 
(69974, 72327, 8, 0, 4307, 1, 19865), 
(69974, 72325, 9, 0, 4307, 1, 19865), 
(69974, 72326, 10, 0, 4307, 1, 19865), 
(69974, 72331, 15, 0, 4307, 1, 19865), 
(69974, 72330, 16, 0, 4307, 1, 19865), 
(69974, 72329, 17, 0, 4307, 1, 19865), 
(69974, 72361, 18, 0, 4306, 1, 19865), 
(69974, 72414, 19, 0, 4306, 1, 19865), 
(69974, 72359, 21, 0, 4306, 1, 19865), 
(69974, 72411, 22, 0, 4306, 1, 19865), 
(69974, 72360, 24, 0, 4306, 1, 19865), 
(69974, 72412, 25, 0, 4306, 1, 19865), 
(69974, 72304, 27, 0, 4306, 1, 19865), 
(69974, 72309, 28, 0, 4306, 1, 19865), 
(69974, 72448, 29, 0, 4306, 1, 19865), 
(69974, 72449, 30, 0, 4306, 1, 19865), 
(69974, 72450, 31, 0, 4306, 1, 19865), 
(69974, 72455, 32, 0, 4306, 1, 19865), 
(69974, 72324, 37, 0, 4307, 1, 19865), 
(69974, 72323, 38, 0, 4307, 1, 19865), 
(69974, 72322, 39, 0, 4307, 1, 19865), 
(69974, 72406, 40, 0, 4308, 1, 19865), 
(69974, 72401, 42, 0, 4308, 1, 19865), 
(69974, 72404, 44, 0, 4306, 1, 19865), 
(69974, 72409, 45, 0, 4306, 1, 19865), 
(69974, 72315, 48, 0, 4306, 1, 19865), 
(69974, 72314, 49, 0, 4306, 1, 19865), 
(69974, 72313, 50, 0, 4306, 1, 19865), 
(69974, 72402, 51, 0, 4308, 1, 19865), 
(69974, 72407, 54, 0, 4308, 1, 19865), 
(69974, 72318, 55, 0, 4306, 1, 19865), 
(69974, 72316, 56, 0, 4306, 1, 19865), 
(69974, 72317, 57, 0, 4306, 1, 19865), 
(69974, 72321, 58, 0, 4307, 1, 19865), 
(69974, 72319, 59, 0, 4307, 1, 19865), 
(69974, 72320, 60, 0, 4307, 1, 19865), 
(69974, 72405, 63, 0, 4306, 1, 19865), 
(69974, 72400, 64, 0, 4306, 1, 19865), 
(69974, 72403, 65, 0, 4308, 1, 19865), 
(69974, 72408, 67, 0, 4308, 1, 19865), 
(69974, 70228, 165, 0, 4309, 1, 19865), 
(69974, 70231, 166, 0, 4309, 1, 19865), 
(69974, 70226, 167, 0, 4309, 1, 19865), 
(69974, 70213, 168, 0, 4309, 1, 19865), 
(69974, 70227, 169, 0, 4309, 1, 19865), 
(69974, 70224, 170, 0, 4309, 1, 19865), 
(69974, 70225, 171, 0, 4309, 1, 19865), 
(69974, 70239, 172, 0, 4311, 1, 19865), 
(69974, 70240, 173, 0, 4311, 1, 19865), 
(69974, 70220, 174, 0, 4310, 1, 19865), 
(69974, 70217, 175, 0, 4310, 1, 19865), 
(69974, 70223, 176, 0, 4310, 1, 19865), 
(69974, 70216, 177, 0, 4310, 1, 19865), 
(69974, 70219, 178, 0, 4310, 1, 19865), 
(69974, 70215, 179, 0, 4311, 1, 19865), 
(69974, 70218, 180, 0, 4310, 1, 19865), 
(69974, 70236, 181, 0, 4309, 1, 19865), 
(69974, 70234, 182, 0, 4311, 1, 19865), 
(69974, 70237, 183, 0, 4309, 1, 19865), 
(69974, 70238, 184, 0, 4309, 1, 19865), 
(69974, 70235, 185, 0, 4311, 1, 19865), 
(69974, 70241, 186, 0, 4311, 1, 19865), 
(69974, 70242, 187, 0, 4311, 1, 19865), 
(69974, 70243, 188, 0, 4311, 1, 19865), 
(69974, 70222, 189, 0, 4310, 1, 19865), 
(69974, 70214, 190, 0, 4310, 1, 19865), 
(69974, 70229, 191, 0, 4310, 1, 19865), 
(69974, 70212, 192, 0, 4310, 1, 19865), 
(69974, 70221, 193, 0, 4310, 1, 19865), 
(69974, 70230, 194, 0, 4310, 1, 19865), 
(69974, 70211, 195, 0, 4310, 1, 19865), 
-- 69975 (Captain Dirgehammer)
(69975, 73462, 1, 0, 4309, 1, 19865), 
(69975, 73456, 2, 0, 4309, 1, 19865), 
(69975, 73457, 3, 0, 4309, 1, 19865), 
(69975, 73475, 4, 0, 4309, 1, 19865), 
(69975, 73466, 5, 0, 4309, 1, 19865), 
(69975, 73476, 6, 0, 4309, 1, 19865), 
(69975, 73477, 7, 0, 4309, 1, 19865), 
(69975, 73465, 8, 0, 4311, 1, 19865), 
(69975, 73469, 9, 0, 4311, 1, 19865), 
(69975, 73467, 10, 0, 4310, 1, 19865), 
(69975, 73452, 11, 0, 4310, 1, 19865), 
(69975, 73454, 12, 0, 4310, 1, 19865), 
(69975, 73459, 13, 0, 4310, 1, 19865), 
(69975, 73461, 14, 0, 4311, 1, 19865), 
(69975, 73451, 15, 0, 4310, 1, 19865), 
(69975, 73453, 16, 0, 4311, 1, 19865), 
(69975, 73470, 17, 0, 4309, 1, 19865), 
(69975, 73464, 18, 0, 4311, 1, 19865), 
(69975, 73460, 19, 0, 4309, 1, 19865), 
(69975, 73463, 20, 0, 4309, 1, 19865), 
(69975, 73450, 21, 0, 4311, 1, 19865), 
(69975, 73446, 22, 0, 4311, 1, 19865), 
(69975, 73458, 23, 0, 4311, 1, 19865), 
(69975, 73468, 24, 0, 4311, 1, 19865), 
(69975, 73455, 25, 0, 4310, 1, 19865), 
(69975, 73447, 26, 0, 4310, 1, 19865), 
(69975, 73473, 27, 0, 4310, 1, 19865), 
(69975, 73472, 28, 0, 4310, 1, 19865), 
(69975, 73449, 29, 0, 4310, 1, 19865), 
(69975, 73448, 30, 0, 4310, 1, 19865), 
(69975, 73474, 31, 0, 4310, 1, 19865), 
(69975, 73625, 36, 0, 4307, 1, 19865), 
(69975, 73627, 37, 0, 4307, 1, 19865), 
(69975, 73626, 38, 0, 4307, 1, 19865), 
(69975, 73621, 43, 0, 4307, 1, 19865), 
(69975, 73622, 44, 0, 4307, 1, 19865), 
(69975, 73623, 45, 0, 4307, 1, 19865), 
(69975, 73591, 46, 0, 4306, 1, 19865), 
(69975, 73535, 47, 0, 4306, 1, 19865), 
(69975, 73593, 49, 0, 4306, 1, 19865), 
(69975, 73539, 50, 0, 4306, 1, 19865), 
(69975, 73592, 52, 0, 4306, 1, 19865), 
(69975, 73536, 53, 0, 4306, 1, 19865), 
(69975, 73648, 55, 0, 4306, 1, 19865), 
(69975, 73643, 56, 0, 4306, 1, 19865), 
(69975, 73498, 57, 0, 4306, 1, 19865), 
(69975, 73497, 58, 0, 4306, 1, 19865), 
(69975, 73496, 59, 0, 4306, 1, 19865), 
(69975, 73491, 60, 0, 4306, 1, 19865), 
(69975, 73628, 65, 0, 4307, 1, 19865), 
(69975, 73629, 66, 0, 4307, 1, 19865), 
(69975, 73630, 67, 0, 4307, 1, 19865), 
(69975, 73543, 68, 0, 4308, 1, 19865), 
(69975, 73548, 70, 0, 4308, 1, 19865), 
(69975, 73545, 72, 0, 4306, 1, 19865), 
(69975, 73540, 73, 0, 4306, 1, 19865), 
(69975, 73637, 76, 0, 4306, 1, 19865), 
(69975, 73638, 77, 0, 4306, 1, 19865), 
(69975, 73639, 78, 0, 4306, 1, 19865), 
(69975, 73547, 79, 0, 4308, 1, 19865), 
(69975, 73542, 82, 0, 4308, 1, 19865), 
(69975, 73634, 83, 0, 4306, 1, 19865), 
(69975, 73636, 84, 0, 4306, 1, 19865), 
(69975, 73635, 85, 0, 4306, 1, 19865), 
(69975, 73631, 86, 0, 4307, 1, 19865), 
(69975, 73633, 87, 0, 4307, 1, 19865), 
(69975, 73632, 88, 0, 4307, 1, 19865), 
(69975, 73544, 91, 0, 4306, 1, 19865), 
(69975, 73549, 92, 0, 4306, 1, 19865), 
(69975, 73546, 93, 0, 4308, 1, 19865), 
(69975, 73541, 95, 0, 4308, 1, 19865), 
(69975, 77140, 194, 0, 3872, 1, 19865), 
(69975, 77130, 195, 0, 3872, 1, 19865), 
(69975, 77131, 196, 0, 3872, 1, 19865), 
(69975, 77154, 197, 0, 3872, 1, 19865), 
(69975, 77137, 198, 0, 3872, 1, 19865), 
(69975, 77139, 199, 0, 3872, 1, 19865), 
(69975, 77142, 200, 0, 3872, 1, 19865), 
(69975, 77143, 201, 0, 3872, 1, 19865), 
(69975, 77132, 202, 0, 3872, 1, 19865), 
(69975, 77136, 203, 0, 3872, 1, 19865), 
(69975, 77138, 204, 0, 3872, 1, 19865), 
(69975, 77141, 205, 0, 3872, 1, 19865), 
(69975, 77144, 206, 0, 3872, 1, 19865), 
(69975, 77133, 207, 0, 3872, 1, 19865), 
(69975, 77134, 208, 0, 3872, 1, 19865), 
-- 5494 (Catherine Leland)
(5494, 6325, 5, 0, 0, 1, 19865), 
(5494, 6368, 6, 0, 0, 1, 19865), 
(5494, 6330, 7, 0, 0, 1, 19865), 
(5494, 6256, 304, 0, 0, 1, 19865), 
(5494, 6529, 305, 0, 0, 1, 19865), 
(5494, 6530, 306, 0, 0, 1, 19865), 
(5494, 6532, 307, 0, 0, 1, 19865), 
(5494, 6533, 308, 2, 0, 1, 19865), 
-- 483 (Elaine Trias)
(483, 2070, 1, 0, 0, 1, 19865), 
(483, 414, 2, 0, 0, 1, 19865), 
(483, 422, 3, 0, 0, 1, 19865), 
(483, 1707, 4, 0, 0, 1, 19865), 
(483, 3927, 5, 0, 0, 1, 19865), 
(483, 8932, 6, 0, 0, 1, 19865), 
(483, 27857, 7, 0, 0, 1, 19865), 
(483, 33443, 8, 0, 0, 1, 19865), 
(483, 35952, 9, 0, 0, 1, 19865), 
(483, 58258, 10, 0, 0, 1, 19865), 
(483, 58259, 11, 0, 0, 1, 19865), 
(483, 81922, 12, 0, 0, 1, 19865), 
(483, 81921, 13, 0, 0, 1, 19865), 
-- 5509 (Kathrum Axehand)
(5509, 2522, 1, 0, 0, 1, 19865), 
(5509, 2530, 2, 0, 0, 1, 19865), 
(5509, 2523, 3, 0, 0, 1, 19865), 
(5509, 2531, 4, 0, 0, 1, 19865), 
-- 28347 (Miles Sidney)
(28347, 4565, 2, 0, 0, 1, 19865), 
(28347, 38579, 63, 1, 0, 1, 19865), 
-- 5512 (Kaita Deepforge)
(5512, 2901, 1, 0, 0, 1, 19865), 
(5512, 5956, 2, 0, 0, 1, 19865), 
(5512, 2880, 3, 0, 0, 1, 19865), 
(5512, 3466, 4, 0, 0, 1, 19865), 
(5512, 18567, 5, 0, 0, 1, 19865), 
(5512, 3857, 6, 0, 0, 1, 19865), 
(5512, 66117, 7, 0, 3312, 1, 19865), 
(5512, 66118, 8, 0, 3312, 1, 19865), 
(5512, 66125, 9, 0, 3312, 1, 19865), 
(5512, 66126, 10, 0, 3312, 1, 19865), 
(5512, 66103, 11, 0, 3312, 1, 19865), 
(5512, 66119, 12, 0, 3312, 1, 19865), 
(5512, 66107, 13, 0, 3312, 1, 19865), 
(5512, 66105, 14, 0, 3312, 1, 19865), 
(5512, 66109, 15, 0, 3312, 1, 19865), 
(5512, 66127, 16, 0, 3312, 1, 19865), 
(5512, 66120, 17, 0, 3313, 1, 19865), 
(5512, 67603, 18, 0, 3313, 1, 19865), 
(5512, 67606, 19, 0, 3314, 1, 19865), 
(5512, 66113, 20, 0, 3313, 1, 19865), 
(5512, 66106, 21, 0, 3313, 1, 19865), 
(5512, 66111, 22, 0, 3313, 1, 19865), 
(5512, 66104, 23, 0, 3313, 1, 19865), 
(5512, 66108, 24, 0, 3313, 1, 19865), 
(5512, 66128, 25, 0, 3313, 1, 19865), 
(5512, 66121, 26, 0, 3313, 1, 19865), 
(5512, 66115, 27, 0, 3313, 1, 19865), 
(5512, 66112, 28, 0, 3313, 1, 19865), 
(5512, 66114, 29, 0, 3313, 1, 19865), 
(5512, 66110, 30, 0, 3313, 1, 19865), 
(5512, 66116, 31, 0, 3313, 1, 19865), 
(5512, 66129, 32, 0, 3313, 1, 19865), 
(5512, 66124, 33, 0, 3314, 1, 19865), 
(5512, 66123, 34, 0, 3314, 1, 19865), 
(5512, 66122, 35, 0, 3314, 1, 19865), 
(5512, 66100, 36, 0, 3314, 1, 19865), 
(5512, 66132, 37, 0, 3314, 1, 19865), 
(5512, 66131, 38, 0, 3314, 1, 19865), 
(5512, 66130, 39, 0, 3314, 1, 19865), 
(5512, 66101, 40, 0, 3314, 1, 19865), 
(5512, 12162, 174, 1, 0, 1, 19865), 
-- 5514 (Brooke Stonebraid)
(5514, 2901, 1, 0, 0, 1, 19865), 
(5514, 30746, 2, 0, 0, 1, 19865), 
(5514, 2880, 3, 0, 0, 1, 19865), 
(5514, 3466, 4, 0, 0, 1, 19865), 
(5514, 3857, 5, 0, 0, 1, 19865), 
(5514, 20815, 6, 0, 0, 1, 19865), 
-- 4981 (Ben Trias)
(4981, 2070, 1, 0, 0, 1, 19865), 
(4981, 414, 2, 0, 0, 1, 19865), 
(4981, 422, 3, 0, 0, 1, 19865), 
(4981, 1707, 4, 0, 0, 1, 19865), 
(4981, 3927, 5, 0, 0, 1, 19865), 
(4981, 8932, 6, 0, 0, 1, 19865), 
(4981, 27857, 7, 0, 0, 1, 19865), 
(4981, 33443, 8, 0, 0, 1, 19865), 
(4981, 35952, 9, 0, 0, 1, 19865), 
(4981, 58258, 10, 0, 0, 1, 19865), 
(4981, 58259, 11, 0, 0, 1, 19865), 
(4981, 81922, 12, 0, 0, 1, 19865), 
(4981, 81921, 13, 0, 0, 1, 19865), 
(4981, 159, 14, 0, 0, 1, 19865), 
(4981, 1179, 15, 0, 0, 1, 19865), 
(4981, 1205, 16, 0, 0, 1, 19865), 
(4981, 1708, 17, 0, 0, 1, 19865), 
(4981, 1645, 18, 0, 0, 1, 19865), 
(4981, 8766, 19, 0, 0, 1, 19865), 
(4981, 28399, 20, 0, 0, 1, 19865), 
(4981, 58274, 21, 0, 0, 1, 19865), 
(4981, 33444, 22, 0, 0, 1, 19865), 
(4981, 33445, 23, 0, 0, 1, 19865), 
(4981, 58256, 24, 0, 0, 1, 19865), 
(4981, 58257, 25, 0, 0, 1, 19865), 
(4981, 81924, 26, 0, 0, 1, 19865), 
(4981, 81923, 27, 0, 0, 1, 19865), 
-- 12784 (Lieutenant Jackspring)
(12784, 18876, 1, 0, 2257, 1, 19865), 
(12784, 18830, 2, 0, 2257, 1, 19865), 
(12784, 18867, 3, 0, 2257, 1, 19865), 
(12784, 23455, 4, 0, 2257, 1, 19865), 
(12784, 18869, 5, 0, 2257, 1, 19865), 
(12784, 18827, 6, 0, 2291, 1, 19865), 
(12784, 12584, 7, 0, 2291, 1, 19865), 
(12784, 23456, 8, 0, 2291, 1, 19865), 
(12784, 18865, 9, 0, 2291, 1, 19865), 
(12784, 23454, 10, 0, 2291, 1, 19865), 
(12784, 18843, 11, 0, 2291, 1, 19865), 
(12784, 18847, 12, 0, 2291, 1, 19865), 
(12784, 18838, 13, 0, 2291, 1, 19865), 
(12784, 18825, 14, 0, 2291, 1, 19865), 
(12784, 18833, 15, 0, 2291, 1, 19865), 
(12784, 18836, 16, 0, 2291, 1, 19865), 
(12784, 18855, 17, 0, 2291, 1, 19865), 
(12784, 18873, 18, 0, 2257, 1, 19865), 
(12784, 23451, 19, 0, 2291, 1, 19865), 
(12784, 23452, 20, 0, 2291, 1, 19865), 
(12784, 23453, 21, 0, 2291, 1, 19865), 
(12784, 35015, 22, 0, 1664, 1, 19865), 
(12784, 34989, 23, 0, 1664, 1, 19865), 
(12784, 34997, 24, 0, 1664, 1, 19865), 
(12784, 35064, 25, 0, 1664, 1, 19865), 
(12784, 35101, 26, 0, 2287, 1, 19865), 
(12784, 35072, 27, 0, 2283, 1, 19865), 
(12784, 35076, 28, 0, 2287, 1, 19865), 
(12784, 35038, 29, 0, 2287, 1, 19865), 
(12784, 35037, 30, 0, 2287, 1, 19865), 
(12784, 34995, 31, 0, 2283, 1, 19865), 
(12784, 34996, 32, 0, 2287, 1, 19865), 
(12784, 35017, 33, 0, 2283, 1, 19865), 
(12784, 35071, 34, 0, 2287, 1, 19865), 
(12784, 34988, 35, 0, 2283, 1, 19865), 
(12784, 35093, 36, 0, 2287, 1, 19865), 
(12784, 35095, 37, 0, 2283, 1, 19865), 
(12784, 35058, 38, 0, 2283, 1, 19865), 
(12784, 35018, 39, 0, 1664, 1, 19865), 
(12784, 35075, 40, 0, 1664, 1, 19865), 
(12784, 35047, 41, 0, 1664, 1, 19865), 
(12784, 35014, 42, 0, 2284, 1, 19865), 
(12784, 35082, 43, 0, 2284, 1, 19865), 
(12784, 35102, 44, 0, 2284, 1, 19865), 
(12784, 35109, 45, 0, 1664, 1, 19865), 
(12784, 34987, 46, 0, 1664, 1, 19865), 
(12784, 35103, 47, 0, 1664, 1, 19865), 
(12784, 35107, 48, 0, 1758, 1, 19865), 
(12784, 35065, 49, 0, 1758, 1, 19865), 
(12784, 34985, 50, 0, 1758, 1, 19865), 
(12784, 35008, 51, 0, 2283, 1, 19865), 
(12784, 35016, 52, 0, 2283, 1, 19865), 
(12784, 35074, 53, 0, 2283, 1, 19865), 
(12784, 34986, 56, 0, 2285, 1, 19865), 
(12784, 35073, 57, 0, 2285, 1, 19865), 
(12784, 35094, 58, 0, 2285, 1, 19865), 
(12784, 51388, 59, 0, 2936, 1, 19865), 
(12784, 51390, 60, 0, 2936, 1, 19865), 
(12784, 51392, 61, 0, 2936, 1, 19865), 
(12784, 51394, 62, 0, 2936, 1, 19865), 
(12784, 51397, 63, 0, 2943, 1, 19865), 
(12784, 51400, 64, 0, 2936, 1, 19865), 
(12784, 51402, 65, 0, 2936, 1, 19865), 
(12784, 51404, 66, 0, 2936, 1, 19865), 
(12784, 51406, 67, 0, 2943, 1, 19865), 
(12784, 51411, 68, 0, 2936, 1, 19865), 
(12784, 51431, 69, 0, 2936, 1, 19865), 
(12784, 51439, 70, 0, 2946, 1, 19865), 
(12784, 51441, 71, 0, 2946, 1, 19865), 
(12784, 51443, 72, 0, 2945, 1, 19865), 
(12784, 51445, 73, 0, 2946, 1, 19865), 
(12784, 51447, 74, 0, 2946, 1, 19865), 
(12784, 51449, 75, 0, 2936, 1, 19865), 
(12784, 51453, 76, 0, 2943, 1, 19865), 
(12784, 51456, 77, 0, 2936, 1, 19865), 
(12784, 51480, 78, 0, 2936, 1, 19865), 
(12784, 51515, 79, 0, 2945, 1, 19865), 
(12784, 51517, 80, 0, 2945, 1, 19865), 
(12784, 51519, 81, 0, 2945, 1, 19865), 
(12784, 51521, 82, 0, 2945, 1, 19865), 
(12784, 51523, 83, 0, 2945, 1, 19865), 
(12784, 51525, 84, 0, 2946, 1, 19865), 
(12784, 51527, 85, 0, 2946, 1, 19865), 
(12784, 51530, 86, 0, 2945, 1, 19865), 
(12784, 51410, 87, 0, 2951, 1, 19865), 
(12784, 51451, 88, 0, 2951, 1, 19865), 
(12784, 51531, 89, 0, 2951, 1, 19865), 
(12784, 51532, 90, 0, 2951, 1, 19865), 
(12784, 51452, 91, 0, 2950, 1, 19865), 
(12784, 51455, 92, 0, 2950, 1, 19865), 
(12784, 51533, 93, 0, 2950, 1, 19865), 
(12784, 51396, 94, 0, 2950, 1, 19865), 
(12784, 51407, 95, 0, 2950, 1, 19865), 
(12784, 51408, 96, 0, 2950, 1, 19865), 
(12784, 51409, 97, 0, 2950, 1, 19865), 
-- 12785 (Sergeant Major Clate)
(12785, 23288, 3, 0, 428, 1, 19865), 
(12785, 23289, 4, 0, 2549, 1, 19865), 
(12785, 23302, 9, 0, 653, 1, 19865), 
(12785, 23303, 10, 0, 652, 1, 19865), 
(12785, 23316, 15, 0, 444, 1, 19865), 
(12785, 23317, 16, 0, 2549, 1, 19865), 
(12785, 17607, 25, 0, 465, 1, 19865), 
(12785, 17608, 26, 0, 541, 1, 19865), 
(12785, 17605, 27, 0, 463, 1, 19865), 
(12785, 17603, 28, 0, 542, 1, 19865), 
(12785, 17602, 29, 0, 464, 1, 19865), 
(12785, 17604, 30, 0, 465, 1, 19865), 
(12785, 18440, 109, 0, 1050, 1, 19865), 
(12785, 18441, 110, 0, 986, 1, 19865), 
(12785, 16342, 111, 0, 774, 1, 19865), 
(12785, 18442, 112, 0, 838, 1, 19865), 
(12785, 18444, 113, 0, 930, 1, 19865), 
(12785, 18443, 114, 0, 491, 1, 19865), 
(12785, 18457, 123, 0, 931, 1, 19865), 
(12785, 18456, 124, 0, 492, 1, 19865), 
(12785, 18862, 128, 0, 634, 1, 19865), 
(12785, 35083, 158, 0, 2342, 1, 19865), 
(12785, 35053, 159, 0, 2342, 1, 19865), 
(12785, 35054, 177, 0, 2285, 1, 19865), 
(12785, 35084, 178, 0, 2285, 1, 19865), 
(12785, 35008, 180, 0, 2283, 1, 19865), 
(12785, 35074, 181, 0, 2283, 1, 19865), 
(12785, 35016, 182, 0, 2283, 1, 19865), 
(12785, 35055, 198, 0, 2285, 1, 19865), 
(12785, 35085, 199, 0, 2285, 1, 19865), 
(12785, 35057, 206, 0, 2285, 1, 19865), 
(12785, 35087, 207, 0, 2285, 1, 19865), 
(12785, 34986, 209, 0, 2285, 1, 19865), 
(12785, 35073, 210, 0, 2285, 1, 19865), 
(12785, 35094, 211, 0, 2285, 1, 19865), 
(12785, 35056, 227, 0, 2288, 1, 19865), 
(12785, 35086, 228, 0, 2288, 1, 19865), 
(12785, 51483, 236, 0, 2954, 1, 19865), 
(12785, 51488, 237, 0, 2954, 1, 19865), 
(12785, 51484, 252, 0, 2949, 1, 19865), 
(12785, 51489, 253, 0, 2949, 1, 19865), 
(12785, 51407, 268, 0, 2950, 1, 19865), 
(12785, 51408, 269, 0, 2950, 1, 19865), 
(12785, 51409, 270, 0, 2950, 1, 19865), 
(12785, 51396, 271, 0, 2950, 1, 19865), 
(12785, 51485, 272, 0, 2953, 1, 19865), 
(12785, 51490, 273, 0, 2953, 1, 19865), 
(12785, 51487, 288, 0, 2952, 1, 19865), 
(12785, 51482, 292, 0, 2952, 1, 19865), 
(12785, 51533, 298, 0, 2950, 1, 19865), 
(12785, 51455, 299, 0, 2950, 1, 19865), 
(12785, 51452, 300, 0, 2950, 1, 19865), 
(12785, 51486, 301, 0, 2948, 1, 19865), 
(12785, 51491, 316, 0, 2948, 1, 19865), 
(12785, 51330, 317, 0, 2964, 1, 19865), 
(12785, 51332, 318, 0, 2964, 1, 19865), 
(12785, 51334, 319, 0, 2964, 1, 19865), 
(12785, 51346, 320, 0, 2964, 1, 19865), 
(12785, 51348, 321, 0, 2964, 1, 19865), 
(12785, 51354, 322, 0, 2964, 1, 19865), 
(12785, 51356, 323, 0, 2964, 1, 19865), 
(12785, 51328, 324, 0, 2961, 1, 19865), 
(12785, 51338, 325, 0, 2961, 1, 19865), 
(12785, 51366, 329, 0, 2961, 1, 19865), 
(12785, 51358, 335, 0, 2964, 1, 19865), 
(12785, 51336, 336, 0, 2964, 1, 19865), 
(12785, 51331, 337, 0, 2964, 1, 19865), 
(12785, 51333, 338, 0, 2964, 1, 19865), 
(12785, 51335, 339, 0, 2964, 1, 19865), 
(12785, 51347, 340, 0, 2964, 1, 19865), 
(12785, 51357, 341, 0, 2964, 1, 19865), 
(12785, 51349, 342, 0, 2964, 1, 19865), 
(12785, 51353, 343, 0, 2964, 1, 19865), 
(12785, 51355, 344, 0, 2964, 1, 19865), 
(12785, 51377, 345, 0, 2965, 1, 19865), 
(12785, 51327, 347, 0, 2962, 1, 19865), 
(12785, 51365, 353, 0, 2962, 1, 19865), 
(12785, 51337, 357, 0, 2962, 1, 19865), 
(12785, 51329, 358, 0, 2963, 1, 19865), 
(12785, 51339, 360, 0, 2963, 1, 19865), 
(12785, 51367, 365, 0, 2963, 1, 19865), 
(12785, 35149, 381, 0, 1911, 1, 19865), 
(12785, 35164, 382, 0, 1923, 1, 19865), 
(12785, 35179, 383, 0, 1935, 1, 19865), 
(12785, 35144, 384, 0, 1911, 1, 19865), 
(12785, 35159, 385, 0, 1923, 1, 19865), 
(12785, 35174, 386, 0, 1935, 1, 19865), 
(12785, 35138, 387, 0, 1911, 1, 19865), 
(12785, 35153, 388, 0, 1923, 1, 19865), 
(12785, 35168, 389, 0, 1935, 1, 19865), 
(12785, 37929, 414, 0, 127, 1, 19865), 
(12785, 37928, 415, 0, 127, 1, 19865), 
(12785, 35135, 416, 0, 127, 1, 19865), 
(12785, 35134, 417, 0, 127, 1, 19865), 
(12785, 35133, 418, 0, 127, 1, 19865), 
(12785, 35132, 419, 0, 127, 1, 19865), 
(12785, 41592, 420, 0, 2428, 1, 19865), 
(12785, 41591, 421, 0, 2428, 1, 19865), 
(12785, 44431, 422, 0, 2428, 1, 19865), 
(12785, 44429, 423, 0, 2428, 1, 19865), 
(12785, 35129, 424, 0, 2028, 1, 19865), 
(12785, 35130, 425, 0, 2028, 1, 19865), 
(12785, 35131, 426, 0, 2028, 1, 19865), 
(12785, 37927, 427, 0, 2028, 1, 19865), 
(12785, 25829, 428, 0, 125, 1, 19865), 
(12785, 69861, 431, 0, 821, 1, 19865); 

UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `item` IN (6270, 6272, 18649, 4827, 4828, 4826, 39489, 38579, 6533);
UPDATE `npc_vendor` SET `incrtime`=7200 WHERE `item`=3356;
UPDATE `npc_vendor` SET `incrtime`=3600 WHERE `entry`=5512 AND `item`=12162;
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=1307 AND `item` IN (929, 3385);
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=1286 AND `item`=20856;
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=1298 AND `item` IN (11303, 11306);
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=1316 AND `item` IN (3013, 955, 1180, 954, 3012, 1711);
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=1257 AND `item`=858;
UPDATE `npc_vendor` SET `incrtime`=7200 WHERE `entry`=1313 AND `item` IN (785, 2453);
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=1313 AND `item`=3357;
UPDATE `npc_vendor` SET `incrtime`=2700 WHERE `entry`=1313 AND `item`=9301;
UPDATE `npc_vendor` SET `incrtime`=7200 WHERE `entry`=1318 AND `item` IN (10940, 10938);
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=1347 AND `item`=10325;

UPDATE `creature_template` SET `npcflag`=`npcflag`|128 WHERE entry IN (69974, 69975, 73190, 87501);
 
 
-- -------------------------------------------------------- 
-- 2015_04_26_02_world.sql 
-- -------------------------------------------------------- 
-- Vendors Westfall (19865)

UPDATE `npc_vendor` SET `VerifiedBuild` = 19865 WHERE `entry` IN (42497, 8931, 1670, 233, 491, 4305);

DELETE FROM `npc_vendor` WHERE `entry` IN (1668, 8934, 43948, 10045);
INSERT INTO `npc_vendor` (`entry`, `item`, `slot`, `maxcount`, `ExtendedCost`, `Type`, `VerifiedBuild`) VALUES
-- 1668 (William MacGregor)
(1668, 2507, 1, 0, 0, 1, 19865),
(1668, 3026, 2, 0, 0, 1, 19865),
(1668, 11304, 41, 1, 0, 1, 19865),
-- 8934 (Christopher Hewen)
(8934, 7005, 1, 0, 0, 1, 19865),
(8934, 2901, 2, 0, 0, 1, 19865),
(8934, 85663, 3, 0, 0, 1, 19865),
(8934, 5956, 4, 0, 0, 1, 19865),
(8934, 6256, 5, 0, 0, 1, 19865),
(8934, 6217, 6, 0, 0, 1, 19865),
(8934, 2678, 7, 0, 0, 1, 19865),
(8934, 30817, 8, 0, 0, 1, 19865),
(8934, 2320, 9, 0, 0, 1, 19865),
(8934, 2880, 10, 0, 0, 1, 19865),
(8934, 3371, 11, 0, 0, 1, 19865),
(8934, 4289, 12, 0, 0, 1, 19865),
(8934, 6529, 13, 0, 0, 1, 19865),
(8934, 2604, 14, 0, 0, 1, 19865),
(8934, 2324, 15, 0, 0, 1, 19865),
(8934, 6260, 16, 0, 0, 1, 19865),
(8934, 39354, 17, 0, 0, 1, 19865),
(8934, 20815, 18, 0, 0, 1, 19865),
(8934, 39505, 19, 0, 0, 1, 19865),
(8934, 2321, 20, 0, 0, 1, 19865),
(8934, 6530, 21, 0, 0, 1, 19865),
(8934, 2605, 22, 0, 0, 1, 19865),
(8934, 4291, 23, 0, 0, 1, 19865),
(8934, 3466, 24, 0, 0, 1, 19865),
(8934, 3857, 25, 0, 0, 1, 19865),
(8934, 4399, 26, 0, 0, 1, 19865),
(8934, 4400, 27, 0, 0, 1, 19865),
(8934, 6532, 28, 0, 0, 1, 19865),
(8934, 8343, 29, 0, 0, 1, 19865),
(8934, 14341, 30, 0, 0, 1, 19865),
(8934, 2325, 31, 0, 0, 1, 19865),
(8934, 4341, 32, 0, 0, 1, 19865),
(8934, 4342, 33, 0, 0, 1, 19865),
(8934, 4340, 34, 0, 0, 1, 19865),
-- 43948 (Private Jackson)
(43948, 851, 1, 0, 0, 1, 19865),
(43948, 1198, 2, 0, 0, 1, 19865),
(43948, 2207, 3, 0, 0, 1, 19865),
(43948, 2027, 4, 0, 0, 1, 19865),
(43948, 2208, 5, 0, 0, 1, 19865),
(43948, 2024, 6, 0, 0, 1, 19865),
(43948, 2209, 7, 0, 0, 1, 19865),
(43948, 922, 8, 0, 0, 1, 19865),
(43948, 923, 9, 0, 0, 1, 19865),
-- 10045 (Kirk Maxwell)
(10045, 37460, 1, 0, 0, 1, 19865);
 
 
-- -------------------------------------------------------- 
-- 2015_04_26_03_world.sql 
-- -------------------------------------------------------- 
-- Vendors Redridge Mountains (19865)

UPDATE `npc_vendor` SET `VerifiedBuild` = 19865 WHERE `entry` IN (3085, 3086, 3089, 956, 6727, 789, 791, 5620, 1671);

DELETE FROM `npc_vendor` WHERE `entry` IN (1678, 3088, 3090, 3091, 844, 777, 793, 9982);
INSERT INTO `npc_vendor` (`entry`, `item`, `slot`, `maxcount`, `ExtendedCost`, `Type`, `VerifiedBuild`) VALUES
-- 1678 (Vernon Hale)
(1678, 787, 5, 0, 0, 1, 19865),
(1678, 4592, 6, 0, 0, 1, 19865),
(1678, 4593, 7, 0, 0, 1, 19865),
(1678, 4594, 8, 0, 0, 1, 19865),
(1678, 21552, 9, 0, 0, 1, 19865),
(1678, 8957, 10, 0, 0, 1, 19865),
(1678, 6256, 61, 0, 0, 1, 19865),
(1678, 6529, 62, 0, 0, 1, 19865),
(1678, 6530, 63, 0, 0, 1, 19865),
(1678, 6532, 64, 0, 0, 1, 19865),
(1678, 6533, 65, 2, 0, 1, 19865),
-- 3088 (Henry Chapal)
(3088, 2511, 1, 0, 0, 1, 19865),
(3088, 3023, 2, 0, 0, 1, 19865),
-- 3090 (Gerald Crawley)
(3090, 4565, 1, 0, 0, 1, 19865),
-- 3091 (Franklin Hamar)
(3091, 2320, 1, 0, 0, 1, 19865),
(3091, 2321, 2, 0, 0, 1, 19865),
(3091, 4291, 3, 0, 0, 1, 19865),
(3091, 8343, 4, 0, 0, 1, 19865),
(3091, 14341, 5, 0, 0, 1, 19865),
(3091, 38426, 6, 0, 0, 1, 19865),
(3091, 2324, 7, 0, 0, 1, 19865),
(3091, 2604, 8, 0, 0, 1, 19865),
(3091, 6260, 9, 0, 0, 1, 19865),
(3091, 2605, 10, 0, 0, 1, 19865),
(3091, 4341, 11, 0, 0, 1, 19865),
(3091, 4340, 12, 0, 0, 1, 19865),
(3091, 6261, 13, 0, 0, 1, 19865),
(3091, 2325, 14, 0, 0, 1, 19865),
(3091, 4342, 15, 0, 0, 1, 19865),
(3091, 10290, 16, 0, 0, 1, 19865),
(3091, 4782, 171, 1, 0, 1, 19865),
(3091, 4786, 172, 1, 0, 1, 19865),
-- 844 (Antonio Perelli)
(844, 4794, 1, 1, 0, 1, 19865),
(844, 4795, 2, 1, 0, 1, 19865),
(844, 4796, 3, 1, 0, 1, 19865),
(844, 3371, 4, 0, 0, 1, 19865),
-- 777 (Amy Davenport)
(777, 7005, 1, 0, 0, 1, 19865),
(777, 2901, 2, 0, 0, 1, 19865),
(777, 85663, 3, 0, 0, 1, 19865),
(777, 5956, 4, 0, 0, 1, 19865),
(777, 6256, 5, 0, 0, 1, 19865),
(777, 6217, 6, 0, 0, 1, 19865),
(777, 2320, 7, 0, 0, 1, 19865),
(777, 2321, 8, 0, 0, 1, 19865),
(777, 2678, 9, 0, 0, 1, 19865),
(777, 2880, 10, 0, 0, 1, 19865),
(777, 3371, 11, 0, 0, 1, 19865),
(777, 4289, 12, 0, 0, 1, 19865),
(777, 6529, 13, 0, 0, 1, 19865),
(777, 6530, 14, 0, 0, 1, 19865),
(777, 2324, 15, 0, 0, 1, 19865),
(777, 39354, 16, 0, 0, 1, 19865),
(777, 20815, 17, 0, 0, 1, 19865),
(777, 39505, 18, 0, 0, 1, 19865),
(777, 20576, 96, 1, 0, 1, 19865),
(777, 5772, 97, 1, 0, 1, 19865),
-- 793 (Kara Adams)
(793, 17188, 1, 0, 0, 1, 19865),
(793, 2445, 2, 0, 0, 1, 19865),
(793, 4822, 91, 1, 0, 1, 19865),
(793, 4820, 92, 1, 0, 1, 19865),
-- 9982 (Penny)
(9982, 37460, 1, 0, 0, 1, 19865);

UPDATE `npc_vendor` SET `incrtime`=43200 WHERE `entry`=1678 AND `item`=6533;
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=3091 AND `item` IN (4782, 4786);
UPDATE `npc_vendor` SET `incrtime`=7200 WHERE `entry`=777 AND `item`=5772;
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=777 AND `item`=20576;
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=793 AND `item`=4822;
UPDATE `npc_vendor` SET `incrtime`=14400 WHERE `entry`=793 AND `item`=4820;
 
 
-- -------------------------------------------------------- 
-- 2015_04_26_04_world.sql 
-- -------------------------------------------------------- 
UPDATE `npc_vendor` SET `incrtime`=86400 WHERE `entry`=844 AND `item` IN (4794, 4795, 4796);
UPDATE `npc_vendor` SET `incrtime`=9000 WHERE `entry`=1668 AND `item`=11304;
 
 
-- -------------------------------------------------------- 
-- _MERGED.sql 
-- -------------------------------------------------------- 
 
 

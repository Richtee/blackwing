-- -------------------------------------------------------- 
-- 2015_03_29_00_characters.sql 
-- -------------------------------------------------------- 
ALTER TABLE `corpse` DROP `phaseMask`;

DROP TABLE IF EXISTS `corpse_phases`;
CREATE TABLE `corpse_phases` (
  `Guid` int(10) unsigned NOT NULL,
  `PhaseId` int(10) unsigned NOT NULL,
  `OwnerGuid` int(10) unsigned NOT NULL,
  `Time` int(10) unsigned NOT NULL DEFAULT '0',
  `CorpseType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Guid`,`PhaseId`)
); 
 
-- -------------------------------------------------------- 
-- 2015_04_21_00_characters.sql 
-- -------------------------------------------------------- 
DROP TABLE IF EXISTS `character_template`;
CREATE TABLE IF NOT EXISTS `character_template` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(70) NOT NULL,
  `description` varchar(100) NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `character_template_class`;
CREATE TABLE IF NOT EXISTS `character_template_class` (
  `templateId` int(10) unsigned NOT NULL,
  `factionGroup` tinyint(3) unsigned NOT NULL COMMENT '3 - Alliance, 5 - Horde',
  `class` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`templateId`,`factionGroup`,`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 
-- -------------------------------------------------------- 
-- 2015_04_28_00_characters.sql 
-- -------------------------------------------------------- 
DELETE FROM `corpse` WHERE `corpseType`=0;

DROP PROCEDURE IF EXISTS `CheckCorpseData`;

DELIMITER $$

CREATE PROCEDURE CheckCorpseData()
BEGIN
  DECLARE max_corpses_per_player int;
  SELECT MAX(`counts`) FROM (SELECT COUNT(`corpseGuid`) AS `counts` FROM `corpse` GROUP BY `guid`) AS `counted` INTO max_corpses_per_player;

  IF max_corpses_per_player > 50000 THEN
    TRUNCATE `corpse`;
  END IF;

END$$
DELIMITER ;

CALL CheckCorpseData();
DROP PROCEDURE `CheckCorpseData`;

ALTER TABLE `corpse`
  DROP `corpseGuid`,
  DROP INDEX `idx_player`,
  ADD PRIMARY KEY (`guid`);

ALTER TABLE `corpse_phases`
  DROP PRIMARY KEY,
  DROP `Guid`,
  DROP `CorpseType`,
  DROP `Time`,
  CHANGE `OwnerGuid` `OwnerGuid` bigint(20) unsigned NOT NULL DEFAULT '0' FIRST,
  ADD PRIMARY KEY (`OwnerGuid`,`PhaseId`);
 
 
-- -------------------------------------------------------- 
-- 2015_05_08_00_characters.sql 
-- -------------------------------------------------------- 
--
-- Table structure for table `character_garrison`
--

DROP TABLE IF EXISTS `character_garrison`;
CREATE TABLE `character_garrison` (
  `guid` bigint(20) unsigned NOT NULL,
  `siteLevelId` int(10) unsigned NOT NULL DEFAULT '0',
  `followerActivationsRemainingToday` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `character_garrison_blueprints`
--

DROP TABLE IF EXISTS `character_garrison_blueprints`;
CREATE TABLE `character_garrison_blueprints` (
  `guid` bigint(20) unsigned NOT NULL,
  `buildingId` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`buildingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `character_garrison_buildings`
--

DROP TABLE IF EXISTS `character_garrison_buildings`;
CREATE TABLE `character_garrison_buildings` (
  `guid` bigint(20) unsigned NOT NULL,
  `plotInstanceId` int(10) unsigned NOT NULL DEFAULT '0',
  `buildingId` int(10) unsigned NOT NULL DEFAULT '0',
  `timeBuilt` bigint(20) unsigned NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`plotInstanceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 
-- -------------------------------------------------------- 
-- 2015_05_22_00_characters.sql 
-- -------------------------------------------------------- 
DROP TABLE IF EXISTS `character_garrison_follower_abilities`;
DROP TABLE IF EXISTS `character_garrison_followers`;

--
-- Table structure for table `character_garrison_followers`
--

CREATE TABLE `character_garrison_followers` (
  `dbId` bigint(20) unsigned NOT NULL,
  `guid` bigint(20) unsigned NOT NULL,
  `followerId` int(10) unsigned NOT NULL,
  `quality` int(10) unsigned NOT NULL DEFAULT '2',
  `level` int(10) unsigned NOT NULL DEFAULT '90',
  `itemLevelWeapon` int(10) unsigned NOT NULL DEFAULT '600',
  `itemLevelArmor` int(10) unsigned NOT NULL DEFAULT '600',
  `xp` int(10) unsigned NOT NULL DEFAULT '0',
  `currentBuilding` int(10) unsigned NOT NULL DEFAULT '0',
  `currentMission` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dbId`),
  UNIQUE KEY `idx_guid_id` (`guid`,`followerId`),
  CONSTRAINT `fk_foll_owner` FOREIGN KEY (`guid`) REFERENCES `characters` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `character_garrison_follower_abilities`
--

CREATE TABLE `character_garrison_follower_abilities` (
  `dbId` bigint(20) unsigned NOT NULL,
  `abilityId` int(10) unsigned NOT NULL,
  `slot` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dbId`,`abilityId`,`slot`),
  CONSTRAINT `fk_foll_dbid` FOREIGN KEY (`dbId`) REFERENCES `character_garrison_followers` (`dbId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 
-- -------------------------------------------------------- 
-- _MERGED.sql 
-- -------------------------------------------------------- 
 
 

/*
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/*Comment:
* This script is purely meant for educational purposes
* It should, and has, not be(en) compiled and has not been intended to work with any TC releases because of it
* Intended to be unfinished to befit personal purposes.
*/
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellAuraEffects.h"
#include "throne_of_thunder.h"

/*
    The whole script needs sniffs, everything is based of assumptions.
*/
enum SAY
{
    // Lei-Shen
    SAY_INTRO               = 0, // You! You have earned my ire. I will make an example of you, such that all who look upon my might will tremble and submit.
    SAY_AGGRO               = 1, // I am Lei Shen, slayer of kings and gods. You have made a grave mistake.
    SAY_THUNDERSTRUCK_1     = 2, // Kneel!
    SAY_THUNDERSTRUCK_2     = 3, // Die!
    SAY_THUNDERSTRUCK_3     = 4, // Submit!
    SAY_LIGHTNING_WHIP      = 5, // Lightning, seek my foes!
    SAY_VIOLENT_GALE_WINDS  = 6, // Wind, heed my call!
    SAY_CONDUIT_CHARGE_1    = 7, // My power cannot be contained!
    SAY_CONDUIT_CHARGE_2    = 8, // You have sealed your doom!
    SAY_PHASE_2A            = 9, // You would challenge me in my own sanctum? Witness your folly!
    SAY_PHASE_2B            = 10, // Bah! I will crush you myself. Let us see how you fare against Lightning.
    SAY_PHASE_3A            = 11, // You are NOTHING! I wield the power of the heavens!
    SAY_PHASE_3B            = 12, // NO! You are UNWORTHY! I... AM... THE THUNDER KING!
    SAY_BERSERK             = 13, // Enough of this!
    SAY_SLAY_1              = 14, // Minuscule.
    SAY_SLAY_2              = 15, // Unworthy.
    SAY_SLAY_3              = 16, // My judgement is final.
    SAY_SLAY_4              = 17, // Lightning strikes.
    SAY_SLAY_5              = 18, // Kneel before me!
    SAY_DEATH               = 19, // I sought only to finish... the work of the Gods...
};

enum Spells
{
    // Lei-shen
    // Phase 1
    SPELL_DISCHARGED_ENERGY          = 134820,
    SPELL_DECAPITATE                = 134916,
    SPELL_THUNDERSTRUCK             = 135095, // 2 versions, 134912, also used in Phase 3
    SPELL_CRASHING_THUNDER          = 135150, // Effect #2 Area Trigger (613)
    SPELL_LIGHTNING_CRASH           = 138272, // need sniff, possible spawn by Crashing Thunder

    // Intermission
    SPELL_HELM_OF_COMMAND           = 139011,
    SPELL_SUPERCHARGE_CONDUITS      = 137045,

    // Phase 2
    SPELL_FUSION_SLASH              = 136478,
    SPELL_SUMMON_BALL_LIGHTNING     = 136543,
    SPELL_LIGHTNING_WHIP            = 136850,
    SPELL_LIGHTNING_WHIP_TRIGGER    = 137499, // Triggered by SPELL_LIGHTNING_WHIP, floor visual(?)

    // Phase 3
    SPELL_OVERWHELMING_POWER        = 136913,
    SPELL_VIOLENT_GALE_WINDS        = 136889,
    SPELL_WINDBURN                  = 140208,
    
    // Conduits
    SPELL_OVERCHARGE                = 135682,
    SPELL_STATIC_SHOCK              = 135680,
    SPELL_DIFFUSION_CHAIN           = 135681,
    SPELL_BOUNCING_BOLT             = 135683,

    // Intermission
    SPELL_OVERLOADED_CIRCUITS       = 137176,

    // Unharnessed Power
    SPELL_AMPLIFIER                 = 138070, // Needs sniff

    // Diffused Lightning
    SPELL_THUNDER_BLESSED_WEAPON    = 139487, // Needs sniff
    SPELL_CHAIN_LIGHTNING           = 139502, // Aura Proc Effect #1 by SPELL_THUNDER_BLESSED_WEAPON
    SPELL_CONDUCTIVITY              = 139508, // Aura Proc Effect #2 by SPELL_THUNDER_BLESSED_WEAPON
};

enum Phases
{
    PHASE_ONE           = 1,
    PHASE_TWO           = 2,
    PHASE_THREE         = 3,
    PHASE_INTERMISSION  = 4,
};

enum Events
{
    // Lei-Shen
    EVENT_BERSERK                   = 1,
    EVENT_DISCHARGED_ENERGY         = 2,
    EVENT_DECAPITATE                = 3,
    EVENT_THUNDERSTRUCK             = 4,
    EVENT_CRASHING_THUNDER          = 5,
    EVENT_LIGHTNING_CRASH           = 6,
    
    // Intermission
    EVENT_INTERMISSION              = 7,
    EVENT_HELM_OF_COMMAND           = 9,
    EVENT_SUPERCHARGE_CONDUITS      = 9,
    
    // Phase 2
    EVENT_FUSION_SLASH              = 10,
    EVENT_SUMMON_BALL_LIGHTNING     = 11,
    EVENT_LIGHTNING_WHIP            = 12,
    EVENT_LIGHTNING_WHIP_TRIGGER    = 13,

    // Phase 3
    EVENT_OVERWHELMING_POWER        = 14,
    EVENT_VIOLENT_GALE_WINDS        = 15,
    EVENT_WINDBURN                  = 16,

    // Conduits
    SPELL_OVERCHARGE                = 17,
    SPELL_STATIC_SHOCK              = 18,
    SPELL_DIFFUSION_CHAIN           = 19,
    SPELL_BOUNCING_BOLT             = 20,

    // (Conduit) Intermission
    EVENT_OVERLOADED_CIRCUITS       = 21,

    // Unharnessed Power
    EVENT_AMPLIFIER                 = 22,
    
    // Diffused Lightning
    EVENT_THUNDER_BLESSED_WEAPON    = 23,
    EVENT_CHAIN_LIGHTNING           = 24,
    EVENT_CONDUCTIVITY              = 25,
};

enum Misc
{
    DATA_CONDUIT_DISABLED = 0,
};

Position const CenterPosition = { 0.0f, 0.0f, 0.0f, 0.0f };
Position const conduitSpawnPoint[4] = 
{
    { 1075.201f, -57.84896f, 55.42427f, 3.159046f }, // North, Static Shock
    { 1075.201f, -57.84896f, 55.42427f, 3.159046f }, // East, Diffusion Chain
    { 1075.201f, -57.84896f, 55.42427f, 3.159046f }, // South, Overcharge
    { 1075.201f, -57.84896f, 55.42427f, 3.159046f }, // West, Bouncing Bolt
};

enum MovePoints
{
    POINT_INTERMISSION_CENTER   = 1,
};

class boss_lei_shen : public CreatureScript
{
public:
    boss_lei_shen() : CreatureScript("boss_lei_shen") { }

    struct boss_lei_shenAI : public BossAI
    {
        boss_lei_shenAI(Creature* creature) : BossAI(creature, DATA_LEI_SHEN)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;

        void Reset() override
        {
            _Reset();

            instance->SetBossState(DATA_LEI_SHEN, NOT_STARTED);
            instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);

            me->SetReactState(REACT_PASSIVE);
            events.SetPhase(PHASE_ONE);
            SetEquipmentSlots(true);
        }

        void EnterCombat(Unit* /*who*/) override
        {
            me->SetReactState(REACT_AGGRESSIVE);
            me->setActive(true);
            DoZoneInCombat();

            Talk(SAY_AGGRO);

            instance->SetBossState(DATA_LEI_SHEN, IN_PROGRESS);
            instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me);
        }

        void JustDied(Unit* /*killer*/) override
        {
            instance->SetBossState(DATA_LEI_SHEN, DONE);
            instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
        }

        void DamageTaken(Unit* /*attacker*/, uint32& damage) override
        {
            if (events.IsInPhase(PHASE_ONE) && !HealthAbovePct(65))
            {
                // Intermission 1
                events.SetPhase(PHASE_INTERMISSION);
                me->SetReactState(REACT_PASSIVE);
                me->AttackStop();
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                me->GetMotionMaster()->MovePoint(POINT_INTERMISSION_CENTER, CenterPosition);
                return;
            }

            if (events.IsInPhase(PHASE_TWO) && !HealthAbovePct(30))
            {
                // Intermission 2
                events.SetPhase(PHASE_INTERMISSION);
                me->SetReactState(REACT_PASSIVE);
                me->AttackStop();
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                me->GetMotionMaster()->MovePoint(POINT_INTERMISSION_CENTER, CenterPosition);
                return;
            }
        }

        void KilledUnit(Unit* victim/*Killed*/) override
        {
            if (!me->IsAlive)
                return;

            if (victim->GetTypeId() == TYPEID_PLAYER) {
                switch (urand(1, 5))
                {
                    case 1:
                        Talk(SAY_SLAY_1);
                        break;
                    case 2:
                        Talk(SAY_SLAY_2);
                        break;
                    case 3:
                        Talk(SAY_SLAY_3);
                        break;
                    case 4:
                        Talk(SAY_SLAY_4);
                        break;
                    case 5:
                        Talk(SAY_SLAY_5);
                        break;
                    default:
                        Talk(SAY_SLAY_1);
                        break;
                }
            }
        }

        void JustSummoned(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
                case NPC_BALL_LIGHTNING:
                    if (IsHeroic())
                    {
                        // Logic for fixation 
                        //summon_>GetMotionMaster()->MoveFollow(target, 0.0f, 0.0f)
                    }
                    break;
                case NPC_UNHARNESSED_POWER:
                    if (Unit* target = SelectTarget(SELECT_TARGET_NEAREST, 0, 0.0f, true))

                    break;
                case NPC_DIFFUSED_LIGHTNING:
                    break;
                default:
                    break;
            }

            BossAI::JustSummoned(summon);
        }

        void SummonedCreatureDies(Creature* summon, Unit* /*killer*/) override
        {
            switch (summon->GetEntry())
            {
                case NPC_BALL_LIGHTNING:
                case NPC_UNHARNESSED_POWER:
                case NPC_DIFFUSED_LIGHTNING:
                    summon->ToTempSummon()->SetTempSummonType(TEMPSUMMON_CORPSE_DESPAWN);
                    break;
                default:
                    break;
            }
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type != POINT_MOTION_TYPE)
                return;

            switch (pointId)
            {
                case POINT_INTERMISSION_CENTER:
                    me->SetFacingTo(0.0f);
                    if (HealthBelowPct(31) && events.IsInPhase(PHASE_INTERMISSION))
                    {
                        switch (urand(1, 2))
                        {
                            case 1:
                                Talk(SAY_PHASE_3A);
                                break;
                            case 2:
                                Talk(SAY_PHASE_3B);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (urand(1, 2))
                        {
                            case 1:
                                Talk(SAY_PHASE_2A);
                                break;
                            case 2:
                                Talk(SAY_PHASE_2B);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_DISCHARGED_ENERGY:
                        // Check for charging conduit before we cast it
                        DoCastAOE(SPELL_DISCHARGED_ENERGY);
                        events.ScheduleEvent(EVENT_DISCHARGED_ENERGY, 500, 0, events.IsInPhase(PHASE_ONE) || events.IsInPhase(PHASE_TWO));
                        break;
                    case EVENT_DECAPITATE:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, NonTankTargetSelector(me)))
                        {
                            // Make throw animation
                            me->HandleEmoteCommand(EMOTE_STATE_READY_THROWN);
                            DoCast(target, SPELL_DECAPITATE);
                        }
                        events.ScheduleEvent(EVENT_DECAPITATE, 30000, 0, PHASE_ONE);
                        break;
                    case EVENT_THUNDERSTRUCK:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, NonTankTargetSelector(me)))
                        {
                            switch (urand(1, 3))
                            {
                            case 1:
                                Talk(SAY_THUNDERSTRUCK_1);
                                break;
                            case 2:
                                Talk(SAY_THUNDERSTRUCK_2);
                                break;
                            case 3:
                                Talk(SAY_THUNDERSTRUCK_3);
                                break;
                            default:
                                break;
                            }
                            DoCast(target, SPELL_THUNDERSTRUCK);
                        }
                        events.ScheduleEvent(EVENT_THUNDERSTRUCK, 15000, 0, !events.IsInPhase(PHASE_INTERMISSION));
                        break;
                    case EVENT_CRASHING_THUNDER:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0.0f, true))
                            DoCast(target, SPELL_CRASHING_THUNDER);
                        events.ScheduleEvent(EVENT_CRASHING_THUNDER, urand(15000, 20000), 0, !events.IsInPhase(PHASE_INTERMISSION));
                        break;
                    case EVENT_INTERMISSION:
                        if (events.IsInPhase(PHASE_INTERMISSION))
                        {
                            me->HandleEmoteCommand(EMOTE_ONESHOT_KNEEL); // Needs sniff for correct emote
                        }
                        break;
                    case EVENT_OVERLOADED_CIRCUITS:
                        break;
                    case EVENT_HELM_OF_COMMAND:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0.0f, true))
                            DoCast(target, SPELL_HELM_OF_COMMAND);
                        events.ScheduleEvent(EVENT_HELM_OF_COMMAND, 60000, 0, PHASE_INTERMISSION);
                        break;
                    case EVENT_SUMMON_BALL_LIGHTNING:
                        DoCast(me, SPELL_SUMMON_BALL_LIGHTNING);
                        events.ScheduleEvent(EVENT_SUMMON_BALL_LIGHTNING, urand(50000, 60000), 0, PHASE_TWO);
                        break;
                }
            }

            DoMeleeAttackIfReady();
            
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_lei_shenAI(creature);
    }
};

class npc_bouncing_bolt_conduit : public CreatureScript
{
    public:
        npc_bouncing_bolt_conduit() : CreatureScript("npc_bouncing_bolt_conduit") { }

        struct npc_bouncing_bolt_conduitAI : public ScriptedAI
        {

            npc_bouncing_bolt_conduitAI(Creature* creature) : ScriptedAI(creature)
            {
                _disabled = false;
                _level = 0;
            }

        private:
            EventMap _events;
            bool _disabled;
            int32 _level;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_bouncing_bolt_conduitAI(creature);
        }
};

class npc_static_shock_conduit : public CreatureScript
{
    public:
        npc_static_shock_conduit() : CreatureScript("npc_static_shock_conduit") { }

        struct npc_static_shock_conduitAI : public ScriptedAI
        {

            npc_static_shock_conduitAI(Creature* creature) : ScriptedAI(creature)
            {
                _disabled = false;
                _level = 0;
            }

        private:
            EventMap _events;
            bool _disabled;
            int32 _level;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_static_shock_conduitAI(creature);
        }
};

class npc_diffusion_chain_conduit : public CreatureScript
{
    public:
        npc_diffusion_chain_conduit() : CreatureScript("npc_diffusion_chain_conduit") { }

        struct npc_diffusion_chain_conduitAI : public ScriptedAI
        {

            npc_diffusion_chain_conduitAI(Creature* creature) : ScriptedAI(creature)
            {
                _disabled = false;
                _level = 0;
            }

        private:
            EventMap _events;
            bool _disabled;
            int32 _level;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_diffusion_chain_conduitAI(creature);
        }
};

class npc_overcharge_conduit : public CreatureScript
{
    public:
        npc_overcharge_conduit() : CreatureScript("npc_overcharge_conduit") { }

        struct npc_overcharge_conduitAI : public ScriptedAI
        {

            npc_overcharge_conduitAI(Creature* creature) : ScriptedAI(creature)
            {
                _disabled = false;
                _level = 0;
            }

        private:
            EventMap _events;
            bool _disabled;
            int32 _level;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_overcharge_conduitAI(creature);
        }
};

class npc_unharnessed_power : public CreatureScript
{
public:
    npc_unharnessed_power() : CreatureScript("npc_unharnessed_power") { }

    struct npc_unharnessed_powerAI : public ScriptedAI
    {

        npc_unharnessed_powerAI(Creature* creature) : ScriptedAI(creature)
        {

        }

    private:
        EventMap _events;
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_unharnessed_powerAI(creature);
    }
};

class npc_diffused_lightning : public CreatureScript
{
public:
    npc_diffused_lightning() : CreatureScript("npc_diffused_lightning") { }

    struct npc_diffused_lightningAI : public ScriptedAI
    {

        npc_diffused_lightningAI(Creature* creature) : ScriptedAI(creature),
            _instance(creature->GetInstanceScript())
        {
        }

        void Reset() override
        {
            _events.Reset();
            
        }

        void JustDied(Unit* /*killer*/) override
        {
            if (TempSummon* summon = me->ToTempSummon())
                summon->SetTempSummonType(TEMPSUMMON_CORPSE_DESPAWN);
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            _events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;


        }
    private:
        EventMap _events;
        InstanceScript* _instance;
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_diffused_lightningAI(creature);
    }
};

class npc_ball_lightning : public CreatureScript
{
    public:
        npc_ball_lightning() : CreatureScript("npc_ball_lightning") { }

        struct npc_ball_lightningAI : public ScriptedAI
        {

            npc_ball_lightningAI(Creature* creature) : ScriptedAI(creature),
                _instance(creature->GetInstanceScript())
            {
            }

        private:
            EventMap _events;
            InstanceScript* _instance;
        };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_ball_lightningAI(creature);
    }
};

class achievement_a_complete_circuit : public AchievementCriteriaScript
{
    public:
        achievement_a_complete_circuit() : AchievementCriteriaScript("achievement_a_complete_circuit") { }

        bool OnCheck(Player* /*source*/, Unit* target) override 
        {
            if (!target)
                return false;

        }
};

void AddSC_boss_lei_shen()
{
    new boss_lei_shen();

    // Object-like
    new npc_bouncing_bolt_conduit();
    new npc_static_shock_conduit();
    new npc_diffusion_chain_conduit();
    new npc_overcharge_conduit();

    // NPC
    new npc_unharnessed_power();
    new npc_diffused_lightning();
    new npc_ball_lightning();

    // Achievement
    new achievement_a_complete_circuit();
}
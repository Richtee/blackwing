/*
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Player.h"
#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "throne_of_thunder.h"

DoorData const doorData[1] =
{
    // doorData[], when we get door sniffs
};

class instance_throne_of_thunder : public InstanceMapScript
{
public:
    instance_throne_of_thunder() : InstanceMapScript(ThroneOfThunderScriptName, 1098) { }

    struct instance_throne_of_thunder_InstanceScript : public InstanceScript
    {
        instance_throne_of_thunder_InstanceScript(InstanceMap* map) : InstanceScript(map)
        {
            SetHeaders(DataHeader);
            SetBossNumber(EncounterCount);
            LoadDoorData(doorData);
        }

        void OnPlayerEnter(Player* player) override
        {
            if (instance->IsHeroic() && GetBossState(DATA_LEI_SHEN) == DONE)
            { 
                player->GetCharmerOrOwnerPlayerOrPlayerItself->TeleportTo(4493);
                return;
            } 
            else if (GetBossState(DATA_TWIN_CONSORTS) == DONE) 
            {
                player->GetCharmerOrOwnerPlayerOrPlayerItself->TeleportTo(4496);
                return;
            }
            else if (GetBossState(DATA_IRON_QON) == DONE)
            {
                player->GetCharmerOrOwnerPlayerOrPlayerItself->TeleportTo(4495);
                return;
            }
            else if (GetBossState(DATA_DARK_ANIMUS) == DONE)
            {
                player->GetCharmerOrOwnerPlayerOrPlayerItself->TeleportTo(4494);
                return;
            }
            else if (GetBossState(DATA_PRIMORDIUS) == DONE)
            {
                player->GetCharmerOrOwnerPlayerOrPlayerItself->TeleportTo(4493);
                return;
            }
            else if (GetBossState(DATA_COUNCIL_OF_ELDERS) == DONE)
            {
                player->GetCharmerOrOwnerPlayerOrPlayerItself->TeleportTo(4492);
                return;
            }
            else if (GetBossState(DATA_HORRIDON) == DONE)
            {
                player->GetCharmerOrOwnerPlayerOrPlayerItself->TeleportTo(4491);
                return;
            }
        }

        void OnCreatureCreate(Creature* creature) override
        {
            switch (creature->GetEntry())
            {
                case NPC_JIN_ROKH_THE_BREAKER:
                    JinrokhTheBreakerGUID = creature->GetGUID();
                    break;
                case NPC_HORRIDON:
                    HorridonGUID = creature->GetGUID();
                    break;
                case NPC_KAZRA_JIN:
                    CouncilOfEldersGUID[0] = creature->GetGUID();
                    break;
                case NPC_SUL_THE_SANDCRAWLER:
                    CouncilOfEldersGUID[1] = creature->GetGUID();
                    break;
                case NPC_FROST_KING_MALAKK:
                    CouncilOfEldersGUID[2] = creature->GetGUID();
                    break;
                case NPC_HIGH_PRIESTESS_MAR_LI:
                    CouncilOfEldersGUID[3] = creature->GetGUID();
                    break;
                case NPC_TORTOS:
                    TortosGUID = creature->GetGUID();
                    break;
                case NPC_FLAMING_HEAD:
                    MegaeraGUID[0] = creature->GetGUID();
                    break;
                case NPC_FROZEN_HEAD:
                    MegaeraGUID[1] = creature->GetGUID();
                    break;
                case NPC_VENOMOUS_HEAD:
                    MegaeraGUID[2] = creature->GetGUID();
                    break;
                case NPC_JI_KUN:
                    JiKunGUID = creature->GetGUID();
                    break;
                case NPC_DURUMU_THE_FORGOTTEN:
                    DurumuTheForgottenGUID = creature->GetGUID();
                    break;
                case NPC_PRIMORDIUS:
                    PrimordiusGUID = creature->GetGUID();
                    break;
                case NPC_DARK_ANIMUS:
                    DarkAnimusGUID = creature->GetGUID();
                    break;
                case NPC_IRON_QON:
                    IronQonGUID = creature->GetGUID();
                    break;
                case NPC_LU_LIN:
                    TwinConsortsGUID[0] = creature->GetGUID();
                    break;
                case NPC_SUEN:
                    TwinConsortsGUID[1] = creature->GetGUID();
                    break;
                case NPC_LEI_SHEN:
                    LeiShenGUID = creature->GetGUID();
                    break;
                case NPC_RA_DEN:
                    RaDenGUID = creature->GetGUID();
                    break;
                default:
                    break;
            }
        }

        ObjectGuid GetGuidData(uint32 type) const override
        {
            switch (type){
                case DATA_JIN_ROKH_THE_BREAKER:
                    return JinrokhTheBreakerGUID;
                case DATA_HORRIDON:
                    return HorridonGUID;
                case DATA_KAZRA_JIN_GUID:
                    return CouncilOfEldersGUID[0];
                case DATA_SUL_THE_SANDCRAWLER_GUID:
                    return CouncilOfEldersGUID[1];
                case DATA_FROST_KING_MALAKK_GUID:
                    return CouncilOfEldersGUID[2];
                case DATA_HIGH_PRIESTESS_MAR_LI_GUID:
                    return CouncilOfEldersGUID[3];
                case DATA_TORTOS:
                    return TortosGUID;
                case DATA_FLAMING_HEAD_GUID:
                    return MegaeraGUID[0];
                case DATA_FROZEN_HEAD_GUID:
                    return MegaeraGUID[1];
                case DATA_VENOMOUS_HEAD_GUID:
                    return MegaeraGUID[2];
                case DATA_JI_KUN:
                    return JiKunGUID;
                case DATA_DURUMU_THE_FORGOTTEN:
                    return DurumuTheForgottenGUID;
                case DATA_PRIMORDIUS:
                    return PrimordiusGUID;
                case DATA_DARK_ANIMUS:
                    return DarkAnimusGUID;
                case DATA_IRON_QON:
                    return IronQonGUID;
                case DATA_LU_LIN_GUID:
                    return TwinConsortsGUID[0];
                case DATA_SUEN_GUID:
                    return TwinConsortsGUID[1];
                case DATA_LEI_SHEN:
                    return LeiShenGUID;
                case DATA_RA_DEN:
                    return RaDenGUID;
                default:
                    break;
            }
            
            return ObjectGuid::Empty;
        }

    protected:
        EventMap Events;
        ObjectGuid JinrokhTheBreakerGUID;
        ObjectGuid HorridonGUID;
        ObjectGuid CouncilOfEldersGUID[4];
        ObjectGuid TortosGUID;
        ObjectGuid MegaeraGUID[3];
        ObjectGuid JiKunGUID;
        ObjectGuid DurumuTheForgottenGUID;
        ObjectGuid PrimordiusGUID;
        ObjectGuid DarkAnimusGUID;
        ObjectGuid IronQonGUID;
        ObjectGuid TwinConsortsGUID[2];
        ObjectGuid LeiShenGUID;
        ObjectGuid RaDenGUID;
        InstanceMap* _Instance;
    };

    InstanceScript* GetInstanceScript(InstanceMap* map) const override
    {
        return new instance_throne_of_thunder_InstanceScript(map);
    }
};

void AddSC_instance_throne_of_thunder()
{
    new instance_throne_of_thunder();
}

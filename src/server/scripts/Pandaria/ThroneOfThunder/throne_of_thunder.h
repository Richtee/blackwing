/*
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef THRONE_OF_THUNDER_H
#define THRONE_OF_THUNDER_H

#define DataHeader "TOT"
#define ThroneOfThunderScriptName "instance_throne_of_thunder"

uint32 const EncounterCount = 13;

enum SharedSpells
{
    SPELL_BERSERK               = 47008,
};

enum DataTypes
{
    // Encounter States/Boss GUIDs
    DATA_JIN_ROKH_THE_BREAKER   = 0,
    DATA_HORRIDON               = 1,
    DATA_COUNCIL_OF_ELDERS      = 2,
    DATA_TORTOS                 = 3,
    DATA_MEGAERA                = 4,
    DATA_JI_KUN                 = 5,
    DATA_DURUMU_THE_FORGOTTEN   = 6,
    DATA_PRIMORDIUS             = 7,
    DATA_DARK_ANIMUS            = 8,
    DATA_IRON_QON               = 9,
    DATA_TWIN_CONSORTS          = 10,
    DATA_LEI_SHEN               = 11,
    DATA_RA_DEN                 = 12,

    // Additional data
    DATA_KAZRA_JIN_GUID                 = 13,
    DATA_SUL_THE_SANDCRAWLER_GUID       = 14,
    DATA_FROST_KING_MALAKK_GUID         = 15,
    DATA_HIGH_PRIESTESS_MAR_LI_GUID     = 16,
    DATA_FLAMING_HEAD_GUID              = 17,
    DATA_FROZEN_HEAD_GUID               = 18,
    DATA_VENOMOUS_HEAD_GUID             = 19,
    DATA_LU_LIN_GUID                    = 20,
    DATA_SUEN_GUID                      = 21,
};

enum CreaturesIds
{
    // Jin'Rokh The Breaker
    NPC_JIN_ROKH_THE_BREAKER    = 69465,

    // Horridon
    NPC_HORRIDON                = 68476,

    // Council of Elders
    NPC_KAZRA_JIN               = 69134,
    NPC_SUL_THE_SANDCRAWLER     = 69078,
    NPC_FROST_KING_MALAKK       = 69131,
    NPC_HIGH_PRIESTESS_MAR_LI   = 69132,

    // Tortos
    NPC_TORTOS                  = 67977,
    
    // Megaera
    NPC_FLAMING_HEAD            = 70212,
    NPC_FROZEN_HEAD             = 70235,
    NPC_VENOMOUS_HEAD           = 70247,

    // Ji-Kun
    NPC_JI_KUN                  = 69712,

    // Durumu the Forgotten
    NPC_DURUMU_THE_FORGOTTEN    = 68036,

    // Primordius
    NPC_PRIMORDIUS              = 69017,

    // Dark Animus
    NPC_DARK_ANIMUS             = 69427,

    // Iron Qon
    NPC_IRON_QON                = 68078,

    // Twin Consorts
    NPC_LU_LIN                  = 68905,
    NPC_SUEN                    = 68904,

    // Lei-Shen
    NPC_LEI_SHEN                = 68397,
    NPC_BOUNCING_BOLT_CONDUIT   = 68698,
    NPC_STATIS_SHOCK_CONDUIT    = 69309,
    NPC_DIFFUSION_CHAIN_CONDUIT = 68696,
    NPC_OVERCHARGE_CONDUIT      = 68697,
    NPC_DIFFUSED_LIGHTNING      = 69013,
    NPC_UNHARNESSED_POWER       = 69133,
    NPC_BALL_LIGHTNING          = 69462,

    // Ra-Den
    NPC_RA_DEN                  = 69473,
};

enum GameObjectsIds
{
    // Jin'Rokh The Breaker

    // Horridon

    // Council of Elders

    // Tortos

    // Megaera

    // Ji-Kun

    // Durumu the Forgotten

    // Primordius

    // Dark Animus

    // Iron Qon

    // Twin Consorts

    // Lei-Shen

    // Ra-Den
};

enum AchievementCriteriaIds
{

};

template<class AI>
AI* GetThroneOfThunderAI(Creature* creature)
{
    return GetInstanceAI<AI>(creature, TOTScriptName);
}

#endif // THRONE_OF_THUNDER_H
